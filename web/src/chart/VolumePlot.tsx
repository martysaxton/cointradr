import * as React from 'react'
import { Plot } from './Plot'
import { Rect } from './CoreModel'
import { Dot } from './ScatterPlot'
import { Bar, Trade } from '../model'
import ChartLine from './ChartLine'
import ChartRect, { ChartRectProps } from './ChartRect'
import { AlgebraicLine } from '../linearRegression'

export default class VolumePlot extends Plot {
  constructor (private readonly bars: Bar[]) {
    super()
    this.setDataBounds(this.computeBounds())
  }

  render (): JSX.Element[] {
    const line = this.getDiagonal()
    const barRect: Rect = {
      left: this.dataBounds.left,
      right: this.dataBounds.right,
      top: this.dataBounds.top,
      bottom: this.dataBounds.bottom
    }
    const chartRect = this.dataToChartRect(barRect)
    console.log('barRect', barRect)
    console.log('chartRect', chartRect)
    // chartRect.top += 10
    // chartRect.bottom -= 1
    // console.log('rect', chartRect)
    // const r = (
    //   <rect
    //     x = { chartRect.left }
    //     y = { chartRect.top }
    //     width = {chartRect.right - chartRect.left }
    //     height = {chartRect.bottom - chartRect.top}
    //     style={{ fill: 'red', stroke: 'blue', strokeWidth: 3 }}
    //   />
    // )

    // return [ r, line ]
    console.log(line)
    const bars2 = this.bars.map((bar) => {
      const startMillis = bar.start.getTime()
      const dataRect = {
        left: startMillis,
        right: startMillis + bar.dur,
        top: bar.vol,
        bottom: 0
      }
      const bidsRect = {
        ...dataRect,
        right: startMillis + bar.dur / 2,
        top: bar.bv
      }
      const asksRect = {
        ...dataRect,
        left: startMillis + bar.dur / 2,
        top: bar.av
      }
      return [
        this.getSvgRect('volRect', dataRect, { fill: '#808080', stroke: 'white' }),
        this.getSvgRect('volRectAsks', asksRect, { fill: 'green' }),
        this.getSvgRect('volRectBids', bidsRect, { fill: 'red' })
        // this.getDiagonal()
      ]
    })
    let rv: JSX.Element[] = []
    for (const barArray of bars2) {
      rv = rv.concat(barArray)
    }
    return rv

  }

  computeBounds (): Rect {
    if (this.bars.length === 0) {
      return { top: 0, left: 0, bottom: 0, right: 0 }
    }
    const lastBar = this.bars[this.bars.length - 1]
    const r = {
      top: 0,
      bottom: 0,
      left: this.bars[0].start.getTime(),
      right: lastBar.start.getTime()
    }
    for (const bar of this.bars) {
      r.top = Math.max(r.top, bar.vol)
    }
    return r
  }

  private getDiagonal () {
    const dataBounds = this.getDataBounds()
    // console.log('dataBounds', dataBounds)
    // const lowerLeft = this.dataToChart({ x: dataBounds.left, y: dataBounds.bottom })
    // const upperRight = this.dataToChart({ x: dataBounds.right, y: dataBounds.top })
    // const stroke = { stroke: '#ccc', strokeWidth: 1 }
    // console.log('diagonal', lowerLeft, upperRight)
    // return (
    //   <line
    //     key='foo'
    //     x1={lowerLeft.x}
    //     y1={lowerLeft.y}
    //     x2={upperRight.x}
    //     y2={upperRight.y}
    //     style={stroke} />
    // )
    const start = { x: dataBounds.left, y: dataBounds.bottom }
    const end = { x: dataBounds.right, y: dataBounds.top }
    const style: React.CSSProperties = {
      stroke: '#ddd',
      strokeWidth: 1
    }

    const line = new ChartLine(this, { start, end, style })
    return line.render()
  }

  private getSvgRect (key: string, dataRect: Rect, style: React.CSSProperties) {
    const rectProps: ChartRectProps = {
      key: key + JSON.stringify(dataRect),
      rect: dataRect,
      style: style
    }
    return new ChartRect(this, rectProps).render()
    // const chartRect = this.dataToChartRect(dataRect)
    // return <rect
    //   key={ key + dataRect.left}
    //   x={chartRect.left}
    //   y={chartRect.top}
    //   width={chartRect.right - chartRect.left}
    //   height={chartRect.bottom - chartRect.top}
    //   style={style}
    // />

  }
}
