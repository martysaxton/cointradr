import * as React from 'react'
import { Plot } from './Plot'
import { Rect } from './CoreModel'
import { Dot } from './ScatterPlot'

export default class SolidColorPlot extends Plot {

  constructor (private readonly dots: Dot[]) {
    super()
    this.setDataBounds(this.computeBounds())
  }

  render (): JSX.Element[] {
    const dataBounds = this.getDataBounds()
    const lowerLeft = this.dataToChart({ x: dataBounds.left, y: dataBounds.bottom })
    const upperRight = this.dataToChart({ x: dataBounds.right, y: dataBounds.top })
    const stroke = { stroke: '#ccc', strokeWidth: 1 }
    return [
      <line
        key='foo'
        x1 = { lowerLeft.x }
        y1 = { lowerLeft.y }
        x2 = { upperRight.x }
        y2 = { upperRight.y }
        style = { stroke }
      />
    ]
  }

  computeBounds (): Rect {
    const r = {
      top: Number.MIN_VALUE,
      left: Number.MAX_VALUE,
      bottom: Number.MAX_VALUE,
      right: Number.MIN_VALUE
    }
    for (const dot of this.dots) {
      r.left = Math.min(r.left, dot.x)
      r.bottom = Math.min(r.bottom, dot.y)
      r.right = Math.max(r.right, dot.x)
      r.top = Math.max(r.top, dot.y)
    }
    return r
  }

}
