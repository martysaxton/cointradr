import * as React from 'react'

interface ChartTextProps {
  x: number
  y: number
  yPosition?: 'top' | 'center' | 'bottom'
  xPosition?: 'left' | 'center' | 'right'
}

export default class ChartText extends React.Component<ChartTextProps> {
  public render () {
    let dy = 0
    switch (this.props.yPosition) {
      case 'top':
        dy = 10
        break
      case 'center':
        dy = 6
        break
    }
    let textAnchor = 'start'
    switch (this.props.xPosition) {
      case 'center':
        textAnchor = 'middle'
        break
      case 'right':
        textAnchor = 'end'
        break
    }
    return (
      <text
        textAnchor={textAnchor}
        x={this.props.x}
        y={this.props.y}
        dy={dy}
        fill='white'
        style={{ font: '12px sans-serif', fontWeight: 'lighter' }}
        className='small'>
        {this.props.children}
      </text>

    )
  }
}
