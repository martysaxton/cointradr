
export interface Point {
  x: number
  y: number
}

export interface Rect {
  top: number
  left: number
  right: number
  bottom: number
}
