import * as React from 'react'
import { Point } from './CoreModel'
import { Plot } from './Plot'

export interface ChartLineProps {
  start: Point
  end: Point,
  style: React.CSSProperties
}

export default class ChartLine {
  static keyCounter = 0
  constructor (readonly parent: Plot, readonly props: ChartLineProps) {
    // no op
  }

  public render () {
    const chartStart = this.parent.dataToChart(this.props.start)
    const chartEnd = this.parent.dataToChart(this.props.end)
    // const stroke = { stroke: '#ccc', strokeWidth: 1 }

    const key = 'line' + (ChartLine.keyCounter++)

    return (
      <line
        key={key}
        x1={chartStart.x}
        y1={chartStart.y}
        x2={chartEnd.x}
        y2={chartEnd.y}
        style={this.props.style} />
    )
  }
}
