import * as React from 'react'
import { Plot } from './Plot'
import { Point, Rect } from './CoreModel'
import { AlgebraicLine } from '../linearRegression'
import ChartLine, { ChartLineProps } from './ChartLine'
import ChartRect, { ChartRectProps } from './ChartRect'

export interface Dot {
  x: number
  y: number
  r: number
  color: string
  opacity: number
}

// export interface LineSegment extends AlgebraicLine {
//   startX: number,
//   endX: number
// }
export interface LineSegment {
  start: Point
  end: Point
}

export class ScatterPlot extends Plot {

  constructor (
    private readonly dots: Dot[],
    private readonly lines: ChartLineProps[],
    private readonly rects: ChartRectProps[]) {
    super()
    this.setDataBounds(this.computeBounds())
  }

  // NOTE this makes it not a generic scatter plot, but a return based plot
  renderYAxisLabel (value: number): string {
    // if (!this.dataBounds) {
    //   return super.renderYAxisLabel(value)
    // }
    // const t = this.dataBounds.top
    // const b = this.dataBounds.bottom
    // const range = t - b
    // const ret = range / b * 100
    // const returnValue = Math.round((value - b) / range * ret * 100) / 100
    // return returnValue.toString()
    return value.toString()
  }

  computeBounds (): Rect {
    const r = {
      top: Number.MIN_VALUE,
      left: Number.MAX_VALUE,
      bottom: Number.MAX_VALUE,
      right: Number.MIN_VALUE
    }
    for (const dot of this.dots) {
      r.left = Math.min(r.left, dot.x)
      r.bottom = Math.min(r.bottom, dot.y)
      r.right = Math.max(r.right, dot.x)
      r.top = Math.max(r.top, dot.y)
    }
    // for (const line of this.lrLines) {
    //   r.left = Math.min(r.left, line.start.x)
    //   r.bottom = Math.min(r.bottom, line.start.y, line.end.y)
    //   r.right = Math.max(r.right, line.end.x)
    //   r.top = Math.max(r.top, line.start.y, line.end.y)
    // }
    return r
  }

  render (): JSX.Element[] {
    let key = 0
    const dots: JSX.Element[] = this.dots.map((dot) => {
      const cp = this.dataToChart({ x: dot.x, y: dot.y })
      //console.log('trade dot', dot, cp)
      return <circle key={'dot' + key++} cx={cp.x} cy={cp.y} r={dot.r} fill={dot.color} opacity={dot.opacity} />
    })
    // const style: React.CSSProperties = {
    //   stroke: '#ddd',
    //   strokeWidth: 1
    // }
    // const lines: JSX.Element[] = this.lines.map((line) => {
    //   return new ChartLine(this, line).render()
    // })

    // const rects: JSX.Element[] = this.rects.map((rectProps) => new ChartRect(this, rectProps).render())

    // return [ ...dots, ...lines, ...rects ]
    return dots
  }
}
