import * as React from 'react'
import { Rect } from './CoreModel'
import { Plot } from './Plot'

export interface ChartRectProps {
  key: string
  rect: Rect,
  style: React.CSSProperties
}

export default class ChartRect {
  constructor (readonly parent: Plot, readonly props: ChartRectProps) {
    // no op
  }

  public render () {
    const chartRect = this.parent.dataToChartRect(this.props.rect)

    return (
      <rect
        key={this.props.key}
        x={chartRect.left}
        y={chartRect.top}
        width={chartRect.right - chartRect.left}
        height={chartRect.bottom - chartRect.top}
        style={this.props.style} />
    )
  }
}
