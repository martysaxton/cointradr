import * as React from 'react'
import ChartText from './ChartText'
import { pageDimensions } from '../utils'
import { Dot, ScatterPlot } from '../chart/ScatterPlot'
import { Rect } from '../chart/CoreModel'
import { Plot } from './Plot'
import { Interval } from '../Interval'

interface ChartProps {
  // scatterPlot?: ScatterPlot
  bounds: Rect
  interval: Interval
  plots: Plot[]
}

interface ChartState {
  // scatterPlot?: ScatterPlot
}

const stroke = { stroke: '#ccc', strokeWidth: 1 }

class Chart extends React.Component<ChartProps, ChartState> {

  state: ChartState = {}
  private chartRect!: Rect

  private svgWidth: number
  private svgHeight: number
  private margin = 10
  private chartPadding = 0
  private minX!: number
  private maxX!: number

  constructor (props: Readonly<ChartProps>) {
    super(props)
    console.log('chart ctor', props)
    // margin around the chart
    const d = pageDimensions()
    this.svgWidth = d.x - this.margin * 2
    this.svgHeight = d.y - this.margin * 2
    const chartMarginBottomLeft = 55
    this.chartRect = {
      left: chartMarginBottomLeft,
      top: 10,
      right: chartMarginBottomLeft + this.svgWidth - chartMarginBottomLeft * 2,
      bottom: this.svgHeight - chartMarginBottomLeft
    }
  }

  render () {
    console.log('chart render', this.props)
    if (this.props.plots.length === 0) {
      return ''
    }
    this.computeBounds()
    const plotCount = this.props.plots.length
    const totalHeight = this.chartRect.bottom - this.chartRect.top
    const heightMinusPaddings = totalHeight - this.chartPadding * (plotCount - 1)
    const plotHeight = heightMinusPaddings / this.props.plots.length
    const rv: JSX.Element[][] = []
    for (let i = 0; i < this.props.plots.length; ++i) {
      const plotTop = this.chartRect.top + i * plotHeight + i * this.chartPadding
      const plotRect = {
        ...this.chartRect,
        top: plotTop,
        bottom: plotTop + plotHeight
      }
      // rv.push(this.renderPlot(i, plotRect))
      const plot = this.props.plots[i]
      plot.setChartBounds(plotRect)
      const dataRect = plot.getDataBounds()
      rv.push([
        ...plot.render(),
        <line
          key={'plotYAxisLine' + i}
          x1={plotRect.left}
          y1={plotRect.bottom}
          x2={plotRect.left}
          y2={plotRect.top}
          style={stroke}
        />,
        <line
          key={'plotXAxisLine' + i}
          x1={plotRect.left}
          y1={plotRect.bottom}
          x2={plotRect.right}
          y2={plotRect.bottom}
          style={stroke}
        />,
        <ChartText
          key={'plotYLegendTop' + i}
          x={plotRect.left - 5}
          y={plotRect.top}
          yPosition='top'
          xPosition='right'>
          {plot.renderYAxisLabel(dataRect.top)}
        </ChartText>,
        <ChartText
          key={'plotYLegendBottom' + i}
          x={plotRect.left - 5}
          y={plotRect.bottom}
          yPosition='bottom'
          xPosition='right'>
          {plot.renderYAxisLabel(dataRect.bottom)}
        </ChartText>

      ])
    }
    const i = 0
    return (
    <svg style={{
      position: 'absolute',
      width: '100%',
      height: '90%'
    }} viewBox={`0 0 ${this.svgWidth} ${this.svgHeight}`}>
      <rect width='100%' height='100%' fill='black' />
        {rv}
        {this.renderXAxisLabels()}
      </svg>
    )
  }

  private renderPlot (plotIndex: number, chartRect: Rect) {
    const plot = this.props.plots[plotIndex]
    plot.setChartBounds(chartRect)
    const dataRect = plot.getDataBounds()
    return (
      <g>
        <ChartText
          x={this.chartRect.left - 5}
          y={this.chartRect.top}
          yPosition='center'
          xPosition='right'>
          {plot.renderYAxisLabel(dataRect.top)}
        </ChartText>
        <ChartText
          x={this.chartRect.left - 5}
          y={this.chartRect.bottom}
          yPosition='bottom'
          xPosition='right'>
          {plot.renderYAxisLabel(dataRect.bottom)}
        </ChartText>
        <line
          x1={this.chartRect.left}
          y1={this.chartRect.bottom}
          x2={this.chartRect.left}
          y2={this.chartRect.top}
          style={stroke}
        />
      </g>
    )
  }

  private renderXAxisLabels () {
    const startDate = new Date(this.minX).toLocaleString()
    const endDate = new Date(this.maxX).toLocaleString()
    return (
      <g>
      <ChartText x={this.chartRect.left} y={this.chartRect.bottom + 10} yPosition='top' xPosition='left'>{startDate}</ChartText>
      <ChartText x={this.chartRect.right} y={this.chartRect.bottom + 10} yPosition='top' xPosition='right'>{endDate}</ChartText>
      </g>
    )
  }

  private computeBounds () {
    this.minX = Number.MAX_VALUE
    this.maxX = Number.MIN_VALUE
    for (const plot of this.props.plots) {
      const bounds = plot.getDataBounds()
      this.minX = Math.min(this.minX, bounds.left)
      this.maxX = Math.max(this.maxX, bounds.right)
    }
  }

}

export default Chart
