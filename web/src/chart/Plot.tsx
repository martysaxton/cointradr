import * as React from 'react'
import { Point, Rect } from './CoreModel'

export abstract class Plot {
  protectedchartHeight: number

  protected dataBounds!: Rect
  protected chartBounds!: Rect
  protected dataWidth!: number
  protected dataHeight!: number
  protected chartWidth!: number
  protected chartHeight!: number

  getDataBounds () {
    if (!this.dataBounds) {
      throw new Error('dataBounds not defined')
    }
    return this.dataBounds
  }

  renderYAxisLabel (value: number): string {
    return value.toFixed(2).toString()
  }

  abstract render (): JSX.Element[]

  abstract computeBounds (): Rect

  setChartBounds (chartBounds: Rect) {
    this.chartBounds = chartBounds
    this.computeWidthHeights()
  }

  dataToChart (dataPoint: Point): Point {
    this.checkPreconditions()
    // chart coordinates, y increases down
    // data coordinates, y increases up
    return {
      x: this.dataToChartX(dataPoint.x),
      y: this.dataToChartY(dataPoint.y)
    }
  }

  dataToChartRect (dataRect: Rect): Rect {
    this.checkPreconditions()
    // chart coordinates, y increases down
    // data coordinates, y increases up
    const right = this.dataToChartX(dataRect.right)
    const top = this.dataToChartY(dataRect.top)
    return {
      left: this.dataToChartX(dataRect.left),
      bottom: this.dataToChartY(dataRect.bottom),
      right,
      top
    }
  }

  protected setDataBounds (dataBounds: Rect) {
    this.dataBounds = dataBounds
    this.computeWidthHeights()
  }

  protected dataToChartX (x: number): number {
    return (x - this.dataBounds.left) / this.dataWidth * this.chartWidth + this.chartBounds.left
  }

  protected dataToChartY (y: number): number {
    return this.chartBounds.bottom - (y - this.dataBounds.bottom) / this.dataHeight * this.chartHeight
  }

  private checkPreconditions () {
    if (!this.chartBounds) {
      throw new Error('chartBounds not set')
    }
    if (!this.dataBounds) {
      throw new Error('dataBounds not set')
    }
  }

  private computeWidthHeights () {
    if (this.chartBounds && this.dataBounds) {
      this.dataWidth = this.dataBounds.right - this.dataBounds.left
      this.dataHeight = this.dataBounds.top - this.dataBounds.bottom
      this.chartWidth = this.chartBounds.right - this.chartBounds.left
      this.chartHeight = this.chartBounds.bottom - this.chartBounds.top
    }
  }

}
