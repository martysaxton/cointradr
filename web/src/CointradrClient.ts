import {Market} from './Market'
import { Bar, Trade } from './model';
import { Interval } from './Interval';


interface BarRequestParams {
  market: Market
  start?: Date
  end?: Date
  dur: number
  threshold?: number
}

interface TradeRequestParams {
  market: Market
  start: Date
  end: Date
}

class CointradrClient {

  readonly startOfMeaningfulTime = '2018-11-01T00:00:00.000Z'

  baseUrl: string

  constructor() {
    if (process.env.NODE_ENV === 'production') {
      console.log('running in prod mode')
      this.baseUrl = ''
    } else {
      console.log('running in dev mode')
      this.baseUrl = 'http://localhost:8080'
    }

  }

  async fetchBars(params: BarRequestParams): Promise<Bar[]> {
    // const url = `${this.baseUrl}/bars?market=bina_btc_usd&dur=240&threshold=${this.state.thresholdInput}&start=2018-11-01T00:00:00.000Z`
    let url = `${this.baseUrl}/bars?${this.marketAndTime(params.market, params.start, params.end)}&dur=${params.dur}`
    if (params.threshold) {
      url += `&threshold=${params.threshold}`
    }
    console.log(`fetching bars: ${url}`)
    const response = await fetch(url)
    console.log(`got response for bars: ${response.status}`)
    const bars = await response.json() as Bar[]
    console.log(`got ${bars.length} bars`)
    bars.forEach((bar) => bar.start = new Date(bar.start))
    return bars
  }

  async fetchTrades(params: TradeRequestParams): Promise<Trade[]> {
    // this.setState({ loading: true })
    // const startEndStr = this.createStartEnd(params.start, params.end)
    // const tradesUrl = `${this.baseUrl}/trades?${startEndStr}}&market=bina_btc_usd`
    let url = `${this.baseUrl}/trades?${this.marketAndTime(params.market, params.start, params.end)}`

    // const tradesUrlFutures = `${this.baseUrl}/trades?start=${params.start.toISOString()}&end=${params.end.toISOString()}&market=okex_btc_usd-f`
    console.log(`loading trades: ${url}`)
    const tradesResponse = await fetch(url)
    console.log(`got responses trades: ${tradesResponse.status}`)
    const trades = await tradesResponse.json() as Trade[]
    trades.forEach((t) => t.t = new Date(t.t))
    return trades
  }

  private createStartEnd(start?: Date, end?: Date): string {
    const paramStartStr = start ? start.toISOString() : undefined
    const startStr = paramStartStr || this.startOfMeaningfulTime
    let queryStr = `start=${startStr}`
    if (end) {
      queryStr += `&end=${end.toISOString()}`
    }
    return queryStr
  }

  private urlFriendlyMarket(market: Market) {
    return `${market.exch}_${market.symb.replace('/', '_')}`
  }

  private marketAndTime(market: Market, start?: Date, end?: Date) {
    return `market=${this.urlFriendlyMarket(market)}&${this.createStartEnd(start, end)}`
  }
}
  
const cointradrClient = new CointradrClient()
export default cointradrClient