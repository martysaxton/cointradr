import { ExchangeSymbol } from "./model";

export interface Market {
  exch: ExchangeSymbol
  symb: string
}