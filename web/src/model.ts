export type ExchangeSymbol = 'bina' | 'okex' | 'huob'

export type Currency = 'usd' | 'btc' | 'eos' | 'eth'

export interface Trade {
  exch: ExchangeSymbol
  symb: string
  id: number
  t: Date
  price: string
  size: string
  bid: boolean
}

export interface Bar {
  exch: ExchangeSymbol
  symb: string
  start: Date
  dur: number
  open: number
  high: number
  low: number
  close: number
  vol: number
  vwap: number
  bv: number
  av: number
  ret: number
  aret: number
}
