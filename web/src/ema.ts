
export class WeightedExpodentialMovingAverage {

  private numerator = NaN
  private denominator = NaN
  private ema = NaN
  private readonly oneMinusAlpha: number
  constructor (readonly alpha: number) {
    this.oneMinusAlpha = 1 - alpha
  }

  nextValue (value: number, weight: number) {
    if (isNaN(this.numerator)) {
      this.numerator = value
      this.denominator = 1
    }
    this.numerator = this.alpha * this.numerator + this.oneMinusAlpha * weight * value
    this.denominator = this.alpha * this.denominator + this.oneMinusAlpha * weight
    this.ema = this.numerator / this.denominator
  }

  getValue () {
    return this.ema
  }
}
