import { Trade } from '../../common/model'
import { Dot } from './chart/ScatterPlot'

export function tradeDots (trades: Trade[]): Dot[] {
  return trades
    // .filter((trade) => trade.bid)
    .map((trade) => {
      const size = Number.parseFloat(trade.size)
      const price = Number.parseFloat(trade.price)
      if (isNaN(size) || isNaN(price)) {
        console.log('bad trade', trade)
      }
      if (!trade.t || !trade.t.getTime) {
        console.error('wtf trade %j', trade)
      }

      return {
        x: trade.t.getTime(),
        y: price,
        r: size * 8 + 2,
        opacity: 0.05,
        color: trade.bid ? 'red' : 'green'
      } as Dot
    })
}
