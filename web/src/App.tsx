import VolumePlot from './chart/VolumePlot'
import { Plot } from './chart/Plot'
import ChartText from './chart/ChartText'
import { pageDimensions } from './utils'
import { Dot, LineSegment, ScatterPlot } from './chart/ScatterPlot'
import './App.css'
import Chart from './chart/Chart'
import * as React from 'react'
import SolidColorPlot from './chart/SolidColorPlot'
import { Bar, Trade } from './model'
import { Point, Rect } from './chart/CoreModel'
import { AlgebraicLine, weightedLinearRegression, WeightedPair } from './linearRegression'
import { ChartLineProps } from './chart/ChartLine'
import { WeightedExpodentialMovingAverage } from './ema'
import { SpikeDetector, SpikeParameters } from './indicators/SpikeDetector'
import { ChartRectProps } from './chart/ChartRect'
import { Logger } from 'mongodb'
import { TradeDotProps, tradeDots } from './indicators/tradeDots'
import { createVwapLines } from './indicators/vwapLines'
import { createSpikeRects } from './indicators/spikeRects'
import { Interval } from './Interval'
import cointradrClient from './CointradrClient'
import { Market } from './Market';
// tslint:disable-next-line:no-commented-code
// const samples: WeightedPair[] = [
//   {
//     x: new Date(2018, 1, 1).getTime(),
//     y: 10,
//     w: 0.1
//   },
//   {
//     x: new Date(2018, 1, 2).getTime(),
//     y: 40,
//     w: 3
//   },
//   {
//     x: new Date(2018, 1, 3).getTime(),
//     y: 90,
//     w: 0.1
//   },
//   {
//     x: new Date(2018, 1, 4).getTime(),
//     y: 100,
//     w: 0.1
//   }
// ]

interface AppState {
  trades?: Trade[]
  thresholdInput: string
  prevThresholdInput: string
  loading: boolean
  currentBarIndex: number
  pumpDumpBars: Bar[]
  volumePlotBars: Bar[]
  dots: Dot[]
  plots: Plot[]
  chartInterval?: Interval
}



// tslint:disable-next-line:max-classes-per-file
class App extends React.Component<{}, AppState> {

  static spikeRectCount = 0
  baseUrl: string

  state: AppState = {
    thresholdInput: '0.5',
    prevThresholdInput: '',
    loading: false,
    currentBarIndex: 0,
    pumpDumpBars: [],
    volumePlotBars: [],
    dots: [],
    plots: [],
  }

  private readonly chartRect!: Rect

  private readonly svgWidth: number
  private readonly svgHeight: number
  private readonly margin = 10

  constructor (props: Readonly<{}>) {
    super(props)
    // margin around the chart
    const d = pageDimensions()
    this.svgWidth = d.x - this.margin * 2
    this.svgHeight = d.y - this.margin * 2
    // this.svgWidth = 1000
    // this.svgHeight = 1000
    const chartMarginBottomLeft = 55
    this.chartRect = {
      left: chartMarginBottomLeft,
      top: 10,
      right: chartMarginBottomLeft + this.svgWidth - chartMarginBottomLeft * 2,
      bottom: this.svgHeight - chartMarginBottomLeft
    }
    if (process.env.NODE_ENV === 'production') {
      console.log('running in prod mode')
      this.baseUrl = ''
    } else {
      console.log('running in dev mode')
      this.baseUrl = 'http://localhost:8080'
    }
    
  }

  componentDidMount = async () => {
    if (this.state.prevThresholdInput !== this.state.thresholdInput) {
      this.fetchFourMinuteBars()
    }
  }

  public render () {
    return (
      <div style={{
        backgroundColor: 'black',
        position: 'absolute',
        height: '100%',
        left: 0,
        top: 0,
        width: '100%',
        color: 'white'
        // padding: '10px'
      }} >
        
        <div style={{ padding: '10px' }}>
          <form onSubmit={this.handleSubmit} style={{ display: 'inline' }}>
            <label>
              Threshold:{'\u00A0\u00A0'}
              <input type='text' size={5} value={this.state.thresholdInput} onChange={this.handleChange} />
            </label>
            <span>{'\u00A0\u00A0'}</span>
          </form>
          <span>{this.state.loading ? 'Loading...' : this.nextPrevButtons()}</span>
        </div>
        {this.state.chartInterval &&
          <Chart bounds={this.chartRect} plots={this.state.plots} interval={this.state.chartInterval}/>
        }
      </div>
    )
  }

  handleChange = (event: React.FormEvent<HTMLInputElement>) => {
    console.log('change')
    this.setState({ thresholdInput: event.currentTarget.value })
  }

  handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    console.log('A name was submitted: ' + this.state.thresholdInput)
    event.preventDefault()
    this.fetchFourMinuteBars()
  }

  private async fetchFourMinuteBars () {
    if (this.state.loading) {
      return
    }
    this.setState({ loading: true })
    // const url = `${this.baseUrl}/bars?market=bina_btc_usd&dur=240&threshold=${this.state.thresholdInput}&start=2018-11-01T00:00:00.000Z`
    // console.log(`fetching bars: ${url}`)
    // const bars = await this.awaitBars(fetch(url))
    // console.log(`got ${bars.length} bars`)
    const bars = await cointradrClient.fetchBars({
      market: { exch: 'bina', symb: 'btc/usd'},
      dur: 4 * 60,
      threshold: Number.parseFloat(this.state.thresholdInput)
    })
    if (bars.length > 0) {
      this.setState({ pumpDumpBars: bars })
      this.fetchTrades(0, bars)
    } else {
      // no bars
      this.setState({ loading: false })
    }
  }

  private async awaitBars (promise: Promise<Response>): Promise<Bar[]> {
    const response = await promise
    console.log(`got response for bars: ${response.status}`)
    const bars = await response.json() as Bar[]
    console.log(`got ${bars.length} bars`)
    bars.forEach((bar) => bar.start = new Date(bar.start))
    return bars
  }

  private async fetchTrades (barIndex: number, bars?: Bar[]) {
    const bs = bars || this.state.pumpDumpBars
    console.log(`loading bars for ${barIndex}`)
    const bar = bs[barIndex]
    const paddingTime = 1000 * 60 * 2
    const startMillis = bar.start.getTime() - paddingTime
    const endMillis = startMillis + bar.dur + paddingTime
    const start = new Date(startMillis)
    const end = new Date(endMillis)
    if (!start || !end) {
      return
    }
    this.setState({ loading: true })

    const market: Market = {exch: 'bina', symb: 'btc/usd'}

    const tradesPromise = cointradrClient.fetchTrades({
      market,
      start,
      end
    })
    const barsPromise = cointradrClient.fetchBars({
      market,
      start,
      end,
      dur: 5
    })

    const [trades, volumeBars ] = await Promise.all([ tradesPromise, barsPromise ])
    console.log(`got ${trades.length} trades, ${volumeBars.length} bars`)

    const dotProps: TradeDotProps = {
      opacity: 0.1,
      volumeScale: 6,
      bidColor: 'red',
      askColor: 'green',
      relative: false
    }

    const dots: Dot[] = tradeDots(trades, dotProps)

    const dotFutureProps = {
      ...dotProps,
      bidColor: 'magenta',
      askColor: 'cyan'
    }

    const style: React.CSSProperties = {
      stroke: 'white',
      strokeWidth: 1,
      opacity: 0.5
    }
    const styleAsks = { ...style, stroke: 'green' }
    const styleBids = { ...style, stroke: 'red' }
    // const styleVwap = { ...style, stroke: 'yellow' }
     const lrDuration = 5000
    let linearRegressionLines = this.computeLinearRegressions(trades, lrDuration, style)
    linearRegressionLines = [ ...linearRegressionLines, ...this.computeLinearRegressions(trades, lrDuration, styleBids, true) ]
    linearRegressionLines = [ ...linearRegressionLines, ...this.computeLinearRegressions(trades, lrDuration, styleAsks, false) ]

    // let emaLines = this.computeEma(trades, style)
    // emaLines = [ ...emaLines, ...this.computeEma(trades, styleBids, true) ]
    // emaLines = [ ...emaLines, ...this.computeEma(trades, styleAsks, false) ]
    const vwapLines = createVwapLines(trades, 5000, style)
    // vwapLines = [ ...vwapLines, ...createVwapLines(trades, 5000, styleBids, true) ]
    // vwapLines = [ ...vwapLines, ...createVwapLines(trades, 5000, styleAsks, false) ]

    // const scatterPlot = new ScatterPlot(dots, [...vwapLines, ...emaLines, ...linearRegressionLines])

    const spikeRects = createSpikeRects(trades)

    // console.log(dotsFutures[0])
    const scatterPlot = new ScatterPlot([ ...dots ], [ ...linearRegressionLines, ...vwapLines ], [ ...spikeRects ])
    const volumePlot = new VolumePlot(volumeBars)

    this.setState({
      trades,
      plots: [ scatterPlot, volumePlot ],
      // plots: [ scatterPlot ],
      loading: false,
      currentBarIndex: barIndex,
      chartInterval: {start: bar.start, end: new Date(bar.start.getTime() + bar.dur)},
      dots
    })

  }

  private computeEma (trades: Trade[], style: React.CSSProperties, isBid?: boolean): ChartLineProps[] {

    const ema = new WeightedExpodentialMovingAverage(0.995)
    const lines: ChartLineProps[] = []
    let prevPoint: Point = { x: NaN, y: NaN }
    for (let i = 0; i < trades.length; ++i) {
      const trade = trades[i]
      if (isBid === undefined || trade.bid === isBid) {
        const millis = trade.t.getTime()
        // ema.nextValue(Number.parseFloat(trade.price), Number.parseFloat(trade.size))
        const price = Number.parseFloat(trade.price)
        const size = Number.parseFloat(trade.size)
        ema.nextValue(price, size)
        if (isNaN(prevPoint.x)) {
          prevPoint.x = millis
          prevPoint.y = ema.getValue()
        } else {
          const nextPoint: Point = {
            x: millis,
            y: ema.getValue()
          }
          const nextLine: ChartLineProps = {
            start: prevPoint,
            end: nextPoint,
            style: style
          }
          lines.push(nextLine)
          prevPoint = nextPoint
        }
      }
    }
    return lines
  }

  private computeLinearRegressions (trades: Trade[], duration: number, style: React.CSSProperties, isBid?: boolean): ChartLineProps[] {
    if (trades.length === 0) {
      return []
    }

    const lines: ChartLineProps[] = []
    let wps: WeightedPair[] = []
    let startBarMillis = Math.floor(trades[0].t.valueOf() / duration) * duration
    let endBarMillis = startBarMillis + duration

    for (let i = 0; i < trades.length; ++i) {
      const trade = trades[i]
      const millis = trade.t.getTime()
      if (millis < endBarMillis) {
        if (isBid === undefined || isBid === trade.bid) {
          const time = trade.t.getTime() - startBarMillis
          const price = Number.parseFloat(trade.price)
          const size = Number.parseFloat(trade.size)
          const wp: WeightedPair = {
            x: time,
            y: price,
            w: size
          }
          wps.push(wp)
        }
      } else {
        if (wps.length > 3) {
          lines.push(this.weightedPairsToChartLineProps(wps, style, startBarMillis, duration))
        }
        startBarMillis = endBarMillis
        endBarMillis += duration
        wps = []
      }
    }
    if (wps.length > 3) {
      lines.push(this.weightedPairsToChartLineProps(wps, style, startBarMillis, duration))
    }
    return lines
  }

  private weightedPairsToChartLineProps (wps: WeightedPair[], style: React.CSSProperties, startBarMillis: number, duration: number) {
    const algebraicLine = weightedLinearRegression(wps)
    const lineSegment = this.shiftedLineToSegment(algebraicLine, startBarMillis, duration)
    return { ...lineSegment, style }
  }

  private shiftedLineToSegment (algebraicLine: AlgebraicLine, shift: number, duration: number): LineSegment {
    const lineSegment: LineSegment = {
      // x is 0, so mx+b is just b
      start: {
        x: 0,
        y: algebraicLine.intercept
      },
      // x is duration
      end: {
        x: duration,
        y: algebraicLine.slope * duration + algebraicLine.intercept
      }
    }
    // shift it back
    lineSegment.start.x += shift
    lineSegment.end.x += shift
    return lineSegment
  }

  private nextPrevButtons () {
    return (
      <div style={{ display: 'inline' }}><button onClick={this.prev}>Prev</button><button onClick={this.next}>Next</button></div>
    )
  }

  private prev = () => {
    if (this.state.currentBarIndex === 0) {
      return
    }
    this.fetchTrades(this.state.currentBarIndex - 1)
  }
  private next = () => {
    if (this.state.currentBarIndex === this.state.pumpDumpBars.length - 1) {
      return
    }
    this.fetchTrades(this.state.currentBarIndex + 1)
  }

}

export default App
