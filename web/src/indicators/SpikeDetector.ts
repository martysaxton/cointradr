import { Trade } from '../model'

export interface SpikeParameters {
  timeThreshold: number,
  countThreshold: number,
  volumeThreshold: number
}

export class SpikeDetector {
  startPrice = 0
  endPrice = 0
  startTime!: Date
  endTime!: Date
  count = 0
  volume = 0
  private prevTrade?: Trade
  private prevPrice!: number

  constructor (readonly bids: boolean, readonly params: SpikeParameters, readonly callback: (spikeDetector: SpikeDetector) => void) {
    // noop
  }

  // tslint:disable-next-line:cognitive-complexity
  addTrade (trade: Trade) {
    if (trade.bid === this.bids) {
      const currentPrice = Number.parseFloat(trade.price)
      if (this.prevTrade) {
        const timeDelta = trade.t.getTime() - this.prevTrade.t.getTime()
        // console.log('bid delta %d', timeDelta)
        if (timeDelta <= this.params.timeThreshold) {
          if (!this.isSpiking()) {
            this.startPrice = this.prevPrice
            this.startTime = this.prevTrade.t
            this.addTradeToCurrentSpike(this.prevTrade)
          }
          this.addTradeToCurrentSpike(trade)
        } else {
          if (this.isSpiking()) {
            this.endPrice = this.prevPrice
            this.endTime = this.prevTrade.t
            if (this.count >= this.params.countThreshold && this.volume >= this.params.volumeThreshold) {
              this.callback(this)
            }
            this.resetValues()
          }
        }
      }
      this.prevTrade = trade
      this.prevPrice = currentPrice
    }
  }

  private addTradeToCurrentSpike (trade: Trade) {
    ++this.count
    this.volume += Number.parseFloat(trade.size)
  }

  private isSpiking () {
    return this.count !== 0
  }

  private resetValues () {
    this.startPrice = 0
    this.endPrice = 0
    this.count = 0
    this.volume = 0
  }

}
