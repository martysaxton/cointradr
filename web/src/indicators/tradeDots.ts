import { Trade } from '../model'
import { Dot } from '../chart/ScatterPlot'

export interface TradeDotProps {
  opacity: number,
  volumeScale: number,
  bidColor: string,
  askColor: string,
  relative: boolean
}

class RelativePriceComputer {

  private prevRelativeValue = Number.NaN
  private prevAbsoluteValue = Number.NaN

  nextValue (currentValue: number): number {
    let rv: number
    if (isNaN(this.prevRelativeValue)) {
      rv = 0
    } else {
      rv = this.prevRelativeValue + (currentValue - this.prevAbsoluteValue) / this.prevAbsoluteValue
    }
    this.prevRelativeValue = rv
    this.prevAbsoluteValue = currentValue
    return rv
  }
}

export function tradeDots (trades: Trade[], props: TradeDotProps): Dot[] {

  const prevValue = 0
  const prevPrice = 0

  const rpc = new RelativePriceComputer()

  return trades
    // .filter((trade) => trade.bid)
    .map((trade) => {
      const size = Number.parseFloat(trade.size)
      const price = Number.parseFloat(trade.price)
      if (isNaN(size) || isNaN(price)) {
        console.log('bad trade', trade)
      }
      if (!trade.t || !trade.t.getTime) {
        console.error('wtf trade %j', trade)
      }

      const value = props.relative ? rpc.nextValue(price) : price

      return {
        x: trade.t.getTime(),
        y: value,
        r: size * props.volumeScale + 2,
        opacity: 0.2,
        color: trade.bid ? props.bidColor : props.askColor
      } as Dot
    })
}
