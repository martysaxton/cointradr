import { ChartRectProps } from 'src/chart/ChartRect'
import { Trade } from '../model'
import { SpikeDetector, SpikeParameters } from './SpikeDetector'
import { Rect } from 'src/chart/CoreModel'

let spikeRectCount = 0

export function createSpikeRects (trades: Trade[]): ChartRectProps[] {
  const styleBids: React.CSSProperties = {
    stroke: 'magenta',
    strokeWidth: 1,
    opacity: 1,
    fill: 'none'
  }
  const styleAsks: React.CSSProperties = {
    ...styleBids,
    stroke: 'cyan'
  }
  const spikeParams: SpikeParameters = {
    countThreshold: 10,
    timeThreshold: 20,
    volumeThreshold: 1
  }
  const rv: ChartRectProps[] = []
  const bidsSpikeDetector = new SpikeDetector(true, spikeParams, (sd) => {
    rv.push(spikeCallback(sd, styleBids))
  })
  const asksSpikeDetector = new SpikeDetector(false, spikeParams, (sd) => {
    rv.push(spikeCallback(sd, styleAsks))
  })

  trades.forEach((trade) => {
    bidsSpikeDetector.addTrade(trade)
    asksSpikeDetector.addTrade(trade)
  })

  return rv
}

function spikeCallback (sd: SpikeDetector, style: React.CSSProperties): ChartRectProps {
  const rect: Rect = {
    top: Math.max(sd.startPrice, sd.endPrice),
    bottom: Math.min(sd.startPrice, sd.endPrice),
    left: sd.startTime.getTime(),
    right: sd.endTime.getTime() + 1000
  }
  return {
    key: 'spike' + ++spikeRectCount,
    rect: rect,
    style: style
  }
}
