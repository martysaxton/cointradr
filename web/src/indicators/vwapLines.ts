import { Trade } from '../model'
import { ChartLineProps } from '../chart/ChartLine'

export function createVwapLines (trades: Trade[], duration: number, style: React.CSSProperties, isBid?: boolean): ChartLineProps[] {
  if (trades.length === 0) {
    return []
  }
  const lines: ChartLineProps[] = []
  let startBarMillis = Math.floor(trades[0].t.valueOf() / duration) * duration
  let endBarMillis = startBarMillis + duration
  let volumeSum = 0
  let priceSum = 0
  for (let i = 0; i < trades.length; ++i) {
    const trade = trades[i]
    if (isBid !== undefined && isBid !== trade.bid) {
      continue
    }
    const millis = trade.t.getTime()
    const price = Number.parseFloat(trade.price)
    const size = Number.parseFloat(trade.size)
    if (millis < endBarMillis) {
      priceSum += price * size
      volumeSum += size
    } else {
      if (priceSum > 0 && volumeSum > 0) {
        const vwap = priceSum / volumeSum
        lines.push({
          start: { x: startBarMillis, y: vwap },
          end: { x: endBarMillis, y: vwap },
          style
        })
      }
      volumeSum = 0
      priceSum = 0
      startBarMillis = endBarMillis
      endBarMillis += duration
    }
  }
  if (priceSum > 0 && volumeSum > 0) {
    const vwap = priceSum / volumeSum
    lines.push({
      start: { x: vwap, y: startBarMillis },
      end: { x: vwap, y: endBarMillis },
      style
    })
  }
  return lines
}
