import { loggerFactory, configureLoggerFactory } from './logging'
configureLoggerFactory({
  console: true,
  sourceRootDir: 'server/src'
  // logDir: './logs'
})

import { db } from './db'
import { Bar, Trade } from './model'

const logger = loggerFactory()

process.on('unhandledRejection', function (error, p) {
  logger.error('unhandledRejection: %s %s', error, p)
  console.error('unhandled rejection', error)
  process.exit(1)
})

function createSyntheticBar (prevBar: Bar, duration: number): Bar {
  return {
    exch: prevBar.exch,
    symb: prevBar.symb,
    start: new Date(prevBar.start.getTime() + duration),
    dur: duration,
    open: prevBar.close,
    close: prevBar.close,
    high: prevBar.close,
    low: prevBar.close,
    vol: 0,
    bv: 0,
    av: 0,
    vwap: prevBar.close,
    ret: 0,
    aret: 0
  }
}

// tslint:disable-next-line:cognitive-complexity
async function go () {
  const bars = await db.getBars()
  const query = {
    exch: 'bina',
    symb: 'btc/usd',
    dur: 5000
  }
  const barCursor = bars.find(query)
  logger.info('going')
  let prevBar: Bar | undefined
  let count = 0
  const history: Bar[] = []
  while (true) {
    const bar = await barCursor.next()
    if (!bar) {
      break
    }
    // logger.info('bar %j', bar)
    if (prevBar) {
      const barTime = bar.start.getTime()
      const prevBarTime = prevBar.start.getTime()
      let diff = barTime - prevBarTime
      if (diff > 5000 * 5) {
        // gap too big, just start over
        history.splice(0)
      } else {
        while (diff > 5000) {
          const syntheticBar = createSyntheticBar(prevBar, 5000)
          history.push(syntheticBar)
          diff = barTime - syntheticBar.start.getTime()
          prevBar = syntheticBar
        }
        history.push(bar)
        const len = history.length
        if (len > 5) {
          history.splice(0, len - 5)
        }
      }
      processBars(history)
    }
    prevBar = bar
    ++count
    if (count % 10000 === 0) {
      logger.info('current bar %s', bar.start.toString())
    }
    if (bar.vol === 0) {
      logger.info('empty bar %j', bar)
    }
  }
  logger.info('done')
  // tslint:disable-next-line:no-floating-promises
  db.close()
}

// tslint:disable-next-line:cognitive-complexity
function processBars (bars: Bar[]) {
  if (bars.length !== 5) {
    return
  }
  for (let i = 1; i < 5; ++i) {
    if (bars[i - 1].start.getTime() + 5000 !== bars[i].start.getTime()) {
      logger.error('wtf')
    }
  }
  const vwaps = bars.map(bar => (Math.round(bar.vwap * 1000) / 1000).toFixed(3))

  const bar5 = bars[4]
  const bar1 = bars[0]
  const bar3 = bars[2]

  const return5 = (bar5.vwap - bar1.vwap) / bar1.vwap * 100
  const return3 = (bar5.vwap - bar3.vwap) / bar3.vwap * 100
  const r5 = return5.toFixed(3)
  const r3 = return3.toFixed(3)
  if (Math.abs(return5) >= 0.25) {
    let b = false
    if (bar5.vwap < bar1.vwap) {
      // bullish
      if (bar5.vwap < bar3.vwap) {
        b = true
      }
    } else {
      // bearish
      if (bar5.vwap > bar3.vwap) {
        b = true
      }
    }
    if (b) {
      logger.info('vwaps %s %j %s %s', bar5.start.toString(), vwaps, r5, r3)
    }
  }

}

type TradeCallback = (trade: Trade) => void

async function eachTrade (query: any, callback: TradeCallback) {
  const trades = await db.getTrades()
  const tradeCursor = trades.find(query).sort({ sort: { t: 1 } })
  while (true) {
    const trade = await tradeCursor.next()
    if (!trade) {
      break
    }
    callback(trade)
  }
}

interface SpikeParameters {
  timeThreshold: number,
  countThreshold: number,
  volumeThreshold: number
}

class SpikeDetector {
  startPrice = 0
  endPrice = 0
  startTime!: Date
  endTime!: Date
  count = 0
  volume = 0
  trade?: Trade
  price!: number

  constructor (readonly bids: boolean, readonly params: SpikeParameters, readonly callback: (spikeDetector: SpikeDetector) => void) {
    // noop
  }

  // tslint:disable-next-line:cognitive-complexity
  addTrade (trade: Trade) {
    if (trade.bid === this.bids) {
      const currentPrice = Number.parseFloat(trade.price)
      if (this.trade) {
        const timeDelta = trade.t.getTime() - this.trade.t.getTime()
        // console.log('bid delta %d', timeDelta)
        if (timeDelta <= this.params.timeThreshold) {
          if (!this.isSpiking()) {
            this.startPrice = this.price
            this.startTime = this.trade.t
            this.addTradeToCurrentSpike(this.trade)
          }
          this.addTradeToCurrentSpike(trade)
        } else {
          if (this.isSpiking()) {
            this.endPrice = this.price
            this.endTime = this.trade.t
            if (this.count >= this.params.countThreshold && this.volume >= this.params.volumeThreshold) {
              this.callback(this)
            }
            this.resetValues()
          }
        }
      }
      this.trade = trade
      this.price = currentPrice
    }
  }

  private addTradeToCurrentSpike (trade: Trade) {
    ++this.count
    this.volume += Number.parseFloat(trade.size)
  }

  private isSpiking () {
    return this.count !== 0
  }

  private resetValues () {
    this.startPrice = 0
    this.endPrice = 0
    this.count = 0
    this.volume = 0
  }

}

// tslint:disable-next-line:cognitive-complexity
async function goSpikeDetector () {

  const query = {
    exch: 'bina',
    symb: 'btc/usd',
    $and: [
      { t: { $gte: new Date('7/26/2018, 3:56:00 PM') } },
      { t: { $lt: new Date('7/26/2018, 4:00:00 PM') } }
    ]
  }
  const spikeParams: SpikeParameters = {
    countThreshold: 10,
    timeThreshold: 10,
    volumeThreshold: 3
  }

  const bidSpikeDetector = new SpikeDetector(true, spikeParams, (sd) => {
    console.log('spike %s %s.%d %d count=%d vol=%f price=%f',
      sd.startTime.toLocaleDateString('en-US'),
      sd.startTime.toLocaleTimeString('en-US'),
      sd.startTime.getMilliseconds(),
      sd.startTime.getTime(),
      sd.count, sd.volume, sd.endPrice - sd.startPrice)
  })

  eachTrade(query, (trade) => {
    bidSpikeDetector.addTrade(trade)
  })
}

async function goTrades () {
  const trades = await db.getTrades()
  const query = {
    exch: 'bina',
    symb: 'btc/usd'
  }
  const tradeCursor = trades.find(query).sort({ sort: { t: 1 } })
  let prevTrade: Trade | undefined
  let count = 0
  while (true) {
    const trade = await tradeCursor.next()
    if (!trade) {
      break
    }

    if (prevTrade) {
      const tradeTime = trade.t.getTime()
      const prevTradeTime = prevTrade.t.getTime()
      const diff = tradeTime - prevTradeTime
      if (diff > 5000) {
        logger.info('gap %s %d', trade.t.toString(), diff)
      }
    }

    prevTrade = trade
    ++count
    if (count % 10000 === 0) {
      logger.info('current trade %s', trade.t.toString())
    }
  }
  logger.info('done')
  db.close()
}

goSpikeDetector()
