import { Currency, Trade } from '../model'
import { Exchange, TradeHandler } from '../Exchange'

import { BinanceStreamTrade, BinanceTradeDataStreamParam } from '../clients/binance/BinanceTypes'
import { BinanceTradeDataStream } from '../clients/binance/BinanceStreams'

function toStandardCurrency (currency: string): Currency {
  const c = currency.toLowerCase()
  if (c === 'usdt') {
    return 'usd'
  }
  return c as Currency
}

// tslint:disable-next-line:no-identical-functions
function toExchangeCurrency (currency: Currency): string {
  if (currency === 'usd') {
    return 'usdt'
  }
  return currency as Currency
}

function toStandardCurrencyPair (exchangeBase: string, exchangeQuote: string): string {
  return toStandardCurrency(exchangeBase) + '/' + toStandardCurrency(exchangeQuote)
}

function toExchangeCurrencyPair (pair: string) {
  const [base, quote] = pair.split('/') as Currency[]
  return toExchangeCurrency(base) + toExchangeCurrency(quote)
}

function splitBinancePair (pair: string): [string, string] {
  return [ pair.slice(0, 3), pair.slice(3) ]
}

export class BinanceExchange implements Exchange {

  private tradeStreamClient?: BinanceTradeDataStream
  private tradeHandler?: TradeHandler

  subscribeTrades (handler: TradeHandler, ...symbols: string[]): void {
    this.tradeHandler = handler
    symbols.forEach(s => { if (s.endsWith('-f')) throw new Error('bina doesn\'t support futures') })

    if (this.tradeStreamClient) {
      this.tradeStreamClient.disconnect()
    }
    const streamParams: BinanceTradeDataStreamParam[] = symbols.map(s => {
      return { symbol: toExchangeCurrencyPair(s), aggregated: false }
    })

    this.tradeStreamClient = new BinanceTradeDataStream(this.clientTradeHandler, streamParams)
    this.tradeStreamClient.connect()
  }

  clientTradeHandler = (binaTrade: BinanceStreamTrade): void => {
    if (!this.tradeHandler) {
      return
    }

    const bp = splitBinancePair(binaTrade.s)
    //
    const trade: Trade = {
      exch: 'bina',
      id: binaTrade.t,
      symb: toStandardCurrencyPair(bp[0], bp[1]),
      t: new Date(),
      size: binaTrade.q,
      price: binaTrade.p,
      bid: binaTrade.m
    }

    this.tradeHandler(trade)

  }

}
