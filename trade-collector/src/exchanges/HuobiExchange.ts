import { Exchange, TradeHandler } from '../Exchange'
import { HuobiClient, HuobiTradeHandler, HuobiTrade } from '../clients/Huobi'
import { loggerFactory } from '../logging'
import { Currency, Trade } from '../model'

const logger = loggerFactory()

function toHuobiCurrency (currency: string) {
  if (currency === 'usd') {
    return 'usdt'
  }
  return currency
}

function toHuobiPair (symbol: string): string {
  const [base, quote] = symbol.split('/') as Currency[]
  return toHuobiCurrency(base) + toHuobiCurrency(quote)
}

const standardPairMap: {[key: string]: string} = {
  'btcusdt': 'btc/usd',
  'ethusdt': 'eth/usd',
  'eosusdt': 'eos/usd'
}

function toStandardCurrencyPair (huobiSymbol: string): string {
  return standardPairMap[huobiSymbol]
}

const HUOBI_SYMBOL = 'huob'

export class HuobiExchange implements Exchange {

  private client?: HuobiClient

  private tradeHandler?: TradeHandler

  subscribeTrades (handler: TradeHandler, ...symbols: string[]): void {

    if (this.client) {
      this.client.disconnect()
      this.client = undefined
    }

    const huobiSymbols = symbols.map(toHuobiPair)
    this.tradeHandler = handler
    this.client = new HuobiClient(this.clientTradeHandler, huobiSymbols)
    this.client.connect()

  }

  clientTradeHandler: HuobiTradeHandler = (huobiTrade: HuobiTrade): void => {
    logger.debug('trade %j', huobiTrade)
    if (this.tradeHandler) {
      const trade: Trade = {
        exch: HUOBI_SYMBOL,
        id: huobiTrade.id,
        symb: toStandardCurrencyPair(huobiTrade.symbol),
        t: new Date(),
        size: huobiTrade.amount.toString(),
        price: huobiTrade.price.toString(),
        bid: huobiTrade.direction === 'sell'
      }
      this.tradeHandler(trade)
    }

  }

}
