import { Trade, Currency } from '../model'
import { Exchange, TradeHandler } from '../Exchange'
import { OkExFuturesStream, OkExSpotStream, OkExTradeHandler, OkExTrade } from '../clients/OkEx'

function toStandardCurrencyPair (okExPair: string, isFutures: boolean): string {
  const f = isFutures ? '-f' : ''
  switch (okExPair) {
    case 'btc_usdt':
    case 'usd_btc':
      return 'btc/usd' + f
    case 'eos_usdt':
    case 'usd_eos':
      return 'eos/usd' + f
    case 'eth_usdt':
    case 'usd_eth':
      return 'eth/usd' + f
    default:
      throw new Error(`unknown OkEx pair: ${okExPair}`)
  }
}

function toOkexCurrency (currency: string) {
  if (currency === 'usd') {
    return 'usdt'
  }
  return currency
}

function toOkExPair (symbol: string): string {
  const [pair] = symbol.split('-f')
  const [base, quote] = pair.split('/') as Currency[]
  if (isFutures(symbol)) {
    return quote + '_' + base
  }
  return toOkexCurrency(base) + '_' + toOkexCurrency(quote)
}

function isFutures (symbol: string) {
  return symbol.endsWith('-f')
}

export class OkExExchange implements Exchange {

  private spotStream?: OkExSpotStream
  private futuresStream?: OkExFuturesStream
  private tradeHandler?: TradeHandler

  subscribeTrades (handler: TradeHandler, ...markets: string[]): void {
    this.tradeHandler = handler

    if (this.spotStream) {
      this.spotStream.disconnect()
      this.spotStream = undefined
    }
    if (this.futuresStream) {
      this.futuresStream.disconnect()
      this.futuresStream = undefined
    }

    const spot = markets.filter(market => !isFutures(market)).map(toOkExPair)
    const futures = markets.filter(market => isFutures(market)).map(toOkExPair)

    if (spot.length > 0) {
      this.spotStream = new OkExSpotStream(this.clientTradeHandler, ...spot)
      this.spotStream.connect()
    }
    if (futures.length > 0) {
      this.futuresStream = new OkExFuturesStream(this.clientTradeHandler, ...futures)
      this.futuresStream.connect()
    }
  }

  clientTradeHandler: OkExTradeHandler = (okExTrade: OkExTrade): void => {
    if (this.tradeHandler) {
      const trade: Trade = {
        exch: 'okex',
        id: parseInt(okExTrade.id, 10),
        symb: toStandardCurrencyPair(okExTrade.symbol, okExTrade.future),
        t: new Date(),
        size: okExTrade.size,
        price: okExTrade.price,
        bid: okExTrade.side === 'bid'
      }
      this.tradeHandler(trade)
    }
  }

}
