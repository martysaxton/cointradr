import { MongoClient, Db as MongoDb, Collection, InsertOneWriteOpResult } from 'mongodb'
import { loggerFactory } from './logging'
import Utils from './utils/Utils'

const logger = loggerFactory()

type ResolveType<T> = (value?: T | PromiseLike<T>) => void

import { Bar, Trade } from './model'

class Db {

  private pendingOps: ResolveType<MongoDb>[] = []
  private mongoClient ?: MongoClient
  private db?: MongoDb
  private uniqueIndexesCreated = false

  constructor () {
    // tslint:disable-next-line:no-floating-promises
    Utils.retry(1000, this.mongoConnectionAttempt)
  }

  mongoConnectionAttempt = async (): Promise<boolean> => {
    const mongoUrl = 'mongodb://mongo:27017'
    logger.info('connecting to mongo')
    return new Promise<boolean>(async (resolve, reject) => {
      try {
        this.mongoClient = await MongoClient.connect(mongoUrl, { reconnectTries: 1000, reconnectInterval: 1000 })
        logger.info('connected to mongo')
        this.db = this.mongoClient.db('cointradr')
        await this.createUniqueIndexes(this.db)
        this.createSearchIndexes()
        this.resolveAllWaiting(this.db)
        resolve(true)
      } catch (e) {
        logger.info(`error while connecting to mongo, retrying:  ${e.message}`)
        resolve(false)
      }
    })
  }

  async close () {
    if (this.mongoClient) {
      return this.mongoClient.close()
    }
  }

  private async createUniqueIndexes (db: MongoDb) {
    logger.debug('creating unique indexes')
    const trades = db.collection<Trade>('trades')
    let result = await trades.createIndex({ exch: 1, symb: 1, id: 1 }, { unique: true })
    logger.debug('create unqique indexes for trades result: %s', result)
    const bars = db.collection<Bar>('bars')
    result = await bars.createIndex({ exch: 1, symb: 1, dur: 1, start: 1 }, { unique: true })
    logger.debug('create unqique indexes for bars result: %s', result)
    this.uniqueIndexesCreated = true
    logger.debug('done creating unique indexes')
  }

  private createSearchIndexes () {
    const indexes = [
      { t: 1 },
      { t: 1, exch: 1, symb: 1 },
      { exch: 1, symb: 1, dur: 1 }
    ]
    this.getTrades().then(trades => {
      const promises = indexes.map(indexDef => trades.createIndex(indexDef))
      // tslint:disable-next-line:no-floating-promises
      Promise.all(promises).then(() => {
        logger.debug('done creating search indexes')
      })
    })
  }

  async getTrades () {
    const db = await this.getDb()
    return db.collection<Trade>('trades')
  }

  async getBars () {
    const db = await this.getDb()
    return db.collection<Bar>('bars')
  }

  async insertTrade (trade: Trade) {
     // tslint:disable-next-line:no-floating-promises
    this.insert(await this.getTrades(), trade)
  }
  async insertBar (bar: Bar): Promise<void | InsertOneWriteOpResult> {
    logger.debug('inserting bar')
    return this.insert(await this.getBars(), bar)
  }

  private insert<T> (collection: Collection<T>, value: T): Promise<void | InsertOneWriteOpResult> {
    return collection.insertOne(value)
      .catch(e => {
        if (e.message && e.message.includes('duplicate key')) {
          logger.warn('duplicate %s: %j', value.constructor.name, e)
        } else {
          throw e
        }
      })
  }

  async getDb (): Promise<MongoDb> {
    if (this.uniqueIndexesCreated && this.db) {
      return this.db
    }
    return new Promise<MongoDb>((resolve, reject) => {
      this.pendingOps.push(resolve)
    })
  }

  private resolveAllWaiting (db: MongoDb) {
    this.pendingOps.forEach(resolve => resolve(db))
  }

}

export const db = new Db()
