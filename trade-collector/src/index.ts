import { loggerFactory, configureLoggerFactory } from './logging'
configureLoggerFactory({
  console: true,
  sourceRootDir: 'server/src',
  logDir: './logs'
})

import { createMissingBars } from './createMissingBars'
import startServer from './server'
import { db } from './db'
import store, { createTradeReceivedAction } from './store'
import { BinanceExchange } from './exchanges/BinanceExchange'
import { Trade } from './model'
import { OkExExchange } from './exchanges/OkExExchange'
import { barFactoryManager } from './BarFactoryManager'
import { HuobiExchange } from './exchanges/HuobiExchange'

const logger = loggerFactory()

const SPOT_MARKETS = ['btc/usd', 'eth/usd', 'eos/usd' ]
const FUTURES_MARKETS = SPOT_MARKETS.map(s => s + '-f')
const ALL_MARKETS = SPOT_MARKETS.concat(FUTURES_MARKETS)

async function tradeHandler (trade: Trade) {
  logger.info('got trade %j', trade)
  store.dispatch(createTradeReceivedAction({ exchange: trade.exch }))
  await barFactoryManager.handleTrade(trade)
  // tslint:disable-next-line:no-floating-promises
  db.insertTrade(trade)
}

function startTradeListeners () {

  new BinanceExchange().subscribeTrades(tradeHandler, ...SPOT_MARKETS)
  new OkExExchange().subscribeTrades(tradeHandler, ...ALL_MARKETS)
  new HuobiExchange().subscribeTrades(tradeHandler, ...SPOT_MARKETS)
}

process.on('unhandledRejection', function (error, p) {
  logger.error('unhandledRejection: %s %s', error, p)
  console.error('unhandled rejection', error)
  process.exit(1)
})

function foo (): string {
  return 'asdf'
}

foo()

// startServer()
// tslint:disable-next-line:no-floating-promises
startTradeListeners()
// tslint:disable-next-line:no-floating-promises
createMissingBars()
