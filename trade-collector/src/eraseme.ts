import { HuobiExchange } from './exchanges/HuobiExchange'
import { configureLoggerFactory, loggerFactory } from './logging'
import { Trade } from './model'

configureLoggerFactory({
  console: true,
  sourceRootDir: 'serer/src'
})

const logger = loggerFactory()

function handler (trade: Trade): void {
  logger.info('trade %j', trade)

}

async function go2 () {
  logger.info('going')
  const huobi = new HuobiExchange()
  huobi.subscribeTrades(handler, 'btc/usd', 'eth/usd', 'eos/usd')
}

go2()
