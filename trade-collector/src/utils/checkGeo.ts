const fetch = require('nosde-fetch')
const { spawnSync } = require('child_process')

export async function isUnitedStatesIP (): Promise<boolean> {
  const response = await fetch('http://ipecho.net/plain')
  const ip = await response.text()
  // console.log(ip)
  const geo = spawnSync('geoiplookup', [ip]).stdout.toString('utf-8')
  // console.log(geo)
  return geo.includes('United States')
}
