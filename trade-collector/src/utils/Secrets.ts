import { encrypt, decrypt, SymetricEncryptionFunction } from './symetric-encryption'
import * as os from 'os'
import * as path from 'path'
import * as fs from 'fs'
import appRoot from 'app-root-path'
const prompt = require('password-prompt')
import { loggerFactory } from '../logging'
const LOGGER = loggerFactory()

type KeyValPairs = { [key: string]: string }

const SECRETS_FILENAME = 'secrets.json'
const SECRETS_ENCRYPTED_FILENAME = 'secrets-encrypted.json'
const SECRETS_PASSPHRASE_FILENAME = '.secrets-passphrase'

export class Secrets {

  private secrets?: KeyValPairs

  static async getPassphrase (): Promise<string> {
    const passphraseFile = path.resolve(os.homedir(), SECRETS_PASSPHRASE_FILENAME)
    if (fs.existsSync(passphraseFile)) {
      const stats = fs.statSync(passphraseFile)
      if ((stats.mode && 0x22) !== 0) {
        LOGGER.warn(`${passphraseFile} is group or other readable`)
      }
      LOGGER.info(`using passphrase from ${passphraseFile}`)
      return fs.readFileSync(passphraseFile, 'utf8').trim()
    }
    return prompt('passphrase: ', { method: 'hide' })
  }

  static processKeyValPairs (kvp: KeyValPairs, password: string, cryptoFunction: SymetricEncryptionFunction): KeyValPairs {
    const rv: KeyValPairs = {}
    for (const key in kvp) {
      rv[key] = cryptoFunction(kvp[key], password)
    }
    return rv
  }

  async init (): Promise<void> {
    if (this.secrets) {
      return Promise.resolve()
    }
    const passphrase = await Secrets.getPassphrase()
    const secretsEncrypted: KeyValPairs = JSON.parse(appRoot.resolve(SECRETS_FILENAME))
    this.secrets = Secrets.processKeyValPairs(secretsEncrypted, passphrase, decrypt)
  }

  get (key: string): string {
    if (!this.secrets) {
      throw new Error('Secrets not initialized')
    }
    return this.secrets[key]
  }

}

const secrets = new Secrets()
export default secrets

export async function encryptSecrets () {
  const unencryptedSec: KeyValPairs = appRoot.require(SECRETS_FILENAME)
  const passphrase = await Secrets.getPassphrase()
  const encryptedSecrets = Secrets.processKeyValPairs(unencryptedSec, passphrase, encrypt)
  const outPath = appRoot.resolve(SECRETS_ENCRYPTED_FILENAME)
  fs.writeFileSync(outPath, JSON.stringify(encryptedSecrets, null, 2) + '\n')

}

export async function decryptSecrets () {
  const unencryptedSec: KeyValPairs = appRoot.require(SECRETS_ENCRYPTED_FILENAME)
  const passphrase = await Secrets.getPassphrase()
  const unencryptedSecrets = Secrets.processKeyValPairs(unencryptedSec, passphrase, decrypt)
  const outPath = appRoot.resolve(SECRETS_FILENAME)
  fs.writeFileSync(outPath, JSON.stringify(unencryptedSecrets, null, 2) + '\n')

}
