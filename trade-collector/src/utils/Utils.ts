const { promisify } = require('util')

import { loggerFactory } from '../logging'
const logger = loggerFactory()

const setTimeoutPromise = promisify(setTimeout)

export type Retriable = () => Promise<boolean>

export default class Utils {

  static max (a?: number, b?: number): number {
    if (!a && !b) {
      throw new Error(`both arguments undefined`)
    }
    if (!a) {
      return b as number
    }
    if (!b) {
      return a
    }
    return Math.max(a,b)
  }

  static async sleep (millis: number): Promise<void> {
    await setTimeoutPromise(millis)
  }

  static async forever (loopInitiationMessage: string, loopFunc: () => void): Promise<never> {
    for (;;) {
      try {
        logger.debug(loopInitiationMessage)
        loopFunc()
      } catch (e) {
        logger.error(e)
      }
    }
  }

  static normalize (s: string): string {
    return s.normalize('NFC').toLowerCase().replace(/\s/g,'')
  }

  static mapify<T> (array: T[], selector: (t: T) => string) {
    const rv: { [key: string]: T } = {}
    for (const element of array) {
      rv[selector(element)] = element
    }
    return rv
  }

  static async retry (sleepIntervalMillis: number, retryable: Retriable): Promise<void> {
    let success = false
    while (!success) {
      success = await retryable()
      if (!success) {
        await Utils.sleep(sleepIntervalMillis)
      }
    }
  }

  static clone (object: any): any {
    return JSON.parse(JSON.stringify(object))
  }
}
