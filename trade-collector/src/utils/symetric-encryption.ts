import * as crypto from 'crypto'

export type SymetricEncryptionFunction = (text: string, masterkey: string) => string

/**
 * Encrypts text by given key
 * @param String text to encrypt
 * @param Buffer masterkey
 * @returns String encrypted text, base64 encoded
 */
export const encrypt: SymetricEncryptionFunction = (text: string, masterkey: string): string => {
  // random initialization vector
  let iv = crypto.randomBytes(12)

  // random salt
  let salt = crypto.randomBytes(64)

  // derive key: 32 byte key length - in assumption the masterkey is a cryptographic and NOT a password there is no need for
  // a large number of iterations. It may can replaced by HKDF
  let key = crypto.pbkdf2Sync(masterkey, salt, 2145, 32, 'sha512')

  // AES 256 GCM Mode
  let cipher = crypto.createCipheriv('aes-256-gcm', key, iv) as crypto.CipherCCM

  // encrypt the given text
  let encrypted = Buffer.concat([cipher.update(text, 'utf8'), cipher.final()])

  // extract the auth tag
  let tag = cipher.getAuthTag()

  // generate output
  return Buffer.concat([salt, iv, tag, encrypted]).toString('base64')

}

/**
 * Decrypts text by given key
 * @param String base64 encoded input data
 * @param Buffer masterkey
 * @returns String decrypted (original) text
 */
export const decrypt: SymetricEncryptionFunction = (data: string, masterkey: string): string => {
  // base64 decoding
  let bData = Buffer.from(data, 'base64')

  // convert data to buffers
  let salt = bData.slice(0, 64)
  let iv = bData.slice(64, 76)
  let tag = bData.slice(76, 92)
  let text = bData.slice(92)

  // derive key using; 32 byte key length
  let key = crypto.pbkdf2Sync(masterkey, salt, 2145, 32, 'sha512')

  // AES 256 GCM Mode
  let decipher = crypto.createDecipheriv('aes-256-gcm', key, iv) as crypto.DecipherCCM
  decipher.setAuthTag(tag)

  // encrypt the given text
  let decrypted = decipher.update(text, 'binary', 'utf8') + decipher.final('utf8')

  return decrypted
}
