import path from 'path'
import fs from 'fs'

export function mkdirs (folderPath: string) {
  const dirs = []
  let tmpPath = path.normalize(folderPath)
  let exists = fs.existsSync(tmpPath)
  while (!exists) {
    dirs.push(tmpPath)
    tmpPath = path.resolve(tmpPath, '..')
    exists = fs.existsSync(tmpPath)
  }

  while (true) {
    const dir = dirs.shift()
    if (!dir) {
      break
    }
    fs.mkdirSync(dir)
  }

}
