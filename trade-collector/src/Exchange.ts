import { Trade } from './model'

export type TradeHandler = (trade: Trade) => void

export interface Exchange {
  subscribeTrades (handler: TradeHandler, ...symbols: string[]): void
}
