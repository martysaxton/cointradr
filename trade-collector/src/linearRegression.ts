
interface WeightedPair {
  x: number,
  y: number,
  w: number
}

function weightedMeans (pairs: WeightedPair[]) {
  let sumX = 0
  let sumY = 0
  let sumW = 0
  for (const p of pairs) {
    sumX += p.x * p.w
    sumY += p.y * p.w
    sumW += p.w
  }
  const meanX = sumX / sumW
  const meanY = sumY / sumW
  return [ meanX, meanY ]
}

// https://stats.stackexchange.com/questions/221246/such-thing-as-a-weighted-correlation
export function weightedCorrelation (pairs: WeightedPair[]) {
  const [ meanX, meanY ] = weightedMeans(pairs)
  let sumW = 0
  let varianceXNumerator = 0
  let varianceYNumerator = 0
  let covarianceNumerator = 0
  for (const s of pairs) {
    const xdiff = s.x - meanX
    const ydiff = s.y - meanY
    varianceXNumerator += s.w * xdiff
    varianceYNumerator += s.w * ydiff
    covarianceNumerator += s.w * xdiff * ydiff
    sumW += s.w
  }
  const varianceX = varianceXNumerator / sumW
  const varianceY = varianceYNumerator / sumW
  const covariance = covarianceNumerator / sumW
  const r = covariance / Math.pow(varianceX * varianceY, 0.5)
  return r
}

// don't need y-intercept, only care about slope
// simple linear regression formula: http://www.statisticshowto.com/probability-and-statistics/regression-analysis/find-a-linear-regression-equation/
// weighted linear regression formula:  https://www.vttoth.com/CMS/physics-notes/254-weighted-linear-regression
export function weightedLinearRegression (pairs: WeightedPair[]) {
  let sumW = 0
  let sumX = 0
  let sumXSquared = 0
  let sumY = 0
  let sumXY = 0
  for (const s of pairs) {
    sumW += s.w
    sumX += s.w * s.x
    sumXSquared = s.w * s.x * s.x
    sumY = s.w * s.y
    sumXY = s.w * s.x * s.y
  }

  const slopeNumerator = (sumW * sumXY) - (sumX * sumY)
  const slopeDenominator = (sumW * sumXSquared) - (sumX * sumX)
  const slope = slopeNumerator / slopeDenominator
  return slope
}
