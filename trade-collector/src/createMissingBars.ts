import { loggerFactory } from './logging'
import { db } from './db'
import { ONE_HOUR, EIGHT_MINUTES, barFactoryManager } from './BarFactoryManager'

const logger = loggerFactory()

// tslint:disable-next-line:no-commented-code
// function roundToHour (date: Date) {
//   return new Date(Math.round(date.getTime() / ONE_HOUR) * ONE_HOUR)
// }

async function getStartTime () {
  const bars = await db.getBars()
  const oldestHourCursorPromise = bars.find({ dur: ONE_HOUR }).sort({ start: -1 }).limit(1)
  const oldestEightCursorPromise = bars.find({ dur: EIGHT_MINUTES }).sort({ start: -1 }).limit(1)
  const [oldestHourCursor, oldestEightCursor ] = await Promise.all([oldestHourCursorPromise, oldestEightCursorPromise])
  const oldestHour = await oldestHourCursor.next()
  const oldestEight = await oldestEightCursor.next()
  if (!oldestHour || !oldestEight) {
    return null
  }
  return new Date(Math.min(oldestHour.start.getTime() + ONE_HOUR, oldestEight.start.getTime() + EIGHT_MINUTES))
}

const MAX_ITERATIONS = 4

export async function createMissingBars () {
  logger.debug('creating missing bars')
  for (let i = 1; i <= MAX_ITERATIONS; ++i) {
    logger.debug('starting iteration %d', i)
    const trades = await db.getTrades()
    const startTime = await getStartTime()
    const query = startTime ? { t: { $gte: startTime } } : {}
    if (i === MAX_ITERATIONS) {
      logger.debug('last iteration, telling barFactoryManager to enqueue trades')
      barFactoryManager.enqueueTrades()
    }
    logger.debug('querying for exiting trades query: %j', query)
    const tradeCursor = trades.find(query).sort({ t: 1 })
    for (;;) {
      const trade = await tradeCursor.next()
      if (!trade) {
        break
      }
      logger.debug('trade from db: %j', trade)
      await barFactoryManager.handleMissingTrade(trade)
    }
  }
  await barFactoryManager.missingBarsComplete()
}
