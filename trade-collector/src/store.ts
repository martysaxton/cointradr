import { createStore, AnyAction, Reducer } from 'redux'
import ReducerRegistry from './utils/ReducerRegistry'
import { ExchangeSymbol } from './model'

interface CoinTradrState {
  startTime: Date
  lastTrade: { [exchange: string]: Date }
}

export const reducerRegistry = new ReducerRegistry<CoinTradrState>()

const startState = {
  startTime: new Date(),
  lastTrade: {}
}

interface TradeReceived {
  exchange: ExchangeSymbol
}

const tradeReceivedReducer = (state: CoinTradrState, tradeReceived: TradeReceived): CoinTradrState => {
  return {
    ...state,
    lastTrade: {
      ...state.lastTrade,
      [tradeReceived.exchange]: new Date()
    }
  }
}

export const createTradeReceivedAction = reducerRegistry.registerReducer<TradeReceived>('TRADE_RECEIVED', tradeReceivedReducer)

const rootReducer: Reducer<CoinTradrState, AnyAction> = (state: CoinTradrState | undefined, action: AnyAction) => {
  if (!state) {
    return startState
  }
  if (reducerRegistry.canHandleAction(action)) {
    return reducerRegistry.reducer(state, action)
  }
  return state
}

const store = createStore(rootReducer, startState)

export default store
