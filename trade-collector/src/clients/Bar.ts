export interface Bar {
  time: Date,
  open: number,
  high: number,
  low: number,
  close: number,
  volume: number
}
