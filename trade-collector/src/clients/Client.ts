import { Bar } from './Bar'

export enum BarInterval {
  oneMinute = 60,
  fiveMinutes = 300
  // fifteenMinutes = 900,
  // oneHour = 3600,
  // sixHours = 21600,
  // oneDay = 86400
}

export interface Client {
  init (): Promise<void>
  loadRecentBars (symbol: string, barInterval: BarInterval, count: number): Promise<Bar[]>
}
