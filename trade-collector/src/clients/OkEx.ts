import { AbstractWsClientStream } from './AbstractWsClientStream'
import { loggerFactory } from '../logging'
import pako from 'pako'

const logger = loggerFactory()

export interface OkExTrade {
  id: string
  symbol: string
  price: string
  size: string
  time: Date
  side: string,
  future: boolean
}

export type OkExTradeHandler = (trade: OkExTrade) => void

abstract class OkExBaseStream extends AbstractWsClientStream {

  private firstMessage = true

  constructor (
    url: string,
    readonly handler: OkExTradeHandler,
    readonly channelPrefix: string,
    readonly channelSuffix: string,
    readonly markets: string[]) {
    super(url)
  }

  protected handleBuffer (buffer: Buffer) {
    let text = pako.inflateRaw(buffer, { to: 'string' })
    this.handleString(text)
  }

  protected handleJson (message: any) {
    if (this.firstMessage) {
      this.firstMessage = false
    } else {
      for (const x of message) {
        const channel = x.channel
        const market = this.parseChannel(channel)
        if (market) {
          for (const trade of x.data) {
            const [id, price, size, time, side] = trade
            // logger.debug('symbol=%s id=%s  price=%s  size=%s  time=%s  side=%s', market.symbol, id, price, size, time, side)

            const future = market.symbol.startsWith('usd')

            this.handler({
              id,
              symbol: market.symbol,
              price,
              size,
              time,
              side,
              future
            })
          }

        }
      }
    }
  }

  private parseChannel (channel: string) {
    const prefixGood = channel.startsWith(this.channelPrefix)
    const end = channel.lastIndexOf(this.channelSuffix)
    if (!prefixGood || end === -1) {
      logger.warn('cannot parse channel: %s', channel)
      return undefined
    }
    return {
      symbol: channel.slice(this.channelPrefix.length, end)
    }
  }

}

export class OkExSpotStream extends OkExBaseStream {

  constructor (readonly handler: OkExTradeHandler, ...markets: string[]) {
    super('wss://real.okex.com:10441/websocket', handler, 'ok_sub_spot_', '_deals', markets)
  }

  protected onConnect () {
    super.onConnect()
    if (this.webSocket) {
      for (const market of this.markets) {
        this.webSocket.send(`{'event':'addChannel','channel': 'ok_sub_spot_${market}_deals'}`)
      }
    }
  }
}

export class OkExFuturesStream extends OkExBaseStream {

  constructor (readonly handler: OkExTradeHandler, ...markets: string[]) {
    super('wss://real.okex.com:10440/websocket/okexapi', handler, 'ok_sub_future', '_trade_quarter', markets)
  }

  // tslint:disable-next-line:no-identical-functions
  protected onConnect () {
    super.onConnect()
    if (this.webSocket) {
      for (const market of this.markets) {
        this.webSocket.send(`{'event':'addChannel','channel': 'ok_sub_future${market}_trade_quarter'}`)
      }
    }
  }

}
