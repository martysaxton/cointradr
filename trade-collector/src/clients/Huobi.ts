import { AbstractWsClientStream } from './AbstractWsClientStream'
import { loggerFactory } from '../logging'

const logger = loggerFactory()

const HUOBI_URL = 'wss://api.huobi.pro/ws'

export interface HuobiTrade {
  id: number
  symbol: string
  price: number
  amount: number
  direction: string
}

export type HuobiTradeHandler = (trade: HuobiTrade) => void

export class HuobiClient extends AbstractWsClientStream {

  constructor (
    private readonly handler: HuobiTradeHandler,
    readonly markets: string[]) {
    super(HUOBI_URL, true)
  }

  protected handleJson (messages: any): void {
    logger.debug('message %j', messages)

    if (messages.ping && this.webSocket) {
      this.webSocket.send(JSON.stringify({ pong: messages.ping }))
    }

    const ch = messages.ch
    if (ch && ch.endsWith('.trade.detail')) {
      const segments = ch.split('.')
      const hsymbol = segments[1]
      this.handleTrades(hsymbol, messages.tick)
    }
  }

  private handleTrades (hsymbol: string, tick: any): any {
    const id: number = tick.id
    for (let i = 0; i < tick.data.length; ++i) {
      const trade = tick.data[i]
      const hTrade: HuobiTrade = {
        id: id * -100 - i,  // each trade needs its own id
        symbol: hsymbol,
        price: trade.price,
        amount: trade.amount,
        direction: trade.direction
      }
      this.handler(hTrade)
    }
  }

  protected onConnect () {
    super.onConnect()
    if (this.webSocket) {
      console.log('readystate', this.webSocket.readyState)
      for (const market of this.markets) {
        this.webSocket.send(JSON.stringify({
          'sub': `market.${market}.trade.detail`,
          'id': `${market}`
        }))
      }
    }
  }

}
