import PriorityQueue from './PriorityQueue'
import { loggerFactory } from '../logging'

const logger = loggerFactory()

interface Delegate {
  func: any
  args: any[]
  resolve?: any
}

export default class TimeThrottler {

  private delegates = new PriorityQueue<Delegate>()
  private nextReady = 0
  private timeoutSet = false
  private onEmptyCallback?: () => void

  constructor (readonly interval: number, readonly logName: string, onEmpty?: () => void) {
    this.onEmptyCallback = onEmpty
  }

  async enqueue<T> (func: (...args: any[]) => Promise<T>, ...args: any[]): Promise<T> {
    return this.enqueueWithPriority(0, func, args)
  }

  async enqueueWithPriority<T> (priority: number, func: (...args: any[]) => Promise<T>, ...args: any[]): Promise<T> {
    const now = Date.now()
    if (now >= this.nextReady && this.delegates.isEmpty()) {
      this.nextReady = now + this.interval
      if (this.onEmptyCallback) {
        this.onEmptyCallback()
      }
      return func(...args)
    } else {
      this.ensureTimeoutIsSet()
      return new Promise<T>((resolve) => {
        const delegate: Delegate = {
          func: func,
          args: args,
          resolve: resolve
        }
        const position = this.delegates.push(delegate, priority)
        const logMessage = `push ${this.logName} ${this.delegates.getLength()} in queue at position ${position}`
        logger.debug(logMessage)
      })
    }
  }

  private ensureTimeoutIsSet () {
    if (!this.timeoutSet) {
      setTimeout(this.onTimeout, this.nextReady - Date.now())
      this.timeoutSet = true
    }
  }

  private onTimeout = async () => {
    logger.debug(`pop  ${this.logName} ${this.delegates.getLength()} in queue`)
    this.timeoutSet = false
    const delegate = this.delegates.shift()
    if (this.delegates.getLength() > 0) {
      this.nextReady = Date.now() + this.interval
      this.ensureTimeoutIsSet()
    } else {
            // queue is now empty
      if (this.onEmptyCallback) {
        this.onEmptyCallback()
      }
    }
    if (delegate) {
      delegate.func(...(delegate.args)).then((rv: any) => {
        delegate.resolve(rv)
      }).catch((e: any) => {
        logger.error(e)
      })
    }
  }

}
