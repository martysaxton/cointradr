
import fetch, { Request, RequestInit, Headers } from 'node-fetch'

import { loggerFactory } from '../logging'

const logger = loggerFactory()

export class HttpError extends Error {
  constructor (readonly status: number, message: string, readonly body: string) {
    super(message)
    Object.setPrototypeOf(this, HttpError.prototype)
  }
}
/**
 * Adding some logging and throw an error if it's status indicates an error
 * @param url
 * @param init
 */
export default async function myfetch (url: string | Request, init?: RequestInit): Promise<any> {
  logger.debug('fetch requesting %s', url)
  if (!init) {
    init = {}
    const headers = new Headers()
    // headers.set('User-Agent', 'Mozilla/5.0 ' + Date.now())
    init.headers = headers
  }

  const resp = await fetch(url, init)
  logger.debug('fetch %s response %d %s', url, resp.status, resp.statusText)
  if (resp.status === 429) {
    logger.error('too many requests %s', url)
    process.exit(1)
  }
  const body = await resp.text()
  if (resp.status >= 400) {
    throw new HttpError(resp.status, resp.statusText, body)
  }
  try {
    return JSON.parse(body)
  } catch (e) {
    throw new Error('could not parse json body: ' + body)
  }
}
