import WebSocket from 'ws'
import { AbstractClientStream } from './AbstractClientStream'
import Utils from '../utils/Utils'
import { loggerFactory } from '../logging'
import pako from 'pako'
const logger = loggerFactory()

const HEARTBEAT_INTERVAL = 30 * 1000

export abstract class AbstractWsClientStream extends AbstractClientStream {

  protected webSocket?: WebSocket

  constructor (readonly url: string, readonly deflate?: boolean) {
    super()
    setInterval(this.sendPeriodicPing, HEARTBEAT_INTERVAL)
  }

  connect () {
    // tslint:disable-next-line:no-floating-promises
    Utils.retry(10000, this.connectAttempt)
  }

  disconnect () {
    if (this.webSocket) {
      this.webSocket.terminate()
      this.webSocket = undefined
    }
  }

  protected abstract handleJson (messages: any): void

  protected ping () {
    if (this.webSocket) {
      logger.debug('%s periodic sending ping', this.className)
      this.webSocket.ping('ping', true, this.pingCallback)
    }
  }

  protected onConnect () {
    super.onConnect()
  }

  private onMessage = (event: { data: WebSocket.Data; type: string; target: WebSocket }): void => {
    // logger.debug('onMessage: %s', JSON.stringify(event.data))

    this.onEvent()
    if (event.type !== 'message') {
      logger.warn(`unknown event type:  ${event.type}`)
    } else {
      if (typeof event.data === 'string') {
        this.handleString(event.data)
      } else if (event.data instanceof Buffer) {
        if (this.deflate) {
          let text = pako.inflate(event.data, { to: 'string' })
          this.handleString(text)
        } else {
          this.handleBuffer(event.data)
        }
      } else if (Array.isArray(event.data)) {
        event.data.forEach(element => this.handleString(element.toString()))
      } else {
        logger.warn('websocket gave data as an ArrayBuffer ')
      }
    }
  }

  protected handleBuffer (buffer: Buffer) {
    logger.warn('got a Buffer and don\'t know how to handle it')
  }

  protected handleString (s: string): void {
    // logger.debug('handleString: %s', s)
    try {
      this.handleJson(JSON.parse(s))
    } catch (e) {
      logger.error('handleString error parsing JSON: %s', s)
      this.resetConnection()
    }
  }

  private sendPeriodicPing = () => {
    this.ping()
  }

  private connectAttempt = async (): Promise<boolean> => {
    return new Promise<boolean>((resolve, reject) => {
      const newWebSocket = new WebSocket(this.url)
      newWebSocket.onerror = (event: { error: any, message: string, type: string, target: WebSocket }): void => {
        logger.debug('websocket error event while connecting %j', event)
        resolve(false)
      }
      newWebSocket.onclose = (event: { wasClean: boolean; code: number; reason: string; target: WebSocket }): void => {
        logger.debug(`websocket close event while connecting %j', event}`)
        resolve(false)
      }
      newWebSocket.onopen = (event: { target: WebSocket }): void => {
        logger.debug('websocket open event')
        this.webSocket = event.target
        this.webSocket.onclose = this.onClose
        this.webSocket.onerror = this.onError
        this.webSocket.onmessage = this.onMessage
        this.webSocket.on('ping', this.onPing)
        this.webSocket.on('pong', this.onPong)
        this.onConnect()
        resolve(true)
      }
    })
  }

  private onClose = (event: { wasClean: boolean; code: number; reason: string; target: WebSocket }): void => {
    logger.info('websocket close event %j', event)
    this.connect()
  }

  private onError = (event: { error: any, message: string, type: string, target: WebSocket }): void => {
    logger.error(`websocket error event ${JSON.stringify(event)}`)
  }

  private onPong = () => {
    logger.debug('%s recieved pong', this.className)
    this.onEvent()
  }
  private onPing = (ws: WebSocket, data: Buffer) => {
    logger.info('%s recieved ping', this.className)
    if (this.webSocket) {
      this.webSocket.pong(data)
    }
    this.onEvent()
  }

  private pingCallback = (error: Error) => {
    if (error) {
      logger.error('pingCallback error: %j', error)
      this.resetConnection()
    } else {
      logger.debug('ping sent')
    }
  }

}
