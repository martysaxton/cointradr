import SortedArray from './SortedArray'

interface Item<T> {
  sequence: number
  priority: number
  value: T
}

function comparator<T> (a: Item<T>, b: Item<T>): number {
  if (a.priority !== b.priority) {
    return b.priority - a.priority
  }
    // same priority
  return a.sequence - b.sequence
}

export default class PriorityQueue<T> {

  private sequence = 0
  private queue = new SortedArray<Item<T>>(comparator)

  push (t: T, priority: number): number {
    return this.queue.insert({ sequence: ++this.sequence, priority: priority, value: t })
  }

  shift (): T | undefined {
    const nextItem = this.queue.shift()
    if (nextItem === undefined) {
      return undefined
    }
    return nextItem.value
  }

  isEmpty (): boolean {
    return this.queue.values.length === 0
  }

  getLength (): number {
    return this.queue.values().length
  }

    // log() {
    //     for (const item of this.queue.values()) {
    //         console.log(item)
    //     }
    //     console.log("")
    // }

}
