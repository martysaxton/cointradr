import {
  BinanceStreamKlineMessage,
  KlineHandler,
  KlineDataStreamParam,
  BinanceTradeHandler,
  BinanceTradeDataStreamParam,
  BinanceCombinedStream,
  BinanceStreamTrade,
  UserAccountUpdate,
  UserAccountUpdateHandler,
  StreamParam,
  AllTickers,
  BinanceTickersHandler,
  ALL_TICKERS,
  BinanceTicker,
  BinanceDepthParam,
  BinanceDepthHandler,
  BinanceStreamDepth
} from './BinanceTypes'

import BinanceClient from './BinanceClient'
import { loggerFactory } from '../../logging'
import { AbstractWsClientStream } from '../AbstractWsClientStream'

const logger = loggerFactory()

const BASE_URL = 'wss://stream.binance.com:9443'

export abstract class BinanceDataStream extends AbstractWsClientStream {

  constructor (readonly url: string) {
    super(url)
  }

}
export class BinanceDepthStream extends BinanceDataStream {

  constructor (readonly symbol: string, readonly handler: BinanceDepthHandler) {
    super(BASE_URL + `/ws/${symbol.toLowerCase()}@depth`)
  }

  protected handleJson (depthUpdate: any) {
    this.handler(depthUpdate)
  }

}

export abstract class BinanceCombinedStreamBase extends BinanceDataStream {

  constructor (query: string) {
    super(BASE_URL + '/stream?streams=' + query)
  }

  protected abstract handle (data: BinanceCombinedStream): void

  protected handleJson (messages: any) {
    this.handle(messages)
  }
}

export class CombinedStream extends BinanceCombinedStreamBase {

  private tradeHandler?: BinanceTradeHandler
  private klineHandler?: KlineHandler
  private tickersHandler?: BinanceTickersHandler
  private depthHandler?: BinanceDepthHandler

  constructor (readonly params: StreamParam[]) {
    super(params.map((param) => CombinedStream.paramToString(param)).join('&'))
  }

  private static paramToString (param: StreamParam): string {
    if (this.isTrade(param)) {
      return `${param.symbol}@${param.aggregated ? 'aggTrade' : 'trade'}`
    }
    if (this.isKline(param)) {
      return `${param.symbol}@kline_${param.interval}`
    }
    if (this.isAllTickers(param)) {
      return ALL_TICKERS
    }
    if (this.isDepth(param)) {
      return `${param.symbol}@depth`
    }
    throw new Error('unknown param')
  }

  private static isAllTickers (param: StreamParam): param is AllTickers {
    return (param === ALL_TICKERS)
  }

  private static isTrade (param: StreamParam): param is BinanceTradeDataStreamParam {
    return (param as any).aggregated !== undefined
  }

  private static isKline (param: StreamParam): param is KlineDataStreamParam {
    return (param as any).interval !== undefined
  }

  private static isDepth (param: StreamParam): param is BinanceDepthParam {
    return (param as any).type === 'depth'
  }

  setTradeHandler (tradeHandler: BinanceTradeHandler) {
    this.tradeHandler = tradeHandler
  }

  setKlineHandler (klineHandler: KlineHandler) {
    this.klineHandler = klineHandler
  }

  setTickersHandler (tickersHandler: BinanceTickersHandler) {
    this.tickersHandler = tickersHandler
  }

  setDepthHandler (depthHandler: BinanceDepthHandler) {
    this.depthHandler = depthHandler
  }

  protected handle (data: BinanceCombinedStream): void {
    if (this.klineHandler && data.stream.includes('kline')) {
      this.klineHandler(data.data as BinanceStreamKlineMessage)
    } else if (this.tradeHandler && data.stream.toLowerCase().includes('trade')) {
      this.tradeHandler(data.data as BinanceStreamTrade)
    } else if (this.tickersHandler && data.stream === ALL_TICKERS) {
      this.tickersHandler(data.data as BinanceTicker[])
    } else if (this.depthHandler && data.stream.includes('depth')) {
      this.depthHandler(data.data as BinanceStreamDepth)
    }
  }

}

export class KlineDataStream extends BinanceCombinedStreamBase {
  private handler: KlineHandler
  constructor (handler: KlineHandler, params: KlineDataStreamParam[]) {
    let query = params.map((param) => `${param.symbol}@kline_${param.interval}`).join('&')
    super(query)
    this.handler = handler
  }
  protected handle (data: BinanceCombinedStream) {
    this.handler(data.data as BinanceStreamKlineMessage)
  }

}

export class BinanceTradeDataStream extends BinanceCombinedStreamBase {
  private handler: BinanceTradeHandler
  constructor (handler: BinanceTradeHandler, params: BinanceTradeDataStreamParam[]) {
    let query = params.map((param) => `${param.symbol}@${param.aggregated ? 'aggTrade' : 'trade'}`).join('/')
    super(query)
    this.handler = handler
  }
  protected handle (data: BinanceCombinedStream): void {
    this.handler(data.data as BinanceStreamTrade)
  }
}

const TWENTY_MINUTES = 1000 * 60 * 20
const KEEPALIVE_INTERVAL = TWENTY_MINUTES

export class UserAccountUpdateStream extends BinanceDataStream {

  constructor (private readonly client: BinanceClient, private readonly handler: UserAccountUpdateHandler, private readonly listenKey: string) {
    super(BASE_URL + '/ws/' + listenKey)
    setTimeout(this.heartbeat, KEEPALIVE_INTERVAL)
  }

  protected handleJson (s: any): void {
    const message: UserAccountUpdate = s
    this.handler(message)
  }

  private heartbeat = () => {
    logger.debug('sending keepalive for user data')
    this.client.keepaliveUserDataStream(this.listenKey)
    setTimeout(this.heartbeat, KEEPALIVE_INTERVAL)
  }
}

// tslint:disable-next-line:no-commented-code
// const bc = new BinanceClient()
// const BARS_TO_GET = 50
// const startMillis = new Date('2018-01-01T00:00:00').valueOf()
// const endMillis = startMillis + 1000 * 60 * BARS_TO_GET
// bc.getBarsInterval('BTCUSDT', '1m', startMillis, endMillis).then((latestBars) => {
//   for (const bar of latestBars) {
//     console.log(new Date(bar.time))
//   }
// })
// bc.getBars('BTCUSDT', '1m').then((latestBars) => {
//     for (const bar of latestBars) {
//         console.log(new Date(bar.time))
//     }
// })
// const order: BinanceNewOrderRequest = {
//     symbol: 'BTCUSDT',
//     side: 'BUY',
//     type: 'MARKET',
//     // type: 'STOP_LOSS',
//     quantity: 0.1,
//     // stopPrice: 11000
// }
// bc.getExchangeInfo().then(() => {
//     // bc.g
//     bc.submitOrderTest(order).then((result) => {
//         console.log('------------------------------------')
//         console.log(result)
//         console.log('------------------------------------')
//     })
//     bc.getBars('BTCUSDT', '5m', 6).then(bars => {
//         console.log(bars)
//         console.log('------------------------------------')
//     })
// })
// const channelBreakout = new ChannelBreakout()
// const incoming = (data: string) => {
//     // console.log('message', data);
//     const message: BinanceCombinedStream = JSON.parse(data)
//     if (message.stream.includes('@kline_')) {
//         handleStreamKline(message.data)
//     }
//     else if (message.stream.includes('@trade')) {
//         handleStreamTrade(message.data)
//     }
// }
// function handleStreamTrade(trade: BinanceStreamTrade) {
//     channelBreakout.onTrade(Number(trade.p))
// }
// ws.on('message', incoming)
    // const now = Date.now()
    // const accountInfoPromise = this.client.getAccountInformation()
    // const getBarsPromise = this.client.getBars('BTCUSDT', '5m', 6)
    // const accountInfo = await accountInfoPromise
    // for (const bal of accountInfo.balances) {
    //   const b = Number(bal.free)
    //   switch (bal.asset) {
    //     case 'BTC': this.btcBalance = b; break
    //     case 'USDT': this.usdtBalance = b; break;
    //   }
    // }
    // const bars = await getBarsPromise
    // for (const bar of bars) {
    //   const barEnd = bar.time.valueOf() + 5 * 60 * 1000
    //   if (barEnd < now) {
    //     this.bars.push(bar)
    //   }
    // }
    // const ws = new WebSocket('wss://stream.binance.com:9443/stream?streams=btcusdt@kline_5m/btcusdt@trade')
    // ws.on('message', incoming)
