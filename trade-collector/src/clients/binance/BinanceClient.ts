
import myfetch from '../fetch-wrapper'
import TimeThrottler from '../TimeThrottler'
import { RequestInit } from 'node-fetch'
import { Bar } from '../Bar'
import secrets from '../../utils/Secrets'
import * as crypto from 'crypto'
import {
  BinanceCurrency,
  BinanceExchangeInfo,
  BinanceDepth,
  BinanceTransaction,
  DepositResponse,
  BinanceAccountInformation,
  BinanceNewOrderRequest,
  BinanceNewOrderResult,
  Interval,
  BinanceTradeDataStreamParam,
  KlineDataStreamParam,
  BinanceTradeHandler,
  KlineHandler,
  UserAccountUpdateHandler,
  StartUserDataStreamResponse,
  DepthSizes,
  StreamParam
} from './BinanceTypes'
import { BinanceTradeDataStream, KlineDataStream, UserAccountUpdateStream, CombinedStream } from './BinanceStreams'
import { loggerFactory } from '../../logging'

const logger = loggerFactory()

const BASE_ADDRESS = 'https://api.binance.com/'
const REST_API = BASE_ADDRESS + 'api/v1/'
const REST_API_V3 = BASE_ADDRESS + 'api/v3/'
const WALLET_API = BASE_ADDRESS + 'wapi/v3/'

async function noop (): Promise<void> {
  //
}

export default class BinanceClient {

  private throttler = new TimeThrottler(100, 'binance')
  private clockSkew = 0
  private binanceApiKey: string
  private binanceSecret: string

  constructor () {
    this.binanceApiKey = secrets.get('binanceApiKey')
    this.binanceSecret = secrets.get('binanceSecret')
  }

  private static objectToQuery (params: any): string {
    let rv = []
    for (let p in params) {
      if (params[p] !== undefined) {
        rv.push(encodeURIComponent(p) + '=' + encodeURIComponent(params[p]))
      }
    }
    return rv.join('&')
  }

  async getCurrencies (): Promise<BinanceCurrency[]> {
    return await myfetch('https://www.binance.com/assetWithdraw/getAllAsset.html') as BinanceCurrency[]
  }

  async getExchangeInfo (): Promise<BinanceExchangeInfo> {
    const rv = await this.executePublic<BinanceExchangeInfo>(REST_API + 'exchangeInfo')
    const hostTime = Date.now()
    this.clockSkew = rv.serverTime - hostTime
    return rv
  }

  setClockSkew (clockSkew: number): void {
    this.clockSkew = clockSkew
  }

  getDepth (binanceMarketSymbol: string, limit?: DepthSizes): Promise<BinanceDepth> {
    logger.debug(`requesting depth for ${binanceMarketSymbol}`)
    let s = `depth?symbol=${binanceMarketSymbol}`
    if (limit) {
      s += `&limit=${limit}`
    }
    return this.executePublic<BinanceDepth>(REST_API + s)
  }

  async getDeposits (): Promise<BinanceTransaction[]> {
    const resp = await this.execPrivate<DepositResponse>(WALLET_API + 'depositHistory.html')
    return resp.depositList
  }

  async getAccountInformation (): Promise<BinanceAccountInformation> {
    return this.execPrivate<BinanceAccountInformation>(REST_API_V3 + 'account')
  }

  async submitOrder (order: BinanceNewOrderRequest): Promise<BinanceNewOrderResult> {
    return this.execPrivate<BinanceNewOrderResult>(REST_API_V3 + 'order', order, { method: 'POST' })
  }

  async submitOrderTest (order: BinanceNewOrderRequest): Promise<BinanceNewOrderResult> {
    const o = { ...order, newOrderRespType: 'RESULT' }
    return this.execPrivate<BinanceNewOrderResult>(REST_API_V3 + 'order/test', o, { method: 'POST' })
  }

  async getBarsInterval (symbol: string, interval: Interval, startMillis: number, endMillis: number): Promise<Bar[]> {
    return this.getBarsCommon(`symbol=${symbol}&interval=${interval}&startTime=${startMillis}&endTime=${endMillis}`)
  }

  async getBars (symbol: string, interval: Interval, limit ?: number): Promise < Bar[] > {
    let query = `symbol=${symbol}&interval=${interval}`
    if (limit) {
      query += `&limit=${limit}`
    }
    return this.getBarsCommon(query)
  }

  startCombinedDataStream (...params: StreamParam[]) {
    return new CombinedStream(params)
  }

  async startTradeDataStream (handler: BinanceTradeHandler, ...params: BinanceTradeDataStreamParam[]) {
    // tslint:disable-next-line:no-unused-expression
    new BinanceTradeDataStream(handler, params)
  }

  async startKlineDataStream (handler: KlineHandler, ...params: KlineDataStreamParam[]) {
    // tslint:disable-next-line:no-unused-expression
    new KlineDataStream(handler, params)
  }

  async startUserDataStream (handler: UserAccountUpdateHandler) {
    const resp = await this.execUserStreamSecurity<StartUserDataStreamResponse>(REST_API + 'userDataStream', {}, { method: 'POST' })
    return new UserAccountUpdateStream(this, handler, resp.listenKey)
  }

  keepaliveUserDataStream (listenKey: string) {
    // tslint:disable-next-line:no-floating-promises
    this.execUserStreamSecurity<StartUserDataStreamResponse>(REST_API + 'userDataStream', {}, { method: 'PUT' }).then(() => {
      logger.debug('keepaliveUserDataStream done')
    })
  }

  private async getBarsCommon (query: string): Promise<Bar[]> {
    const result = await this.executePublic<any>(REST_API + 'klines?' + query)
    return result.map((binanceKline: any[]) => {
      const [openTimeMillis, openStr, highStr, lowStr, closeStr, volumeStr] = binanceKline
      return {
        time: new Date(openTimeMillis),
        open: Number(openStr),
        high: Number(highStr),
        low: Number(lowStr),
        close: Number(closeStr),
        volume: Number(volumeStr)
      }
    })

  }

  private async executePublic<T> (endpoint: string): Promise<T> {
    await this.throttler.enqueue(noop)
    const json = await myfetch(endpoint)
    if (json.code !== undefined) {
      throw new Error(`Binance error code=${json.code}, message=${json.msg}`)
    }
    return json
  }

  private execUserStreamSecurity<T> (endpoint: string, params?: any, fetchOpts?: RequestInit): Promise<T> {
    let query = BinanceClient.objectToQuery(params)
    return this.execCommon(endpoint, query, fetchOpts)
  }

  private execPrivate<T> (endpoint: string, params?: any, fetchOpts?: RequestInit): Promise<T> {
    const hostTime = Date.now()
    const binanceTime = hostTime + this.clockSkew - 2000
    let p = { timestamp: binanceTime }
    if (params) {
      p = { ...p, ...params }
    }
    let query = BinanceClient.objectToQuery(p)
    const hash = crypto.createHmac('sha256', this.binanceSecret)
    hash.update(query)
    const sig = hash.digest('hex')
    query += '&signature=' + sig
    return this.execCommon(endpoint, query, fetchOpts)
  }

  private async execCommon<T> (endpoint: string, query: string, fetchOpts?: RequestInit): Promise<T> {
    await this.throttler.enqueue(noop)
    let fopts: RequestInit = {
      headers: {
        'X-MBX-APIKEY': this.binanceApiKey
      }
    }
    if (fetchOpts) {
      fopts = { ...fopts, ...fetchOpts }
    }

    const json = await myfetch(endpoint + '?' + query, fopts)

    // TODO why use wapi?  diverges from api
    if (json.success === false) {
      throw new Error(`Binance private api error message=${json.msg}`)
    }
    return json
  }

}

// tslint:disable-next-line:no-commented-code
// function roundToStep (value: number, step: number) {
//   return Math.floor(value / step) * step
// }
// async function go () {
//   try {
//     await secrets.init()
//     const binanceClient = new BinanceClient()
//     const exchangeInfo = await binanceClient.getExchangeInfo()
//     const markets = Utils.mapify(exchangeInfo.symbols, (market) => market.symbol)
//     const btcMarket = markets.BTCUSDT
//     if (!btcMarket) {
//       throw new Error('could not find BCTUSDT')
//     }
//     const accountInfo = await binanceClient.getAccountInformation()
//     const balances = Utils.mapify(accountInfo.balances, (balance) => balance.asset)
//     console.log(balances)
//     let quantity = Number(balances.BTC.free)
//     const lotSizeFilter = btcMarket.filters.find((filter) => filter.filterType === 'LOT_SIZE')
//     if (lotSizeFilter) {
//       quantity = roundToStep(quantity, Number(lotSizeFilter.stepSize))
//     }
//     const order: BinanceNewOrderRequest = {
//       symbol: 'BTCUSDT',
//       price: 8000,
//       quantity: quantity,
//       side: 'SELL',
//       type: 'LIMIT',
//       timeInForce: 'GTC'
//     }
//     const result = await binanceClient.submitOrder(order)
//     console.log(result)
//   } catch (error) {
//     console.error(error)
//   }
//   console.log('bye')
// }
// const klineHandler: KlineHandler = (klineMessage: BinanceStreamKlineMessage): void => {
//   console.log(klineMessage)
// }
// const tradeHandler: BinanceTradeHandler = (trade: BinanceStreamTrade): void => {
//   console.log(trade)
// }
// const userDataHandler: UserAccountUpdateHandler = (userData: UserAccountUpdate): void => {
//   console.log(userData)
// }
// interface Book {
//   highestBid: number
//   lowestAsk: number
// }
// type Books = { [marketSymbol: string]: Book }
// const books: Books = {}
// const tickersHandler: BinanceTickersHandler = (tickers: BinanceTicker[]): void => {
//   // console.log(tickers)
//   for (const ticker of tickers) {
//     // console.log(ticker.s)
//     if (markets) {
//       const market = markets[ticker.s] as BinanceMarket
//       // console.log(market.baseAsset + ' ' + market.quoteAsset)
//     }
//     books[ticker.s] = {
//       highestBid: Number(ticker.b),
//       lowestAsk: Number(ticker.a)
//     }
//   }
//   for (const triangle of triangles) {
//     const r = triangle.getReturn()
//     if (r > 1.0019) {
//       console.log(` ${triangle.toString()} ${(triangle.getReturn() - 1) * 100}`)
//     }
//   }
//   console.log('done processing tickers')
// }
// function normalizedTriangleName (triangle: Triangle) {
//   const symbols = new Set<string>()
//   for (const step of triangle.steps) {
//     symbols.add(step.market.baseAsset)
//     symbols.add(step.market.quoteAsset)
//   }
//   assert.equal(symbols.size, 3)
//   const normalized = Array.from(symbols).sort().join('.')
//   return normalized
// }
// function sanityCheckTriangles (triangles: Triangle[]) {
//   const nameToCount = new Map<string, number>()
//   for (const triangle of triangles) {
//     const normalized = normalizedTriangleName(triangle)
//     const currentCount = nameToCount.get(normalized)
//     const newCount = currentCount === undefined ? 1 : currentCount + 1
//     nameToCount.set(normalized, newCount)
//   }
//   nameToCount.forEach((value, key) => console.log(key, value))
// }
// interface Money {
//   symbol: string
//   amount: number
// }
// async function goStream () {
//   await secrets.init()
//   const binanceClient = new BinanceClient()
//   const exchangeInfo = await binanceClient.getExchangeInfo()
//   marketMap = buildMarketMap(exchangeInfo.symbols)
//   Object.assign(markets, Utils.mapify(exchangeInfo.symbols, (market) => market.symbol))
//   triangles = buildAllTriangles(exchangeInfo.symbols)
//   sanityCheckTriangles(triangles)
//   const stream = binanceClient.startCombinedDataStream(ALL_TICKERS)
//   stream.setTradeHandler(tradeHandler)
//   stream.setTickersHandler(tickersHandler)
// }
