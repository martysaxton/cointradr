
export interface BinanceRateLimit {
  rateLimitType: string
  interval: string
  limit: number
}

export interface BinanceFilter {
  filterType: string
  maxQty: string
  minQty: string
  stepSize: string
}

export interface BinanceFilter {
  filterType: string
  maxPrice: string
  minPrice: string
  tickSize: string
}

export interface BinanceCurrency {
  addressUrl: string
  assetCode: string
  assetLabel: string
  assetLabelEn: string
  assetName: string
  blockUrl: string
  chargeDescCn: string
  chargeDescEn: string
  chineseName: string
  cnLink: string
  commissionRate: number
  confirmTimes: string
  createTime: string
  depositTipCn: string
  depositTipEn: string
  depositTipStatus: boolean
  dynamicFeeStatus: boolean
  enLink: string
  enableCharge: boolean
  enableWithdraw: boolean
  feeDigit: number
  feeRate: number
  feeReferenceAsset: string
  forceStatus: boolean
  freeAuditWithdrawAmt: number
  freeUserChargeAmount: number
  gas: number
  id: string
  isLegalMoney: boolean
  legalMoney: boolean
  logoUrl: string
  minProductWithdraw: string
  parentCode: string
  reconciliationAmount: number
  regEx: string
  regExTag: string
  resetAddressStatus: boolean
  sameAddress: boolean
  seqNum: string
  supportMarket: string
  test: number
  transactionFee: number
  unit: string
  url: string
  withdrawIntegerMultiple: string
}

export interface BinanceMarket {

  symbol: string
  status: string
  baseAsset: string
  baseAssetPrecision: number
  quoteAsset: string
  quotePrecision: number
  orderTypes: string[]
  icebergAllowed: boolean
  filters: BinanceFilter[]
}

export interface BinanceExchangeInfo {
  exchangeFilters: any[]
  rateLimits: BinanceRateLimit[]
  serverTime: number
  symbols: BinanceMarket[]
  timezone: string
}

export interface BinanceTransaction {
  insertTime: number
  amount: number
  asset: string
  address: string
  txId: string
  status: number
}

export interface DepositResponse {
  depositList: BinanceTransaction[]
}

export type BinanceDepthEntry = [string, string]

export interface BinanceDepth {
  lastUpdateId: number
  asks: BinanceDepthEntry[]
  bids: BinanceDepthEntry[]
}

export interface BinanceBalance {
  asset: string
  free: string
  locked: string
}

export interface BinanceAccountInformation {
  makerCommission: number,
  takerCommission: number,
  buyerCommission: number,
  sellerCommission: number,
  canTrade: boolean,
  canWithdraw: boolean
  canDeposit: boolean
  updateTime: number
  balances: BinanceBalance[]
}

export type Side = 'BUY' | 'SELL'
export type OrderType = 'LIMIT' | 'MARKET' | 'STOP_LOSS' | 'STOP_LOSS_LIMIT' | 'TAKE_PROFIT' | 'TAKE_PROFIT_LIMIT' | 'LIMIT_MAKER'
export type TimeInForce = 'GTC' | 'IOC' | 'FOK'
export type Interval = '1m' | '3m' | '5m' | '15m' | '30m' | '1h' | '2h' | '4h' | '6h' | '8h' | '12h' | '1d'
export type NewOrderResponseType = 'JSON' | 'ACK' | 'RESULT' | 'FULL'

export interface BinanceNewOrderRequest {
  symbol: string
  side: Side
  type: OrderType
  timeInForce: TimeInForce
  quantity: number
  price?: number
  newClientOrderId?: string
  stopPrice?: number
  icebergQty?: number
  newOrderRespType?: NewOrderResponseType
  recvWindow?: number
  // timestamp: number
}

export interface BinanceNewOrderResult {
  symbol: string
  orderId: number
  clientOrderId: string
  transactTime: number
  price: number
  origQty: number
  executedQty: number
  status: string
  timeInForce: TimeInForce
  type: OrderType
  side: Side
}

export interface KlineDataStreamParam {
  symbol: string
  interval: Interval
}

export type KlineHandler = (klineMessage: BinanceStreamKlineMessage) => void

export const ALL_TICKERS = '!ticker@arr'
export type AllTickers = '!ticker@arr'
export interface BinanceDepthParam {
  type: 'depth'
  symbol: string
}

export type StreamParam = KlineDataStreamParam | BinanceTradeDataStreamParam | AllTickers | BinanceDepthParam

export interface BinanceTradeDataStreamParam {
  symbol: string
  aggregated: boolean,
}

export type BinanceTradeHandler = (trade: BinanceStreamTrade) => void

export type BinanceCombinedStreamDataType = BinanceStreamKlineMessage | BinanceStreamTrade | BinanceTicker[] | BinanceStreamDepth

export interface BinanceCombinedStream {
  stream: string
  data: BinanceCombinedStreamDataType
}

export interface AccountUpdateBalance {
  a: string  // Asset
  f: string  // Free amount
  l: string  // Locked amount
}

export interface AccountUpdate {
  e: 'outboundAccountInfo' // Event type
  E: number                // Event time
  m: number                // Maker commission rate (bips)
  t: number                // Taker commission rate (bips)
  b: number                // Buyer commission rate (bips)
  s: number                // Seller commission rate (bips)
  T: boolean               // Can trade?
  W: boolean               // Can withdraw?
  D: boolean               // Can deposit?
  u: number                // Time of last account update
  B: AccountUpdateBalance[]  // Balances array
}

export interface OrderUpdate {
  e: 'executionReport' // Event type
  E: number          // Event time
  s: string          // Symbol
  c: string          // Client order ID
  S: string          // Side
  o: string          // Order type
  f: string          // Time in force
  q: string          // Order quantity
  p: string          // Order price
  P: string          // Stop price
  F: string          // Iceberg quantity
  g: number          // Ignore
  C: string          // Original client order ID; This is the ID of the order being canceled
  x: string          // Current execution type
  X: string          // Current order status
  r: string          // Order reject reason; will be an error code.
  i: number          // Order ID
  l: string          // Last executed quantity
  z: string          // Cumulative filled quantity
  L: string          // Last executed price
  n: string          // Commission amount
  N: string | null   // Commission asset
  T: number          // Transaction time
  t: number          // Trade ID
  I: number          // Ignore
  w: boolean         // Is the order working? Stops will have
  m: boolean         // Is this trade the maker side?
  M: boolean         // Ignore
}

export type UserAccountUpdate = AccountUpdate | OrderUpdate

export type UserAccountUpdateHandler = (update: UserAccountUpdate) => void

export interface StartUserDataStreamResponse {
  listenKey: string
}

export interface BinanceStreamKlineMessage {
  e: string     // Event type
  E: number,    // Event time
  s: string,    // Symbol
  k: BinanceStreamKline

}

export interface BinanceStreamKline {
  t: number,    // Kline start time
  T: number,    // Kline close time
  s: string     // Symbol
  i: Interval   // Interval
  f: number,    // First trade ID
  L: number,    // Last trade ID
  o: string     // Open price
  c: string     // Close price
  h: string     // High price
  l: string     // Low price
  v: string     // Base asset volume
  n: number,    // Number of trades
  x: false,     // Is this kline closed?
  q: string     // Quote asset volume
  V: string     // Taker buy base asset volume
  Q: string     // Taker buy quote asset volume
  B: string     // Ignore
}

export interface BinanceStreamTrade {
  e: string       // Event type
  E: number       // Event time
  s: string       // Symbol
  // a number  if it's aggregated it's aggregated trade id, else it's seller id
  t: number  // Trade ID(only if not aggregated)
  p: string       // Price
  q: string       // Quantity
  b: number       // Buyer order Id
  a: number       // Seller order Id
  T: number       // Trade time
  m: boolean      // Is the buyer the market maker?
  M: boolean      // Ignore.
}

export interface BinanceTicker {
  e: '24hrTicker'  // Event type
  E: number     // Event time
  s: string     // Symbol
  p: string     // Price change
  P: string     // Price change percent
  w: string     // Weighted average price
  x: string     // Previous day's close price
  c: string     // Current day's close price
  Q: string     // Close trade's quantity
  b: string     // Best bid price
  B: string     // Bid bid quantity
  a: string     // Best ask price
  A: string     // Best ask quantity
  o: string     // Open price
  h: string     // High price
  l: string     // Low price
  v: string     // Total traded base asset volume
  q: string     // Total traded quote asset volume
  O: number     // Statistics open time
  C: number     // Statistics close time
  F: number     // First trade ID
  L: number     // Last trade Id
  n: number     // Total number of trades
}

export type BinanceTickersHandler = (tickers: BinanceTicker[]) => void

export interface BinanceStreamDepth {
  e: 'depthUpdate',              // Event type
  E: number                      // Event time
  s: string                      // Symbol
  U: number                      // First update ID in event
  u: number                      // Final update ID in event
  b: BinanceDepthEntry[]   // Bids to be updated
  a: BinanceDepthEntry[]   // Asks to be updated
}

export type DepthSizes = 5 | 10 | 20 | 50 | 100 | 500 | 1000

export type BinanceDepthHandler = (depth: BinanceStreamDepth) => void
