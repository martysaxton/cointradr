
export class BookEntry {
  constructor (readonly price: number, readonly size: number) {
  }
}

export default class Book {
  constructor (
    readonly exchange: string,
    readonly market: string,
    readonly bids: BookEntry[],
    readonly asks: BookEntry[]
    ) {}

  private static getAverageCostStatic (bookEntries: BookEntry[], targetAmount: number, targetAmountIsQuote: boolean): number {
    let remaining = targetAmount
    let avgPrice: number = -1
    let cumQuantity = 0
    for (const bookEntry of bookEntries) {
      const thisPrice = bookEntry.price
      let size = bookEntry.size
      // sizes in the book are in terms of the quote currency
      if (!targetAmountIsQuote) {
        size *= bookEntry.price
      }
      const thisQuantity = Math.min(remaining, size)
      if (avgPrice === -1) {
        avgPrice = thisPrice
      } else {
        avgPrice = ((avgPrice * cumQuantity) + (thisPrice * thisQuantity)) / (cumQuantity + thisQuantity)
      }
      remaining -= thisQuantity
      if (remaining <= 0) {
        return avgPrice
      }
      cumQuantity += thisQuantity
    }
    return avgPrice
  }

  private static invertBookEntry = (bookEntry: BookEntry) => {
    const price = 1 / bookEntry.price
    return new BookEntry(price, bookEntry.size * price)
  }

  getBidsSize (): number {
    let rv = 0
    this.bids.forEach(bid => rv += bid.size)
    return rv
  }
  getAsksSize (): number {
    let rv = 0
    this.asks.forEach(bid => rv += bid.size)
    return rv
  }

  invert (): Book {
    return new Book(this.exchange, this.market, this.asks.map(Book.invertBookEntry), this.bids.map(Book.invertBookEntry))
  }

  getAverageCostBids (targetAmount: number, targetAmountIsQuote = true) {
    return Book.getAverageCostStatic(this.bids, targetAmount, targetAmountIsQuote)
  }

  getAverageCostAsks (targetAmount: number, targetAmountIsQuote = true): number {
    return Book.getAverageCostStatic(this.asks, targetAmount, targetAmountIsQuote)
  }

}
