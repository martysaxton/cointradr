import { loggerFactory } from '../logging'

const logger = loggerFactory()

const NO_ACTIVITY_RESET_INTERVAL = 1000 * 15

export abstract class AbstractClientStream {

  protected className = this.constructor.name

  private lastActivityTime: number = 0
  private noActivityPingPending: boolean = false

  abstract connect (): void
  protected abstract disconnect (): void
  protected abstract ping (): void

  protected onConnect () {
    logger.info('%s connected', this.className)
    this.noActivityPingPending = false
    this.onEvent()
    setInterval(this.activityCheck, NO_ACTIVITY_RESET_INTERVAL, this)
    this.lastActivityTime = Date.now()
  }

  protected onEvent () {
    if (this.noActivityPingPending) {
      logger.info('%s received event while ping was pending', this.className)
    }
    this.lastActivityTime = Date.now()
    this.noActivityPingPending = false
  }

  private onNoActivity = () => {
    const message = `${this.className} has had no activity for at least ${NO_ACTIVITY_RESET_INTERVAL / 1000} seconds; `
    if (!this.noActivityPingPending) {
      logger.info('%s sending ping', message)
      this.ping()
      this.noActivityPingPending = true
    } else {
      logger.info('%s no ping response, reconnecting', message)
      this.noActivityPingPending = false
      this.resetConnection()
    }
  }

  private activityCheck = (_this: AbstractClientStream) => {
    const now = Date.now()
    const noActivityInterval = now - _this.lastActivityTime
    if (noActivityInterval > NO_ACTIVITY_RESET_INTERVAL) {
      this.onNoActivity()
    }
  }

  protected resetConnection = () => {
    try {
      logger.info('%s resetting connection', this.className)
      this.disconnect()
      logger.info('%s disconnected', this.className)
    } catch (e) {
      logger.info('%s caught error while disconnecting', this.className, e.message)
    }
    this.connect()
    logger.info('%s reconnected', this.className)
  }

}
