
export default class SortedArray<T> {

  protected elements: T[] = []

  constructor (readonly comparator: (a: T, b: T) => number) { }

  insert (t: T): number {
    const loc = this.locationOf(t)
    this.elements.splice(loc, 0, t)
    return loc
  }

  shift (): T | undefined {
    return this.elements.shift()
  }

  remove (index: number): void {
    this.elements.splice(index, 1)
  }

  removeIf (predicate: (t: T) => boolean): void {
    for (let i = 0; i < this.elements.length; ++i) {
      if (predicate(this.elements[i])) {
        this.elements.splice(i, 1)
        return
      }
    }
  }

  values (): T[] {
    return this.elements.slice(0)
  }

  locationOf (t: T): number {
    let lower = 0
    let upper = this.elements.length
    while (true) {
      if (upper <= lower) {
        return lower
      }
      const pivot = lower + Math.trunc((upper - lower) / 2)
      const pivotValue = this.elements[pivot]
      const compareResult = this.comparator(pivotValue, t)
      if (compareResult === 0) {
        return pivot
      }
      if (compareResult > 0) {
        upper = pivot
      } else {
        lower = pivot + 1
      }
    }
  }
}
