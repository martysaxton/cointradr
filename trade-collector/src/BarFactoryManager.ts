import { Trade, Bar, ExchangeSymbol } from './model'
import { BarFactory, BarFactoryObserver } from './BarFactory'

import { loggerFactory } from './logging'
import { db } from './db'
const logger = loggerFactory()

export const ONE_SECOND = 1000
export const FIVE_SECONDS = ONE_SECOND * 5
export const TEN_SECONDS = ONE_SECOND * 10
export const FIFTEEN_SECONDS = ONE_SECOND * 15
export const THIRTY_SECONDS = ONE_SECOND * 30
export const ONE_MINUTE = ONE_SECOND * 60
export const TWO_MINUTES = ONE_MINUTE * 2
export const FOUR_MINUTES = ONE_MINUTE * 4
export const EIGHT_MINUTES = ONE_MINUTE * 8
export const FIVE_MINUTES = ONE_MINUTE * 5
export const TEN_MINUTES = ONE_MINUTE * 10
export const FIFTEEN_MINUTES = ONE_MINUTE * 15
export const THIRTY_MINUTES = ONE_MINUTE * 30
export const ONE_HOUR = ONE_MINUTE * 60

export const DURATIONS = [
  ONE_SECOND,
  FIVE_SECONDS,
  TEN_SECONDS,
  FIFTEEN_SECONDS,
  THIRTY_SECONDS,
  ONE_MINUTE,
  TWO_MINUTES,
  FOUR_MINUTES,
  EIGHT_MINUTES,
  FIVE_MINUTES,
  TEN_MINUTES,
  FIFTEEN_MINUTES,
  THIRTY_MINUTES,
  ONE_HOUR
]

export class BarFactoryManager {

  private barFactoryMap: {[key: string]: BarFactory } = {}

  private observers: BarFactoryObserver[] = []

  // state machine:
  //    ignore trades(don't add them to the barCreators)
  //    enqueue them (don't add them to barCreators)
  //    add them to barCreators
  private enqueue = false
  private ignore = true
  private tradeQueue: Trade[] = []
  private fiveSecondFactories: BarFactory[] = []
  private nextFiveSecondBarStart = 0

  /** called when backlog of trades has been processed */
  async missingBarsComplete (): Promise<void> {
    logger.debug('processing queue trade: %d', this.tradeQueue.length)
    for (const trade of this.tradeQueue) {
      await this.addTrade(trade)
    }
    this.tradeQueue = []
    this.enqueue = false
    this.ignore = false
    this.computeNextFiveSecondBarStart()
    this.setFiveSecondBarTimeout()
  }

  enqueueTrades () {
    logger.debug('enqueueing trades')
    this.enqueue = true
  }

  addObserver (observer: BarFactoryObserver) {
    this.observers.push(observer)
  }

  async handleMissingTrade (trade: Trade) {
    await this.addTrade(trade)
  }

  async handleTrade (trade: Trade) {
    if (this.enqueue) {
      this.tradeQueue.push(trade)
      logger.debug('trade queued, backlog=%d', this.tradeQueue.length)
    } else if (!this.ignore) {
      await this.addTrade(trade)
    }
  }

  private async addTrade (trade: Trade) {
    logger.debug('adding trade: %j', trade)
    const barFactory = this.getBarFactory(trade, FIVE_SECONDS)
    await barFactory.addTrade(trade)
  }

  private barHandler = async (bar: Bar) => {
    logger.debug('insert bar: %s %s', this.createKey(bar.exch, bar.symb, bar.dur), bar.start)
    await db.insertBar(bar)
  }

  private getBarFactory (x: Trade | Bar, duration: number) {
    const key = this.createKey(x.exch, x.symb, duration)
    let barFactory = this.barFactoryMap[key]
    if (!barFactory) {
      this.creatFactories(x.exch, x.symb)
    }
    return this.barFactoryMap[key]
  }

  private creatFactories (exch: ExchangeSymbol, symb: string) {
    const fiveSeconds = this.createFactory(exch, symb, FIVE_SECONDS)
    this.fiveSecondFactories.push(fiveSeconds)
    const tenSeconds = this.createFactory(exch, symb, TEN_SECONDS)
    fiveSeconds.addObserver(tenSeconds.addBar)
    const fifteenSeconds = this.createFactory(exch, symb, FIFTEEN_SECONDS)
    fiveSeconds.addObserver(fifteenSeconds.addBar)
    const thirtySeconds = this.createFactory(exch, symb, THIRTY_SECONDS)
    fifteenSeconds.addObserver(thirtySeconds.addBar)
    const oneMinute = this.createFactory(exch, symb, ONE_MINUTE)
    thirtySeconds.addObserver(oneMinute.addBar)
    const twoMinutes = this.createFactory(exch, symb, TWO_MINUTES)
    oneMinute.addObserver(twoMinutes.addBar)
    const fourMinutes = this.createFactory(exch, symb, FOUR_MINUTES)
    twoMinutes.addObserver(fourMinutes.addBar)
    const eightMinutes = this.createFactory(exch, symb, EIGHT_MINUTES)
    fourMinutes.addObserver(eightMinutes.addBar)
    const sixteenMinutes = this.createFactory(exch, symb, ONE_MINUTE * 16)
    eightMinutes.addObserver(sixteenMinutes.addBar)
    const fiveMinutes = this.createFactory(exch, symb, FIVE_MINUTES)
    oneMinute.addObserver(fiveMinutes.addBar)
    const tenMinutes = this.createFactory(exch, symb, TEN_MINUTES)
    fiveMinutes.addObserver(tenMinutes.addBar)
    const fifteenMinutes = this.createFactory(exch, symb, FIFTEEN_MINUTES)
    fiveMinutes.addObserver(fifteenMinutes.addBar)
    const thirtyMinutes = this.createFactory(exch, symb, THIRTY_MINUTES)
    fifteenMinutes.addObserver(thirtyMinutes.addBar)
    const oneHour = this.createFactory(exch, symb, ONE_HOUR)
    thirtyMinutes.addObserver(oneHour.addBar)
  }

  private createFactory (exch: ExchangeSymbol, symb: string, duration: number) {
    const barFactory = new BarFactory(exch, symb, duration)
    barFactory.addObserver(this.barHandler)
    const key = this.createKey(exch, symb, duration)
    this.barFactoryMap[key] = barFactory
    return barFactory
  }

  private createKey (exch: string, symb: string, duration: number) {
    return exch + ':' + symb + ':' + duration
  }

  private computeNextFiveSecondBarStart () {
    const now = Date.now()
    this.nextFiveSecondBarStart = (Math.floor(now / FIVE_SECONDS) * FIVE_SECONDS) + FIVE_SECONDS
  }

  private setFiveSecondBarTimeout (): boolean {
    const now = Date.now()
    const delay = this.nextFiveSecondBarStart - now
    logger.debug('delay until next 5 second bar: %d', delay)
    if (delay > 0) {
      logger.debug('setting timeout: %d', delay)
      setTimeout(this.onFiveSeconds, delay)
      return true
    }
    return false
  }

  private onFiveSeconds = async () => {
    // if we woke up early, set another timer and abort
    if (this.setFiveSecondBarTimeout()) {
      return
    }
    // clock says we're definitely at or beyond the next five second epoch
    logger.debug('five second epoch')
    for (const barFactory of this.fiveSecondFactories) {
      await barFactory.completeBarUpToTime(new Date())
    }
    // call us back in five more seconds
    this.computeNextFiveSecondBarStart()
    this.setFiveSecondBarTimeout()
  }

}

export const barFactoryManager = new BarFactoryManager()
