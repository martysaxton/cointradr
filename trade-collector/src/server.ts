import express, { Request, Response, NextFunction } from 'express'
import helmet from 'helmet'
import getStatus from './server-status'
import { db } from './db'
import { loggerFactory } from './logging'
import { Cursor } from 'mongodb'
import { Trade } from './model'
// tslint:disable-next-line:no-commented-code
// import { combineReducers } from 'redux'
const LOGGER = loggerFactory()

class ValidationError extends Error {
  constructor (message?: string) {
    super(message)
    Object.setPrototypeOf(this, new.target.prototype)
  }
}

function requiredParam (params: any, attributeName: string): any {
  const value = params[attributeName]
  if (!value) {
    throw new ValidationError(`${attributeName} is required`)
  }
  return value
}

function requiredDate (params: any, attributeName: string): Date {
  const dateStr = requiredParam(params, attributeName)
  const millis = Date.parse(dateStr)
  if (isNaN(millis)) {
    throw new ValidationError(`could not parse ${attributeName}; value=${dateStr}`)
  }
  return new Date(millis)
}

async function forEachMongo<T> (cursor: Cursor<T>, cb: (t: T) => void) {
  while (true) {
    const nextVal = await cursor.next()
    if (!nextVal) {
      return
    }
    cb(nextVal)
  }
}

function addStandardHeaders (res: Response) {
  res.append('Access-Control-Allow-Origin', '*')
}

async function tradesHandler (req: Request, res: Response, next: NextFunction): Promise<any> {
  addStandardHeaders(res)
  try {
    const query: any = {
      t: { $gte: requiredDate(req.query, 'start'), $lt: requiredDate(req.query, 'end') }
    }
    if (req.query.exch) {
      query.exch = req.query.exch
    }
    if (req.query.symb) {
      query.symb = req.query.symb
    }
    const trades = await db.getTrades()
    const tradeCursor = trades.find(query).sort({ t: 1 }).limit(10000)
    const rv: Trade[] = []
    await forEachMongo(tradeCursor, trade => {
      const t = trade as any
      delete t._id
      delete t.id
      delete t.time
      rv.push(t)
    })
    res.json(rv)
  } catch (e) {
    next(e)
  }

}

// tslint:disable-next-line:no-commented-code
// function emptyObject (obj: any): boolean {
//   return Object.keys(obj).length === 0 && obj.constructor === Object
// }

function addIntParamIf (req: Request, paramName: string) {
  const value = parseInt(req.query[paramName], 10)
  const rv: any = {}
  if (!isNaN(value)) {
    rv[paramName] = value
  }
  return rv
}

function addStringParamIf (req: Request, paramName: string) {
  const value = req.query[paramName]
  const rv: any = {}
  if (value) {
    rv[paramName] = value
  }
  return rv
}

function addIf (condition: boolean, value: object): object {
  return condition ? value : {}
}

async function barsHandler (req: Request, res: Response, next: NextFunction): Promise<any> {
  addStandardHeaders(res)
  try {
    const query: any = {
      ...addIntParamIf(req, 'dur'),
      ...addStringParamIf(req, 'exch'),
      ...addStringParamIf(req, 'symb')
    }

    const startDateMillis = Date.parse(req.query['start'])
    if (!isNaN(startDateMillis)) {
      query.start = { $gt: new Date(startDateMillis) }
    }

    const returnGreaterThan = req.query.returnGreaterThan
    const returnLessThan = req.query.returnLessThan

    if (returnGreaterThan || returnLessThan) {
      query.ret = {
        ...addIf(returnGreaterThan, { $gte: parseFloat(returnGreaterThan) }),
        ...addIf(returnLessThan, { $lt: parseFloat(returnLessThan) })
      }
    }

    const startGt = req.query.startGreaterThan
    const startLt = req.query.startLessThan
    if (startGt || startLt) {
      query.start = {
        ...addIf(startGt, { $gte: new Date(startGt) }),
        ...addIf(startLt, { $lt: new Date(startLt) })
      }
    }

    const bars = await db.getBars()
    const barCursor = bars.find(query).sort({ start: 1 }).limit(10000)
    const rv: any = []
    await forEachMongo(barCursor, bar => {
      const b = bar as any
      delete b._id
      rv.push(b)
    })
    res.json(rv)
  } catch (e) {
    next(e)
  }
}

export default function startServer () {
  const app = express()
  app.use(helmet())

  app.set('json spaces', 2)

  app.get('/', (_req, res) => {
    const status = getStatus()
    res.json(status)
  })

  app.get('/trades', tradesHandler)
  app.get('/bars', barsHandler)

  app.use(function (err: any, req: Request, res: Response, next: NextFunction) {
    console.log('in error handler')
    if (res.headersSent) {
      return next(err)
    }

    const statusCode = err instanceof ValidationError ? 400 : 500
    res.statusMessage = err.message
    res.status(statusCode).end()
  })

  app.listen(3000, () => LOGGER.info('sever is up on port 3000'))
}
