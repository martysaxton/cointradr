import winston from 'winston'
import appRoot from 'app-root-path'
import path from 'path'
import callsite from 'callsite'
import { mkdirs } from './utils/fileutils'
import * as Transport from 'winston-transport'

const transports: Transport[] = [new winston.transports.Console()]

export interface LoggerFactoryConfig {
  console?: boolean
  logDir?: string
  sourceRootDir?: string
}

let config: LoggerFactoryConfig = {
  console: true
}

export function configureLoggerFactory (newConfig: LoggerFactoryConfig) {
  config = newConfig
  transports.splice(0, transports.length)
  if (config.console) {
    transports.push(new winston.transports.Console())
  }
  if (config.logDir) {
    mkdirs(config.logDir)
    const fileTransportOpts: winston.transports.FileTransportOptions = {
      filename: path.resolve(config.logDir, 'log'),
      maxsize: 1024 * 1024 * 1000,
      maxFiles: 10,
      tailable: false,
      zippedArchive: false
    }
    transports.push(new winston.transports.File(fileTransportOpts))
  }

}

export function loggerFactory (loggerLabel?: string) {
  const label = loggerLabel || getLabelFromCallSite()
  const alignedWithColorsAndTime = winston.format.combine(
    winston.format.colorize(),
    winston.format.timestamp(),
    winston.format.align(),
    winston.format.label({ label: label }),
    winston.format.splat(),
    winston.format.printf((info) => {
      const { timestamp, level, message, label } = info
      const ts = timestamp.slice(0, 23).replace('T', ' ')
      return `${ts} [${level}] ${label}: ${message}`
    })
  )
  return winston.createLogger({
    level: 'info',
    format: alignedWithColorsAndTime,
    transports: transports
  })
}

function getLabelFromCallSite () {
  const cs = callsite()
  const frame = cs[2]
  const file = frame.getFileName()
  const extName = path.extname(file)
  const fileWithoutExtension = path.basename(file, extName)
  let prefixToIgnore = appRoot.path.length + 1
  if (config.sourceRootDir) {
    prefixToIgnore += config.sourceRootDir.length + 1
  }
  const dirName = path.dirname(file)
  let loggerLabel = dirName.slice(prefixToIgnore).replace(path.sep, '.')
  if (loggerLabel.length > 0) {
    loggerLabel += '.'
  }
  loggerLabel += fileWithoutExtension
  return loggerLabel
}
