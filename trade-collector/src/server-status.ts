import moment from 'moment'
import store from './store'

export default function getStatus () {
  const state = store.getState()
  const startTime = store.getState().startTime
  const lastTrades: { [exchange: string]: string } = {}
  for (const key in state.lastTrade) {
    lastTrades[key] = moment(state.lastTrade[key]).fromNow(false)
  }
  return {
    uptime: moment(startTime).fromNow(true),
    lastTrade: lastTrades
  }
}
