import { Bar, ExchangeSymbol, Trade } from './model'

import { loggerFactory } from './logging'
const logger = loggerFactory()

export type BarFactoryObserver = (bar: Bar) => void

export class BarFactory {

  private currentBar?: Bar

  private observers: BarFactoryObserver[] = []

  private readonly barFactoryName: string

  constructor (
    readonly exchange: ExchangeSymbol,
    readonly symbol: string,
    readonly durationInMillis: number) {
    this.barFactoryName = this.exchange + ':' + this.symbol + ':' + (this.durationInMillis / 1000)
  }

  public addObserver (observer: BarFactoryObserver) {
    this.observers.push(observer)
  }

  public async addTrade (trade: Trade) {
    const tradePrice = parseFloat(trade.price)
    const tradeVol = parseFloat(trade.size)
    await this.completeBarUpToTime(trade.t)
    const currentBar = this.getCurrentBar(tradePrice, trade.t)
    currentBar.high = Math.max(currentBar.high, tradePrice)
    currentBar.low = Math.min(currentBar.low, tradePrice)
    currentBar.close = tradePrice
    const newVol = currentBar.vol + tradeVol
    currentBar.vwap = (currentBar.vwap * currentBar.vol + tradePrice * tradeVol) / newVol
    currentBar.vol = newVol
    if (trade.bid) {
      currentBar.bv += tradeVol
    } else {
      currentBar.av += tradeVol
    }

  }

  public addBar = async (bar: Bar): Promise<void> => {
    logger.debug('%s addBar %j', this.barFactoryName, bar)
    const currentBar = this.getCurrentBar(bar.open, bar.start)
    currentBar.high = Math.max(currentBar.high, bar.high)
    currentBar.low = Math.min(currentBar.low, bar.low)
    currentBar.close = bar.close
    const newVol = currentBar.vol + bar.vol
    currentBar.vwap = (currentBar.vwap * currentBar.vol + bar.vwap * bar.vol) / newVol
    currentBar.vol = newVol
    currentBar.bv += bar.bv
    currentBar.av += bar.av
    const barEnd = new Date(bar.start.getTime() + bar.dur)
    await this.completeBarUpToTime(barEnd)
  }

  public async completeBarUpToTime (time: Date) {
    if (!this.currentBar) {
      return
    }

    const thisTime = time.getTime()
    const currentBarEndTime = this.currentBar.start.getTime() + this.durationInMillis
    if (thisTime >= currentBarEndTime) {
        // a time beyond the current bar
      if (this.currentBar.vol === 0) {
        logger.debug('%s empty bar at %s', this.barFactoryName, this.currentBar.start)
      } else {
        logger.info('%s %s complete non-empty bar', this.currentBar.start.toISOString(), this.barFactoryName)
        this.currentBar.ret = (this.currentBar.high - this.currentBar.low) / this.currentBar.low * 100
        this.currentBar.aret = Math.abs(this.currentBar.ret)
        for (const observer of this.observers) {
          logger.debug('%s calling observer with %j', this.barFactoryName, this.currentBar)
          observer(this.currentBar)
        }
      }
      logger.debug('%s setting bar to undefined', this.barFactoryName)
      this.currentBar = undefined
    }
  }

  private getBarStart (time: Date): Date {
    const millis = time.getTime()
    const remainder = millis % this.durationInMillis
    const startMillis = millis - remainder
    const rv = new Date(startMillis)
    logger.debug('start time %s', rv.toISOString())
    return rv
  }

  private getCurrentBar (price: number, startTime: Date) {
    if (!this.currentBar) {
      this.currentBar = this.createBar(price, startTime)
    }
    return this.currentBar
  }

  private createBar (openPrice: number, time: Date): Bar {
    // round based on interval

    const start = this.getBarStart(time)
    logger.debug('%s creating bar, start: %s  duration=%d ends %d', this.barFactoryName, start, this.durationInMillis / 1000, start.getTime() + this.durationInMillis)

    return {
      exch: this.exchange,
      symb: this.symbol,
      start,
      dur: this.durationInMillis,
      open: openPrice,
      high: openPrice,
      low: openPrice,
      close: openPrice,
      vol: 0,
      vwap: openPrice,
      bv: 0,
      av: 0,
      ret: 0,
      aret: 0
    }
  }

}
