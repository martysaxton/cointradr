db.trades.aggregate(
  [
    {
      $group : {
        _id : {
          exch: "$exch",
          id: "$id",
	  symb: "$symb"
        },
        dups: { $push: "$_id" },
        count: { $sum: 1 }
      }
    },
    {
      $match: {"_id" :{ "$ne" : null } , "count" : {"$gt": 1} }
    }
  ],
  {
    allowDiskUse: true
  }
).forEach(function(doc) {
  var dupes = doc.dups
  //var x = dupes.shift()
  //db.trades.remove({ "_id": {"$in": doc.dups }})
  //print('hi', x)
  print (dupes)
})
