#!/bin/bash
set -e
set -x
if [[ -n $(git status --porcelain) ]]; then echo "repo is dirty"; fi
rm -rf logs
rm -rf dist
./node_modules/.bin/tsc
docker build --tag cointradr/trade-collector --label githash=$(git rev-parse --short HEAD) .
docker push cointradr/trade-collector
