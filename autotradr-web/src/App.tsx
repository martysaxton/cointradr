import React, { Component } from 'react'
import logo from './logo.svg'
import './App.css'
import FastJsonPatch from 'fast-json-patch'
import moment from 'moment'

interface CoinTradrState {
  wallet: Wallet
  lastTrade: Trade
  book: Book
  psar: ParabolicSarValues
  params: AutoTradrParams
  recentRoundTrips: RoundTrip[]
}

interface AutoTradrParams {
  barIntervalSeconds: number
  parabolicSar: ParabolicSarParams
}
interface ParabolicSarParams {
  startAF: number,
  increment: number,
  maxAF: number
}

interface ParabolicSarValues {
  sar: number,
  af: number,
  ep: number
}

interface Wallet {
  usdCash: number,
  usdInPosition: number
  usdTotal: number
}

interface Trade {
  id: string,
  exch: string,
  symb: string,
  t: number,
  price: number,
  size: number,
  bid: boolean 
}

interface Book {
  bestBid: number
  bestAsk: number
}

interface Order {
  id: string
  mktSym: string
  type: string
  direction: string
  size: number
  price: number
  filledQuantity: number
  filledPrice: number
  whenPlaced: number
  whenFilled: number
  state: string
}

interface RoundTrip {
  entry: Order
  exit?: Order
}

interface State {
  loading: boolean,
  ctstate: CoinTradrState
}


const initialState: State = {
  loading: true,
  ctstate: {
    wallet: {
      usdCash: NaN,
      usdInPosition: NaN,
      usdTotal: NaN
    },
    lastTrade: {
      id: "",
      exch: "",
      symb: "",
      t: 0,
      price: 0,
      size: 0,
      bid: false
    },
    book: {
      bestBid: NaN,
      bestAsk: NaN
    },
    psar: {
      sar: NaN,
      af: NaN,
      ep: NaN
    },
    params: {
      barIntervalSeconds: NaN,
      parabolicSar: {
        startAF: NaN,
        increment: NaN,
        maxAF: NaN
      }
    },
    recentRoundTrips: []
  }
}


const AUTH_COOKIE_NAME = "ctauth"

class App extends Component<object, State> {

  private webSocket?: WebSocket
  state: State = initialState
  private rootUrl = ''
  componentDidMount() {
    console.log('process.env.NODE_ENV', process.env.NODE_ENV)
    if (process.env.NODE_ENV == 'development') {
      this.rootUrl = 'http://localhost:8080'
    }
      this.loadState()
  }

  render() {
      return this.renderMainScreen()
  }
  renderMainScreen() {


    return (
      <div>
        <div className="rTable">
          <div className="rTableRow">
            <div className="rTableCell">
              
              <div className="rTable">
                <div className="rTableRow">
                  <div className="rTableCell label">Cash:</div>
                  <div className="rTableCell numeric">{this.renderMoney(this.state.ctstate.wallet.usdCash)}</div>
                </div>
                <div className="rTableRow">
                  <div className="rTableCell label">Positions:</div>
                  <div className="rTableCell numeric">{this.renderMoney(this.state.ctstate.wallet.usdInPosition)}</div>
                </div>
                <div className="rTableRow">
                  <div className="rTableCell label">Total:</div>
                  <div className="rTableCell numeric">{this.renderMoney(this.state.ctstate.wallet.usdTotal)}</div>
                </div>
              </div>
            </div>

            <div className="rTableCell">
              <div className="rTable">
                <div className="rTableRow">
                  {this.renderPriceData()}
                </div>
              </div>
            </div>            
          </div>
        </div>
        <fieldset className="grouping">
          <legend className="label">Current Position</legend>
          {this.renderCurrentPosition()}
        </fieldset>
        <fieldset className="grouping">
          <legend className="label">Recent Round Trips</legend>
          {this.renderRoundTrips()}
        </fieldset>
        <fieldset className="grouping">
          <legend className="label">Params</legend>
          {this.renderParams()}
        </fieldset>
        <fieldset className="grouping">
          <legend className="label">Parabolic Sar Details</legend>
          {this.renderPsarDetails()}
        </fieldset>

      </div>
    )
  }
  renderPsarDetails(){
    return (
      <table>
        <tr>
          <td className="label">SAR</td>
          <td className="numeric">{this.safeToFixed(this.state.ctstate.psar.sar, 5)}</td>
        </tr>
        <tr>
          <td className="label">EP</td>
          <td className="numeric">{this.safeToFixed(this.state.ctstate.psar.ep, 5)}</td>
        </tr>
        <tr>
          <td className="label">AF</td>
          <td className="numeric">{this.safeToFixed(this.state.ctstate.psar.af,5)}</td>
        </tr>
      </table>
    )

  }

  safeToFixed(n: number, x: number) {
    return isNaN(n) ? "" : n.toFixed(x)
  }

  renderParams() {
    return (
      <table>
        <tr>
          <td className="label">Bar Interval</td>
          <td className="numeric">{this.state.ctstate.params.barIntervalSeconds}</td>
        </tr>
        <tr>
          <td className="label">PSAR Start</td>
          <td className="numeric">{this.state.ctstate.params.parabolicSar.startAF.toFixed(5)}</td>
        </tr>
        <tr>
          <td className="label">PSAR Increment</td>
          <td className="numeric">{this.state.ctstate.params.parabolicSar.increment.toFixed(5)}</td>
        </tr>
        <tr>
          <td className="label">PSAR Max</td>
          <td className="numeric">{this.state.ctstate.params.parabolicSar.maxAF.toFixed(5)}</td>
        </tr>
      </table>
    )
  }

  renderCurrentPosition() {
    const rrt = this.state.ctstate.recentRoundTrips
    if (rrt.length === 0) {
      return <div/>
    }
    const cp = rrt[0].entry
    let unrealized = this.state.ctstate.lastTrade.price - cp.filledPrice
    if (cp.direction === "SELL") {
      unrealized *= -1
    }
    const unrealizedPercent = unrealized / cp.filledPrice * 100
    return (
      <table>
        <tr>
          <td className='label'>Disposition</td>
          <td>{this.toDisposition(cp.direction)}</td>
        </tr>
        <tr>
          <td className='label'>Price</td>
          <td className='numeric'>{this.renderMoney(cp.filledPrice)}</td>
        </tr>
        <tr>
          <td className='label'>Since</td>
          <td className='numeric'>{moment(cp.whenFilled).fromNow()}</td>
        </tr>
        <tr>
          <td className='label'>UPL</td>
          <td className='numeric'>{unrealizedPercent.toFixed(3)}%</td>
        </tr>
      </table>
    )
  }

  renderPct(n: number) {
    return n.toFixed(2)
  }

  renderRoundTrips() {
    return (
      <table>
        <thead className="label">
          <tr>
            <th> </th>
            <th colSpan={2} >Entry</th>
            <th colSpan={2} >Exit</th>
          </tr>
          <tr>
            <th>Direction</th>            
            <th>Time</th>            
            <th>Price</th>            
            <th>Time</th>            
            <th>Price</th>            
            <th>PL</th>            
          </tr>
        </thead>
        <tbody>
          {this.renderRoundTripRows()}
        </tbody>
      </table>
    )
  }

  renderRoundTripRows() {

    const rts = this.state.ctstate.recentRoundTrips
      .slice(1)
      .map(rt => { 

      const cells = [ <td > { this.toDisposition(rt.entry.direction) }</td > ]
        .concat(this.renderOrder(rt.entry))
        .concat(this.renderOrderIf(rt.exit))
        .concat(this.renderRoundTripPL(rt))

      return <tr>{cells}</tr>
    })
    // console.log('rts', rts)
    return rts
  }

  toDisposition(direction: string) {
    console.log('direction', direction)
    return direction === "BUY" ? "LONG" : "SHORT"    
  }

  renderOrderIf(order?: Order) {
    if (order) {
      return this.renderOrder(order)
    }
    return [<td></td>]
  }

  renderOrder(order: Order) {
    return [
      <td>{this.renderTime(order.whenFilled)}</td>,
      <td className="borderRight numeric">{this.renderMoney(order.filledPrice)}</td>
    ]
  }

  renderRoundTripPL(rt: RoundTrip) {
    if (rt.exit != null) {
      const diff = rt.exit.filledPrice - rt.entry.filledPrice
      const percent = diff / rt.entry.filledPrice * 100
      const mult = rt.entry.direction === "SELL" ? -1 : 1
      return <td className='numeric'>{(percent * mult).toFixed(3) + '%'}</td>
    }
    return <td></td>
  }


  renderTime(time: number) {
    return moment(time).format('HH:mm:ss')
  }

  renderPriceData = () => {

    const LAST_TRADE_LABEL = "Last Trade:"
    const BEST_BID_LABEL = "Best Bid:"
    const BEST_ASK_LABEL = "Best Ask:"

    const priceData: [string, number][] = [
      [LAST_TRADE_LABEL, this.state.ctstate.lastTrade.price],
      [BEST_BID_LABEL, this.state.ctstate.book.bestBid],
      [BEST_ASK_LABEL, this.state.ctstate.book.bestAsk],
      ["SAR", this.state.ctstate.psar.sar]
    ]

    if (this.state.ctstate.book.bestAsk === this.state.ctstate.lastTrade.price) {
      console.log("trade == bestAsk")
    }
    if (this.state.ctstate.book.bestBid === this.state.ctstate.lastTrade.price) {
      console.log("trade == bestBid")
    }

    const sortedPriceData = priceData.sort((a, b) => b[1] - a[1])
    const lastTradeIndex = sortedPriceData.findIndex(pd => pd[0] === LAST_TRADE_LABEL)
      // see if we need to bump it down or up
    if (lastTradeIndex > -1) {
      const lastTradePrice = sortedPriceData[lastTradeIndex][1]
      if (lastTradeIndex < sortedPriceData.length - 1) {
        const pdBelow = sortedPriceData[lastTradeIndex + 1]
        if (pdBelow[0] == BEST_ASK_LABEL && lastTradePrice == pdBelow[1]) {
          console.log("swapping trade ask")
          const temp = sortedPriceData[lastTradeIndex]
          sortedPriceData[lastTradeIndex] = sortedPriceData[lastTradeIndex + 1]
          sortedPriceData[lastTradeIndex + 1] = temp
        }
      }
      if (lastTradeIndex > 0) {
        const pdAbove = sortedPriceData[lastTradeIndex - 1]
        if (pdAbove[0] == BEST_BID_LABEL && lastTradePrice == pdAbove[1]) {
          console.log("swapping trade bid")
          const temp = sortedPriceData[lastTradeIndex]
          sortedPriceData[lastTradeIndex] = sortedPriceData[lastTradeIndex - 1]
          sortedPriceData[lastTradeIndex - 1] = temp
        }
      }
    }
    const rows: JSX.Element[] = []
    sortedPriceData.forEach( e => {
      rows.push(<div className="rTableRow">{this.renderPriceDataCells(e[0], e[1])}</div>)
    })
    return rows
  }

  renderPriceDataCells(label: string, value: number) {
    const cells: JSX.Element[] = []
    cells.push(<div className="rTableCell label">{label}</div>)
    cells.push(<div className="rTableCell numeric">{this.renderMoney(value)}</div>)

    return cells
  }

  renderMoney(x: number): String {
    if (!isNaN(x)) {
      return x.toFixed(2)
    }
    return ""
  }

  renderLoading() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Editt <code>src/App.tsx</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
      </div>
    )

  }

  // isCookiePresent(cookieName: string): boolean {
  //   const regexp = new RegExp(`^(.*;)?\\s*${cookieName}\\s*=\\s*[^;]+(.*)?$`)
  //   return document.cookie.match(regexp) != null
  // }

  getCookieValue(cookieName: string): string | null {
    const regexp = new RegExp(`^(?:.*;)?\\s*${cookieName}\\s*=\\s*([^;]+)(?:.*)?$`)
    const xx = document.cookie.match(regexp)
    console.log("xx", xx)
    const x = (document.cookie.match(regexp) || [null, null])

    const y = x[1]
    return y
  }

  // value_or_null = (document.cookie.match(/^(?:.*;)?\s*MyCookie\s*=\s*([^;]+)(?:.*)?$/) || [, null])[1]

  async loadState() {
    console.log("auth cookie", this.getCookieValue(AUTH_COOKIE_NAME))
    const authHeader = this.getCookieValue(AUTH_COOKIE_NAME)!
    const headers = new Headers()
    headers.set(AUTH_COOKIE_NAME, authHeader)
    headers.append('X-My-Custom-Header', 'CustomValue')
    console.log('headers', headers)
    try {
      var request = new Request(this.rootUrl + '/api/state', {
        mode: "cors",
        // headers: new Headers({
        // 'Content-Type': 'text/plain',
        // 'X-My-Custom-Header': 'CustomValue'
        // })
      })
      console.log('awaiting response')
      const response = await fetch(request)
      console.log('got response')
      if (response.status == 403) {
        window.location.href = this.rootUrl + '/login/google'
      } else if (response.status >= 300) {
        console.log('got non successful response', response)
      } else {
        const appState = await response.json()
        console.log("appState", appState)
        this.setState({
          ...this.state,
          loading: false,
          ctstate: appState
        })
      }

      this.webSocket = new WebSocket( this.toWebSocketUrl('/api/websocket'))
      this.webSocket.onerror = (ev: Event) => {
        console.error('websocket error event', event)
      }
        
      this.webSocket.onclose = (event: CloseEvent) => {
        console.error(`websocket close event', event}`)
      }
      this.webSocket.onmessage = this.onMessage

      this.webSocket.onopen = (event: Event) => {
        console.debug('websocket open event')
        // this.webSocket.on('ping', this.onPing)
        // this.webSocket.on('pong', this.onPong)
        // this.onConnect()
        // resolve(true)
      }


    } catch (e) {
      console.log("caught while fetching", e)
    }


  }

  private onMessage = (event: MessageEvent) => {
    if (event.type !== 'message') {
      console.warn(`unknown event type:  ${event.type}`)
    } else {
      if (typeof event.data === 'string') {
        this.handleString(event.data)
      } else {
        console.warn('can\'t handle websocket message')
      }
    }

  }

  handleString(data: string): any {
    const stateOrPatch = JSON.parse(data)
    let newCtState: CoinTradrState
    if (Array.isArray(stateOrPatch)) {
      newCtState = FastJsonPatch.applyPatch(this.state.ctstate, stateOrPatch).newDocument
      // console.log("got patch")
    } else {
      newCtState = stateOrPatch
      console.log('got full state')
    }
    this.setState({
      ...this.state,
      loading: false,
      ctstate: newCtState
    })

  }

  private toWebSocketUrl(path: string): string {
    if (process.env.NODE_ENV == 'development') {
      return 'ws://localhost:8080' + path
    }
    const loc = window.location
    var rv
    if (loc.protocol === "https:") {
      rv = "wss:"
    } else {
      rv = "ws:"
    }
    rv += "//" + loc.host
    rv += path
    console.log('websocket url', rv)
    return rv
  }

}

export default App;
