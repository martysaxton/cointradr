#!/bin/bash

docker build --tag cointradr/jvm8 --label githash=$(git rev-parse --short HEAD) .
docker push cointradr/jvm8
