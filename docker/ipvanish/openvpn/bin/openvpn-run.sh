#!/bin/sh
exec 2>&1
set -x
echo "OpenVPN OPTS: ${OPENVPN_OPTS}"
echo "OpenVPN config: ${OPENVPN_CONFIG}"
java -jar /etc/openvpn/bin/secrets-0.0.1-all.jar ipvanish Password >> /etc/openvpn/conf/ipvanish/auth.txt
exec openvpn --script-security 2 \
    --up "/etc/openvpn/bin/openvpn-up.sh" \
    --down "/etc/openvpn/bin/openvpn-down.sh" \
    $OPENVPN_OPTS --config "$OPENVPN_CONFIG"
