#!/bin/bash
set -e
cp ../../kotlin/secrets/build/libs/secrets-0.0.1-all.jar openvpn/bin
docker build --tag cointradr/ipvanish --label githash=$(git rev-parse --short HEAD) .
#docker push cointradr/ipvanish
