# -d is detach

set -x
docker run \
	--name asdf \
	--mount source=ctsecrets,destination=/ctsecrets \
	--device=/dev/net/tun \
	--cap-add=NET_ADMIN \
	      -it --rm \
              cointradr/ipvanish
