package cointradr.tradecollector

import cointradr.exchanges.exchangeModule
import cointradr.exchanges.impl.MongoTradeQueues
import cointradr.exchanges.impl.okex.OkExMarketDataExchange
import cointradr.fin.model.Market
import cointradr.fin.model.Trade
import cointradr.mongo.InitializeLogging
import cointradr.utils.loggingLaunch
import com.soywiz.klock.DateTime
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import org.koin.core.KoinComponent
import org.koin.core.context.startKoin
import org.koin.core.inject
import org.koin.core.logger.Level
import org.koin.dsl.module
import org.litote.kmongo.coroutine.insertOne

class TradeCollector(private val market: Market): KoinComponent {

    companion object {
        private val logger = KotlinLogging.logger {}
    }

    private val marketDataExchange: OkExMarketDataExchange by inject()
    private val mongoTradeQueues: MongoTradeQueues by inject()

    suspend fun start() {
        marketDataExchange.addTradeHandler(::onTrade)
        marketDataExchange.subscribeToTrades(market)

    }

    private suspend fun onTrade(trade: Trade) {
        logger.debug{ "got trade $trade time: ${DateTime.fromUnix(trade.t)}" }
        val tradesCollection = mongoTradeQueues.getCollection(trade)
        GlobalScope.loggingLaunch {
            tradesCollection.insertOne(trade)
        }
    }

}


fun main() = runBlocking {
    InitializeLogging.init("trade-collector")
    val market = Market("okex", "btc","usd","190329")
    val koinApplication = startKoin {
        val tradeCollectorModule = module {
            single{ TradeCollector(market) }
        }
        modules(tradeCollectorModule, exchangeModule(market))
        logger(level=Level.DEBUG)
    }
    val tradeCollector = koinApplication.koin.get<TradeCollector>()
    runBlocking {
        tradeCollector.start()
        delay(Long.MAX_VALUE)
    }
}

