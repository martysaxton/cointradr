# TODO

## Interesting State
* Current PSAR
* Most Recent trade/bid/ask
* Open stop orders (client side)
* Trade History
* Account Info

## Live psar with mock exchange

### Goals
1. Single contract only (btc/usd)
2. Read balances and open positions
3. Proper support for understanding value of open positions
4. No contract modeling for now

### Steps 

After a stop orderRequest has executed, place another one in the opposite direction based on the new EP indicator
Finish off positions as completed trades and archive
 
### Challenges

1.  There's a model conflict:
  * For spot trading, there are no contracts.  There are just currency balances.  
  Buying btc/usd means converting usd to btc.
  Selling btc/usd means converting btc to usd.   
  * For contract trading positions you own contracts, not just balances.
  Buying btc/usd means subtracting net capital and adding contracts
  Selling btc/usd means subtracting contracts and adding capital to some currency
  * Neutral in spot currency really means equal amounts of both currencies
Should everything be modeled more like contracts?  
For spot trading, the only balance would be USD  


Long term, we can allocate portions of net capital to trading in multiple markets. 
 
## Dockerize

1. update Db class to use 'mongo' as hostname to connect

### Fix Race Conditions 

When a new SAR is computed, the previous STOP must be canceled and a new one must be submitted.
It's possible for the open STOP to execute just before the cancelation is received.
In this case, hopefully the exchange will send an error code indicating why the cancelation failed.

If cancelation fails for any reason, maybe the best thing is to retrieve the lastest open orders from the server.
It should be OK for the client to cache the orderRequest ID and use it to cancel before requesting open positions. 
99% of the time this should work. 
```

Client                                     Exchange
   |           cancel <cached orderRequest Id>        |
   |------------------------------------------>|
   |                                           |
   |                 success                   |
   |<------------------------------------------|
   |                                           |
   |         get positions and open orders     |
   |------------------------------------------>|
   |                                           |
   |          positions and open orders        |
   |<------------------------------------------|
   |                                           |
   |             submit new orderRequest              |
   |------------------------------------------>|
   |                                           |
   |                 success                   |
   |<------------------------------------------|
   |                                           |
    
```

Might be good to develop a declarative API to submit desired end state.

### Strategy Executer
SRP:  Receives signals from a Strategy, interfaces with an exchange to place or cancel orders.

States:  
IDLE                                   no active orders, nothing is pending
ORDER_SUBMISSION_PENDING               orderRequest has been submitted but exchange has not acked yet
ORDER_ACTIVE                           orderRequest has been submitted and exchanged acked
ORDER_CANCEL_PENDING                   orderRequest cancellation has been submitted and exchange has not acked yet
ORDER_CANCEL_DURING_SUBMISSION   cancellation event has occurred while an orderRequest submission is pending

Events:
SUBMIT_ORDER                           client wants to submit a new orderRequest
CANCEL_ORDER                           client wants to cancel a submitted orderRequest
ORDER_SUBMITTED                        exchange ACKed a submitted orderRequest
ORDER_FILLED                           exchange tells us a submitted orderRequest has been filled
ORDER_CANCELLED                        exchange tells us that an orderRequest has been canceled 

TODO:   submitOrder and cancelOrder can be atomic, get response while holding mutex


## Transitions per state (event -> end state):
IDLE:
  SUBMIT_ORDER  -> ORDER_SUBMISSION_PENDING
  
ORDER_SUBMISSION_PENDING:
  ORDER_SUBMITTED -> ORDER_ACTIVE
  CANCEL_ORDER -> ORDER_CANCEL_DURING_SUBMISSION
  ORDER_FILLED -> IDLE
  
ORDER_ACTIVE:
  CANCEL_ORDER -> ORDER_CANCEL_PENDING
  ORDER_FILLED -> IDLE
  
ORDER_CANCEL_PENDING:
  ORDER_CANCELLED -> IDLE
  ORDER_FILLED -> IDLE
  
ORDER_CANCEL_DURING_SUBMISSION:
  ORDER_SUBMITTED -> ORDER_CANCEL_PENDING
  ORDER_CANCELLED -> IDLE
  ORDER_FILLED -> IDLE

## Transitions per event event (start state -> end state):
SUBMIT_ORDER:
  IDLE  -> ORDER_SUBMISSION_PENDING

CANCEL_ORDER 
  ORDER_SUBMISSION_PENDING              -> ORDER_CANCEL_DURING_SUBMISSION
  ORDER_ACTIVE                          -> ORDER_CANCEL_PENDING

ORDER_SUBMITTED:
  ORDER_SUBMISSION_PENDING              -> ORDER_ACTIVE
  ORDER_CANCEL_DURING_SUBMISSION  -> ORDER_CANCEL_PENDING
  
ORDER_FILLED
  ORDER_ACTIVE                          -> IDLE
  ORDER_SUBMISSION_PENDING              -> IDLE
  ORDER_CANCEL_DURING_SUBMISSION  -> IDLE
  ORDER_CANCEL_PENDING                  -> IDLE

ORDER_CANCELLED 
  ORDER_CANCEL_DURING_SUBMISSION        -> IDLE
  ORDER_CANCEL_PENDING                  -> IDLE
  


