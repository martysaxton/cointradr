package cointradr.secrets

import kotlinx.coroutines.runBlocking
import org.linguafranca.pwdb.kdbx.KdbxCreds
import org.linguafranca.pwdb.kdbx.simple.SimpleDatabase
import java.lang.IllegalArgumentException
import jdk.nashorn.internal.runtime.ScriptingFunctions.readLine
import org.spongycastle.pqc.math.linearalgebra.ByteUtils.toCharArray
import jdk.nashorn.internal.runtime.ScriptingFunctions.readLine
import org.linguafranca.pwdb.kdbx.dom.DomDatabaseWrapper
import java.io.*


class Secrets {

    companion object {

        @Throws(IOException::class)
        private fun readLine(format: String, vararg args: Any): String {
            if (System.console() != null) {
                return System.console().readLine(format, *args)
            }
            print(String.format(format, *args))
            val reader = BufferedReader(InputStreamReader(System.`in`))
            return reader.readLine()
        }

        @Throws(IOException::class)
        private fun readPassword(format: String, vararg args: Any): String {
            return if (System.console() != null)
                String(System.console().readPassword(format, *args))
            else
                this.readLine(format, args)
        }

        private fun getMasterPassword(): String {
            val masterPasswordFile = File("/ctsecrets/master-password")
            return try {
                val istr = masterPasswordFile.inputStream()
                istr.readBytes().toString(Charsets.UTF_8).trim()
            } catch (e: FileNotFoundException) {
                println("file not found: $masterPasswordFile")
                readPassword("master password: ")
            }
        }



        val database: DomDatabaseWrapper

        init {
            val password = getMasterPassword()
            val creds = KdbxCreds(password.toByteArray())
            val inputStream = Secrets::class.java.classLoader.getResourceAsStream("secrets.kdbx")
            database = DomDatabaseWrapper.load(creds, inputStream)
        }

        fun getSecret(key: String): Secret {
            val entries = database.findEntries(key)
            if (entries.size == 0) {
                throw IllegalArgumentException("entry not found for $key")
            }
            val entry = entries.first()
            val apiKey = entry.getProperty("apiKey")
            val secret = entry.getProperty("secret")
            val passPhrase = entry.getProperty("passPhrase")
            return Secret(apiKey, secret, passPhrase)
        }
        fun getSecret(entry: String, property: String): String {
            val entries = database.findEntries(entry)
            if (entries.size == 0) {
                throw IllegalArgumentException("entry not found for $entry")
            }
            return entries.first().getProperty(property)
        }
    }
}

fun main(args: Array<String>) {
    println(Secrets.getSecret(args[0], args[1]))
}
