package cointradr.secrets

data class Secret(
        val apiKey: String,
        val secret: String,
        val passPhrase: String?
)