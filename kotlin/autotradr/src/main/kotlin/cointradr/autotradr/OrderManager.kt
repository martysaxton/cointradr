package cointradr.autotradr

import cointradr.exchanges.Exchange
import cointradr.exchanges.OrderRequest
import cointradr.exchanges.Order
import cointradr.exchanges.TradingExchange
import cointradr.utils.loggingAsync
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import mu.KotlinLogging

typealias FilledTradeObserver = suspend (order: Order) -> Unit

/**
 * Interfaces with an exchange to ensure there is only 1 active order at a time.
 * Maintains a state machine for sanity checking.
 * Callers can only submit orders and cancel orders
 */
open class OrderManager(private val exchange: TradingExchange, private val observer: FilledTradeObserver): TradingExchange.OrderObserver {

    private val logger = KotlinLogging.logger {}

    enum class State {
        IDLE,                                 // no active orders, nothing is pending
        ORDER_SUBMISSION_PENDING,             // order has been submitted but exchange has not ACKed yet
        ORDER_ACTIVE,                         // order has been submitted and exchanged ACKed
        ORDER_CANCEL_PENDING,                 // order cancellation has been submitted and exchange has not ACKed yet
        ORDER_CANCEL_DURING_SUBMISSION  // cancellation event has occurred while an order submission is pending
    }

    private var activeOrder: Order? = null
    private var state: State = State.IDLE
    private val orderManagerMutex = Mutex()

    init {
        exchange.addOrderObserver(this)
    }

    fun getActiveOrder(): Order? {
        return activeOrder
    }

    suspend fun submitOrder(orderRequest: OrderRequest) {
        var orderDeferred: Deferred<Order>? = null

        orderManagerMutex.withLock {
            logger.debug { "submitOrder:  $orderRequest" }
            when(state) {
                State.IDLE -> {
                    logger.debug { "submitting orderRequest:  $orderRequest" }
                    orderDeferred = GlobalScope.loggingAsync { exchange.placeOrder(orderRequest) }
                    updateState(State.ORDER_SUBMISSION_PENDING)
                }
                else -> throw IllegalStateException("submitOrder during invalid state:  $state")
            }
        }

        val placedOrder= orderDeferred!!.await()
        orderManagerMutex.withLock {
            onOrderSubmitted(placedOrder)
        }

    }

    private suspend fun onOrderSubmitted(order: Order) {
        logger.debug { "order submitted:  $order" }
        when (state) {
            State.ORDER_SUBMISSION_PENDING -> {
                logger.debug { "order now active: $order" }
                activeOrder = order
                updateState(State.ORDER_ACTIVE)
            }
            State.ORDER_CANCEL_DURING_SUBMISSION -> {
                logger.debug { "order canceled while submission pending: $order" }
                updateState(State.ORDER_CANCEL_PENDING)
            }
            State.IDLE -> {
                logger.debug { "assuming order already filled" }
            }
            else -> throw IllegalStateException("onOrderSubmitted during invalid state:  $state")
        }
    }

    suspend fun cancelOrder(): Boolean {
        var cancelJob: Deferred<Boolean>? = null
        orderManagerMutex.withLock {
            logger.debug { "cancelOrder:  activeOrder=$activeOrder state=$state" }
            when (state) {
                State.ORDER_SUBMISSION_PENDING -> {
                    logger.debug { "order submission still pending" }
                    updateState(State.ORDER_CANCEL_DURING_SUBMISSION)
                }
                State.ORDER_ACTIVE -> {
                    activeOrder?.let {
                        updateState(State.ORDER_CANCEL_PENDING)
                        cancelJob = GlobalScope.loggingAsync {  exchange.cancelOrder(it.id)  }
                    } ?: throw IllegalStateException( "order state is $state, but activeOrder is null" )
                }
                else -> throw IllegalStateException("cancelOrder during invalid state: $state")
            }
        }
        val cancelSuccess = cancelJob!!.await()
        onCancelSubmitted(cancelSuccess)
        return cancelSuccess
    }

    private suspend fun onCancelSubmitted(wasCancelled: Boolean) {
        // TODO exchange's http response has a cancellation status, exchange should return boolean
        // TODO so right now there are 2 channels to recieve cancellation status:  websocket and rest response
        orderManagerMutex.withLock {
            activeOrder?.let {order ->
                logger.debug{"orderCanceled:  $order"}

                if (!wasCancelled) {
                    logger.debug { "cancel response returned false, assuming order filled but not yet notified" }
                }

                when(state) {
                    State.ORDER_CANCEL_PENDING, State.ORDER_CANCEL_DURING_SUBMISSION -> {
                        logger.debug { "order canceled: $order" }
                        activeOrder = null
                        updateState(State.IDLE)
                    }
                    else -> throw IllegalStateException("order cancellation during invalid state: $state")
                }
            } ?: logger.debug { "order cancellation rest response received but no order active" }
        }

    }

    override suspend fun orderSubmitted(order: Order) {
        orderManagerMutex.withLock {
            logger.debug{"orderSubmitted:  $order"}
            when (state) {
                State.ORDER_SUBMISSION_PENDING -> {
                    logger.debug { "exchange ACKed order submission" }
                    updateState(State.ORDER_ACTIVE)
                }
                State.ORDER_CANCEL_DURING_SUBMISSION -> {
                    logger.debug { "exchange ACKed order submission after client issued cancellation" }
                    updateState(State.ORDER_CANCEL_PENDING)
                }
                else -> throw IllegalStateException("orderSubmitted during invalid state: $state")
            }
        }
    }

    override suspend fun orderFilled(order: Order) {
        orderManagerMutex.withLock {
            logger.debug{"onOrderFilled:  $order"}
            when (state) {
                State.ORDER_ACTIVE -> {
                    logger.debug { "active order filled:  $order" }
                }
                State.ORDER_SUBMISSION_PENDING -> {
                    logger.debug {"filledOrder filled before exchange ACK"}
                }
                State.ORDER_CANCEL_PENDING, State.ORDER_CANCEL_DURING_SUBMISSION -> {
                    logger.warn { "filledOrder filled after a cancel was issued" }
                }
                else -> throw IllegalStateException("filledOrder filled during invalid state: $state")
            }
            activeOrder = null
            updateState(State.IDLE)
        }
        observer(order)
    }

    override suspend fun orderCanceled(order: Order) {
        logger.debug { "ignoring orderCanceled event since cancellation status is taken from rest response " }
    }


    override suspend fun orderPartiallyFilled(order: Order) {
        logger.debug { "partial fill: $order" }
    }

    protected open fun updateState(newState: State) {
        logger.debug { "state transition $state -> $newState" }
        state = newState
    }

    protected fun getState(): State {
        return state
    }
}