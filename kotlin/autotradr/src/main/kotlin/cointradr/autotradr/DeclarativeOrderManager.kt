package cointradr.autotradr

import cointradr.exchanges.*
import cointradr.fin.model.Market
import mu.KotlinLogging
import kotlin.math.abs

class DeclarativeOrderManager(private val exchange: TradingExchange, observer: FilledTradeObserver) {
    private val logger = KotlinLogging.logger {}
    private val replaceableOrderManager = ReplaceableOrderManager(exchange, observer)

    suspend fun submitIntention(market: Market, intendedDisposition: Disposition, stopPrice: Double) {
        logger.debug { "submitIntention market=$market intendedDisposition=$intendedDisposition stopPrice=$stopPrice" }
        val currentPosition = exchange.getPosition(market)
        logger.debug { "currentPosition = $currentPosition" }
        val currentPositionSize = currentPosition?.size ?: 0.0
        val currentDisposition = currentPosition?.disposition ?: Disposition.NEUTRAL
        if (intendedDisposition != currentDisposition) {

            val exit = (intendedDisposition == Disposition.NEUTRAL)

            val newPositionSize = if (exit) 0.0 else getNewPositionSize(market)
            val orderSize = abs(currentPositionSize) + newPositionSize

            val direction = when (intendedDisposition) {
                Disposition.SHORT -> Direction.SELL
                Disposition.LONG -> Direction.BUY
                Disposition.NEUTRAL -> if (currentDisposition == Disposition.SHORT) Direction.BUY else Direction.SELL

            }
            val order = OrderRequest(
                    market = market,
                    type = if (exit) OrderRequest.Type.MARKET else OrderRequest.Type.STOP,
                    direction = direction,
                    size = orderSize,
                    price = stopPrice)
            replaceableOrderManager.replaceOrder(order)
        } else {
            logger.debug { "already matching intended intendedDisposition" }
        }
    }

    private suspend fun getNewPositionSize(market: Market): Double {
        val usdCash = exchange.getCashValue()
        val usdInPosition = exchange.getLiquidationValue(market)
        val usdTotal = usdCash + usdInPosition
        return exchange.getOrderSize(market, usdTotal)
    }



}