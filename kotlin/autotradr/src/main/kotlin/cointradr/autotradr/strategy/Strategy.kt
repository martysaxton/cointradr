package cointradr.autotradr.strategy

import cointradr.exchanges.OrderRequest
import cointradr.fin.model.Market

typealias StrategyUpdateHandler = suspend (orderRequest: OrderRequest) -> Unit

abstract class Strategy(protected val market: Market) {

    private val handlers = mutableListOf<StrategyUpdateHandler>()

    fun addHandler(handler: StrategyUpdateHandler) {
        handlers.add(handler)
    }

    protected suspend fun broadcastOrder(orderRequest: OrderRequest) {
        handlers.forEach { it(orderRequest) }
    }

}