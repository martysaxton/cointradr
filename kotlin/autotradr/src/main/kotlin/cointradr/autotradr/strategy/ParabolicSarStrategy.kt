package cointradr.autotradr.strategy

import cointradr.exchanges.Direction
import cointradr.exchanges.Disposition
import cointradr.exchanges.OrderRequest
import cointradr.fin.bar.BarFactoryObserver
import cointradr.fin.model.Bar
import cointradr.fin.model.Market
import mu.KotlinLogging
import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormat

class ParabolicSarStrategy(market: Market) : Strategy(market) {

    private val logger = KotlinLogging.logger {}

    private val defaultTimeZone = DateTimeZone.getDefault()
    private val dateTimeFormatter = DateTimeFormat.forPattern("MM-dd-YY hh:mm:ss aa").withZone(defaultTimeZone)
    private var disposition = Disposition.NEUTRAL

    val onBar: BarFactoryObserver = { bar: Bar ->
        logger.debug("new bar: ${dateTimeFormatter.print(bar.start)} $bar")
        val sar = bar.ext["psar"]
        val prevDisposition = disposition
        sar?.let {
            if (it.isNaN()) {
                if  (disposition != Disposition.NEUTRAL) {
                    logger.debug { "sar is NaN, switching to NEUTRAL" }
                    disposition = Disposition.NEUTRAL
                    signalExit()
                }
            } else {
                val stopOrderDirection: Direction
                if (sar >= bar.high) {
                    stopOrderDirection = Direction.BUY
                    disposition = Disposition.SHORT
                } else if (sar <= bar.low) {
                    stopOrderDirection = Direction.SELL
                    disposition = Disposition.LONG
                } else {
                    TODO("this should never happen")
                }

                if (prevDisposition != disposition) {
                    logger.debug { "switched from $prevDisposition to $disposition" }
                }
                val order = OrderRequest(
                        market = market,
                        type = OrderRequest.Type.STOP,
                        direction = stopOrderDirection,
                        size = if (disposition == Disposition.NEUTRAL) 1.0 else 2.0,
                        price = it)
                logger.debug { "SAR order $order" }
                broadcastOrder(order)
            }
        }
    }


    private suspend fun signalExit() {
        val direction = if (disposition == Disposition.LONG) Direction.SELL else Direction.BUY
        val order = OrderRequest(
                market = market,
                type = OrderRequest.Type.MARKET,
                direction = direction,
                size = 1.0)
        logger.debug { "exit order $order" }
        broadcastOrder(order)
    }


}