package cointradr.autotradr.live

import cointradr.autotradr.AutoTradr
import cointradr.clients.okex.OkExStreamV3
import cointradr.exchanges.TradeProvider
import cointradr.exchanges.TradingExchange
import cointradr.exchanges.exchangeModule
import cointradr.exchanges.impl.okex.OkExMakerMarketOrderManager
import cointradr.exchanges.impl.okex.OkExTradingExchange
import cointradr.mongo.InitializeLogging
import cointradr.rest.RestServer
import cointradr.state.Params
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.koin.core.KoinComponent
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import org.koin.dsl.module

//class LiveAutoTradr: AutoTradr(), KoinComponent {
//
////    companion object {
////        private val logger = KotlinLogging.logger {}
////    }
//
////    private val params: Params by inject()
//
//    override fun createExchange(): TradingExchange {
//        val exch = OkExTradingExchange()
//        runBlocking { exch.authenticate() }
//        return exch
//    }
//}

fun main() = runBlocking {


    InitializeLogging.init("autotradr")

    val koinApplication = startKoin {
        val m = module {
            single { AutoTradr() }
            single { Params() }
            single<TradingExchange> { OkExTradingExchange(AutoTradr.market) }
            single { RestServer() }
        }
        modules(m, exchangeModule(AutoTradr.market))
        logger(level= Level.DEBUG)

    }


    with (koinApplication.koin) {
        val okExStream:OkExStreamV3 = get()
        okExStream.connect()
        val tradeProvider: TradeProvider = get()
        tradeProvider.start(AutoTradr.market)

        val exch: OkExTradingExchange = get()
        exch.authenticate()

        get<AutoTradr>()
        get<OkExMakerMarketOrderManager>()
    }



    runBlocking {
        delay(Long.MAX_VALUE)
    }
}
