package cointradr.autotradr.mock

import cointradr.autotradr.AutoTradr
import cointradr.exchanges.TradingExchange
import cointradr.exchanges.exchangeModule
import cointradr.exchanges.impl.okex.MockOkExExchange
import cointradr.exchanges.impl.okex.mockfill.OkExDepthFillPriceProvider
import cointradr.mongo.InitializeLogging
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import org.koin.dsl.module

//class MockLiveAutoTradr: AutoTradr() {
//
//    companion object {
//        private val logger = KotlinLogging.logger {}
//    }
//
//    override fun createExchange(): TradingExchange {
//        val fillPriceProvider = OkExDepthFillPriceProvider()
//        return MockOkExExchange()
//    }
//}

fun main() = runBlocking {
//    TODO("start trade provider here")
//
//    InitializeLogging.init("mock-autotradr")
//    val koinApplication = startKoin {
//        val m = module {
//            single{ MockLiveAutoTradr() }
//        }
//        listOf(m, exchangeModule)
//        logger(level= Level.DEBUG)
//    }
//
//    runBlocking {
//        delay(Long.MAX_VALUE)
//    }
}
