@file:Suppress("DEPRECATION")

package cointradr.autotradr.persistence

import cointradr.fin.model.Bar
import cointradr.fin.model.Market
import cointradr.mongo.Mongo
import com.mongodb.async.client.MongoCollection
import kotlinx.coroutines.runBlocking
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.litote.kmongo.coroutine.findOne
import org.litote.kmongo.coroutine.insertOne

class StrategyState(val market: Market, val name: String): KoinComponent {

    data class State(
            val lastBar: Bar
    )
    private val mongo: Mongo by inject()

    private lateinit var collection: MongoCollection<State>

    init {
        runBlocking {
            collection = mongo.getCappedCollection(getCollectionName(), 10240L, 1L, State::class.java)
        }
    }

    suspend fun getState(): State? {
        return collection.findOne("")
    }

    suspend fun setState(state: State) {
        collection.insertOne(state)
    }



    private fun getCollectionName(): String {
        return "state.strategy.$market"
    }

}