package cointradr.autotradr.persistence

import cointradr.fin.bar.BarFactoryObserver
import cointradr.fin.model.Bar
import cointradr.fin.model.Market
import cointradr.mongo.Mongo
import cointradr.utils.loggingRunBlocking
import com.mongodb.MongoWriteException
import com.mongodb.client.model.Filters.and
import com.mongodb.client.model.Filters.eq
import com.mongodb.client.model.IndexOptions
import com.mongodb.client.model.UpdateOptions
import mu.KotlinLogging
import org.bson.conversions.Bson
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.litote.kmongo.async.getCollectionOfName
import org.litote.kmongo.coroutine.createIndex
import org.litote.kmongo.coroutine.findOne
import org.litote.kmongo.coroutine.updateOne


class MongoPersistence<T: Any>(val collectionName: String, uniqueCompoundKey: List<String>): KoinComponent {
        val logger = KotlinLogging.logger {}
        val mongo: Mongo by inject()
        val db = mongo.getMongoDatabase()
        val upsertOptions: UpdateOptions = UpdateOptions().upsert(true)


        init {
            logger.debug("creating unique indexes for $collectionName")
            val uniqueIndexOptions = IndexOptions()
            uniqueIndexOptions.unique(true)
            val s = uniqueCompoundKey.joinToString(",", "{", "}") { "$it:1" }
            loggingRunBlocking {
                db.getCollectionOfName<Any>(collectionName).createIndex(s, uniqueIndexOptions)
            }
        }


        suspend inline fun <reified T: Any> read(filter: Map<String, Any>): T? {
            val rv = db.getCollectionOfName<T>(collectionName).findOne(mapToFilter(filter))
            return rv
        }



        suspend inline fun <reified T : Any>  write(filterAsMap: Map<String,Any>, t: T) {
            try {
                val result = db.getCollectionOfName<T>(collectionName).updateOne(mapToFilter(filterAsMap), t, upsertOptions)
                logger.debug { "presisted value=$t;   result=$result" }
            } catch(mwe:  MongoWriteException) {
                if (mwe.code != 11000) {
                    logger.error(mwe) { "error while persisting $t" }
                }
            }
        }

        fun mapToFilter(map: Map<String, Any>): Bson {
            val eqList = map.map { eq(it.key, it.value) }
            return and(eqList)
        }

}


class BarFactoryPersistence {
    companion object {

        private val mongoPersistence = MongoPersistence<Bar>("latestBars", listOf("exch", "symb", "dur"))

        suspend fun getLatestBar(market: Market, dur: Int): Bar? {
            val fut = if (market.expiry == null) "" else "-f"
            val symb = "${market.baseCurrency}/${market.quoteCurrency}$fut"
            val filterMap = mapOf("exch" to market.exchange, "symb" to symb, "dur" to dur)
            return mongoPersistence.read(filterMap)
        }

        val onBar: BarFactoryObserver = { bar -> mongoPersistence.write(instanceToFilter(bar), bar)  }

        private fun instanceToFilter(bar: Bar): Map<String, Any> {
            return with(bar) {
                mapOf("exch" to exch, "symb" to symb, "dur" to dur)
            }
        }

    }
}


//fun main() = runBlocking {
//
//    val bar = Bar(
//        exch = "okex",
//        symb = "btc/usd-f",
//        dur = 5000,
//        start = DateTime.nowUnixLong(),
//        open = 3000.0,
//        high = 3001.0,
//        low = 2999.0,
//        close = 3000.01,
//        vol = 10.0,
//        ext = mapOf("psar" to 3002.0)
//    )
//
//    val market = Market("okex", "btc", "usd", "190329")
//    val result = BarFactoryPersistence.getLatestBar(market, 5000)
//    if (result != null) {
//        val newBar = bar.copy(high = bar.high + 1)
//        BarFactoryPersistence.onBar(newBar)
//    }
//    println("result: $result")
////    barFactoryState.rep
//    println("hi")
//
//}