package cointradr.autotradr.persistence

import cointradr.exchanges.Direction
import com.soywiz.klock.DateTime

data class EntryExit(
        val direction: Direction,
        val size: Double,
        val leverage: Double,
        val entryTime: DateTime,
        val entryPrice: Double,
        val exitTime: DateTime,
        val exitPrice: Double,
        val fee: Double
) {
    fun getProfit(): Double {
        var priceDelta = exitPrice - entryPrice
        if (direction == Direction.SELL) {
            priceDelta *= -1.0
        }
        return priceDelta * size * leverage - fee
    }
}