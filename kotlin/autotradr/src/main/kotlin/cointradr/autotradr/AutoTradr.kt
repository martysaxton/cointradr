package cointradr.autotradr

import cointradr.autotradr.persistence.BarFactoryPersistence
import cointradr.autotradr.strategy.ParabolicSarStrategy
import cointradr.autotradr.strategy.StrategyUpdateHandler
import cointradr.backtest.BAR_INTERVAL
import cointradr.exchanges.*
import cointradr.exchanges.TradeProvider
import cointradr.fin.bar.BarFactory
import cointradr.fin.bar.ParabolicSar
import cointradr.fin.model.Bar
import cointradr.fin.model.Market
import cointradr.fin.model.Trade
import cointradr.rest.RestServer
import cointradr.state.*
import cointradr.utils.loggingLaunch
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.util.*


class AutoTradr: KoinComponent {

    companion object {
        private val logger = KotlinLogging.logger {}
        var market = Market("okex", "btc", "usd", "190329")
    }
    protected var barFactory: BarFactory

    private val params: Params by inject()
    private val tradeProvider: TradeProvider by inject()

    protected val parabolicSar = with (params.getParams().parabolicSar) {
        ParabolicSar(startAF = startAF, increment = increment, maxAF = maxAF)
    }
    private val store: Store by inject()
    private val restServer: RestServer by inject()

    private val exchange: TradingExchange by inject()

    private var strategy = ParabolicSarStrategy(market)
    private var isRealTime = false
//    protected var lastTrade: Trade? = null

    protected var entryOrder: Order? = null
    private lateinit var orderManager: DeclarativeOrderManager


    protected open suspend fun onTrade(trade: Trade, isRealTime: Boolean) {
        this.isRealTime = isRealTime
//        lastTrade = trade
    }

    private suspend fun onBar(bar: Bar) {
        bar.ext["psar"]?.let {  psar ->
            store.update {
                it.copy(
                    psar = ParabolicSarValues(
                        sar = psar,
                        ep = bar.ext.getValue("ep"),
                        af = bar.ext.getValue("af")
                    )
                )
            }
        }
    }

    private val strategyUpdateHandler: StrategyUpdateHandler= { order ->
        if (shouldTrade()) {
            if (order.size == 1.0) {
                logger.debug { "psar indicates exit position" }
                exitPosition()
            } else {
                val disposition = if (order.direction == Direction.BUY) Disposition.LONG else Disposition.SHORT
                    orderManager.submitIntention(market, disposition, order.price!!)
            }
        }
    }

    protected open fun shouldTrade(): Boolean {
        return isRealTime
    }

    private suspend fun filledTradeObserver(order: Order) {
        logger.debug { "filledTradeObserver:  $order  entryOrder: $entryOrder" }

        val roundTrips = store.appState.recentRoundTrips.toMutableList()

        entryOrder?.let {
            val roundTrip = RoundTrip(it, order)
            roundTrips[0] = roundTrip
            onRoundTrip(roundTrip)
        }

        if (order.type == OrderRequest.Type.STOP) {
            while (roundTrips.size > 10) {
                roundTrips.removeAt(roundTrips.lastIndex)
            }
            roundTrips.add(0, RoundTrip(order, null))
            entryOrder = order
        } else {
            entryOrder = null
        }

        logger.debug { "recent round trips:  ${roundTrips[0].entry.direction} ${roundTrips[1].entry.direction} ${roundTrips[2].entry.direction}" }

        GlobalScope.loggingLaunch {
            store.update { it.copy(recentRoundTrips = roundTrips) }
        }
    }

    protected open fun onRoundTrip(roundTrip: RoundTrip) {
        logger.info { "round trip completed $roundTrip" }
        roundTrip.exit?.let { exit ->
            val entrySlippage = computeSlippage(roundTrip.entry)
            val exitSlippage = computeSlippage(exit)
            val totalSlippage = entrySlippage + exitSlippage
            logger.info ("round trip stats:  enterSlip=$entrySlippage exitSlip=$exitSlippage totSlip=$totalSlippage favpd=${computeFavorablePriceDiff(roundTrip)}")
        }
    }



    protected fun computeSlippage(order: Order): Double {
        val dir = if (order.direction == Direction.BUY) -1.0 else 1.0
        return (order.price!! - order.filledPrice!!) * dir
    }

    protected fun computeFavorablePriceDiff(roundTrip: RoundTrip): Double {
        val dir = if (roundTrip.entry.direction == Direction.BUY) -1.0 else 1.0
        return (roundTrip.entry.filledPrice!! - roundTrip.exit!!.filledPrice!!) * dir
    }

    protected open suspend fun exitPosition() {
        logger.debug { "placing exit order" }
        orderManager.submitIntention(market, Disposition.NEUTRAL, 0.0)
    }

    init {
        val params = params.getParams()
        logger.debug { "autotradr starting" }
        barFactory = BarFactory(
                exch = "okex",
                symb = "btc/usd-f",
                duration = params.barIntervalSeconds * 1000 * 60
        )
        barFactory.addIndicator(parabolicSar)
        barFactory.addObserver(strategy.onBar)

        barFactory.addObserver(BarFactoryPersistence.onBar)
        runBlocking {
            val latestBar = BarFactoryPersistence.getLatestBar(market, BAR_INTERVAL)
            if (latestBar != null) {
                barFactory.setLastBarSeen(latestBar)
                parabolicSar.setLastBarSeen(latestBar)
            }
        }
//        exchange = createExchange()
        orderManager = DeclarativeOrderManager(exchange, ::filledTradeObserver)
        tradeProvider.addHandler(barFactory.addTrade)
        tradeProvider.addHandler(::onTrade)
        strategy.addHandler(strategyUpdateHandler)
        barFactory.addObserver(::onBar)
//        GlobalScope.loggingLaunch {
//            exchange.startOrderBookStream(market)
//        }
        restServer.startRest()
        logger.debug { "autotradr init done" }
    }

//    private val dateTimeFormatter = DateTimeFormat.forPattern("MM-dd-YY hh:mm:ss aa").withZone(defaultTimeZone)

}



val rand = Random()

fun permuteDouble(d: Double) : Double {
    val oneSD = d / 10.0
    val minValue = oneSD / 2
    return maxOf(d + rand.nextGaussian() * oneSD, minValue)

}

fun permuteParams(p: ParabolicSarParams): ParabolicSarParams {

    val whichAttribute = rand.nextInt(4)
    return if (whichAttribute == 3) {
        ParabolicSarParams(
                startAF = permuteDouble(p.startAF),
                increment = permuteDouble(p.increment),
                maxAF = permuteDouble(p.maxAF)
        )
    } else {
        ParabolicSarParams(
                startAF = if (whichAttribute == 0) permuteDouble(p.startAF) else p.startAF,
                increment = if (whichAttribute == 1) permuteDouble(p.increment) else p.increment,
                maxAF = if (whichAttribute == 2) permuteDouble(p.maxAF) else p.maxAF

        )

    }
}

val prettyGood1m = ParabolicSarParams(startAF = 0.0016, increment = 0.003, maxAF = 0.02)
val prettyGood5m = ParabolicSarParams(startAF = 0.0019, increment = 0.023, maxAF = 0.35)




