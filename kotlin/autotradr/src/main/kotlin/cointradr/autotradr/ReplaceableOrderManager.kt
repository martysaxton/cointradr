package cointradr.autotradr

import cointradr.exchanges.Exchange
import cointradr.exchanges.OrderRequest
import cointradr.exchanges.TradingExchange
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import mu.KotlinLogging
import java.lang.IllegalStateException

/**
 * Extends behavior of OrderManager to enable callers to replace an existing order with a new one.
 * Next step would be a more imperative interface:  tell some manager what position you want,
 * and if things get out of sync due to race conditions, auto correct it with market orders
 */
class ReplaceableOrderManager(exchange: TradingExchange, observer: FilledTradeObserver) {

    private val logger = KotlinLogging.logger {}
    private val replaceableOrderManagerMutex = Mutex()
    private val orderManager = OrderManager(exchange, observer)
//    var pendingOrder: OrderRequest? = null
//    var pendingOrderMutex = Mutex()
//
//    suspend fun replaceOrder(order: OrderRequest) {
//        orderManagerMutex.withLock {
//            logger.debug { "replacement order:  $order"}
//            val state = getState()
//            if (state != State.IDLE) {
//                logger.debug { "submitting new order when state reaches idle, state=$state "}
//                if (state == State.ORDER_ACTIVE || state == State.ORDER_SUBMISSION_PENDING) {
//                    logger.debug { "cancelling existing order"}
//                    GlobalScope.loggingLaunch { cancelOrder() }
//                }
//                pendingOrderMutex.withLock {
//                    pendingOrder = order
//                }
//            } else {
//                submitOrderWithCurrentStateIdle(order)
//            }
//        }
//    }
//
//    override fun updateState(newState: State) {
//        super.updateState(newState)
//        if (newState == State.IDLE) {
//            onIdleState()
//        }
//    }
//
//    private fun onIdleState() {
//        GlobalScope.loggingLaunch {
//            pendingOrderMutex.withLock {
//                pendingOrder?.let {
//                    logger.debug { }
//                    orderManagerMutex.withLock {
//                        submitOrderWithCurrentStateIdle(it)
//                        pendingOrder = null
//                    }
//                }
//            }
//        }
//    }

    suspend fun replaceOrder(orderRequest: OrderRequest) {
        replaceableOrderManagerMutex.withLock {
            var wasCanceled = true
            try {
                logger.debug { "canceling orderRequest if exists" }
                wasCanceled = orderManager.cancelOrder()
            } catch (illegalStateException: IllegalStateException) {
                logger.debug { "caught illegal state exception while canceling: ${illegalStateException.message}" }
            }
            if (!wasCanceled) {
                logger.warn { "orderRequest cancel submission was successful and exchange refused" }
            }
            logger.debug { "submitting orderRequest: $orderRequest" }
            orderManager.submitOrder(orderRequest)
        }

    }
}

