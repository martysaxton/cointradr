package cointradr.backtest

import cointradr.state.AutoTradrParams
import cointradr.state.ParabolicSarParams
import cointradr.exchanges.RoundTrip
import cointradr.exchanges.Direction
import cointradr.exchanges.Order
import cointradr.exchanges.OrderRequest
import cointradr.exchanges.impl.okex.mocktrade.HistoricTradeProvider
import cointradr.exchanges.TradeProvider
import cointradr.fin.bar.BarFactory
import cointradr.fin.bar.BarFactoryObserver
import cointradr.fin.bar.ParabolicSar
import cointradr.fin.model.Bar
import cointradr.fin.model.Market
import cointradr.fin.model.Trade
import com.soywiz.klock.DateTime
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import org.nustaq.serialization.FSTConfiguration
import org.nustaq.serialization.FSTObjectOutputNoShared
import java.io.File
import java.io.FileNotFoundException
import java.io.Serializable
import java.lang.Math.pow
import java.util.*

const val BAR_INTERVAL = 1000 * 60 * 5
const val FEE = 0.03
const val USE_TRAINING_PERIOD = false
val TRADING_SAMPLE_CUTOFF = DateTime(2019,1,15).unixMillisLong

// how long to wait to before trading to get indicators seeded
const val WARMUP_INTERVAL = 1000 * 60 * 60

fun List<Bar>.copy(): List<Bar> {
    val rv = ArrayList<Bar>(this.size)
    forEach {
        val b = it.copy(ext = mutableMapOf())
        rv.add(b)
    }
    return rv
}


class JvmBar(
        val exch: String,
        val symb: String,
        val start: Long,
        val dur: Int,
        val open: Double,
        val high: Double,
        val low: Double,
        val close: Double,
        val vol: Double
) : Serializable

fun List<Bar>.toJvmBarList(): List<JvmBar> {
    return this.map { JvmBar(
            exch = it.exch,
            symb = it.symb,
            start = it.start,
            dur = it.dur,
            open = it.open,
            high = it.high,
            low = it.low,
            close = it.close,
            vol = it.vol)
    }
}

fun List<JvmBar>.toBarList(): List<Bar> {
    return this.map { Bar(
            exch = it.exch,
            symb = it.symb,
            start = it.start,
            dur = it.dur,
            open = it.open,
            high = it.high,
            low = it.low,
            close = it.close,
            vol = it.vol,
            ext = mutableMapOf())
    }
}


class BackTest(val bars: List<Bar>) {
    companion object {

        val logger = KotlinLogging.logger {}

        var market = Market("okex", "btc", "usd", "190329")


        private fun List<ParabolicSarParams>.copy(): MutableList<ParabolicSarParams> {
            val rv = ArrayList<ParabolicSarParams>(this.size)
            forEach { rv.add(it.copy()) }
            return rv
        }

        val rand = Random()

        fun permuteDouble(d: Double) : Double {
            val oneSD = d / 2.0
            val minValue = oneSD / 2
            return maxOf(d + rand.nextGaussian() * oneSD, minValue)

        }

        fun permuteParams(p: ParabolicSarParams): ParabolicSarParams {

            val whichAttribute = rand.nextInt(3)
            return ParabolicSarParams(
                    startAF = if (whichAttribute == 0) permuteDouble(p.startAF) else p.startAF,
                    increment = if (whichAttribute == 1) permuteDouble(p.increment) else p.increment,
                    maxAF = if (whichAttribute == 2) permuteDouble(p.maxAF) else p.maxAF
            )
        }

        private fun loadBarsFromBin(): List<Bar> {
            val fst  = FSTConfiguration.createDefaultConfiguration()
            fst.registerClass(JvmBar::class.java)
            logger.debug { "reading trades from bin file to buffer" }
            val bin = File(getBarsBinFileName())
            val buffer = bin.readBytes()
            logger.debug { "instantiating objects from buffer" }
            val bars = fst.asObject(buffer) as MutableList<JvmBar>
            logger.debug { "done reading trades from bin: ${HistoricTradeProvider.trades.size}" }
            return bars.toBarList()
        }

        private fun loadBarsFromHistoricTradeProvider(): List<Bar> {
            val bars = mutableListOf<Bar>()
            runBlocking {
                val tradeProvider: TradeProvider = HistoricTradeProvider()
                val barFactory = BarFactory(
                        exch = "okex",
                        symb = "btc/usd-f",
                        duration = BAR_INTERVAL)
                val barFactoryObserver: BarFactoryObserver = { bar ->
                    //        println("bar $bar")
                    bars.add(bar)
                    if (bars.size % 100 == 0) {
                        println(" ${bars.size} bars")
                    }
                }

                barFactory.addObserver(barFactoryObserver)
                tradeProvider.addHandler(barFactory.addTrade)
                val job = tradeProvider.start(market)
                job.join()
                println(" ${bars.size} bars")
            }
            return bars
        }

        private fun loadBars(): List<Bar> {
            var bars: List<Bar>
            try {
                bars = loadBarsFromBin()
            } catch (_: FileNotFoundException) {
                bars = loadBarsFromHistoricTradeProvider()
                val outputNoShared = FSTObjectOutputNoShared(HistoricTradeProvider.fst)
                logger.debug { "serializing trades to byte array" }
                val jvmBars = bars.toJvmBarList()
                outputNoShared.writeObject(jvmBars)
                val buffer = outputNoShared.buffer
                val bin = File(getBarsBinFileName())
                logger.debug("writing bytes to bin file")
                bin.writeBytes(buffer)
                logger.debug("done writing trades to bin file")
            }
            return bars
        }

        private fun getBarsBinFileName(): String {
            return "bars.${(BAR_INTERVAL / 1000L).toString()}.bin"
        }

        suspend fun go() {
            val bars = loadBars()
            println("loaded bars")
            val startAF = 0.04
            val increment = 0.04
            val maxAF = 0.4

            val bestParams = mutableListOf<ParabolicSarParams>()
            val best = mutableListOf<Double>()
            for (i in 0..16) {
                // 0 = 1
                // 1 = 0.75
                // 2 = 0.75 * 0.75
                // pow
                val m = pow(0.7, i.toDouble())
                bestParams.add(ParabolicSarParams(startAF * m, increment * m, maxAF * m))
                best.add(Double.NEGATIVE_INFINITY)
            }

            val currentParams = bestParams.copy()
            for (iteration in 1..3000) {
                println("--------------  round $iteration --------------")
                val deferreds = mutableListOf<Deferred<Double>>()

                // kick everything off
                for (i in 0 until bestParams.size) {
                    deferreds.add(GlobalScope.async {
                        val atp = AutoTradrParams(BAR_INTERVAL / 1000, currentParams[i])
                        BackTest(bars.copy()).start(atp)
                    })
                }

                for (i in 0 until bestParams.size) {
                    val ret = deferreds[i].await()
                    val prevBestReturn = best[i]
                    if (ret > prevBestReturn) {
                        println("new best $ret prev=$prevBestReturn prevParams=${bestParams[i]} newParams=${currentParams[i]}")
                        bestParams[i] = currentParams[i]
                        best[i] = ret
                    }
                }
                for (i in 0 until bestParams.size) {
                    currentParams[i] = permuteParams(bestParams[i])
                }
            }

            var globalBest = Double.NEGATIVE_INFINITY
            var globalBestParams: ParabolicSarParams? = null
            for (i in 0 until bestParams.size) {
                println("ret ${best[i]} params=${bestParams[i]}")
                if (best[i] > globalBest) {
                    globalBest = best[i]
                    globalBestParams = bestParams[i]
                }
            }

            globalBestParams?.let {
                val atp = AutoTradrParams(BAR_INTERVAL / 1000, it)
                BackTest(bars.copy()).start(atp, true)
//                println(ret)
            }


        }

        private fun summarize(autoTradrParams: AutoTradrParams, roundTrips: List<RoundTrip>): Double {
            var totalNetProfitPct = 0.0
            roundTrips.forEach {
                if (!USE_TRAINING_PERIOD || it.exit!!.whenFilled!! < TRADING_SAMPLE_CUTOFF) {
                    val entry = it.entry
                    val grossProfit = when (entry.direction ) {
                        Direction.BUY -> it.exit!!.price!! - it.entry.price!!
                        Direction.SELL -> it.entry.price!! - it.exit!!.price!!
                    }
                    val grossProfitPct = grossProfit / entry.price!! * 100
                    val netProfitPct = grossProfitPct - (FEE * 2)
//                println("netProfit: $netProfitPct")
                    totalNetProfitPct += netProfitPct
                }
            }
//            println("$parabolicSarParams num trades:  ${roundTrips.size}  total net profit:  $totalNetProfitPct")
            return totalNetProfitPct
        }

        private fun summarizeExtended(autoTradrParams: AutoTradrParams, roundTrips: List<RoundTrip>): Double {
            var tradeCount = 0
            var totalNetProfitPct = 0.0
            var winningCount = 0
            var losingCount = 0
            var winningTradeSum = 0.0
            var losingTradeSum = 0.0
            var maxDrawDown = 0.0
            var currentDrawDown = 0.0
            roundTrips.forEach {
                if (!USE_TRAINING_PERIOD || it.exit!!.whenFilled!! >= TRADING_SAMPLE_CUTOFF) {
                    ++tradeCount
                    val entry = it.entry
                    val grossProfit = when (entry.direction ) {
                        Direction.BUY -> it.exit!!.price!! - it.entry.price!!
                        Direction.SELL -> it.entry.price!! - it.exit!!.price!!
                    }
                    val grossProfitPct = grossProfit / entry.price!! * 100
                    val netProfitPct = grossProfitPct - (FEE * 2)
    //                println("${it.entry.direction} ${it.entry.price} ${it.exit.price} $grossProfit $netProfitPct")
                    totalNetProfitPct += netProfitPct
                    if (netProfitPct > 0.0) {
                        ++winningCount
                        winningTradeSum += netProfitPct
                        maxDrawDown = minOf(maxDrawDown, currentDrawDown)
                        currentDrawDown = 0.0
                    } else {
                        ++losingCount
                        losingTradeSum += netProfitPct
                        currentDrawDown += netProfitPct
                    }
                }
            }
            println(autoTradrParams.toYaml())
            println("num trades:  $tradeCount  total net profit:  $totalNetProfitPct pctWin=${winningCount.toDouble() /roundTrips.size.toDouble() * 100.0} avgWinSize=${winningTradeSum/winningCount} avgLoseSize=${losingTradeSum/losingCount} maxdrawdown=$maxDrawDown")
            return totalNetProfitPct
        }
    }

    var orderId = 1
    val emptyTradeList = listOf<Trade>()
    var prevBar: Bar? = null
    var prevPsar: Double? = null
    var prevOrder: Order? = null
    var completedTrades = mutableListOf<RoundTrip>()


    fun start(autoTradrParams: AutoTradrParams, printExtendedSummary: Boolean = false): Double {

        val psar =  with(autoTradrParams.parabolicSar) { ParabolicSar(startAF, increment, maxAF) }
        bars.forEach { bar ->
            val ext = bar.ext as MutableMap<String, Double>
            psar.onBar(bar, emptyTradeList, ext)
//            println(bar)

            prevPsar?.let {
                when {
                    it.isNaN() -> {
                        // pretend the entry didn't happen
                        prevOrder = null
//                        println("omg nan")
                    }
                    it >= bar.low && it <= bar.high -> {
                        // normal trade

                        val order = Order(
                                id = orderId.toString(),
                                mktSym = market.toString(),
                                type = OrderRequest.Type.MARKET,
                                direction = if (it < prevBar!!.low) Direction.SELL else Direction.BUY,
                                size = if (prevOrder != null) 2.0 else 1.0,
                                price = it,
                                filledQuantity = 2.0,
                                filledPrice = 2.0,
                                whenPlaced = bar.start - 1,
                                whenFilled = bar.start - 1,
                                state = Order.State.FILLED)
                        prevOrder?.let { po ->
                            completedTrades.add(RoundTrip(po, order))
//                            println("completed trade:  ${completedTrades.last()}")

                        }
                        prevOrder = order
                    }
                    else -> {}
                }
            }

            prevPsar = ext["psar"]
            prevBar = bar
        }
        val rv = summarize(autoTradrParams, completedTrades)
        if (printExtendedSummary) {
            summarizeExtended(autoTradrParams, completedTrades)
        }
        return rv
    }


}




fun main() = runBlocking {
    System.setProperty("logback.statusListenerClass", "ch.qos.logback.core.status.OnConsoleStatusListener")
    System.setProperty("logback.configurationFile", "logback-backtest.xml")
    BackTest.go()
}