package cointradr.clients.websocket

interface WebSocketClient {

    suspend fun connect()
    suspend fun onOpen()
    suspend fun onMessage(message: String)
    suspend fun onMessage(bytes: ByteArray)
    suspend fun onClosed()

}