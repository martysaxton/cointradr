package cointradr.clients.websocket

import cointradr.utils.loggingLaunch
import io.ktor.http.cio.websocket.Frame
import okhttp3.*
import okio.ByteString
import kotlinx.coroutines.*
import mu.KotlinLogging

abstract class AbstractWebsocketClientOkHttp(name: String, url: String): AbstractWebSocketClient(name, url) {

    companion object {
        val logger = KotlinLogging.logger {}
    }

    private var webSocket: WebSocket? = null

    override suspend fun connect() {

        webSocket?.let {
            it.cancel()
        }

        val client = OkHttpClient.Builder()
//                .callTimeout(30000, TimeUnit.MILLISECONDS)
//                .readTimeout(30000, TimeUnit.MILLISECONDS)
//                .writeTimeout(30000, TimeUnit.MILLISECONDS)
                .build()
        val request = Request.Builder()
                .url(url)
                .build()
        webSocket = client.newWebSocket(request, object : WebSocketListener() {
            override fun onOpen(webSocket: WebSocket?, response: Response?) {
                GlobalScope.loggingLaunch {
                    this@AbstractWebsocketClientOkHttp.onOpenInternal(response)
                }
            }

            override fun onMessage(webSocket: WebSocket?, text: String?) {
                GlobalScope.loggingLaunch {
                    text?.let { this@AbstractWebsocketClientOkHttp.onMessage(text) }
                }
            }

            override fun onMessage(webSocket: WebSocket?, bytes: ByteString?) {
                GlobalScope.loggingLaunch {
                    bytes?.let { this@AbstractWebsocketClientOkHttp.onMessage(bytes.toByteArray()) }
                }
            }

            override fun onClosing(webSocket: WebSocket?, code: Int, reason: String?) {
                this@AbstractWebsocketClientOkHttp.onClosingInternal(code, reason)
            }

            override fun onClosed(webSocket: WebSocket?, code: Int, reason: String?) {
                GlobalScope.loggingLaunch {
                    this@AbstractWebsocketClientOkHttp.onClosedInternal(code, reason)
                }
            }

            override fun onFailure(webSocket: WebSocket?, t: Throwable?, response: Response?) {
                this@AbstractWebsocketClientOkHttp.onFailureInernal(t, response)
            }
        })
    }

    private suspend fun onOpenInternal(response: Response?) {
        logger.debug { "websocket connected, response=$response" }
        onOpenInternal()
    }

    private fun onClosingInternal(code: Int, reason: String?) {
        logger.debug { "websocket closing, code=$code reason=$reason" }
    }

    private suspend fun onClosedInternal(code: Int, reason: String?) {
        logger.debug { "websocket closed, code=$code reason=$reason" }
        super.onClosed()
    }

    private fun onFailureInernal(t: Throwable?, response: Response?) {
        logger.warn(t) { "websocket onFailure response=$response" }
    }

    override suspend fun sendImpl(text: String) {
        webSocket?.send(text) ?: logger.error { "websocket not initialized" }
    }


}