package cointradr.clients.websocket

import io.ktor.client.HttpClient
import io.ktor.client.engine.cio.CIO
import io.ktor.client.features.websocket.WebSockets
import io.ktor.client.features.websocket.wss
import io.ktor.http.HttpMethod
import io.ktor.http.cio.websocket.*
import io.ktor.util.KtorExperimentalAPI
import kotlinx.coroutines.*
import mu.KotlinLogging
import org.apache.http.client.utils.URIBuilder
import java.time.Duration

abstract class AbstractWebSocketClientKtor(name: String, url: String): AbstractWebSocketClient(name, url) {

    companion object {
        val logger = KotlinLogging.logger {}
    }

    @io.ktor.util.KtorExperimentalAPI
    val client = HttpClient(engineFactory = CIO) {

    }.config { install(WebSockets) }
    lateinit var webSocketSession: DefaultWebSocketSession

    @KtorExperimentalAPI
    override suspend fun connect() {
        val u = URIBuilder(url)
        client.wss(method = HttpMethod.Get, host = u.host, port=u.port, path=u.path) {
            logger.info { "websocket connected" }
            webSocketSession = this
            with(webSocketSession){
                pingIntervalMillis = Duration.ofSeconds(45).toMillis()
                logger.info { "opened pingInterval=$pingIntervalMillis timeout=$pingIntervalMillis"}

            }
            this@AbstractWebSocketClientKtor.onOpenInternal()

            while (true) {
                GlobalScope.launch {
                    try {
                        val frame = incoming.receive()
                        when (frame) {
                            is Frame.Text -> onMessage(frame.readText())
                            is Frame.Binary -> onMessage(frame.readBytes())
                            is Frame.Close -> onClosedInternal(frame.readReason())
                        }
                    } catch (t: Throwable) {
                        logger.error("exception in $name.onMessage(text)", t)
                    }
                }
            }
        }
    }

    override suspend fun sendImpl(text: String) {
        webSocketSession.outgoing.send(Frame.Text(text))
    }

    private fun onClosedInternal(reason: CloseReason?) {
        logger.info { "websocket closed: ${reason?.message}" }
    }

}