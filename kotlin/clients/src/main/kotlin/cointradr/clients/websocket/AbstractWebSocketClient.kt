package cointradr.clients.websocket

import cointradr.utils.loggingLaunch
import kotlinx.coroutines.GlobalScope
import mu.KotlinLogging
import java.util.LinkedList

abstract class AbstractWebSocketClient(val name: String, protected val url: String): WebSocketClient {

    companion object {
        val logger = KotlinLogging.logger {}
    }

    @Volatile
    internal var isConnected = false

    protected fun getIsConnected(): Boolean { return isConnected }

    private var sendQueue = LinkedList<String>()

    protected open suspend fun onOpenInternal() {
        logger.info { "websocket connected" }
        isConnected = true
        while (!sendQueue.isEmpty()) {
            val message = sendQueue.poll()
            sendImpl(message)
        }

        GlobalScope.loggingLaunch {
            onOpen()
        }
    }

    open suspend fun send(text: String) {
        if (!isConnected) {
            sendQueue.add(text)
        } else {
            sendImpl(text)
        }
    }

    override suspend fun onClosed() {
        isConnected = false
        logger.info { "websocket closed, reconnecting" }
        connect()
    }

    protected abstract suspend fun sendImpl(text: String)

}