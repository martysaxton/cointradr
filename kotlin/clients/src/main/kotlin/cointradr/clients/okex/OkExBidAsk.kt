package cointradr.clients.okex


// TODO throw out amountCoin, cumulativeAmountCoin and cumulativeAmountContract
data class OkExBidAsk(
    val price: Double,
    val amountContract: Double,
    val amountCoin: Double,
    val cumulativeAmountCoin: Double,
    val cumulativeAmountContract: Double )
