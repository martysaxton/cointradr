package cointradr.clients.okex

import cointradr.secrets.Secrets
import com.okcoin.commons.okex.open.api.bean.futures.param.CancelOrders
import com.okcoin.commons.okex.open.api.bean.futures.param.Order
import com.okcoin.commons.okex.open.api.bean.futures.result.OrderResult
import com.okcoin.commons.okex.open.api.config.APIConfiguration
import com.okcoin.commons.okex.open.api.service.futures.FuturesTradeAPIService
import com.okcoin.commons.okex.open.api.service.futures.impl.FuturesTradeAPIServiceImpl
import io.ktor.client.HttpClient
import io.ktor.client.engine.apache.Apache
import io.ktor.client.request.get
import io.ktor.client.features.json.JacksonSerializer
import io.ktor.client.features.json.JsonFeature
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import org.koin.core.KoinComponent
import org.koin.core.inject
import kotlin.math.pow

//data class OkExFuturesBidAsk(
//        val price: Double,
//        val contracts: Int,
//        val forcedLiquidation: Int,
//        val numOrders: Int
//)

data class OkExBookResponse (
        val bids: List<List<Double>>,
        val asks: List<List<Double>>,
        val timestamp: String
)




class OkExRestClient: KoinComponent {

    companion object {
        val logger = KotlinLogging.logger {}
        private const val baseUrl = "https://www.okex.com/api"


        fun createApiConfig(): APIConfiguration {
            val secret = Secrets.getSecret("okex")
            val config = APIConfiguration()
            config.endpoint = "https://www.okex.com"
            config.apiKey = secret.apiKey
            config.secretKey = secret.secret
            config.passphrase = secret.passPhrase
            config.isPrint = false
            return config
        }
    }

    private val futuresTradingApi by inject<FuturesTradeAPIService>()
    

    val client = HttpClient(Apache) {
        install(JsonFeature) {
            serializer = JacksonSerializer()
        }
    }

    suspend fun getDepthFutures(base: String, quote: String, expiry: String, size: Int = 1) : OkExBookResponse {
        val marketStr = "${base.toUpperCase()}-${quote.toUpperCase()}-$expiry"
        val url = "$baseUrl/futures/v3/instruments/$marketStr/book?size=$size"
        val rv = client.get<OkExBookResponse>(url)
        return rv
    }

    fun getAccount(): Map<String,OkExAccount> {
        logger.debug { "getting accounts" }
        val okexClientResponse = futuresTradingApi.accounts
        val okexClientResponseStr = okexClientResponse.toJSONString()
        logger.debug { "deserializing account from $okexClientResponseStr" }
        val jsonNode = OkExStreamV3.mapper.readTree(okexClientResponseStr)
        val infoNode = jsonNode["info"]
        val account = OkExStreamV3.deserializeAccount(infoNode)
        logger.debug { "got accounts" }
        return account
    }

    fun getPositions(): List<OkExPosition> {
        logger.debug { "getting positions" }
        val okexClientResponse = futuresTradingApi.positions
        val okexClientResponseStr = okexClientResponse.toJSONString()
//        val xx = """{"result":true,"holding":[[{"long_qty":"0","long_avail_qty":"0","long_margin":"0","long_liqui_price":"0","long_pnl_ratio":"0.167","long_avg_cost":"3638.72","long_settlement_price":"3638.72","realised_pnl":"-0.00019218","short_qty":"1","short_avail_qty":"1","short_margin":"0.003","short_liqui_price":"4066.36","short_pnl_ratio":"-0.00037831805198673685","short_avg_cost":"3700.45","short_settlement_price":"3700.45","instrument_id":"BTC-USD-190329","long_leverage":"10","short_leverage":"10","created_at":"2019-01-04T13:42:23.0Z","updated_at":"2019-01-04T23:00:42.0Z","margin_mode":"fixed"}]]}"""
        logger.debug { "deserializing positions from $okexClientResponseStr" }
        val jsonNode = OkExStreamV3.mapper.readTree(okexClientResponseStr)
        val holding = jsonNode["holding"]
        val rv = mutableListOf<OkExPosition>()
        holding.forEach { it1 ->
            it1.forEach { it2 ->
                rv.add(OkExStreamV3.mapper.treeToValue(it2, OkExPosition::class.java))
            }
        }
        logger.debug { "got positions" }

        return rv
    }

    fun order(order: Order): OrderResult {
        order.price = order.price.roundToDecimals(2)
        return futuresTradingApi.order(order)
    }

    fun cancelOrders(instrumentId: String, orderIds: List<Long>) {
        if (!orderIds.isEmpty()) {
            val cancelOrders = CancelOrders().apply{ order_ids = orderIds }
            futuresTradingApi.cancelOrders(instrumentId, cancelOrders)
        }
    }

}

private fun Double.roundToDecimals(numDecimals: Int): Double? {
    val x = 10.0.pow(numDecimals)
    return Math.round(this * x) / x
}

fun main() = runBlocking {
    val client = OkExRestClient()
    try {
        val book = client.getDepthFutures("BTC", "USD", "190329")
        println(book.toString())
    } catch (t: Throwable){
        println(t)
    }
}
