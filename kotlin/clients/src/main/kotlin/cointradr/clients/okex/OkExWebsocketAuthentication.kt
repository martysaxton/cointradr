package cointradr.clients.okex

import com.okcoin.commons.okex.open .api.constant.APIConstants
import com.okcoin.commons.okex.open .api.enums.AlgorithmEnum
import com.okcoin.commons.okex.open .api.enums.CharsetEnum
import org.apache.commons.lang3.StringUtils

import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec
import java.io.UnsupportedEncodingException
import java.security.InvalidKeyException
import java.util.Base64
import java.security.NoSuchAlgorithmException






class OkExWebsocketAuthentication {

    companion object {
        private var MAC = Mac.getInstance(AlgorithmEnum.HMAC_SHA256.algorithm())
        fun getWebsocketLoginSignature(timestamp: String, secretKey: String): String {
            val preHash = "${timestamp}GET/users/self/verify"
            val secretKeyBytes = secretKey.toByteArray(charset(CharsetEnum.UTF_8.charset()))
            val secretKeySpec = SecretKeySpec(secretKeyBytes, AlgorithmEnum.HMAC_SHA256.algorithm())
            val mac = MAC.clone() as Mac
            mac.init(secretKeySpec)
            return Base64.getEncoder().encodeToString(mac.doFinal(preHash.toByteArray(charset(CharsetEnum.UTF_8.charset()))))
        }

    }
}