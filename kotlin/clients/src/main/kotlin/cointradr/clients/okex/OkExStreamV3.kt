package cointradr.clients.okex

import cointradr.secrets.Secrets
import cointradr.utils.loggingLaunch
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.databind.node.ObjectNode
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.soywiz.klock.DateTime
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import mu.KotlinLogging
import org.joda.time.format.ISODateTimeFormat


typealias OkExDepthHandler = suspend (book: OkExBook) -> Unit
typealias OkExTradeHandler = suspend (trades: List<OkExTrade>) -> Unit

const val OKEX_ORDER_FAILURE = "-2"
const val OKEX_ORDER_CANCELLED = "-1"
const val OKEX_ORDER_PENDING = "0"
const val OKEX_ORDER_PARTIALLYFILLED = "1"
const val OKEX_ORDER_FILLED = "2"

enum class FuturesTransactionTypeEnum(private val code: Int) {

    OPEN_LONG(1), OPEN_SHORT(2), CLOSE_LONG(3), CLOSE_SHORT(4);

    fun code(): Int {
        return code
    }

    companion object {
        fun from(value: Int): FuturesTransactionTypeEnum = FuturesTransactionTypeEnum.values().first { it.code == value }
        fun from(value: String) : FuturesTransactionTypeEnum = from(value.toInt())
    }
}


data class OkExV3BidAsk(val price: Double, val size: Double)

data class OkExOrder (
    val leverage: String,
    val size: String,
    val filled_qty: String,
    val price: String?,
    val fee: String,
    val contract_val: String,
    val price_avg: String,
    val type: String,
    val instrument_id: String,
    val order_id: String,
    val timestamp: String,
    val status: String
)

fun String.shortenTo(maxLength: Int): String {
    return if (length <= maxLength) {
        this
    } else {
        "${substring(0, maxLength)}..."
    }
}

// account
//{"BTC":{"contracts":[{"available_qty":"1.19015683","fixed_balance":"0.00274546","instrument_id":"BTC-USD-190329","margin_for_unfilled":"0.00273658","margin_frozen":"0.00273725","realized_pnl":"-0.00000821","unrealized_pnl":"0"}],"equity":"1.19563665","margin_mode":"fixed","total_avail_balance":"1.19289341"}}
//{
//    "BTC": {
//      "contracts": [
//        {
//          "available_qty": "1.19015683",
//          "fixed_balance": "0.00274546",
//          "instrument_id": "BTC-USD-190329",
//          "margin_for_unfilled": "0.00273658",
//          "margin_frozen": "0.00273725",
//          "realized_pnl": "-0.00000821",
//          "unrealized_pnl": "0"
//        }
//      ],
//      "equity": "1.19563665",
//      "margin_mode": "fixed",
//      "total_avail_balance": "1.19289341"
//    }
//}

data class OkExAccountContract(
        val available_qty: String,
        val fixed_balance: String,
        val instrument_id: String,
        val margin_for_unfilled: String,
        val margin_frozen: String,
        val realized_pnl: String,
        val unrealized_pnl: String
)


data class OkExAccount(
        val contracts: List<OkExAccountContract>,
        val equity: String,
        val margin_mode: String,
        val total_avail_balance: String
)

//position
//{
//    "long_qty": "1",
//    "long_avail_qty": "1",
//    "long_margin": "0.003",
//    "long_liqui_price": "3351.66",
//    "long_pnl_ratio": "0.002",
//    "long_avg_cost": "3653.3",
//    "long_settlement_price": "3653.3",
//    "realised_pnl": "-0.00000821",
//    "short_qty": "0",
//    "short_avail_qty": "0",
//    "short_margin": "0",
//    "short_liqui_price": "0",
//    "short_pnl_ratio": "-0",
//    "short_avg_cost": "0",
//    "short_settlement_price": "0",
//    "instrument_id": "BTC-USD-190329",
//    "long_leverage": "10",
//    "short_leverage": "0",
//    "created_at": "2019-01-04T13:42:23.000Z",
//    "updated_at": "2019-01-04T13:42:59.000Z",
//    "margin_mode": "fixed"
//}
data class OkExPosition(
    val long_qty: String,
    val long_avail_qty: String,
    val long_margin: String,
    val long_liqui_price: String,
    val long_pnl_ratio: String,
    val long_avg_cost: String,
    val long_settlement_price: String,
    val realised_pnl: String,
    val short_qty: String,
    val short_avail_qty: String,
    val short_margin: String,
    val short_liqui_price: String,
    val short_pnl_ratio: String,
    val short_avg_cost: String,
    val short_settlement_price: String,
    val instrument_id: String,
    val long_leverage: String,
    val short_leverage: String,
    val created_at: String,
    val updated_at: String,
    val margin_mode: String
)

class OkExBook {
    val logger = KotlinLogging.logger {}

    data class OkExBookEntry(val price: Double, val size: Double)
    private var bids = mutableMapOf<Double, Double>()
    private var asks = mutableMapOf<Double, Double>()

    private var bestBid: Double = Double.NaN
    private var bestAsk: Double = Double.NaN
    private var mutex = Mutex()

    data class BestBidAsk(val bestBid: Double, val bestAsk: Double)

    suspend fun getBestBidAsk(): BestBidAsk {
        mutex.withLock {
            return BestBidAsk(bestBid, bestAsk)
        }
    }

    suspend fun replace(bids: List<OkExV3BidAsk>, asks: List<OkExV3BidAsk>) {
        mutex.withLock {
            this.bids = mutableMapOf<Double, Double>()
            bestBid = Double.MIN_VALUE
            bestAsk = Double.MAX_VALUE
            bids.forEach {
                this.bids[it.price] = it.size
                bestBid = maxOf(bestBid, it.price)
            }
            this.asks = mutableMapOf<Double, Double>()
            asks.forEach {
                bestAsk = minOf(bestAsk, it.price)
                this.asks[it.price] = it.size
            }
            logger.debug { "after replace new best bid=$bestBid ask=$bestAsk" }

        }
    }

    private fun updateBid(price: Double, size: Double) {
//        logger.debug { "updating bids ($price $size) best bid=$bestBid" }
        if (size == 0.0) {
            val removed = bids.remove(price)
            if (removed == null) {
                logger.debug { "price $price was not present" }
            } else if (bestBid == price) {
                // search for new best bid
                bestBid = Double.MIN_VALUE
                bids.forEach { bestBid = maxOf(it.key, bestBid) }
                logger.debug { "after removal new best bid $bestBid" }
            }
        } else {
            bids[price] = size
            if (price > bestBid) {
                bestBid = price
                logger.debug { "new best bid $bestBid" }
            }
        }
    }

    private fun updateAsk(price: Double, size: Double) {
//        logger.debug { "updating asks ($price $size) best ask=$bestAsk" }
        if (size == 0.0) {
            val removed = asks.remove(price)
            if (removed == null) {
                logger.debug { "price $price was not present" }
            } else if (bestAsk == price) {
                // search for new best ask
                bestAsk = Double.MAX_VALUE
                asks.forEach { bestAsk = minOf(it.key, bestAsk) }
                logger.debug { "after removal new best ask $bestAsk" }
            }
        } else {
            asks[price] = size
            if (price < bestAsk) {
                bestAsk = price
                logger.debug { "new best ask $bestAsk" }
            }
        }
    }


    suspend fun update(bids: List<OkExV3BidAsk>, asks: List<OkExV3BidAsk>) {
        mutex.withLock {
            bids.forEach { updateBid(it.price, it.size) }
            asks.forEach { updateAsk(it.price, it.size) }
        }
    }


}


class OkExStreamV3 : AbstractOkExStream("wss://real.okex.com:10442/ws/v3") {

    companion object {
        val mapper = ObjectMapper().registerModule(KotlinModule())!!
        val logger = KotlinLogging.logger {}
        fun deserializeAccount(it: JsonNode): Map<String,OkExAccount> {
            val rv = mutableMapOf<String, OkExAccount>()
            var contracts = listOf<OkExAccountContract>()
            var equity = ""
            var margin_mode = ""
            var total_avail_balance = ""
            it.fields().forEach { entry ->
                var currency = entry.key.toLowerCase()
                entry.value.fields().forEach {
                    val fieldName = it.key
                    val s = it.value.textValue()
                    when (fieldName) {
                        "equity" -> equity = s
                        "margin_mode" -> margin_mode = s
                        "total_avail_balance" -> total_avail_balance = s
                        "contracts" -> {
                            contracts = it.value.map { c -> mapper.treeToValue(c, OkExAccountContract::class.java)  }
                        }
                    }
                }
                rv[currency] = OkExAccount(
                        contracts = contracts,
                        equity =  equity,
                        margin_mode = margin_mode,
                        total_avail_balance = total_avail_balance)
            }

            return rv
        }
    }

//    init {
//        runBlocking {
//            connect()
//        }
//    }




    enum class SubscriptionState {
        IDLE,
        SUBSCRIBED,
        UNSUBSCRIBING,
        SUBSCRIBING
    }

    interface AccountObserver {
        suspend fun onOrders(orders: List<OkExOrder>)
        suspend fun onAccounts(accounts: Map<String, OkExAccount>)
        suspend fun onPositions(positions: List<OkExPosition>)
    }

    private val accountObservers = mutableListOf<AccountObserver>()
    private var timePingSent = 0L
    @Volatile
    private var pongReceived = false
    private val book = OkExBook()
    private val depthMutex = Mutex()
    private val subscriptionMutex = Mutex()
//    2019-01-03T06:00:00.086
    private val isoDateTimeFormatter = ISODateTimeFormat.dateTime()
    private var lastDepthTimestamp = org.joda.time.DateTime.now()
    private var doLoginWhenOpen = false
    private val subscriptions = mutableMapOf<String, SubscriptionState>()

    fun addObserver(observer: AccountObserver) {
        accountObservers.add(observer)
    }

    suspend fun subscribe(channel: String) {
        subscriptionMutex.withLock {
            val previousState = subscriptions.put(channel, SubscriptionState.IDLE)
            if (previousState == null) {
                sendSubscription(channel)
            }
        }
    }

    private suspend fun sendSubscription(channel: String) {
        if (getIsConnected()) {
            send("""{"op":"subscribe", "args": ["$channel"]}""")
            subscriptions[channel] = SubscriptionState.SUBSCRIBING
        }

    }

    private suspend fun sendUnsubscription(channel: String) {
        send("""{"op":"unsubscribe", "args": ["$channel"]}""")
        subscriptions[channel] = SubscriptionState.UNSUBSCRIBING
    }

    override suspend fun onOpen() {
//        send("{\"op\": \"subscribe\", \"args\": [\"futures/trade:BTC-USD-190329\"]}")
//        send("{\"op\": \"subscribe\", \"args\": [\"futures/depth:BTC-USD-190329\"]}")
        if (doLoginWhenOpen) {
            login()
        }
        subscriptionMutex.withLock {
            subscriptions.forEach { (channel, _) -> sendSubscription(channel) }
        }
    }

    suspend fun login() {
        if (!getIsConnected()) {
            doLoginWhenOpen = true
        } else {
            val secret = Secrets.getSecret("okex")
            val time = (DateTime.nowUnixLong() / 1000).toString()
            val sign = OkExWebsocketAuthentication.getWebsocketLoginSignature(time, secret.secret)
            val message = """
                {
                    "op":"login",
                    "args":[
                        "${secret.apiKey}",
                        "${secret.passPhrase}",
                        "$time",
                        "$sign"
                    ]}
            """.trimIndent()
            send(message)
        }
    }


    override suspend fun onMessage(message: String) {
        logger.debug { "got message:  ${message.shortenTo(200)}" }
        val m = message.trimStart()
        when {
            m == "pong" -> handlePong()
            m.startsWith('{') -> handleJson(m)
        }
    }


    private fun handlePong() {
        pongReceived = true
    }

    // {"table":"futures/order","data":[{"leverage":"10","size":"1","filled_qty":"0","price":"3654.2","fee":"0","contract_val":"100","price_avg":"0","type":"1","instrument_id":"BTC-USD-190329","order_id":"2088853822520320","timestamp":"2019-01-04T13:42:59.861Z","status":"0"}]}
    // {"table":"futures/account","data":[{"BTC":{"contracts":[{"available_qty":"1.19015683","fixed_balance":"0.00274546","instrument_id":"BTC-USD-190329","margin_for_unfilled":"0.00273658","margin_frozen":"0.00273725","realized_pnl":"-0.00000821","unrealized_pnl":"0"}],"equity":"1.19563665","margin_mode":"fixed","total_avail_balance":"1.19289341"}}]}
    // {"table":"futures/position","data":[{"long_qty":"1","long_avail_qty":"1","long_margin":"0.003","long_liqui_price":"3351.66","long_pnl_ratio":"0.002","long_avg_cost":"3653.3","long_settlement_price":"3653.3","realised_pnl":"-0.00000821","short_qty":"0","short_avail_qty":"0","short_margin":"0","short_liqui_price":"0","short_pnl_ratio":"-0","short_avg_cost":"0","short_settlement_price":"0","instrument_id":"BTC-USD-190329","long_leverage":"10","short_leverage":"0","created_at":"2019-01-04T13:42:23.000Z","updated_at":"2019-01-04T13:42:59.000Z","margin_mode":"fixed"}]}
    // {"table":"futures/account","data":[{"BTC":{"total_avail_balance":"1.19014862","contracts":[{"available_qty":"1.19014862","fixed_balance":"0.00549025","instrument_id":"BTC-USD-190329","margin_for_unfilled":"0","margin_frozen":"0.00547383","realized_pnl":"-0.00001642","unrealized_pnl":"0"}],"equity":"1.19562769","margin_mode":"fixed"}}]}
    // {"table":"futures/position","data":[{"long_qty":"2","long_avail_qty":"2","long_margin":"0.005","long_liqui_price":"3352.11","long_pnl_ratio":"0.0009579798582415311","long_avg_cost":"3653.75","long_settlement_price":"3653.75","realised_pnl":"-0.00001642","short_qty":"0","short_avail_qty":"0","short_margin":"0","short_liqui_price":"0","short_pnl_ratio":"-0","short_avg_cost":"0","short_settlement_price":"0","instrument_id":"BTC-USD-190329","long_leverage":"10","short_leverage":"0","created_at":"2019-01-04T13:42:23.000Z","updated_at":"2019-01-04T13:42:59.000Z","margin_mode":"fixed"}]}
    // {"table":"futures/order","data":[{"leverage":"10","size":"1","filled_qty":"1","price":"3654.2","fee":"-0.00000821","contract_val":"100","price_avg":"3654.2","type":"1","instrument_id":"BTC-USD-190329","order_id":"2088853822520320","timestamp":"2019-01-04T13:42:59.000Z","status":"2"}]}
    private suspend fun handleJson(message: String) {
        val jsonNode = mapper.readTree(message)
        val obj = jsonNode as ObjectNode
        val table = obj["table"]
        if (table != null) {
            val tableStr= table.textValue()
            when (tableStr) {
                "futures/trade" -> handleTrades(obj["data"]!!)
                "futures/depth" -> handleDepth(obj)
                "futures/order" -> handleOrder(obj)
                "futures/account" -> handleAccount(obj)
                "futures/position" -> handlePosition(obj)
            }
        }
        val event = obj["event"]
        if (event != null) {
            handleEvent(obj)
        }
    }


    private suspend fun handleEvent(obj: ObjectNode) {
        val event = obj["event"].textValue()
        logger.debug { "event $event" }
        when (event) {
            "login" -> {
                val success = obj["success"]?.booleanValue() ?: false
                onLogin(success)
            }
            "subscribe" -> {
                val channel = obj["channel"].textValue()
                logger.info { "subscribed to $channel" }
                subscriptionMutex.withLock {
                    subscriptions[channel] = SubscriptionState.SUBSCRIBED
                }
            }
            "unsubscribe" -> {
                val channel = obj["channel"].textValue()
                logger.info { "unsubscribed to $channel" }
                subscriptionMutex.withLock {
                    subscriptions[channel] = SubscriptionState.IDLE
                    sendSubscription(channel)
                }
            }
            "error" -> {
                val message = obj["message"].textValue()
                val errorCode = obj["errorCode"].intValue()
                onErrorEvent(message, errorCode)

            }
        }
    }

    private fun onErrorEvent(message: String?, errorCode: Int) {
        logger.warn { "error code=$errorCode message: $message" }
    }

    private suspend fun onLogin(success: Boolean) {
        logger.info("onLogin success=$success")

        val marketSymbol = listOf("BTC-USD-190329")
        val marketSubs = listOf("position", "order")

        val currencies = listOf("BTC", "USD")

        marketSubs.forEach { sub ->
            marketSymbol.forEach { symb ->
                val message = """{"op": "subscribe", "args": ["futures/$sub:$symb"]}"""
                send(message)
            }
        }

        currencies.filter { it != "USD" }.forEach {
            val message = """{"op": "subscribe", "args": ["futures/account:$it"]}"""
            send(message)
        }

    }

    private suspend fun handleDepth(jsonNode: JsonNode) {
        depthMutex.withLock {
            logger.debug { "handling depth" }
            val data = jsonNode["data"] as ArrayNode
            val action = jsonNode["action"]
            data.forEach { depthElement ->
                sanityCheckTimestamp(depthElement)
                val instrumentId = depthElement["instrument_id"]
                val bidNodes = depthElement["bids"] as ArrayNode
                val askNodes = depthElement["asks"] as ArrayNode

                logger.debug { "got partial update depth for $instrumentId" }
                val bids = bidNodes.map { OkExV3BidAsk(it[0].doubleValue(), it[1].doubleValue()) }
                val asks = askNodes.map { OkExV3BidAsk(it[0].doubleValue(), it[1].doubleValue()) }
                when (action.textValue()) {
                    "partial" -> book.replace(bids, asks)
                    "update" -> {
                        book.update(bids, asks)
                        val bba = book.getBestBidAsk()
                        if (bba.bestBid > bba.bestAsk) {
                            subscriptionMutex.withLock {
                                val channel = "futures/depth:${instrumentId.textValue()}"
                                val currentState = subscriptions[channel]
                                if (currentState == SubscriptionState.SUBSCRIBED) {
                                    logger.debug { "bestBid > bestAsk, resetting channel $channel" }
                                    sendUnsubscription(channel)
                                }
                            }
                        }
                    }
                }
//                printBestBidAsk()
                broadcastDepth(book)
            }
        }
    }

    private fun sanityCheckTimestamp(depthElement: JsonNode) {
        val timestampStr = depthElement["timestamp"].textValue()
        val thisDateTime = isoDateTimeFormatter.parseDateTime(timestampStr)
        if (thisDateTime.isBefore(lastDepthTimestamp)) {
            logger.warn { "depth timestamp out of order:  prev=$lastDepthTimestamp  this=$thisDateTime" }
        } else {
            logger.debug { "depth timestamp is after previous" }
        }
        lastDepthTimestamp = thisDateTime

    }

    private suspend fun handleTrades(jsonNode: JsonNode) {
        if (jsonNode.isArray) {
            var now = DateTime.nowUnixLong()
            val data = jsonNode as ArrayNode
            var trades = data.map {
                OkExTrade(

                    t = now,
                    market =  OkExMarket.constructFromV3Symbol(it["instrument_id"].textValue()),
                    id = it["trade_id"].textValue(),
                    price = it["price"].doubleValue(),
                    size = it["qty"].doubleValue(),
                    bid = it["side"].textValue() == "buy"
                )
            }
            tradeHandlers.forEach { it(trades) }
//            printTrades(trades)
        } else {
            logger.warn("don't know how to handle:  $jsonNode")
        }


    }

    private suspend fun printTrades(trades: List<OkExTrade>) {
        val bba = book.getBestBidAsk()
        trades.forEach {
            when {
                it.price < bba.bestBid -> println( "t=${formatUsd(it.price)} bb=${formatUsd(bba.bestBid)} ba=${formatUsd(bba.bestAsk)} wtf")
                it.price > bba.bestAsk -> println( "bb=${formatUsd(bba.bestBid)} ba=${formatUsd(bba.bestAsk)} t=${formatUsd(it.price)} wtf")
                else -> println( "bb=${formatUsd(bba.bestBid)} t=${formatUsd(it.price)} ba=${formatUsd(bba.bestAsk)}")
            }
        }
    }

    private fun formatUsd(double: Double): String {
        return "%.2f".format(double)
    }

    private suspend fun handlePosition(obj: ObjectNode) {
        val positionUpdates = mutableListOf<OkExPosition>()
        obj["data"].forEach {
            if (it.size() > 0) {
                val positionUpdate = mapper.treeToValue(it, OkExPosition::class.java)
                logger.debug { "position update: $positionUpdate" }
                positionUpdates.add(positionUpdate)
            }
        }
        accountObservers.forEach {
            it.onPositions(positionUpdates)
        }
    }

    private suspend fun handleAccount(obj: ObjectNode) {
        var accounts = deserializeAccount(obj["data"].first())
        accountObservers.forEach {
            it.onAccounts(accounts)
        }
    }

    private suspend fun handleOrder(obj: ObjectNode) {
        val orderUpdates = obj["data"].map {
            val orderUpdate = mapper.treeToValue(it, OkExOrder::class.java)
            logger.debug { "order update: $orderUpdate" }
            orderUpdate
        }
        accountObservers.forEach {
            it.onOrders(orderUpdates)
        }
    }


    init {
        GlobalScope.loggingLaunch {
            while(true) {
                delay(15000)
                if (getIsConnected()) {
                    pongReceived = false
                    send("ping")
                    logger.debug("sent ping")
                    timePingSent = DateTime.nowUnixLong()
                    delay(5000)
                    if (!pongReceived) {
                        connect()
                    }
                }
            }
        }
    }

}

val tradeHandler :OkExTradeHandler = { trades ->
    println("got trades $trades")
}


fun main() = runBlocking {
    System.setProperty("logback.statusListenerClass", "ch.qos.logback.core.status.OnConsoleStatusListener")
    System.setProperty("logback.configurationFile", "logback-eraseme.xml")

    val stream = OkExStreamV3()

//    s.login()
    stream.addTradeHandler(tradeHandler)
    stream.connect()
//    s.onMessage("""{"table":"futures/order","data":[{"leverage":"10","size":"1","filled_qty":"0","price":"3638.72","fee":"0","contract_val":"100","price_avg":"0","type":"1","instrument_id":"BTC-USD-190329","order_id":"2089529804213248","timestamp":"2019-01-04T16:34:54.523Z","status":"0"}]}""")
//    s.onMessage("""{"table":"futures/position","data":[{}]}""")
//    s.onMessage("""{"table":"futures/position","data":[{"long_qty":"1","long_avail_qty":"1","long_margin":"0.003","long_liqui_price":"3338.35","long_pnl_ratio":"0.00008244589488226595","long_avg_cost":"3638.72","long_settlement_price":"3638.72","realised_pnl":"-0.00009561","short_qty":"0","short_avail_qty":"0","short_margin":"0","short_liqui_price":"0","short_pnl_ratio":"-0","short_avg_cost":"0","short_settlement_price":"0","instrument_id":"BTC-USD-190329","long_leverage":"10","short_leverage":"0","created_at":"2019-01-04T13:42:23.000Z","updated_at":"2019-01-04T16:34:58.000Z","margin_mode":"fixed"}]}""")
//    s.onMessage("""{"table":"futures/account","data":[{"BTC":{"total_avail_balance":"1.19279504","contracts":[{"available_qty":"1.19279504","fixed_balance":"0.00284383","instrument_id":"BTC-USD-190329","margin_for_unfilled":"0","margin_frozen":"0.00274822","realized_pnl":"-0.00009561","unrealized_pnl":"0"}],"equity":"1.19554349","margin_mode":"fixed"}}]}""")
    delay(Long.MAX_VALUE)

}