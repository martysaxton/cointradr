package cointradr.clients.okex

data class OkExTrade(
        val t: Long,
        val market: OkExMarket,
        val id: String,
        val price: Double,
        val size: Double,
        val bid: Boolean
)