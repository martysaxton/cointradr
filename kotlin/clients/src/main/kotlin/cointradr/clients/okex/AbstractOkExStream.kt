package cointradr.clients.okex

import cointradr.clients.websocket.AbstractWebSocketClientKtor
import cointradr.clients.websocket.AbstractWebsocketClientOkHttp
import mu.KotlinLogging
import org.apache.commons.compress.compressors.deflate64.Deflate64CompressorInputStream
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.IOException

abstract class AbstractOkExStream(url: String) :  AbstractWebsocketClientOkHttp("okex", url) {

    companion object {
        val logger = KotlinLogging.logger {}
    }
    protected val depthHandlers =  mutableListOf<OkExDepthHandler>()
    protected val tradeHandlers =  mutableListOf<OkExTradeHandler>()

    fun addDepthHandler(depthHandler: OkExDepthHandler) {
        depthHandlers.add(depthHandler)
    }

    fun addTradeHandler(tradeHandler: OkExTradeHandler) {
        tradeHandlers.add(tradeHandler)
    }

    protected suspend fun broadcastDepth(book: OkExBook) {
        depthHandlers.forEach { it(book) }
    }

    override suspend fun onMessage(bytes: ByteArray) {
        val message = uncompress(bytes)
        onMessage(message)
    }

    private fun uncompress(bytes: ByteArray): String {
        try {
            ByteArrayOutputStream().use { out ->
                ByteArrayInputStream(bytes).use { `in` ->
                    Deflate64CompressorInputStream(`in`).use { zin ->
                        val buffer = ByteArray(1024)
                        while (true) {
                            val length = zin.read(buffer)
                            if (length == -1) {
                                break
                            }
                            out.write(buffer, 0, length)
                        }
                        return out.toString()
                    }
                }
            }
        } catch (e: IOException) {
            throw RuntimeException(e)
        }

    }

}