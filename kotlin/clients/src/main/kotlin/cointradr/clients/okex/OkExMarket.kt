package cointradr.clients.okex

data class OkExMarket(val base: String, val quote: String, val futureExpiry: String? = null) {
    companion object {
        fun constructFromChannelParts(parts: List<String>): OkExMarket {
            var quote = parts[2]
            val base = parts[3]
            val futureExpiry = if (quote.startsWith("future")) {
                quote = quote.substring(6)
                if (parts.size == 6) parts[5] else "${parts[5]}_${parts[6]}"
            } else null
            return OkExMarket(base, quote, futureExpiry)
        }
        fun constructFromV3Symbol(symbol: String): OkExMarket {
            var parts = symbol.toLowerCase().split('-')
            return OkExMarket(base=parts[0], quote=parts[1], futureExpiry = if (parts.size == 3) parts[2] else null)
        }
    }
}
