package cointradr.clients.bitforex


import io.ktor.client.*
import io.ktor.client.engine.apache.*
import io.ktor.client.features.json.*
import io.ktor.client.request.*
import mu.KotlinLogging

data class Model(val name: String, val items: List<Item>)
data class Item(val key: String, val value: String)

data class Kline(
        val close: Double,
        val currencyVol: Double,
        val high: Double,
        val low: Double,
        val open: Double,
        val time: Long,
        val vol: Double
)

data class Ticker(
        val date: Long,
        val buy: Double,
        val sell: Double,
        val high: Double,
        val low: Double,
        val vol: Double,
        val last: Double
)

data class BitForexResponse<T>(
        val data: T?,
        val success: Boolean,
        val time: Long?,
        val code: String?,
        val message: String?)


data class BidAsk(val amount: Double, val price: Double)

data class Depth(val asks: List<BidAsk>, val bids: List<BidAsk>)

data class Trade(
    val amount: Double,
    val direction: Int,
    val price: Double,
    val tid: Long,
    val time: Long
)

class BitForexClient {

    private val logger = KotlinLogging.logger {}

    val baseUrl = "https://api.bitforex.com/api/v1/market"
    val client = HttpClient(Apache) {
            install(JsonFeature) {
                serializer = JacksonSerializer()
            }
        }


    suspend fun getKlines(symbol: String, ktype: String, size: Int): BitForexResponse<List<Kline>> {
        logger.debug { "getting klines symbol=${symbol} ktype=${ktype} size=${size}"}
        val queryParams = mapOf("symbol" to symbol, "ktype" to ktype, "size" to size)
        val uri = getUrl("kline", queryParams)
        return client.get(uri)
    }
    suspend fun getTicker(symbol: String): BitForexResponse<Ticker> {
        logger.debug { "getting ticker symbol=${symbol}"}
        val queryParams = mapOf("symbol" to symbol)
        val uri = getUrl("ticker", queryParams)
        return client.get(uri)
    }

    suspend fun getDepth(symbol: String, size: Int): BitForexResponse<Depth> {
        logger.debug { "getting depth symbol=${symbol} size=${size}"}
        val queryParams = mapOf("symbol" to symbol, "size" to size)
        val uri = getUrl("depth", queryParams)
        return client.get(uri)
    }

    suspend fun getTrades(symbol: String, size: Int): BitForexResponse<List<Trade>> {
        logger.debug { "getting trades symbol=${symbol} size=${size}"}
        val queryParams = mapOf("symbol" to symbol, "size" to size)
        val uri = getUrl("trades", queryParams)
        return client.get(uri)
    }

    private fun getUrl(path: String, params: Map<String, Any>): String {
        val query = params.map{ "${it.key}=${it.value}" }.joinToString( "&" )
        return "$baseUrl/$path?$query"
    }

}



