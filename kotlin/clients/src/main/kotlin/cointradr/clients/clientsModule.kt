package cointradr.clients

import cointradr.clients.okex.OkExRestClient
import cointradr.clients.okex.OkExStreamV3
import com.okcoin.commons.okex.open.api.service.futures.FuturesTradeAPIService
import com.okcoin.commons.okex.open.api.service.futures.impl.FuturesTradeAPIServiceImpl
import org.koin.core.module.Module

fun Module.addClientsBeans() {
    single { OkExRestClient() }
    single<FuturesTradeAPIService> { FuturesTradeAPIServiceImpl(OkExRestClient.createApiConfig()) }
    single { OkExStreamV3() }
}

