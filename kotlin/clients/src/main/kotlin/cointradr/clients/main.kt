package cointradr.clients

import cointradr.clients.bitforex.BitForexClient
import cointradr.clients.bitforex.Trade
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import mu.KotlinLogging
import java.text.SimpleDateFormat


fun main(args: Array<String>) = runBlocking<Unit> {

    val logger = KotlinLogging.logger {}
    val uniqueTrades = LinkedHashSet<Trade>()

    val client = BitForexClient()
//    val dateFormatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm") //.withZone(ZoneOffset.UTC)
    val symbol = "coin-usdt-btc"

    val mutex = Mutex()

//    while(true) {
//        try {
//            val x = client.getDepth(symbol, 1)
//            if (x.success) {
//                x.data?.let {
//                    while (true) {
//                        val trades = client.getTrades(symbol, 100)
//                        if (!trades.success) {
//                            logger.debug("trades failed after depth succeeded")
//                            delay(200)
//                        }
//                        else {
//                            logger.info { "bid=${it.bids[0]} ask=${it.asks[0]}" }
//                            trades.data?.let {
//                                it.forEach {
//                                    if (uniqueTrades.add(it)) {
//                                        logger.info { "    trade ${it}" }
//                                        while (uniqueTrades.size > 500) {
//                                             uniqueTrades.iterator().remove()
//                                        }
//                                    }
//                                }
//                            }
//                            break;
//                        }
//                    }
//                }
//            } else {
//                if (x.code != null && x.code == "10204") {
//                    logger.debug{ "fail code=${x.code} message=${x.message}" }
//                } else {
//                    logger.info { "fail code=${x.code} message=${x.message}" }
//                }
//            }
//
//        } catch( t: Throwable) {
//            logger.error { "exception $t" }
//        }
//        delay(100)
//    }

    while (true) {
        val trades = client.getTrades(symbol, 400)
        if (!trades.success) {
            logger.debug("trades failed")
            delay(1000)
        } else {
            trades.data?.let {
                it.forEach { trade ->
                    if (uniqueTrades.add(trade)) {
//                        logger.info { "    trade ${it}" }
                        val bidAskStr = if (trade.direction == 1) "ask" else "bid"
                        val price = "%.2f".format(trade.price)
                        logger.info { "$bidAskStr $price" }
                        mutex.withLock {
                            while (uniqueTrades.size > 500) {
                                uniqueTrades.iterator().remove()
                            }
                        }
                    }
                }
            }
            delay(1000)
        }
    }

}

