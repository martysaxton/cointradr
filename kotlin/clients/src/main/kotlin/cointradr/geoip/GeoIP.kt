package cointradr.geoip

import cointradr.secrets.Secrets
import java.net.InetAddress
import com.maxmind.geoip2.DatabaseReader
import io.ktor.client.HttpClient
import io.ktor.client.call.call
import io.ktor.client.engine.apache.Apache
import io.ktor.client.response.readText
import kotlinx.coroutines.runBlocking


// download updates from https://geolite.maxmind.com/download/geoip/database/GeoLite2-Country.tar.gz

class GeoIP {
    companion object {
        val reader: DatabaseReader
        val client = HttpClient(Apache)

        init {
            val inputStream = Secrets::class.java.classLoader.getResourceAsStream("GeoLite2-Country.mmdb")
            reader = DatabaseReader.Builder(inputStream).build()
        }

        fun getCountry(ipAddress: String): String {
            if (ipAddress == "69.88.134.93") {
                return "HK"
            }
            val ipAddr = InetAddress.getByName(ipAddress)
            val x = reader.country(ipAddr)
            return x.country.isoCode
        }

        suspend fun getMyCountry(): String {
            val myIpResponse = client.call("https://api.ipify.org")
            val myIpAddr = myIpResponse.response.readText()
            val xx = myIpAddr.trim('/')
            return getCountry(myIpAddr)
        }

    }
}

fun main() = runBlocking {
    println(GeoIP.getMyCountry())
}