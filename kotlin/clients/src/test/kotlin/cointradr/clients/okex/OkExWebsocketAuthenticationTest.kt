
package cointradr.clients.okex

import kotlin.test.Test
import kotlin.test.assertEquals

class OkExWebsocketAuthenticationTest {

    @Test
    fun testAuthSig() {
        val sign = OkExWebsocketAuthentication.getWebsocketLoginSignature("1538054050", "A564JASDF4345ASDFGLJEA345LIKJHMNB")
        assertEquals(sign, "RK0hi2XUW5Kbq6/GkOtjIEzwfnAOd+sQqKgBpQCRKOQ=")
    }

}