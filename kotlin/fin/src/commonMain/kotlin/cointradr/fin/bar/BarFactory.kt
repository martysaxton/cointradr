package cointradr.fin.bar

import cointradr.fin.model.Bar
import cointradr.fin.model.Trade
import cointradr.utils.currentTimeMillis
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import mu.KotlinLogging

typealias BarFactoryObserver = suspend (Bar) -> Unit
typealias BarFactoryStateChangeObserver = () -> Unit

typealias TradeExtension = (Trade, MutableMap<String, Double>) -> Unit
typealias BarExtension = (bar: Bar, tradeList: List<Trade>, ext: MutableMap<String, Double>) -> Unit

interface BarFactoryExtension {
    val onBar : BarExtension?
    val onTrade: TradeExtension?
}

class BarFactory(val exch: String, val symb: String, val duration: Int, val timeOffset: Int = 0) {

    private val logger = KotlinLogging.logger {}

    private val observers = mutableListOf<BarFactoryObserver>()
    private val barExtensions = mutableListOf<BarExtension>()
    private val tradeExtensions = mutableListOf<TradeExtension>()

    // to keep trades unique
    private val tradeSet = mutableSetOf<Trade>()

    // to keep trades in order of arrival
    private val tradeList = mutableListOf<Trade>()

    private var open = 0.0
    private var high = 0.0
    private var low = 0.0
    private var close = 0.0
    private var vol = 0.0
    private var ext = HashMap<String, Double>()
    private var start = 0L
    private var liveMode = false
    private var lastBarSeenStart: Long = 0

    fun enableLiveMode() {
        if (!liveMode) {
            liveMode = true
            startTimer()
        }
    }

    fun setLastBarSeen(bar: Bar) {
        lastBarSeenStart = bar.start
        logger.debug { "setting lastBarSeenStart=$lastBarSeenStart" }
    }

    fun addObserver( observer: BarFactoryObserver ) {
        observers.add(observer)
    }

    fun addIndicator(extension: BarFactoryExtension ) {
        extension.onBar?.let { barExtensions.add(it) }
        extension.onTrade?.let { tradeExtensions.add(it) }
    }

    val addTrade: suspend(Trade, Boolean) -> Unit = { trade: Trade, isRealTime: Boolean ->
        if (!liveMode) {
            addTradeBacktest(trade)
        } else {
            addTradeInternal(trade)
        }
        if (isRealTime) {
            enableLiveMode()
        }
    }

    private suspend fun addTradeBacktest(trade: Trade) {
        val thisStart = getBarStart(trade.t)
        if (thisStart != start) {
            barComplete()
            start = thisStart
        }
        addTradeInternal(trade)
    }

    private fun addTradeInternal(trade: Trade) {
        if (lastBarSeenStart != 0L && trade.t < lastBarSeenStart + duration) {
            logger.debug{ "ignoring already seen trade: $trade" }
            return
        }
        logger.debug { "add trade ${trade.exch} ${trade.symb} ${trade.t} ${trade.size}@${trade.price} "}
        val empty = isEmpty()
        if (tradeSet.add(trade)) {
            tradeList.add(trade)
            if (empty) {
                open = trade.price
                high = trade.price
                low = trade.price
                vol = trade.size
            } else {
                high = maxOf(high, trade.price)
                low = minOf(low, trade.price)
                vol += trade.size
            }
            close = trade.price
        }
        logger.debug { "calling trade extensions"}
        tradeExtensions.forEach { it(trade, ext) }
    }



    fun getBarStart(time: Long): Long {
        val timeMinusOffset = time - timeOffset
        return (timeMinusOffset / duration ) * duration  + timeOffset
    }

    /**
     * Tell the BarFactory that we're done.  Used in back test mode and unit tests
     */
    suspend fun done() {
        if (!isEmpty()) {
            barComplete()
        }
    }

    private suspend fun barComplete() {
        if (!isEmpty()) {
            val bar = Bar(exch, symb, start, duration, open, high, low, close, vol, ext)
            logger.debug { "calling bar extensions"}
            barExtensions.forEach { it(bar, tradeList, ext) }
            logger.debug { "calling bar observers"}
            observers.forEach { it(bar) }
            tradeSet.clear()
            tradeList.clear()
        }
    }

    private fun isEmpty(): Boolean {
        return tradeSet.isEmpty()
    }

    private fun startTimer() {
        GlobalScope.launch {
            while (true) {
                try {
                    var now = currentTimeMillis()
                    val thisStart = getBarStart(now)
                    val thisEnd = thisStart + duration
                    while (now < thisEnd) {
                        val d = thisEnd - now
                        logger.debug { "sleeping now=$now d=$d thisStart=$thisStart thisEnd=$thisEnd" }
                        delay(d)
                        now = currentTimeMillis()
                    }
                    barComplete()
                    start = thisEnd
                } catch(t: Throwable) {
                    logger.error(t) { "exception in timer loop" }
                }

            }
        }

    }

}
