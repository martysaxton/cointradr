package cointradr.fin.bar

class Vwap: BarFactoryExtension {

    var vwap = 0.0
    var vol = 0.0

    override var onBar: BarExtension = { _, _, _ ->
        vwap = 0.0
        vol = 0.0
    }

    override var onTrade: TradeExtension = { trade, ext  ->
        val newVol = vol + trade.size
        vwap = (vwap * vol + trade.price * trade.size) / newVol
        vol = newVol
        ext["vwap"]  = vwap
    }
}