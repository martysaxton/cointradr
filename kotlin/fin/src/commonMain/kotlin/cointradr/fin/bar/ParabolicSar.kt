package cointradr.fin.bar

import cointradr.fin.model.Bar
import mu.KotlinLogging

const val PSAR = "psar"
const val EXTREME_POINT = "ep"
const val ACCELLERATION_FACTOR = "af"

class ParabolicSar(
        private val startAF: Double = 0.02,
        private val increment: Double = 0.02,
        private val maxAF: Double = 0.2): BarFactoryExtension {

    private val logger = KotlinLogging.logger {}
    private var previousPsar = Double.NaN
    private var previousBar: Bar? = null
    private var extremeLow = Double.MAX_VALUE
    private var extremeHigh = Double.MIN_VALUE
    private var extremePoint = 0.0
    private var accelerationFactor = 0.0
    private var bullish = true

    override var onTrade: TradeExtension? = null

    override var onBar: BarExtension = { bar, _, ext ->
        previousBar?.let {
            // if there is a gap in the bars, or if the bar is empty (it shoudn't be)
            if ((it.start + it.dur != bar.start) || (bar.vol == 0.0 && it.vol == 0.0)) {
                // then reset previousPsar as if we're just starting
                logger.debug { "previous bar start=${it.start} dur=${it.dur}  this bar start=${bar.start} vol=${bar.vol}" }
                previousPsar = Double.NaN
                previousBar = null
                extremeLow = Double.MAX_VALUE
                extremeHigh = Double.MIN_VALUE
            }
        }

        if (previousPsar.isNaN()) {
            handleStartingBars(bar)
        } else {
            handleRegularBar(bar)
        }
        ext[PSAR] = previousPsar
        ext[EXTREME_POINT] = extremePoint
        ext[ACCELLERATION_FACTOR] = accelerationFactor
        previousBar = bar
    }

    fun setLastBarSeen(bar: Bar) {
        previousBar = bar
        val ps = bar.ext[PSAR]
        if (ps != null) {
            previousPsar = ps
            if (!previousPsar.isNaN()) {
                if (previousPsar >= bar.low && previousPsar <=bar.high) {
                    logger.warn { "previous psar is not NaN and is inside previous bar range, setting psar to NaN" }
                    previousPsar = Double.NaN
                } else {
                    extremePoint = bar.ext[EXTREME_POINT]!!
                    accelerationFactor = bar.ext[ACCELLERATION_FACTOR]!!
                    bullish = previousPsar < bar.low
                }
            }
            logger.debug { "set state psar=$previousPsar ep=$extremePoint af=$accelerationFactor bullish=$bullish" }

        } else {
            logger.warn { "lastBarSeen does not have psar state" }
        }
    }

    // first few bars, not sure yet to start bullish or bearish
    private fun handleStartingBars(bar: Bar) {
        extremeHigh = maxOf(bar.high, extremeHigh)
        extremeLow = minOf(bar.low, extremeLow)
        previousBar?.let {
            if (!isInsideBar(it, bar) && !isOutsideBar(it, bar)) {
                if (it.high < bar.high) {
                    // bullish
                    enterPosition(true, extremeLow)
                    extremePoint = extremeHigh
                } else {
                    // must be bearish
                    enterPosition(false, extremeHigh)
                    extremePoint = extremeLow
                }
            }
        }
    }

    private fun handleRegularBar(bar: Bar) {

        var newHighLow = false
        if (bullish) {
            if (extremePoint < bar.high) {
                extremePoint = bar.high
                newHighLow = true
            }
        } else {
            if (extremePoint > bar.low) {
                extremePoint = bar.low
                newHighLow = true
            }
        }

        // if price action penetrated previous previousPsar then reverse
        // bullish and current low is below previousPsar, or bearish and current high is above previousPsar
        val rev = (bullish && bar.low <= previousPsar) || (!bullish && bar.high >= previousPsar)
        if (rev) {
            enterPosition(!bullish, extremePoint)
        } else {

            var newPsar = previousPsar + accelerationFactor * (extremePoint - previousPsar)
            if (bullish) {
                if (newPsar > bar.low) {
                    logger.info { "when bullish, new previousPsar($newPsar) cannot be higher than previous low (${bar.low})"}
                    newPsar = bar.low
                }
            } else {
                if (newPsar < bar.high) {
                    logger.info { "when bearish, new previousPsar($newPsar) cannot be lower than previous low (${bar.low})"}
                    newPsar = bar.high
                }
            }
            previousPsar = newPsar
            if (newHighLow) {
                increaseAccelerationFactor()
            }

            logger.debug { " ${directionText()} high=${bar.high} low=${bar.low} sar=$previousPsar ep=$extremePoint af=$accelerationFactor" }
        }

    }

    private fun increaseAccelerationFactor() {
        accelerationFactor = minOf( accelerationFactor + increment, maxAF)
    }

    private fun enterPosition(bullish: Boolean, extremePoint: Double) {
        this.bullish = bullish
        logger.info { "entering ${directionText()} ep=$extremePoint" }

        // reset state
        this.extremePoint = extremePoint
        previousPsar = extremePoint
        accelerationFactor = startAF
    }

    private fun isInsideBar(left: Bar, right: Bar): Boolean {
        return left.high <= right.high && left.low >= right.low
    }

    private fun isOutsideBar(left: Bar, right: Bar): Boolean {
        return left.high >= right.high && left.low <= right.low
    }

    private fun directionText(): String { return if (bullish) "bullish" else "bearish" }

}