package cointradr.fin.model

//import com.soywiz.klock.DateTime

data class Trade(
        val id: String,
        val exch: String,
        val symb: String,
        val t: Long,
        val price: Double,
        val size: Double,
        val bid: Boolean) {

    override fun equals(other: Any?): Boolean {
        val ot = other as Trade
        return id == ot.id && exch == ot.exch && symb == ot.symb
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + exch.hashCode();
        result = 31 * result + symb.hashCode();
        return result
    }


//    fun getTime(): DateTime {
//        return DateTime.fromUnix(t)
//    }

}

