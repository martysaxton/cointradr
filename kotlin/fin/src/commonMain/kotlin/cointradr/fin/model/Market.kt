package cointradr.fin.model

data class Market(
        // TODO exchange should this be here?
        val exchange: String,
        val baseCurrency: String,
        val quoteCurrency: String,
        val expiry: String? = null) {

    companion object {
        // TODO maybe get rid of this variant and use the other one everywhere
        fun toMarket(exchange: String, symbol: String): Market {
            var s = symbol
            var expiry: String? = null
            if (s.endsWith("-f")) {
                s = s.substringBefore('-')
                expiry = "190329"
            }
            val x = s.split("/")
            if (x.size != 2 ) {
                throw IllegalArgumentException("cannot parse symbol into Market: $symbol")
            }
            return Market(exchange, x[0], x[1], expiry)
        }

        // this is the inverse of toString
        fun toMarket(s: String): Market {
            val x = s.split(".")
            // supports format exch.base.quote.expiry  (for a futures market)
            // and             exch.base.quote         (for a spot market)
            if (x.size < 3 || x.size > 4) {
                throw IllegalArgumentException("cannot parse symbol into Market: $s")
            }
            return Market(x[0], x[1], x[2], if(x.size == 4) x[3] else null)
        }
    }

    override fun toString(): String {
        val expiry = if (expiry == null) "" else ".$expiry"
        return "$exchange.$baseCurrency.$quoteCurrency$expiry"
    }
}