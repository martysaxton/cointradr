package cointradr.fin.model

data class Bar  (
        val exch: String,
        val symb: String,
        val start: Long,
        val dur: Int,
        val open: Double,
        val high: Double,
        val low: Double,
        val close: Double,
        val vol: Double,
        val ext: Map<String, Double>
        )
