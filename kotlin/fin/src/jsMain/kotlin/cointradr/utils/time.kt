 package cointradr.utils

import kotlin.js.Date

actual fun currentTimeMillis(): Long {
    return Date().getTime().toLong()
}
