package cointradr.fin.bar

import cointradr.utils.currentTimeMillis
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals

class BarFactoryJvmTest {

    @Test fun testTimeBasedBarFactory() {
        runBlocking {
            val now = currentTimeMillis()
            val barFactory = BarFactory(EXCHANGE, SYMBOL, 500, (now % 500).toInt())
            barFactory.enableLiveMode()
            val observer = TestBarObserver()
            barFactory.addObserver(observer.observer)
            barFactory.addTrade(SAMPLE_TRADES[0], true)
            delay(100)
            assertEquals(0, observer.bars.size, "no bars yet ")
            delay(402)
            assertEquals(1, observer.bars.size, "one bar")
        }
    }

//    @Test
//    fun testRealTime()  = {
//
//        Test
//        val deferred = GlobalScope.async {
//
//        }
//        deferred.await()
//        val barFactory = TimeBasedBarFactory(EXCHANGE, SYMBOL, DURATION)
//        val observer = TestBarObserver()
//        barFactory.addObserver(observer.observer)
//
//    }

}