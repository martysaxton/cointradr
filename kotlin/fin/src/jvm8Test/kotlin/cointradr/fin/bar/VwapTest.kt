package cointradr.fin.bar


import cointradr.fin.model.Bar
import kotlin.math.abs
import kotlin.test.Test
import kotlin.test.assertTrue
import kotlin.test.fail

val DUMMY_BAR = Bar( "dummyExch", "dummySymb", 99, 1000, 2.0, 4.0, 1.0, 3.0, 1.1, mapOf())

fun assertDouble(expected: Double, actual: Double?, message: String? = null) {
    if (actual == null) {
        fail(message)
    }
    assertTrue(abs(expected-actual) < 0.0001, message)
}

class VWapTest {
    @Test
    fun oneTrade() {
        val vwap = Vwap()
        val ext = mutableMapOf<String, Double>()
        vwap.onTrade(SAMPLE_TRADES[0], ext)
        assertDouble(1.1, ext["vwap"])
    }

    @Test
    fun twoTrades() {
        val vwap = Vwap()
        val ext = mutableMapOf<String, Double>()
        vwap.onTrade(SAMPLE_TRADES[0], ext)
        vwap.onTrade(SAMPLE_TRADES[1], ext)
        assertDouble(1.150246305418719, ext["vwap"])
    }

    @Test
    fun twoBars() {
        val vwap = Vwap()
        val ext = mutableMapOf<String, Double>()
        vwap.onTrade(SAMPLE_TRADES[0], ext)
        assertDouble(1.1, ext["vwap"])
        vwap.onBar(DUMMY_BAR, listOf(), ext)
        vwap.onTrade(SAMPLE_TRADES[1], ext)
        assertDouble(1.2, ext["vwap"])

    }
}
