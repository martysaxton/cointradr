package cointradr.fin.bar


import cointradr.fin.model.Bar
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

var prevTestBar: Bar? = null

//const val EXCHANGE = "exch"
//const val SYMBOL = "btc/usd"
//const val DURATION = 5000

fun createTestBar(prev: Bar, high: Double, low: Double): Bar {
    prevTestBar = Bar(prev.exch, prev.symb, prev.start + DURATION, DURATION, prev.close, high, low, (high-low)/2, 1.0, mapOf())
    return prevTestBar!!
}

fun createTestBar(high: Double, low: Double): Bar {
    prevTestBar?.let {
        return createTestBar(it, high, low)
    }
    prevTestBar = Bar("dummyExch", "dummySymb", 99, DURATION, 0.0, high, low, 0.0, 1.0, mapOf())
    return prevTestBar!!
}

val FIRST_BAR    = createTestBar(52.30, 50.00)

val INSIDE_BAR   = createTestBar(FIRST_BAR,52.20, 50.10)
val OUTSIDE_BAR  = createTestBar(FIRST_BAR, 52.40, 49.90)
val BEARISH_BAR  = createTestBar(FIRST_BAR, 52.20, 49.90)

// starts the bullish trend, corresponds to bar4 in "New Concepts" book
//val BULLISH_BAR  = createTestBar(52.31, 51.50)
val BULLISH_BAR = createTestBar( FIRST_BAR,52.35, 51.50)
val BULLISH_BARS = listOf(
        Pair(BULLISH_BAR, 50.00),                                           // 4
        Pair(createTestBar(52.10, 51.00), 50.047),               // 5
        Pair(createTestBar(51.80, 50.50), 50.093059999999994),   // 6
        Pair(createTestBar(52.10, 51.25), 50.1381988),   // 7
        Pair(createTestBar(52.50, 51.70), 50.185434824),   // 8
        Pair(createTestBar(52.80, 51.85), 50.29001743104),   // 9
        Pair(createTestBar(52.50, 51.50), 50.4406163851776),   // 10
        Pair(createTestBar(53.50, 52.30), 50.62417940206694),   // 11
        Pair(createTestBar(53.50, 52.50), 50.854245049901586),   // 12
        Pair(createTestBar(53.80, 53.00), 51.08990544590946),   // 13
        Pair(createTestBar(54.20, 53.50), 51.400914901318515),   // 14
        Pair(createTestBar(53.40, 52.50), 51.736805113160294),   // 15
        Pair(createTestBar(53.50, 52.10), 52.03238849958106 ),   // 16
        Pair(createTestBar(54.40, 53.00), 52.1),   // 17
        Pair(createTestBar(55.20, 54.00), 52.534),   // 18
        Pair(createTestBar(55.70, 55.00), 53.04056),   // 19
        Pair(createTestBar(57.00, 56.00), 53.7532592),   // 20
        Pair(createTestBar(57.50, 56.50), 54.50260736),   // 21
        Pair(createTestBar(58.00, 57.00), 55.202085888),   // 22
        Pair(createTestBar(57.70, 56.50), 55.7616687104),   // 23
        Pair(createTestBar(58.00, 57.30), 56.20933496832),   // 24
        Pair(createTestBar(57.50, 56.70), 56.567467974656),   // 25
        Pair(createTestBar(57.00, 56.30), 58.00)    // 26
)


class ParabolicSarTest {
    @Test fun startsAsNan() {
        val psar = ParabolicSar()
        val ext = mutableMapOf<String, Double>()
        psar.onBar(FIRST_BAR, listOf(), ext)
        assertNoPosition(ext)
        psar.onBar(INSIDE_BAR, listOf(), ext)
        assertNoPosition(ext)
        psar.onBar(OUTSIDE_BAR, listOf(), ext)
    }


    @Test fun startsBullish() {
        val psar = ParabolicSar()
        val ext = mutableMapOf<String, Double>()
        psar.onBar(FIRST_BAR, listOf(), ext)
        psar.onBar(BULLISH_BAR, listOf(), ext)
        val value = ext["psar"]
        assertEquals(50.0, value)
    }

    @Test fun startsBearish() {
        val psar = ParabolicSar()
        val ext = mutableMapOf<String, Double>()
        psar.onBar(FIRST_BAR, listOf(), ext)
        psar.onBar(BEARISH_BAR, listOf(), ext)
        val value = ext["psar"]
        assertEquals(52.3, value)
    }

    @Test fun calculateBullish() {
        val psar = ParabolicSar()
        val ext = mutableMapOf<String, Double>()
        psar.onBar(FIRST_BAR, listOf(), ext)
        BULLISH_BARS.forEach {
            psar.onBar(it.first, listOf(), ext)
//            assertEquals(it.second, ext["psar"])
            println("omg ${ext["psar"]}")
        }
    }

    private fun assertNoPosition(ext: Map<String,Double>) {
        val value = ext["psar"]
        assertTrue( value != null && value.isNaN())
    }

}
