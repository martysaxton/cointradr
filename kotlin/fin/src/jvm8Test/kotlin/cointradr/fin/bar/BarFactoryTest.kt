package cointradr.fin.bar

import cointradr.fin.model.Bar
import cointradr.fin.model.Trade
import kotlinx.coroutines.runBlocking
import kotlin.math.abs
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue


class TestBarObserver {
    val bars = ArrayList<Bar>()
    val observer: BarFactoryObserver = { bar: Bar ->
        bars.add(bar)
    }
}

const val EXCHANGE = "exch"
const val SYMBOL = "btc/usd"
const val DURATION = 5000

val SAMPLE_TRADES = listOf(
        Trade("1", EXCHANGE, SYMBOL, 15002, 1.1, 1.01, true),
        Trade("2", EXCHANGE, SYMBOL, 15003, 1.2, 1.02, false)
)

class BarFactoryTest {



    @Test
    fun testAddOneBar() {
        runBlocking {
            val barFactory = BarFactory(EXCHANGE, SYMBOL, DURATION)
            val observer = TestBarObserver()
            barFactory.addObserver(observer.observer)
            barFactory.addTrade(SAMPLE_TRADES[0], false)
            assertEquals(0, observer.bars.size, "no bars until complete is called ")
            barFactory.done()
            assertEquals(1, observer.bars.size, "one bar is created")
            val bar = observer.bars[0]
            assertEquals(EXCHANGE, bar.exch, "exch is correct")
            assertEquals(SYMBOL, bar.symb, "symb is correct")
            assertEquals(15000, bar.start, "start is correct")
            assertEquals(DURATION, bar.dur, "dur is correct")
            assertEquals(1.1, bar.open, "open is correct")
            assertEquals(1.1, bar.high, "high is correct")
            assertEquals(1.1, bar.low, "low is correct")
            assertEquals(1.1, bar.close, "close is correct")
            assertEquals(1.01, bar.vol, "vol is correct")
            assertTrue(bar.ext.isEmpty(), "ext is empty")
        }
    }

    @Test
    fun testTimeOffset() {
        runBlocking {
            val barFactory = BarFactory(EXCHANGE, SYMBOL, DURATION, 3)
            val observer = TestBarObserver()
            barFactory.addObserver(observer.observer)
            barFactory.addTrade(SAMPLE_TRADES[0], false)
            barFactory.addTrade(SAMPLE_TRADES[1], false)
            barFactory.done()
            val bar0 = observer.bars[0]
            val bar1 = observer.bars[1]
            assertEquals(2, observer.bars.size)
            assertEquals(10003, bar0.start, "start is correct")
            assertEquals(15003, bar1.start, "start is correct")

            assertEquals(1.1, bar0.open, "open is correct")
            assertEquals(1.1, bar0.high, "high is correct")
            assertEquals(1.1, bar0.low, "low is correct")
            assertEquals(1.1, bar0.close, "close is correct")
            assertEquals(1.01, bar0.vol, "vol is correct")

            assertEquals(1.2, bar1.open, "open is correct")
            assertEquals(1.2, bar1.high, "high is correct")
            assertEquals(1.2, bar1.low, "low is correct")
            assertEquals(1.2, bar1.close, "close is correct")
            assertEquals(1.02, bar1.vol, "vol is correct")
        }
    }

    @Test fun testTwoTradesInSameBar() {
        runBlocking {
            val barFactory = BarFactory(EXCHANGE, SYMBOL, DURATION)
            val observer = TestBarObserver()
            barFactory.addObserver(observer.observer)
            barFactory.addTrade(SAMPLE_TRADES[0], false)
            barFactory.addTrade(SAMPLE_TRADES[1], false)
            barFactory.done()
            assertEquals(1, observer.bars.size)
            val bar = observer.bars[0]
            assertEquals(15000, bar.start, "start is correct")

            assertEquals(1.1, bar.open, "open is correct")
            assertEquals(1.2, bar.high, "high is correct")
            assertEquals(1.1, bar.low, "low is correct")
            assertEquals(1.2, bar.close, "close is correct")
            assertTrue(abs(2.03-bar.vol) < 0.0001, "vol is correct")
        }
    }

    @Test fun testVwap() {
        runBlocking {
            val ( barFactory, observer ) = setup()
            barFactory.addIndicator(Vwap())
            barFactory.addTrade(SAMPLE_TRADES[0], false)
            barFactory.addTrade(SAMPLE_TRADES[1], false)
            barFactory.done()

            val bar = observer.bars[0]
            assertEquals(1.150246305418719, bar.ext["vwap"])
        }
    }

    private fun setup(): Pair<BarFactory, TestBarObserver> {
        val barFactory = BarFactory(EXCHANGE, SYMBOL, DURATION)
        val observer = TestBarObserver()
        barFactory.addObserver(observer.observer)
        return Pair(barFactory, observer)
    }

}