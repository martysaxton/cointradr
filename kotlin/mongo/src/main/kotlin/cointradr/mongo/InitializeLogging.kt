package cointradr.mongo

class InitializeLogging {
    companion object {
        fun init(appName: String) {
            System.setProperty("APP_NAME", appName)
            System.setProperty("logback.statusListenerClass", "ch.qos.logback.core.status.OnConsoleStatusListener")
            System.setProperty("logback.configurationFile", "mongo-logback.xml")

        }
    }
}