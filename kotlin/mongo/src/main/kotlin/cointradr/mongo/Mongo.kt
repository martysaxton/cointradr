package cointradr.mongo

import com.mongodb.async.client.MongoCollection
import com.mongodb.async.client.MongoDatabase

class Mongo {
    suspend fun <T> getCappedCollection(name: String, maxBytes: Long, maxDocuments: Long?, clazz: Class<T>): MongoCollection<T> {
        return Db.getCappedCollection(name, maxBytes, maxDocuments, clazz)
    }

    fun getMongoDatabase(): MongoDatabase {
        return Db.database
    }

}