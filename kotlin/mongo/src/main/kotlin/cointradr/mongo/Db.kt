@file:Suppress("DEPRECATION")

package cointradr.mongo

import com.mongodb.ConnectionString
import com.mongodb.MongoCommandException
import com.mongodb.async.client.MongoCollection
import com.mongodb.async.client.MongoDatabase
import com.mongodb.client.model.CreateCollectionOptions
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import mu.KotlinLogging
import org.litote.kmongo.async.KMongo
import org.litote.kmongo.coroutine.createCollection
import org.litote.kmongo.coroutine.toList


internal class Db {


    companion object {
        val logger = KotlinLogging.logger {}

        var database: MongoDatabase
        val createCollectionMutex = Mutex()

        init{
            logger.debug("connecting to mongo")
            val client = KMongo.createClient(ConnectionString("mongodb://127.0.0.1:27017"))
            database = client.getDatabase("cointradr")
        }

        fun getMongoDatabase(): MongoDatabase {
            return database
        }

        suspend fun <T> getCappedCollection(name: String, maxBytes: Long, maxDocuments: Long?, clazz: Class<T> ): MongoCollection<T> {
            createCollectionMutex.withLock {
                createCappedCollection(name, maxBytes, maxDocuments)
                return database.getCollection<T>(name, clazz)
            }
        }

        private suspend fun createCappedCollection(name: String, maxBytes: Long, maxDocuments: Long?) {
            logger.debug { "creating capped collection $name maxBytes=$maxBytes maxDocs=$maxDocuments" }
            val tradesCollectionOptions = CreateCollectionOptions()
            tradesCollectionOptions.capped(true)
            tradesCollectionOptions.sizeInBytes(maxBytes)
            maxDocuments?.let {
                tradesCollectionOptions.maxDocuments(maxDocuments)
            }
            if (collectionExists(name)) {
                logger.debug { "$name collection already exists" }
            } else {
                logger.debug { "creating collection $name" }
                try {
                    database.createCollection(name, tradesCollectionOptions)
                    logger.debug { "done creating capped collection $name" }
                } catch (e: MongoCommandException) {
                    // 48 means collection exists, this should be redundant due to collectionExists call
                    if (e.errorCode != 48) {
                        throw e
                    }
                }
            }
        }

        private suspend fun collectionExists(collectionName: String): Boolean {
            return database.listCollectionNames().toList<String>().contains(collectionName)
        }
    }



}