package cointradr.mongo

import org.koin.core.module.Module

fun Module.addMongoBeans() {
    single { Mongo() }
}

