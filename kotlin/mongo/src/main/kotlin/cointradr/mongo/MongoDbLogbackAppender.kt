@file:Suppress("DEPRECATION")

package cointradr.mongo


import java.util.Date

import ch.qos.logback.classic.spi.ILoggingEvent
import ch.qos.logback.core.UnsynchronizedAppenderBase

import com.mongodb.async.client.MongoCollection
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import org.litote.kmongo.coroutine.insertOne
import java.lang.StringBuilder
import ch.qos.logback.classic.pattern.ExtendedThrowableProxyConverter
import java.lang.RuntimeException


class MongoDBAppender : UnsynchronizedAppenderBase<ILoggingEvent>() {

    var collectionName: String = "log"
    var maxSizeBytes: Long = 1024*1024


    data class LogEntry(
            val time: Date,
            val level: String,
            val logger: String,
            val message: String,
            val throwable: String?
    )

    private var logCollection: MongoCollection<LogEntry>? = null

    init {
        GlobalScope.launch {
            logCollection = Db.getCappedCollection(collectionName, maxSizeBytes, null, LogEntry::class.java)
        }
    }

    override fun append(event: ILoggingEvent) {

        logCollection?.let {

            var throwableContent: String? = null
            event.throwableProxy?.let {
                val throwableConverter = ExtendedThrowableProxyConverter()
                throwableConverter.start()
                throwableContent = throwableConverter.convert(event)
                throwableConverter.stop()
            }

            GlobalScope.launch {
                it.insertOne(LogEntry(
                        time = Date(event.timeStamp),
                        level = event.level.toString(),
                        logger = event.loggerName,
                        message = event.formattedMessage,
                        throwable =  throwableContent
                ))
            }
        }

    }
}

fun main() = runBlocking {
    System.setProperty("logback.statusListenerClass", "ch.qos.logback.core.status.OnConsoleStatusListener")
    System.setProperty("logback.configurationFile", "mongo-appender-test-logback.xml")
    val logger = KotlinLogging.logger {}
    logger.debug("debug asdf")
    logger.error("error qwer")
    delay(5000)
    for (i in 0..100) {
        logger.error("ERROR message", RuntimeException("asdf"))
        logger.debug("debug message")
        delay(5000)
    }

}