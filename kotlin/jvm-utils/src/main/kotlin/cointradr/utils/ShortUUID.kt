package cointradr.utils


import java.math.BigInteger
import java.util.Arrays
import java.util.UUID

/**
 * A short, unambiguous and URL-safe UUID
 *
 * @author Harpreet Singh
 */
class ShortUuid private constructor(private val uuid: String) {

    companion object {
        fun create(): String {
            val builder = Builder()
            val shortUuid = builder.build(UUID.randomUUID())
            return shortUuid.toString()
        }
    }

    override fun toString(): String {
        return uuid
    }

    override fun hashCode(): Int {
        return uuid.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (other === this)
            return true

        return if (other !is ShortUuid) false else other.toString() == uuid

    }

    class Builder {
        private var alphabet = "23456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz"
                .toCharArray()
        private var alphabetSize = alphabet.size

        fun alphabet(alphabet: String): Builder {
            this.alphabet = alphabet.toCharArray()

            Arrays.sort(this.alphabet)
            alphabetSize = this.alphabet.size

            return this
        }

        @JvmOverloads
        fun build(uuid: UUID = UUID.randomUUID()): ShortUuid {
            val uuidStr = uuid.toString().replace("-".toRegex(), "")

            val factor = Math.log(25.0) / Math.log(alphabetSize.toDouble())
            val length = Math.ceil(factor * 16)

            val number = BigInteger(uuidStr, 16)
            val encoded = encode(number, alphabet, length.toInt())

            return ShortUuid(encoded)
        }

        fun decode(shortUuid: String): String {
            return decode(shortUuid.toCharArray(), alphabet)
        }

        private fun encode(bigInt: BigInteger, alphabet: CharArray, padToLen: Int): String {
            var value = BigInteger(bigInt.toString())
            val alphaSize = BigInteger.valueOf(alphabetSize.toLong())
            val shortUuid = StringBuilder()

            while (value.compareTo(BigInteger.ZERO) > 0) {
                val fracAndRemainder = value.divideAndRemainder(alphaSize)
                shortUuid.append(alphabet[fracAndRemainder[1].toInt()])
                value = fracAndRemainder[0]
            }

            if (padToLen > 0) {
                val padding = Math.max(padToLen - shortUuid.length, 0)
                for (i in 0 until padding)
                    shortUuid.append(alphabet[0])
            }

            return shortUuid.toString()
        }

        private fun decode(encoded: CharArray, alphabet: CharArray): String {
            var sum = BigInteger.ZERO
            val alphaSize = BigInteger.valueOf(alphabetSize.toLong())
            val charLen = encoded.size

            for (i in 0 until charLen) {
                sum = sum.add(alphaSize.pow(i).multiply(BigInteger.valueOf(
                        Arrays.binarySearch(alphabet, encoded[i]).toLong())))
            }

            var str = sum.toString(16)

            // Pad the most significant bit (MSG) with 0 (zero) if the string is too short.
            if (str.length < 32) {
                str = String.format("%32s", str).replace(' ', '0')
            }

            val sb = StringBuilder()
                    .append(str.substring(0, 8))
                    .append("-")
                    .append(str.substring(8, 12))
                    .append("-")
                    .append(str.substring(12, 16))
                    .append("-")
                    .append(str.substring(16, 20))
                    .append("-")
                    .append(str.substring(20, 32))

            return sb.toString()
        }
    }
}