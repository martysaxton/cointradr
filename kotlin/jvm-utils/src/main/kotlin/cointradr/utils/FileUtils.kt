package cointradr.utils

import com.google.common.io.Files
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.io.File
import java.nio.file.*

fun writeFile(fileName: String, content: String) {
    val file = File(fileName)
    Files.asCharSink(file, Charsets.UTF_8).write(content)
}


