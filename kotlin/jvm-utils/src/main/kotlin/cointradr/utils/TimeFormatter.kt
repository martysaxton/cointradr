package cointradr.utils

import org.joda.time.format.DateTimeFormat

class TimeFormatter {
    companion object {
        val dateTimeFormatter = DateTimeFormat.forPattern("SS")
        fun formatTime(millis: Long): String {
            return dateTimeFormatter.print(millis)
        }
    }
}