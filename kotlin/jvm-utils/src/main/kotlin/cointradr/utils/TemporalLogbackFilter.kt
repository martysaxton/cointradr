package cointradr.utils

import ch.qos.logback.classic.spi.ILoggingEvent
import ch.qos.logback.core.filter.Filter
import ch.qos.logback.core.spi.FilterReply
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import java.time.Duration
import java.util.*

class TemporalLogbackFilter : Filter<ILoggingEvent>() {

    companion object {
        val ONE_MINUTE_IN_MILLIS = 60L * 1000L
        val DEFAULT_INTERVAL = 60L
    }

    private var expirationInterval: Long? = null
    var expirationInMinutes: String? = null

    private val expirations = mutableMapOf<String, Long>()


    override fun decide(event: ILoggingEvent): FilterReply {
        synchronized(this) {
            if (expirationInterval == null) {
                expirationInterval = expirationInMinutes?.toLong() ?: DEFAULT_INTERVAL
            }
            val currentTime = Date().time
            val expiration = expirations[event.message]
            if (expiration == null || expiration < currentTime) {
                expirations[event.message] = currentTime + (expirationInterval!! * ONE_MINUTE_IN_MILLIS)
                return FilterReply.NEUTRAL
            }
            return FilterReply.DENY
        }
    }
}

fun main() = runBlocking {
    System.setProperty("logback.statusListenerClass", "ch.qos.logback.core.status.OnConsoleStatusListener")
    System.setProperty("logback.configurationFile", "logback-console-with-filter.xml")
    val logger = KotlinLogging.logger {}
    logger.debug("debug asdf")
    logger.error("error qwer")
    for (i in 0..100) {
        logger.error("ERROR message")
        logger.debug("debug message")
        delay(5000)
    }

}