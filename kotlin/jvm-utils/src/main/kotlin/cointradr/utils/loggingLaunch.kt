package cointradr.utils

import kotlinx.coroutines.*
import mu.KotlinLogging
import java.lang.RuntimeException
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext


val handler = CoroutineExceptionHandler { _, exception ->
    val logger = KotlinLogging.logger {}
    logger.error(exception) { "exception occurred within coroutine" }
    throw exception
}

fun CoroutineScope.loggingLaunch(
        start: CoroutineStart = CoroutineStart.DEFAULT,
        block: suspend CoroutineScope.() -> Unit
): Job {
    return this.launch(handler, start, block)
}

fun <T> CoroutineScope.loggingAsync(
        start: CoroutineStart = CoroutineStart.DEFAULT,
        block: suspend CoroutineScope.() -> T
): Deferred<T> {
    return this.async(handler, start, block)
}

public fun <T> loggingRunBlocking(block: suspend CoroutineScope.() -> T): T {
    return runBlocking(handler, block)
}

private fun logException(t: Throwable) {
    val logger = KotlinLogging.logger {}
    logger.error(t) { "exception occurred within coroutine" }
}

fun main() = runBlocking {
    val logger = KotlinLogging.logger {}
    System.setProperty("logback.statusListenerClass", "ch.qos.logback.core.status.OnConsoleStatusListener")
    System.setProperty("logback.configurationFile", "../logback-common.xml")
    logger.debug { "launching" }
    try {
        GlobalScope.loggingLaunch {
            throw RuntimeException("omg")
        }

    } catch (t: Throwable) {
        logger.debug { "asdf" }
    }
    delay(1000000L)
}
