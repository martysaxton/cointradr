package cointradr.geoip

import kotlinx.coroutines.runBlocking


fun main() = runBlocking {
    println("my geoip is ${GeoIP.getMyCountry()}")
}

