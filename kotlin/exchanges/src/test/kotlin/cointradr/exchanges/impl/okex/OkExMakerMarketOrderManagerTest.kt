package cointradr.exchanges.impl.okex

import cointradr.clients.okex.*
import cointradr.exchanges.Direction
import cointradr.exchanges.impl.BaseExchangeTest
import cointradr.exchanges.impl.assertNotCompleted
import cointradr.utils.ShortUuid
import com.okcoin.commons.okex.open.api.bean.futures.param.Order
import com.soywiz.klock.DateTime
import io.mockk.*
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import org.koin.core.KoinApplication
import org.koin.dsl.module
import org.koin.test.get
import org.koin.test.inject


@TestInstance(TestInstance.Lifecycle.PER_METHOD)
class OkExMakerMarketOrderManagerTest: BaseExchangeTest() {

    companion object {
        private const val FIRST_ORDER_ID = 1000L
        private const val INSTRUMENT_ID = "INSTRUMENT_ID"
        private const val bestBid = 3000.0
        private const val bestAsk = bestBid + 1.0
    }

    private val okExClient : OkExRestClient by inject()
    private val okExStream : OkExStreamV3 by inject()
    private lateinit var manager: OkExMakerMarketOrderManager

    private lateinit var depthHandlerSlot: CapturingSlot<OkExDepthHandler>
    private lateinit var streamObserver: CapturingSlot<OkExStreamV3.AccountObserver>
    private var currentOrderId = FIRST_ORDER_ID

    private var clientId = ShortUuid.create()
    private lateinit var submittedOrders: MutableList<Order>
    private lateinit var cancelCalls: CancelArgs

    init {
        MockKAnnotations.init(this, relaxUnitFun = true) // turn relaxUnitFun on for all mocks
    }

    @BeforeEach
    fun myBeforeEach() {
//        val mocksModule = module {
//            single(override = true) { mockk<OkExRestClient>() }
//            single(override = true) { mockk<OkExStreamV3>() }
//            factory(override = true) { OkExMakerMarketOrderManager() }
//        }
//        startKoin {
//            modules(exchangeModule, mocksModule)
//        }
//        declareMock<OkExRestClient>()
//        declareMock<OkExStreamV3>()


//        okExClient = get()
//        okExStream = get()
        depthHandlerSlot = slot<OkExDepthHandler>()
        streamObserver = slot<OkExStreamV3.AccountObserver>()
        every { okExStream.addDepthHandler(capture(depthHandlerSlot)) } just Runs
        every { okExStream.addObserver(capture(streamObserver)) } just Runs

        submittedOrders= captureOrders()
        cancelCalls= captureCancels()

//        mockkObject(OkExStreamV3)
        coEvery { okExStream.connect() } just Runs

        manager = get()
        runBlocking { sendBookUpdate(createBook(bestBid, bestBid + 1.0)) }
    }

    override fun addKoinModules(koinApplication: KoinApplication) {
        koinApplication.modules(listOf(module {
            single(override = true) { mockk<OkExRestClient>() }
            single(override = true) { mockk<OkExStreamV3>(relaxUnitFun = true) }
            factory(override = true) { OkExMakerMarketOrderManager(BaseExchangeTest.market) }

        }))
    }


//    @AfterEach
//    fun afterEach() {
//        stopKoin()
//    }


    @Test
    fun `combines multiple orders of the same type and direction`() = runBlocking {
        val orderType = OrderType.OPEN_LONG
        manager.placeOrders(clientId, listOf(
                createMarketOrder(1, orderType),
                createMarketOrder(2, orderType)
        ))
        assertEquals(1, submittedOrders.size)
        val order = submittedOrders.first()
        commonOrderAsserts(order)
        with(order) {
            assertEquals(orderType.code(), type)
            assertEquals(bestBid + 0.01, price)
            assertEquals(3, size)
        }
    }

    @Test
    fun `keeps orders of separate type separate`() = runBlocking {
        manager.placeOrders(clientId, listOf(
                createMarketOrder(1, OrderType.OPEN_LONG),
                createMarketOrder(2, OrderType.CLOSE_SHORT)
        ))
        assertEquals(2, submittedOrders.size)
        val openLongOrder = submittedOrders.find { it.type == OrderType.OPEN_LONG.code() }!!
        commonOrderAsserts(openLongOrder)
        with(openLongOrder) {
            org.junit.jupiter.api.Assertions.assertEquals(bestBid + 0.01, price)
            org.junit.jupiter.api.Assertions.assertEquals(1, size)
        }
        val closeShortOrder = submittedOrders.find { it.type == OrderType.CLOSE_SHORT.code() }!!
        with(closeShortOrder) {
            org.junit.jupiter.api.Assertions.assertEquals(bestBid + 0.01, price)
            org.junit.jupiter.api.Assertions.assertEquals(2, size)
        }


    }

    @ParameterizedTest
    @ValueSource(ints = [1, 2, 3, 4])
    fun `updates an order when book moves beyond currently placed price`(orderTypeInt: Int) = runBlocking {
        val orderType = OrderType.from(orderTypeInt)
        val direction = orderType.toDirection()

        manager.placeOrders(clientId, listOf(
                createMarketOrder(1, orderType)
        ))
        assertEquals(1, submittedOrders.size, "number of orders submitted")
        assertEquals(submittedOrders[0].price, getProjectedPrice(direction, 1))

        // update book to be price at which order was submitted
        sendBookUpdate(createProjectedBook(direction, 1))

        // should not bid against itself
        assertEquals(0, cancelCalls.instrumentIds.size)

        // move book price inside of current order price
        sendBookUpdate(createProjectedBook(direction, 2))

        // verify the cancellation
        with (cancelCalls) {
            assertEquals(1, instrumentIds.size)
            assertEquals(INSTRUMENT_ID, instrumentIds.first())
            assertEquals(listOf(currentOrderId-1L), this.orders.first())
        }

        // still no call to submit order until cancellation comes back from exchange
        assertEquals(1, submittedOrders.size, "number of orders submitted")

        submittedOrders.clear()
        val orderUpdate= createOrderUpdate(
                exchangeId = currentOrderId-1,
                orderType = orderType,
                size = 1,
                price = -99.9,
                status =  OKEX_ORDER_CANCELLED)

        sendStreamUpdate(orderUpdate)
        assertEquals(1, submittedOrders.size)
        assertEquals(1, submittedOrders[0].size)
        assertEquals(getProjectedPrice(direction, 3), submittedOrders[0].price)
//        assertEquals(getProjectedPrice(direction, 3), submittedOrders[0].price, 0.0001)
    }

    @Test
    fun `fills an order`() = runBlocking {
        val orderType = OrderType.OPEN_LONG
        val deferred = manager.placeOrders(clientId, listOf(
                createMarketOrder(1, orderType)
        ))
        assertEquals(1, submittedOrders.size)
        val price = bestBid + 0.01
        assertEquals(price, submittedOrders[0].price)
        val orderUpdate= createOrderUpdate(
                exchangeId = currentOrderId-1,
                orderType = orderType,
                size = 1,
                price = submittedOrders[0].price,
                status =  OKEX_ORDER_FILLED).copy(
                filled_qty = 1.toString(),
                price_avg =  (bestBid + 0.50).toString()
        )
        sendStreamUpdate(orderUpdate)

        val orderResults = deferred.await()
        assertEquals(1, orderResults.size)
        val ores = orderResults.first()
        assertEquals(clientId, ores.clientId)
        assertEquals(INSTRUMENT_ID, ores.instrumentId)
        assertEquals(OrderType.OPEN_LONG, orderType)
        assertEquals(1, ores.size)
        assertEquals(1, ores.filled)
        assertEquals(3000.50, ores.averagePrice)
    }


    @ParameterizedTest
    @ValueSource(ints = [1, 2, 3, 4])
    fun `will not exceed counter bid ask`(type: Int) = runBlocking {
        val orderType = OrderType.from(type)
        val direction = orderType.toDirection()
        manager.placeOrders(clientId, listOf(
                createMarketOrder(1, orderType)
        ))
        assertEquals(1, submittedOrders.size)
        val orderSubmission = submittedOrders.first()
        when (direction) {
            Direction.BUY -> assertEquals(orderSubmission.price, bestBid + 0.01)
            Direction.SELL -> assertEquals(orderSubmission.price, bestAsk - 0.01)
        }

        sendBookUpdate(createBook(3000.50, 3000.51))
        assertEquals(1, cancelCalls.instrumentIds.size)
        assertEquals(1, submittedOrders.size)
        val orderCancellationUpdate= createOrderUpdate(
                exchangeId = 1000,
                orderType = orderType,
                size = 1,
                price = 3000.00,
                status =  OKEX_ORDER_CANCELLED)
        sendStreamUpdate(orderCancellationUpdate)
        assertEquals(2, submittedOrders.size)
        val newSubmission = submittedOrders[1]
        when (direction) {
            Direction.BUY -> assertEquals(newSubmission.price, 3000.50)
            Direction.SELL -> assertEquals(newSubmission.price, 3000.51)
        }
    }

    @ParameterizedTest
    @ValueSource(ints = [1, 2, 3, 4])
    fun `will not unnecessarily resubmit at same price`(type: Int) = runBlocking {
        val orderType = OrderType.from(type)
        val direction = orderType.toDirection()
        manager.placeOrders(clientId, listOf(
                createMarketOrder(1, orderType)
        ))
        assertEquals(1, submittedOrders.size)
        var latestSubmission = submittedOrders.last()
        when (direction) {
            Direction.BUY -> assertEquals(bestBid + 0.01, latestSubmission.price )
            Direction.SELL -> assertEquals(bestAsk - 0.01, latestSubmission.price)
        }

        sendBookUpdate(createBook(3000.49, 3000.52))
        assertEquals(1, cancelCalls.instrumentIds.size)
        assertEquals(1, submittedOrders.size)
        val orderCancellationUpdate= createOrderUpdate(
                exchangeId = 1000,
                orderType = orderType,
                size = 1,
                price = 3000.00,
                status =  OKEX_ORDER_CANCELLED)
        sendStreamUpdate(orderCancellationUpdate)
        assertEquals(2, submittedOrders.size)
        latestSubmission = submittedOrders.last()
        when (direction) {
            Direction.BUY -> assertEquals(3000.50, latestSubmission.price)
            Direction.SELL -> assertEquals(3000.51, latestSubmission.price)
        }
        // submit a book update where book price already matches order price
        sendBookUpdate(createBook(3000.50, 3000.51))
        // should be no attempt to resubmit, so no cancellations
        assertEquals(1, cancelCalls.instrumentIds.size)

    }



    @Test
    fun `fills multiple orders`() = runBlocking {
        val orderType = OrderType.OPEN_LONG
        val deferred = manager.placeOrders(clientId, listOf(
                createMarketOrder(1, OrderType.OPEN_LONG),
                createMarketOrder(2, OrderType.CLOSE_SHORT)
        ))
        assertEquals(2, submittedOrders.size)
        val fillUpdate= createOrderUpdate(
                exchangeId = currentOrderId-2,
                orderType = OrderType.from(submittedOrders[0].type),
                size = submittedOrders[0].size,
                price = submittedOrders[0].price,
                status =  OKEX_ORDER_FILLED).copy(
                filled_qty = submittedOrders[0].size.toString(),
                price_avg =  submittedOrders[0].price.toString()
        )

        // just fill one of the orders
        sendStreamUpdate(fillUpdate)
        assertNotCompleted(deferred)

        val fillUpdate2 = fillUpdate.copy(
                order_id = (currentOrderId-1).toString(),
                type = OrderType.from(submittedOrders[1].type).toString(),
                size = submittedOrders[1].size.toString(),
                filled_qty = submittedOrders[1].size.toString()
        )
        sendStreamUpdate(fillUpdate2)

        val oResList = deferred.await()
        assertEquals(2, oResList.size)
        val ores = oResList.first()
        assertEquals(clientId, ores.clientId)
        assertEquals(INSTRUMENT_ID, ores.instrumentId)
        assertEquals(OrderType.OPEN_LONG, orderType)
        assertEquals(1, ores.size)
        assertEquals(1, ores.filled)
        assertEquals(3000.01, ores.averagePrice)
    }

    @Test
    fun `exchange sends cancellation event twice`() = runBlocking {
        val orderType = OrderType.OPEN_LONG
        val direction = orderType.toDirection()
        manager.placeOrders(clientId, listOf(
                createMarketOrder(1, orderType)
        ))
        sendBookUpdate(createProjectedBook(direction, 2))
        assertEquals(1, cancelCalls.instrumentIds.size)
        val orderUpdate= createOrderUpdate(
                exchangeId = 1000,
                orderType = orderType,
                size = 1,
                price = 3000.00,
                status =  OKEX_ORDER_CANCELLED)
        sendStreamUpdate(orderUpdate)
        assertEquals(2, submittedOrders.size)
        // send same cancellation event twice
        sendStreamUpdate(orderUpdate)

        // should be ignored
        assertEquals(2, submittedOrders.size)


    }

    @Test
    fun `fills an order with partials`() = runBlocking {
        val orderType = OrderType.OPEN_LONG
        val direction = orderType.toDirection()
        val deferred = manager.placeOrders(clientId, listOf(
                createMarketOrder(3, orderType)
        ))
        assertEquals(1, submittedOrders.size)
        val price = bestBid + 0.01
        assertEquals(price, submittedOrders[0].price)
        val partialFillUpdate= createOrderUpdate(
                exchangeId = currentOrderId-1,
                orderType = orderType,
                size = 1,
                price = submittedOrders[0].price,
                status =  OKEX_ORDER_PARTIALLYFILLED).copy(
                filled_qty = 1.toString(),
                price_avg =  price.toString()
        )
        sendStreamUpdate(partialFillUpdate)

        // move book price inside of current order price
        sendBookUpdate(createProjectedBook(direction, 2))
        assertEquals(1, cancelCalls.instrumentIds.size)
        assertEquals(1, submittedOrders.size)

        exchangeSendsCancellation()
        assertEquals(2, submittedOrders.size)

        // send partial fill 1 contract at whatever price last submission was
        sendStreamUpdate(partialFillUpdate.copy(
                order_id = (currentOrderId-1).toString(),
                price = submittedOrders[1].price.toString(),
                price_avg = submittedOrders[1].price.toString()
        ))

        sendBookUpdate(createProjectedBook(direction, 4))
        assertEquals(2, cancelCalls.instrumentIds.size)
        assertEquals(2, submittedOrders.size)
        exchangeSendsCancellation()
        assertEquals(3, submittedOrders.size)

        // send final fill
        assertNotCompleted(deferred)
        sendStreamUpdate(partialFillUpdate.copy(
                order_id = (currentOrderId-1).toString(),
                status = OKEX_ORDER_FILLED,
                price = submittedOrders[2].price.toString(),
                price_avg = submittedOrders[2].price.toString()
        ))
        val oResList = deferred.await()
        assertEquals(1, oResList.size)
        assertEquals(3000.03, oResList.last().averagePrice)


//        val ores = orderResults.first()
//        assertEquals(clientId, ores.clientId)
//        assertEquals(INSTRUMENT_ID, ores.instrumentId)
//        assertEquals(OrderType.OPEN_LONG, orderType)
//        assertEquals(1, ores.size)
//        assertEquals(1, ores.filled)
//        assertEquals(3000.50, ores.averagePrice)


    }

    private suspend fun exchangeSendsCancellation() {
        val lastSubmission = submittedOrders.last()
        val orderUpdate= createOrderUpdate(
                exchangeId = cancelCalls.orders.last().last(),
                orderType = OrderType.from(lastSubmission.type),
                size = lastSubmission.size,
                price = lastSubmission.price,
                status =  OKEX_ORDER_CANCELLED)
        sendStreamUpdate(orderUpdate)
    }

    private suspend fun createProjectedBook(direction: Direction, steps: Int): OkExBook {
        val bba = getProjectedBBA(direction, steps)
        return createBook(bba.bestBid, bba.bestAsk)

    }


    private fun getProjectedBBA(direction: Direction, steps: Int): OkExBook.BestBidAsk {
        return when (direction) {
            Direction.BUY -> OkExBook.BestBidAsk(bestBid + (0.01 * steps), bestAsk)
            Direction.SELL -> OkExBook.BestBidAsk(bestBid, bestAsk - (0.01 * steps))
        }
    }

    private fun getProjectedPrice(direction: Direction, steps: Int): Double {
        val bba = getProjectedBBA(direction, steps)
        return when(direction) {
            Direction.BUY -> bba.bestBid
            Direction.SELL -> bba.bestAsk
        }
    }

    private fun createOrderUpdate(
            exchangeId: Long,
            orderType: OrderType,
            size: Int,
            price: Double,
            status: String,
            filled: Int = 0,
            avgPrice: Double = 0.0): OkExOrder {
        return OkExOrder(
                leverage=  "10",
                size = size.toString(),
                filled_qty = filled.toString(),
                price = price.toString(),
                fee = "0.03",
                contract_val = "100",
                price_avg = avgPrice.toString(),
                type = orderType.code().toString(),
                instrument_id = INSTRUMENT_ID,
                order_id = exchangeId.toString(),
                timestamp = DateTime.nowUnixLong().toString(),
                status = status)

    }


    private fun commonOrderAsserts(order: Order) {
        with(order) {
            assertEquals(instrument_id, INSTRUMENT_ID)
            assertEquals(leverage, 10.0)
            assertEquals(client_oid, clientId)
            assertEquals(match_price, 0)
        }
    }

    private fun createOrderResult() = com.okcoin.commons.okex.open.api.bean.futures.result.OrderResult().apply {
        this.client_oid = clientId
        this.error_code = 0
        this.error_messsage = ""
        this.isResult = true
        this.order_id = currentOrderId.toString()
        ++currentOrderId
    }


    private fun createMarketOrder(size: Int, type: OrderType): Order {
        return Order().apply {
            client_oid = clientId
            setinstrument_id(INSTRUMENT_ID)
            leverage = 10.0
            this.match_price = 1
            this.size = size
            this.type =  type.code()
        }

    }

    private suspend fun createBook(bestBid: Double, bestAsk: Double): OkExBook {
        return OkExBook().apply {
            replace(
                    bids = listOf( OkExV3BidAsk( bestBid, 100.0) ),
                    asks = listOf( OkExV3BidAsk( bestAsk, 100.0) )
            )
        }

    }

    private suspend fun sendBookUpdate(book: OkExBook) {
        depthHandlerSlot.captured(book)
    }

    private suspend fun sendStreamUpdate(okExOrder: OkExOrder) {
        streamObserver.captured.onOrders(listOf(okExOrder))
    }


    private fun captureOrders(): MutableList<Order> {
        val submittedOrders = mutableListOf<Order>()
        every { okExClient.order(capture(submittedOrders)) } answers { createOrderResult() }
        return submittedOrders
    }

    private data class CancelArgs(val instrumentIds: List<String>, val orders: List<List<Long>>)

    private fun captureCancels(): CancelArgs {
        val instrumentIds = mutableListOf<String>()
        val orderIdsList = mutableListOf<List<Long>>()
        every { okExClient.cancelOrders(capture(instrumentIds), capture(orderIdsList)) } just Runs
        return CancelArgs(instrumentIds, orderIdsList)
    }


}