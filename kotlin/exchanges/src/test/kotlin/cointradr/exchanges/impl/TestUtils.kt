package cointradr.exchanges.impl

import cointradr.exchanges.Order
import io.mockk.mockk
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.apache.commons.lang3.builder.EqualsBuilder
import org.junit.jupiter.api.Assertions
import org.koin.core.context.GlobalContext
import org.koin.core.definition.BeanDefinition
import org.koin.core.definition.Definition
import org.koin.core.definition.DefinitionFactory
import org.koin.test.KoinTest
import org.koin.test.mock.applyStub
import org.koin.test.mock.declareMock
import org.koin.test.mock.declareMockedDefinition
import org.koin.test.mock.getDefinition
import org.mockito.ArgumentMatcher
import org.spongycastle.asn1.x500.style.RFC4519Style.name
import java.lang.IllegalStateException

typealias OkExOrderRequest = com.okcoin.commons.okex.open.api.bean.futures.param.Order

class OkExOrderMatcher(val expectedOkExOrderRequest: OkExOrderRequest) : ArgumentMatcher<List<OkExOrderRequest>> {
    override fun matches(argument: List<OkExOrderRequest>?): Boolean {
        argument?.let {
            if (it.size != 1) {
                return false
            }
            return EqualsBuilder.reflectionEquals(expectedOkExOrderRequest, it[0], "client_oid")
        } ?: return false
    }
}

class OrderMatcher(val expectedOrder: Order) : ArgumentMatcher<Order> {
    override fun matches(argument: Order?): Boolean {
        argument?.let {
            return EqualsBuilder.reflectionEquals(expectedOrder, it, "id", "whenPlaced")
        } ?: return false
    }
}

@ExperimentalCoroutinesApi
fun <T> assertNotCompleted(deferred: Deferred<T>) {
    assertThrows<IllegalStateException> { deferred.getCompleted() }
}

inline fun <reified T: Throwable> assertThrows( block: () -> Unit) {
    try {
        block()
        Assertions.fail<T>("did not throw ${T::class}")
    } catch( t: Throwable) {
        if (t !is T) {
            throw t
        }
    }
}
