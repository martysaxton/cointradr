package cointradr.exchanges.impl.okex

import cointradr.clients.okex.OkExRestClient
import cointradr.exchanges.Direction
import cointradr.exchanges.OrderRequest
import cointradr.exchanges.TradeProvider
import cointradr.exchanges.impl.BaseExchangeTest
import cointradr.exchanges.impl.MongoCappedCollectionTradeProvider
import cointradr.exchanges.impl.OkExOrderMatcher
import cointradr.exchanges.impl.OkExOrderRequest
import cointradr.fin.model.Market
import com.nhaarman.mockitokotlin2.argThat
import com.nhaarman.mockitokotlin2.whenever
import io.mockk.mockk
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import org.koin.core.KoinApplication
import org.koin.dsl.module
import org.koin.test.inject
import org.koin.test.mock.declareMock
import org.mockito.ArgumentMatchers.anyList
import org.mockito.Mockito.anyString

import org.mockito.Mockito.verify

class OkExTradingExchangeTest : BaseExchangeTest() {

    val market = Market("base", "quote", "exp")


    override fun addKoinModules(koinApplication: KoinApplication) {
        module {
            single<TradeProvider>(override = true) { mockk<MongoCappedCollectionTradeProvider>() }
        }
    }



    @Test
    fun `market order executes immediately` ()  {

        runBlocking {
            declareMock<OkExRestClient>()
            declareMock<OkExMakerMarketOrderManager>()
            val okExTradingExchange by inject<OkExTradingExchange>()
            val mockMarketOrderManager by inject<OkExMakerMarketOrderManager>()

            val completableDeferred = CompletableDeferred<List<OrderResult>>()
            whenever(mockMarketOrderManager.placeOrders(anyString(), anyList())).thenReturn(completableDeferred)

            val orderRequest = OrderRequest(
                    market = market,
                    type = OrderRequest.Type.MARKET,
                    direction = Direction.BUY,
                    size = 1.0,
                    price = null)

            val expectedOkExOrder = OkExOrderRequest().apply {
                this.
                setinstrument_id("base.quote.exp")
                leverage = 10.0
                type = 1
                price = null
                size = 1
                match_price = 1
            }


            runBlocking { okExTradingExchange.placeOrder(orderRequest) }
            completableDeferred.complete(listOf(OrderResult(
                clientId = "foo",
                instrumentId = "foo",
                type = OrderType.OPEN_LONG,
                price = 1.0,
                size = 1,
                filled = 1,
                averagePrice = 1.0,
                fee = 1.0
            )))
            verify(mockMarketOrderManager).placeOrders(anyString(), argThat(OkExOrderMatcher(expectedOkExOrder)))
        }
    }

}
