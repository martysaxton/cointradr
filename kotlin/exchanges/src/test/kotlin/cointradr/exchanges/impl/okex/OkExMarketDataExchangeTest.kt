package cointradr.exchanges.impl.okex

import cointradr.clients.okex.*
import cointradr.exchanges.exchangeModule
import cointradr.exchanges.impl.BaseExchangeTest
import cointradr.fin.model.Market
import cointradr.fin.model.Trade
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.verify
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.ArgumentsProvider
import org.junit.jupiter.params.provider.ArgumentsSource
import java.util.stream.Stream
import org.junit.jupiter.params.provider.Arguments.arguments
import org.koin.core.KoinApplication
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.test.KoinTest
import org.koin.test.get
import org.koin.test.inject
import org.koin.test.mock.declareMock


@TestInstance(TestInstance.Lifecycle.PER_METHOD)
class OkExMarketDataExchangeTest: BaseExchangeTest() {


    companion object {
        private val market = Market("okex", "btc", "usd", "190329")
        private val okExMarketSpot = OkExMarket(base = "base", quote="quote")
        private val okExMarketFutures = okExMarketSpot.copy(futureExpiry = "fe")
        private val okExTradeSpot = OkExTrade(
                t=1,
                market = okExMarketSpot,
                id="id",
                price = 1.0,
                size = 2.0,
                bid = true
        )
        private val okExTradeFutures = okExTradeSpot.copy(market= okExMarketFutures)
        class TradeArgumentsProvider : ArgumentsProvider {
            override fun provideArguments(context: ExtensionContext): Stream<out Arguments> {
                return Stream.of(arguments(okExTradeSpot), arguments(okExTradeFutures))
            }
        }
    }


    private val okExStream : OkExStreamV3 by inject()
    private val okExClient : OkExRestClient by inject()
    private val okExTradingExchange: OkExMarketDataExchange by inject()

    @BeforeEach
    override fun afterStartKoin() {
        declareMock<OkExStreamV3>()
        declareMock<OkExRestClient>()
//        okExStream = get()
//        okExClient = get()
//        okExTradingExchange = get()
    }

    @ParameterizedTest
    @ArgumentsSource(TradeArgumentsProvider::class)
    fun `broadcasts trades`(okExTrade: OkExTrade) {

        class TestTradeHandler(val okExTrade: OkExTrade) {
            var trade: Trade? = null
            suspend fun tradeHandler(trade: Trade) {
                this.trade = trade
            }
        }

        runBlocking {
            val testTradeHandler = TestTradeHandler(okExTrade)
            okExTradingExchange.addTradeHandler(testTradeHandler::tradeHandler)
            okExTradingExchange.okexTradeHandler(listOf(okExTrade))
            assertTradeEquals(okExTrade, testTradeHandler.trade!!)
        }

    }

    @Test
    fun `doesn't broadcasts trades when given an empty list`() {
        var called = false

        okExTradingExchange.addTradeHandler {
            called = true
        }

        runBlocking {
            okExTradingExchange.okexTradeHandler(listOf())
        }

        assertFalse(called)
    }

    @Disabled
    @Test
    fun `normalized the rest call to get depth`() {
        runBlocking {
            val okExBookResponse = OkExBookResponse(
                    bids = listOf(listOf(1.0, 2.0)),
                    asks = listOf(listOf(3.0, 4.0)),
                    timestamp = "ts"
            )
    //        val restClient = mockkClass(OkExRestClient::class)
    //        coEvery { restClient.getDepthFutures("foo", "foo", "foo", 1) } returns okExBookResponse
            var result = okExTradingExchange.getOrderBook(market)
            verify(okExClient).getDepthFutures(any(), any(), any(), any())
            assertNotNull(result)
        }
    }

    @Test
    fun `tells the stream client to subscribe to trades`() {
        runBlocking {
            okExTradingExchange.subscribeToTrades(market)
            val expected = market.toOkExSubscription("trade")
            verify(okExStream).subscribe(expected)
        }
    }

    private fun assertTradeEquals(okExTrade: OkExTrade, trade: Trade) {
        assertEquals(okExTrade.t, trade.t)
        assertEquals(okExTrade.id, trade.id)
        assertEquals(okExTrade.price, trade.price)
        assertEquals(okExTrade.size, trade.size)
        assertEquals(okExTrade.bid, trade.bid)
        with (okExTrade.market) {
            val expected = if (futureExpiry != null) "$base/$quote-f" else "$base/$quote"
            assertEquals(expected, trade.symb)
        }
    }


}
