package cointradr.exchanges.impl

import cointradr.exchanges.exchangeModule
import cointradr.fin.model.Market
import cointradr.mongo.Mongo
import cointradr.state.Store
import io.mockk.MockKAnnotations
import io.mockk.junit5.MockKExtension
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.koin.core.KoinApplication
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.test.KoinTest
import org.koin.test.mock.declareMock


@ExtendWith(MockKExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_METHOD)
abstract class BaseExchangeTest : KoinTest {

    companion object {
        val market = Market("okex", "btc", "usd", "190329")
    }

    init {
        MockKAnnotations.init(this, relaxUnitFun = true) // turn relaxUnitFun on for all mocks
    }


    @BeforeEach
    fun beforeEach() {


        startKoin {
            modules(exchangeModule(market))
            addKoinModules(this)
        }
        declareMock<OrderPersistence>()
        declareMock<Mongo>()
        declareMock<Store>()
        afterStartKoin()
    }

    open fun addKoinModules(koinApplication: KoinApplication) {}
    open fun afterStartKoin() {}

    @AfterEach
    fun afterEach() {
        stopKoin()
    }

}