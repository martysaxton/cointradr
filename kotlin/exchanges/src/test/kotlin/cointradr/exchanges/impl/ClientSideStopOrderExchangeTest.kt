package cointradr.exchanges.impl

import cointradr.exchanges.*
import cointradr.fin.model.Market
import cointradr.fin.model.Trade
import com.nhaarman.mockitokotlin2.*
import kotlinx.coroutines.Job
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import org.koin.core.KoinApplication
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.bind
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.get
import org.koin.test.inject
import org.mockito.Mockito

@TestInstance(TestInstance.Lifecycle.PER_METHOD)
class ClientSideStopOrderExchangeTest: BaseExchangeTest() {

    class TestExchange : ClientSideStopOrderExchange("testExchange") {
        override suspend fun executeMarketOrder(order: Order) {}

        override fun getWallet(): Wallet {
            TODO("not implemented")
        }

        override suspend fun getLiquidationValue(market: Market): Double {
            return Double.NaN
        }

        override suspend fun getCashValue(): Double {
            return Double.NaN
        }

        override suspend fun getOrderSize(market: Market, usd: Double): Double {
            return Double.NaN
        }
        override suspend fun placeOrder(orderRequest: OrderRequest): Order {
            println("asdf")
            return super.placeOrder(orderRequest)
        }
    }

    val clientSideStopOrderExchange: ClientSideStopOrderExchange by inject()
    val testTradeProvider: TestTradeProvider by inject()

    val testMarket = Market("base", "quote", "exp")

    val testOrderRequest = OrderRequest(
            market = testMarket,
            type = OrderRequest.Type.STOP,
            direction = Direction.BUY,
            size = 1.0,
            price = 2.0)
    val testTrade = Trade(
            id = "id",
            exch = "exch",
            symb = "symb",
            t = 1,
            price = 100.0,
            size = 1.0,
            bid = true
    )

    val expectedOrder = Order(
            id = "id",
            mktSym = "base.quote.exp",
            type = OrderRequest.Type.STOP,
            direction = Direction.BUY,
            size = 1.0,
            price = 2.0,
            filledQuantity = 0.0,
            filledPrice = null,
            whenPlaced = 1,
            whenFilled = null,
            state = Order.State.ACTIVE)

    override fun addKoinModules(koinApplication: KoinApplication) {
        koinApplication.modules(listOf(module {
            single {
                Mockito.spy(TestExchange::class.java)
//                TestExchange()
            } bind ClientSideStopOrderExchange::class
            single(override = true) { TestTradeProvider() } bind TradeProvider::class
        }))
    }


    @Test
    fun testPlaceOrder() {
        assertNotNull(clientSideStopOrderExchange)
    }

    @Test
    fun `returns the correct value when thresholdReached is called`() {
//        val testTrade = Trade(
//                val id: String,
//        val exch: String,
//        val symb: String,
//        val t: Long,
//        val price: Double,
//        val size: Double,
//        val bid: Boolean) {
//
//        )

        assertFalse(ClientSideStopOrderExchange.thresholdReached(null, 1.0, Direction.BUY))
        assertFalse(ClientSideStopOrderExchange.thresholdReached(null, 1.0, Direction.SELL))
        assertFalse(ClientSideStopOrderExchange.thresholdReached(testTrade, 101.0, Direction.BUY))
        assertTrue(ClientSideStopOrderExchange.thresholdReached(testTrade, 99.0, Direction.BUY))
        assertTrue(ClientSideStopOrderExchange.thresholdReached(testTrade, 101.0, Direction.SELL))
        assertFalse(ClientSideStopOrderExchange.thresholdReached(testTrade, 99.0, Direction.SELL))
    }

    @Test
    fun `limit orders are TODO`() {
        assertThrows(Throwable::class.java) {
            runBlocking {
                clientSideStopOrderExchange.placeOrder(testOrderRequest.copy(type = OrderRequest.Type.LIMIT))
            }
        }
    }

    @Test
    fun `stop orders without a price throw`() {
        assertThrows(Throwable::class.java) {
            runBlocking {
                clientSideStopOrderExchange.placeOrder(testOrderRequest.copy(price = null))
            }
        }
    }

    @Test
    fun `executes market order immediately when stop order exceeds threshold`() {
        runBlocking {
            testTradeProvider.broadCast(testTrade)
            val order = clientSideStopOrderExchange.placeOrder(testOrderRequest)

            verify(clientSideStopOrderExchange).executeMarketOrder(argThat(OrderMatcher(expectedOrder)))

        }
    }

    @ParameterizedTest
    @ValueSource(strings = [ "long", "short" ])
    fun `executes market order only after reaching the threshold`(direction: String) {
        val isLong = direction == "long"
        runBlocking {
            val belowThresholdTrade = testTrade.copy(price = if (isLong) 99.0 else 101.0 )
            val alsoBelowThresholdTrade = testTrade.copy(price = if (isLong) 99.5 else 100.5 )
            val aboveThresholdTrade = testTrade.copy(price = if (isLong) 101.0 else 99.0)
            val orderRequest = testOrderRequest.copy(price=100.0, direction = if (isLong) Direction.BUY else Direction.SELL)
            testTradeProvider.broadCast(belowThresholdTrade)
            val order = clientSideStopOrderExchange.placeOrder(orderRequest)
            verify(clientSideStopOrderExchange, never()).executeMarketOrder(any())
            testTradeProvider.broadCast(alsoBelowThresholdTrade)
            verify(clientSideStopOrderExchange, never()).executeMarketOrder(any())
            testTradeProvider.broadCast(aboveThresholdTrade)
            verify(clientSideStopOrderExchange).executeMarketOrder(argThat(OrderMatcher(expectedOrder.copy(price=100.0, direction = orderRequest.direction))))
        }

    }

    @Test
    fun `canceled orders to not execute`() {
        runBlocking {
            val belowThresholdTrade = testTrade.copy(price = 99.0)
            val aboveThresholdTrade = testTrade.copy(price = 101.0 )
            testTradeProvider.broadCast(belowThresholdTrade)
            val order = clientSideStopOrderExchange.placeOrder(testOrderRequest.copy(price=100.0))
            verify(clientSideStopOrderExchange, never()).executeMarketOrder(any())
            clientSideStopOrderExchange.cancelOrder(order.id)
            testTradeProvider.broadCast(aboveThresholdTrade)
            verify(clientSideStopOrderExchange, never()).executeMarketOrder(any())
        }

    }

}