package cointradr.exchanges

import cointradr.exchanges.impl.assertNotCompleted
import cointradr.fin.model.Market
import cointradr.fin.model.Trade
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

class TestTradeProvider: TradeProvider() {
    override fun start(market: Market): Job {
        return GlobalScope.launch {  }
    }

    fun broadCast(trade: Trade) {
        runBlocking {
            broadcastTrade(trade, true)
        }
    }

}


@ExperimentalCoroutinesApi
@TestInstance(TestInstance.Lifecycle.PER_METHOD)
class TradeProviderTest {

    companion object {
        val testTrade = Trade(
                id = "id",
                exch = "exch",
                symb = "symb",
                t = 1,
                price = 100.0,
                size = 1.0,
                bid = true
        )

    }


//    @Test
//    fun `last trade Deferred does not complete before first trade arrives` () {
//        runBlocking {
//            val testTradeProvider = TestTradeProvider()
//            val deferred = testTradeProvider.getLastTrade()
//            assertNotCompleted(deferred)
//            testTradeProvider.broadCast(testTrade)
//            assertEquals(testTrade, deferred.await())
//        }
//    }

    @Test
    fun `last trade Deferred completes immediately if a trade has already been seen` () {
        runBlocking {
            val testTradeProvider = TestTradeProvider()
            testTradeProvider.broadCast(testTrade)
            val deferred = testTradeProvider.getLastTrade()
            assertEquals(testTrade, deferred)
        }
    }

}
