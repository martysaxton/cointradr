package cointradr.state

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import com.fasterxml.jackson.module.kotlin.registerKotlinModule

data class AutoTradrParams(
        val barIntervalSeconds: Int,
        val parabolicSar: ParabolicSarParams) {
    fun toYaml(): String {
        val mapper = ObjectMapper(YAMLFactory()).registerKotlinModule()
        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this)
    }

}