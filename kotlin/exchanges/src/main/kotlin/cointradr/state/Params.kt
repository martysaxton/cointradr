package cointradr.state

import cointradr.utils.loggingLaunch
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import com.fasterxml.jackson.module.kotlin.*
import kotlinx.coroutines.GlobalScope
import mu.KotlinLogging
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.io.File

class Params: KoinComponent {
    companion object {


        private val logger = KotlinLogging.logger {}
    }


//        private val homeDir = File(System.getProperty("user.home"))
//        private val cointradrHome= File(homeDir, ".cointradr")
    private val paramsFile = File("/ctparams", "params.yaml")

    private var autoTradrParams: AutoTradrParams? = null
    private val mapper = ObjectMapper(YAMLFactory()).registerKotlinModule()
    private val store: Store by inject()

    fun getParams(): AutoTradrParams {
        autoTradrParams?.let {
            return it
        } ?: run {
            val params =  try {
                val loaded = mapper.readValue<AutoTradrParams>(paramsFile)
                logger.info { "loaded params from $paramsFile : $loaded" }
                loaded
            } catch(e: Exception) {
                logger.warn(e) { "could not load params from $paramsFile"  }
                val defaults = AutoTradrParams(5, ParabolicSarParams(0.02, 0.02, 0.2))
                saveParams(defaults)
                logger.info { "using defaults: $defaults" }
                defaults
            }
            GlobalScope.loggingLaunch {
                store.update {
                    it.copy(params = autoTradrParams!!)
                }
            }
            autoTradrParams = params
            return params
        }
    }

    fun saveParams(autoTradrParams: AutoTradrParams) {
        paramsFile.writer().use {
            mapper.writerWithDefaultPrettyPrinter().writeValue(it, autoTradrParams)
        }
    }

}