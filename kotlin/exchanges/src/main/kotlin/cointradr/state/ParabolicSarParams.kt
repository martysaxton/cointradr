package cointradr.state

data class ParabolicSarParams(
        val startAF: Double,
        val increment: Double,
        val maxAF: Double
)
