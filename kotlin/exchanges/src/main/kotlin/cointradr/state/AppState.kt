package cointradr.state

import cointradr.exchanges.Order
import cointradr.exchanges.RoundTrip
import cointradr.exchanges.impl.OrderPersistence
import cointradr.fin.model.Trade
import kotlinx.coroutines.runBlocking
import org.koin.core.KoinComponent
import org.koin.core.inject

typealias AppStatePatcher = (AppState) -> AppState
typealias AppStateObserver = suspend (old: AppState, new: AppState) -> Unit



data class Wallet (
    val usdCash: Double,
    val usdInPosition: Double,
    val usdTotal: Double
)

data class BestBidAsk(
        val bestBid: Double,
        val bestAsk: Double
)

data class ParabolicSarValues(
        val sar: Double,
        val ep: Double,
        val af: Double
)

data class AppState(
    val wallet: Wallet,
    val lastTrade: Trade,
    val book: BestBidAsk,
    val psar: ParabolicSarValues,
    val recentOrders: List<Order>,
    val recentRoundTrips: List<RoundTrip>,
    val params: AutoTradrParams
)


class Store: KoinComponent {
    lateinit var appState: AppState

    private val orderPersistence: OrderPersistence by inject()

    init {
        runBlocking {

            val recentClosedOrders = orderPersistence.getFilledOrders(0, 10)

            appState = AppState(
                wallet = Wallet (
                        usdCash = 0.0,
                        usdInPosition = 0.0,
                        usdTotal = 0.0
                ),
                lastTrade =  Trade(
                        id ="0",
                        exch = "no-exchange",
                        symb = "no-symbol",
                        t = 0,
                        price = 0.0,
                        size = 0.0,
                        bid = false
                ),
                book = BestBidAsk(Double.NaN, Double.NaN),
                psar = ParabolicSarValues(sar=Double.NaN, ep=Double.NaN, af=Double.NaN),
                recentOrders = recentClosedOrders,
                recentRoundTrips = listOf(),
                params = AutoTradrParams(
                        barIntervalSeconds = 5,
                        parabolicSar = ParabolicSarParams(0.02, 0.02, 0.2))
            )
        }
    }

    private val observers = mutableListOf<AppStateObserver>()

    fun addObserver(observer: AppStateObserver) {
        observers.add(observer)
    }

    suspend fun update(patcher: AppStatePatcher) {
        val old = appState
        appState = patcher(old)
        observers.forEach { it(old, appState) }
    }
}
