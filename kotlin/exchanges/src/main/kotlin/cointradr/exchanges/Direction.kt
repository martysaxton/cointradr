package cointradr.exchanges

enum class Direction {
    BUY, SELL
}
