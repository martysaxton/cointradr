package cointradr.exchanges

data class RoundTrip(
        val entry: Order,
        val exit: Order?
        )