package cointradr.exchanges

import cointradr.fin.model.Market
import com.soywiz.klock.DateTime

data class Position(
        val market: Market,
        val disposition: Disposition,
        val size: Double,
        val entryPrice: Double,
        val entryTime: DateTime
)

