package cointradr.exchanges

import com.soywiz.klock.DateTime



data class OrderBook (
        val bids: List<Entry>,
        val asks: List<Entry>,
        val time: DateTime
) {

    enum class Side {
        BID, ASK
    }

    data class Entry(
            val side: Side,
            val size: Double,
            val price: Double
    )

}