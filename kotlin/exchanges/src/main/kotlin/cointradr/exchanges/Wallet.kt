package cointradr.exchanges

import java.lang.IllegalArgumentException

///**
// *  Separate wallet for each currency
// */
//data class WalletEntry (
//        /** total amount of capital for this entry */
//        val balance:  Double,
//        /** amount of capital not allocated to an open position or open order */
////        val available: Double
//        )

data class WalletEntry(val currency: String, val amount: Double)

data class Wallet(val balances: MutableMap<String, WalletEntry> = mutableMapOf()) {

    fun setBalance(currency: String, balance: Double) {

        balances[currency] = WalletEntry(currency, balance)
    }

    fun getCurrencies(): Collection<String> {
        return balances.keys.toSet()
    }

    fun getBalance(currency: String): WalletEntry? {
        return balances[currency]
    }

}