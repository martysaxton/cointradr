package cointradr.exchanges

import cointradr.fin.model.Trade
import cointradr.fin.model.Market
import kotlinx.coroutines.Job

interface Exchange {

    interface OrderObserver {

        suspend fun orderFilled(order: Order)

        suspend fun orderSubmitted(order: Order)

        suspend fun orderPartiallyFilled(order: Order)

        suspend fun orderCanceled(order: Order)

    }

    fun addTradeHandler(tradeHandler: TradeHandler)

    fun startTradeProviders(market: Market): Job

    fun addOrderObserver(observer: OrderObserver)

    suspend fun placeOrder(orderRequest: OrderRequest): Order

    /**
     * Returns true if the order was canceled (was still active)
     * Returns false if the order was already executed
     */
    suspend fun cancelOrder(orderId: String): Boolean

    fun getWallet(): Wallet

    fun getPositions(): Collection<Position>

    fun getPosition(market: Market): Position?

    suspend fun getLiquidationValue(market: Market): Double

    suspend fun getCashValue(): Double

    suspend fun getOrderSize(market: Market, usd: Double): Double

    suspend fun collectTrades(market: Market)

    suspend fun getOrderBook(market: Market): OrderBook
    suspend fun startOrderBookStream(market: Market)

}