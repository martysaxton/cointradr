package cointradr.exchanges

import cointradr.clients.addClientsBeans
import cointradr.exchanges.impl.MongoCappedCollectionTradeProvider
import cointradr.exchanges.impl.MongoTradeQueues
import cointradr.exchanges.impl.OrderPersistence
import cointradr.exchanges.impl.okex.OkExMakerMarketOrderManager
import cointradr.exchanges.impl.okex.OkExMarketDataExchange
import cointradr.exchanges.impl.okex.OkExTradingExchange
import cointradr.fin.model.Market
import cointradr.mongo.addMongoBeans
import cointradr.state.Store
import org.koin.core.module.Module
import org.koin.dsl.module

//val exchangeModule = module {
//
//    addClientsBeans()
//    single<TradeProvider> { MongoCappedCollectionTradeProvider() }
//    single { OkExTradingExchange(get()) }
//    single { OkExMarketDataExchange() }
//    single { OkExMakerMarketOrderManager() }
//}

fun exchangeModule(market: Market): Module {
    return module {
        addClientsBeans()
        addMongoBeans()
        single<TradeProvider> { MongoCappedCollectionTradeProvider() }
        single { OkExTradingExchange(market) }
        single { OkExMarketDataExchange() }
        single { OkExMakerMarketOrderManager(market) }
        single { Store() }
        single { OrderPersistence() }
        single { MongoTradeQueues() }
    }
}
