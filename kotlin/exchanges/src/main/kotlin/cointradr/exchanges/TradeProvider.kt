package cointradr.exchanges

import cointradr.fin.model.Market
import cointradr.fin.model.Trade
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Job
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import org.koin.core.KoinComponent

typealias TradeProviderHandler  = suspend (trade: Trade, isRealTime: Boolean) -> Unit



abstract class TradeProvider: KoinComponent {

    private var lastTrade : Trade? = null
    private val lastTradeDeferreds = mutableListOf<CompletableDeferred<Trade>>()
    private val lastTradeMutex = Mutex()
    private val handlers = mutableListOf<TradeProviderHandler>()

    abstract fun start(market: Market): Job

    fun addHandler(tradeHandler: TradeProviderHandler) {
        handlers.add(tradeHandler)
    }

    protected suspend fun broadcastTrade(trade: Trade, isRealTime: Boolean) {
        lastTrade = trade
        handlers.forEach { it(trade, isRealTime) }
        lastTradeMutex.withLock {
            lastTradeDeferreds.forEach {
                it.complete(trade)
            }
        }
    }

    suspend fun getLastTrade(): Trade {
        lastTradeMutex.withLock {
            val rv = CompletableDeferred<Trade>()
            if (lastTrade != null) {
                rv.complete(lastTrade!!)
            } else {
                lastTradeDeferreds.add(rv)
            }
            return (getLastTrade@rv).await()
        }
    }




}