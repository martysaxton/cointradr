package cointradr.exchanges.impl.okex

import cointradr.clients.okex.*
import cointradr.exchanges.Direction
import cointradr.fin.model.Market
import cointradr.state.Store
import cointradr.utils.loggingAsync
import cointradr.utils.loggingLaunch
import com.okcoin.commons.okex.open.api.bean.futures.param.Order
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import mu.KotlinLogging
import org.koin.core.KoinComponent
import org.koin.core.inject
import kotlin.math.pow

private fun Double.roundToDecimals(numDecimals: Int): Double {
    val x = 10.0.pow(numDecimals)
    return Math.round(this * x) / x
}

const val TEST_MARGIN = 0.0

data class OrderResult(
        val clientId: String,
        val instrumentId: String,
        val type: OrderType,
        var price: Double,
        var size: Int,
        var filled: Int,
        var averagePrice: Double,
        var fee: Double
)

typealias OrderFilledHandler = suspend (orders: List<OrderResult>) -> Unit

fun FuturesTransactionTypeEnum.toDirection(): Direction {
    return when (this) {
        FuturesTransactionTypeEnum.OPEN_LONG -> Direction.BUY
        FuturesTransactionTypeEnum.CLOSE_SHORT -> Direction.BUY
        FuturesTransactionTypeEnum.CLOSE_LONG -> Direction.SELL
        FuturesTransactionTypeEnum.OPEN_SHORT -> Direction.SELL
    }
}

typealias OrderType = FuturesTransactionTypeEnum

class OkExMakerMarketOrderManager(market: Market) : KoinComponent {

    private companion object {
        private val logger = KotlinLogging.logger {}
    }

    private val okExClient by inject<OkExRestClient>()
    private val okExStream by inject<OkExStreamV3>()
    private val mutex = Mutex()
    private var book: OkExBook? = null
    private val store: Store by inject()

    private enum class OrderStatus {
        IDLE,
        ACTIVE,
        CANCELLING,
        FILLED,
    }

    private data class FillInfo(val price: Double, val size: Int, val fee: Double)

    private data class OrderState(
            val clientId: String,
            val instrumentId: String,
            val type: OrderType,
            var exchangeId: String,
            var size: Int,
            var price: Double,
            var status: OrderStatus,
            var previousOrdersFillInfo: FillInfo = FillInfo(0.0, 0, 0.0),
            var currentOrderFillInfo: FillInfo = FillInfo(0.0, 0, 0.0)
            )

    private val clientIdToStates = mutableMapOf<String, List<OrderState>>()
    private val clientIdToCompletableDeferred = mutableMapOf<String, CompletableDeferred<List<OrderResult>>>()
    private val okExIdToClientId = mutableMapOf<String,String>()

    init {
        okExStream.addObserver(
            object : OkExStreamV3.AccountObserver {
                override suspend fun onOrders(orders: List<OkExOrder>) {
                    mutex.withLock {
                        orders.forEach {updatedOrder ->
                            val clientId = okExIdToClientId[updatedOrder.order_id]
                            if (clientId != null) {
                                // got an update for an order we are managing
                                val orderStateList = clientIdToStates[clientId]
                                if (orderStateList == null) {
                                    logger.error { "could not find order list for managed order: clientId=$clientId exchangeId=${updatedOrder.order_id}" }
                                } else {
                                    val orderState = orderStateList.find { updatedOrder.instrument_id == it.instrumentId && updatedOrder.order_id == it.exchangeId }
                                    orderState?.let {
                                        when(updatedOrder.status) {
                                            OKEX_ORDER_CANCELLED -> handleCancelled(it, updatedOrder)
                                            OKEX_ORDER_FAILURE -> handleOrderFailure(it)
                                            OKEX_ORDER_PENDING -> handleOrderPending(it)
                                            OKEX_ORDER_PARTIALLYFILLED -> handleOrderPartiallyFilled(it, updatedOrder)
                                            OKEX_ORDER_FILLED -> handleOrderFilled(it, updatedOrder)
                                        }
                                    } ?: logger.error { "could not find order in list for managed order: clientId=$clientId exchangeId=${updatedOrder.order_id}" }
                                    if (orderState == null) {

                                    } else {
                                    }
                                }
                            }
                        }
                    }
                }

                override suspend fun onAccounts(accounts: Map<String, OkExAccount>) {}
                override suspend fun onPositions(positions: List<OkExPosition>) {}
            }
        )

//        okExStream.addTradeHandler { trades ->
//            mutex.withLock { lastTrade = trades.last() }
//        }

        okExStream.addDepthHandler { book ->
            var newBestBid: Double? = null
            var newBestAsk: Double? = null
            mutex.withLock {
                this.book?.let {
                    val prevBba = it.getBestBidAsk()
                    val newBba = book.getBestBidAsk()
                    if (newBestBid != newBba.bestBid || prevBba.bestAsk != newBba.bestAsk) {
                        store.update { state -> state.copy(book = cointradr.state.BestBidAsk(newBba.bestBid, newBba.bestAsk)) }
                    }
                    if (prevBba.bestBid < newBba.bestBid) {
                        newBestBid = newBba.bestBid
                    }
                    if (prevBba.bestAsk > newBba.bestAsk) {
                        newBestAsk = newBba.bestAsk
                    }

                 }

                this.book = book
            }

            if (newBestBid != null || newBestAsk != null) {
                updateOrders(newBestBid, newBestAsk)
            }
        }

        GlobalScope.loggingLaunch {
            okExStream.subscribe(market.toOkExSubscription("depth"))
        }

    }

    private fun handleOrderPending(state: OrderState) {
        logger.debug { "order pending clientId=${state.clientId} instrumentId=${state.instrumentId} type=${state.type} exchangeId=${state.exchangeId}" }
    }

    private fun handleOrderPartiallyFilled(state: OrderState, updatedOrder: OkExOrder) {
        // Multiple partial fills can come in, after which
        // the order can be fully filled, or cancelled for resubmission.
        // We need to keep track of how much of the fills has been baked in to the OrderState
        // OrderState needs 2 sets of (filledSize, filledPrice),
        // one for the current order, and one for all previous orders
        // the fillInfo for the current order can be overwritten by this update (partial fill)
        // When cancelling, the fillInfo can be moved from currentOrder to pastOrders
        logger.debug { "partial fill clientId=${state.clientId} instrumentId=${state.instrumentId} type=${state.type} exchangeId=${state.exchangeId} filledPrice=${updatedOrder.filled_qty} filledSize=${updatedOrder.filled_qty}" }
        state.currentOrderFillInfo = FillInfo(updatedOrder.price_avg.toDouble(), updatedOrder.filled_qty.toInt(), updatedOrder.fee.toDouble())
    }

    private suspend fun handleOrderFilled(state: OrderState, updatedOrder: OkExOrder) {
        // orders can be cancelled and resubmitted after partial fills
        // OrderState holds the original size.
        // Average price must be recomputed after partial fills too, because
        // an order might be partially filled, then resubmitted.
        // Order filled should always be the last update of an order
        // Since a partial fill has already been baked in to the OrderState,
        // only the new part of this update should be considered
        logger.debug { "fill clientId=${state.clientId} instrumentId=${state.instrumentId} type=${state.type} exchangeId=${state.exchangeId} filledPrice=${updatedOrder.price_avg} filledSize=${updatedOrder.filled_qty}" }
        state.currentOrderFillInfo = FillInfo(updatedOrder.price_avg.toDouble(), updatedOrder.filled_qty.toInt(), updatedOrder.fee.toDouble())
        if (allOrdersComplete(state.clientId)) {
            logger.debug { "all orders for clientId=${state.clientId} complete" }
            val resultList = clientIdToStates[state.clientId]!!.map { s ->
                val totalFilled = s.previousOrdersFillInfo.size + s.currentOrderFillInfo.size
                val avgPrice = computeAveragePrice(s.previousOrdersFillInfo, s.currentOrderFillInfo)
                val fee = s.previousOrdersFillInfo.fee + s.currentOrderFillInfo.fee
                OrderResult(
                        clientId =  s.clientId,
                        instrumentId =  s.instrumentId,
                        type =  s.type,
                        price =  s.price,
                        size =  s.size,
                        filled = totalFilled,
                        averagePrice = avgPrice,
                        fee = fee)
            }
            val clientId = resultList.first().clientId
//            orderFilledHandler(resultList)
            val completableDeferred = clientIdToCompletableDeferred[clientId]!!
            completableDeferred.complete(resultList)
        }

    }

    private suspend fun handleCancelled(orderState: OrderState, orderUpdate: OkExOrder) {
        logger.debug { "order cancelled ${buildLogString(orderState, orderUpdate)}" }
        with (orderState) {
//            currentOrderFillInfo = FillInfo(orderUpdate.price_avg.toDouble(), orderUpdate.filled_qty.toInt())
            previousOrdersFillInfo = FillInfo(
                    price = computeAveragePrice(currentOrderFillInfo, previousOrdersFillInfo),
                    size = currentOrderFillInfo.size + previousOrdersFillInfo.size,
                    fee = currentOrderFillInfo.fee + previousOrdersFillInfo.fee)
            status = OrderStatus.IDLE

        }
        val result = submitOrderToExchange(orderState).await()
        updateOrderStateAfterSubmission(result, orderState)
    }

    private fun computeAveragePrice(fillInfo1: FillInfo, fillInfo2: FillInfo): Double {
        val totalFilled = (fillInfo1.size + fillInfo2.size).toDouble()
        return ((fillInfo1.price * fillInfo1.size) + (fillInfo2.price * fillInfo2.size)) / totalFilled
    }

    private fun buildLogString(state: OrderState, updatedOrder: OkExOrder): String {
        return "clientId=${state.clientId} instrumentId=${state.instrumentId} type=${state.type} exchangeId=${state.exchangeId} filledPrice=${updatedOrder.price_avg} filledSize=${updatedOrder.filled_qty}\""
    }


    private fun allOrdersComplete(clientId: String): Boolean {
        val stateList = clientIdToStates[clientId]
        stateList?.let { sl ->
            return sl.all {
                val totalFilled = it.previousOrdersFillInfo.size + it.currentOrderFillInfo.size
                totalFilled == it.size
            }

        }
        logger.error { "should not happen in allOrdersComplete" }
        return false
    }


    private fun handleOrderFailure(state: OrderState) {
        logger.error { "order failure:  $state" }
    }

    private fun updateOrders(newBestBid: Double?, newBestAsk: Double?) {
        val ordersToCancel = mutableListOf<OrderState>()
        clientIdToStates.values.forEach { orderStateList ->
            orderStateList.forEach { orderState ->
                if (shouldUpdateOrder(orderState, newBestBid, newBestAsk)) {
                    logger.debug { "canceling order $orderState, newBestBid=$newBestBid newBestAsk=$newBestAsk" }
                    ordersToCancel.add(orderState)
                }
            }
        }
        cancelOrders(ordersToCancel)
    }

    private fun shouldUpdateOrder(orderState: OrderState, newBestBid: Double?, newBestAsk: Double?): Boolean {
        val direction = orderState.type.toDirection()
        return when (direction) {
            Direction.BUY -> newBestBid != null && orderState.price < newBestBid
            Direction.SELL -> newBestAsk != null && orderState.price > newBestAsk
        }
    }

    private fun cancelOrders(orderStates: List<OrderState>) {
        // TODO this assumes all orders are of same instrument
        if (!orderStates.isEmpty()) {
            val instrumentId = orderStates.first().instrumentId
            val orderIds = orderStates.map { it.exchangeId.toLong() }
            okExClient.cancelOrders(instrumentId, orderIds)
        }
    }

    suspend fun placeOrders(clientId: String, orders: List<Order>): Deferred<List<OrderResult>> {
        mutex.withLock {
            logger.debug { "placing orders:  $clientId $orders" }
            val states = buildStates(clientId, orders)
            clientIdToStates[clientId] = states
            val deferredResults = states.map {
                submitOrderToExchange(it)
            }
            for (i in 0 until deferredResults.size) {
                val result = deferredResults[i].await()
                updateOrderStateAfterSubmission(result, states[i])
            }
            val completableDeferred = CompletableDeferred<List<OrderResult>>()
            clientIdToCompletableDeferred[clientId] = completableDeferred
            return completableDeferred
        }
    }

    private fun updateOrderStateAfterSubmission(result: com.okcoin.commons.okex.open.api.bean.futures.result.OrderResult, orderState: OrderState) {
        if (result.isResult) {
            logger.debug { "order submission successful:  clientId=${result.client_oid} exchangeId=${result.order_id}" }
            orderState.exchangeId = result.order_id
            okExIdToClientId[result.order_id] = orderState.clientId
        } else {
            logger.error{ "orderResult ${result.isResult} serverId=${result.order_id} error_code=${result.error_code} error_message=${result.error_messsage}" }
        }

    }

    private fun buildStates(clientId: String, orders: List<Order>): List<OrderState> {

        data class InstrumentOrderType(val instrumentId: String, val type: OrderType)

        val map = mutableMapOf<InstrumentOrderType, Int>()
        orders.forEach {
            val instOt = InstrumentOrderType(it.instrument_id, OrderType.from(it.type))

            val x = map[instOt] ?: 0
            map[instOt] = it.size + x
        }
        return map.map { (instOt, size) -> OrderState(
                clientId = clientId,
                instrumentId = instOt.instrumentId,
                exchangeId = "",
                type = instOt.type,
                status = OrderStatus.IDLE,
                price = 0.0,
                size = size
                )}
    }

    private suspend fun submitOrderToExchange(orderState: OrderState): Deferred<com.okcoin.commons.okex.open.api.bean.futures.result.OrderResult> {
        val newPrice = getPrice(orderState.type.toDirection())
        val order = Order().apply {
            client_oid = orderState.clientId
            setinstrument_id(orderState.instrumentId)
            leverage = 10.0
            type = orderState.type.code()
            price = newPrice
            size = orderState.size - orderState.previousOrdersFillInfo.size
            match_price = 0
        }
        orderState.price = newPrice
        logger.debug { "submitting order:  $order" }
        return GlobalScope.loggingAsync { okExClient.order(order) }
    }

    private suspend fun getPrice(direction: Direction): Double {
        book?.let {
            val bba = it.getBestBidAsk()
            val rv = when (direction) {
                Direction.BUY ->  minOf( bba.bestBid + 0.01, bba.bestAsk - 0.01 ) - TEST_MARGIN
                Direction.SELL ->  maxOf( bba.bestBid + 0.01, bba.bestAsk - 0.01 ) + TEST_MARGIN
            }
            return rv.roundToDecimals(2)
        } ?: throw IllegalStateException("asked for a price before ever getting a book")
    }

}