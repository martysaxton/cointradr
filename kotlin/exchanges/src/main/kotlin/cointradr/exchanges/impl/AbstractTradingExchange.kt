@file:Suppress("DEPRECATION")

package cointradr.exchanges.impl

import cointradr.exchanges.Order
import cointradr.exchanges.OrderRequest
import cointradr.exchanges.Position
import cointradr.exchanges.TradingExchange
import cointradr.fin.model.Market
import cointradr.state.Store
import cointradr.utils.ShortUuid
import com.soywiz.klock.DateTime
import mu.KotlinLogging
import org.koin.core.KoinComponent
import org.koin.core.inject


abstract class AbstractTradingExchange(private val name: String) : TradingExchange, KoinComponent {

    companion object {

        private val logger = KotlinLogging.logger {}

        fun createPlacedOrder(orderRequest: OrderRequest, filledPrice: Double?, whenFilled: DateTime?): Order {
            val now = whenFilled ?: DateTime.now()
            return Order(
                    id = ShortUuid.create(),
                    mktSym = orderRequest.market.toString(),
                    type = orderRequest.type,
                    direction = orderRequest.direction,
                    size = orderRequest.size,
                    price = orderRequest.price,
                    filledPrice = filledPrice,
                    filledQuantity = if (filledPrice == null) 0.0 else orderRequest.size,
                    whenPlaced = now.unixMillisLong,
                    whenFilled = whenFilled?.unixMillisLong,
                    state = Order.State.ACTIVE)
        }
    }

    private val store: Store by inject()
    private val orderObservers = mutableListOf<TradingExchange.OrderObserver>()
    private val positions = mutableMapOf<Market, Position>()
    private val orderPersistence: OrderPersistence by inject()

    override fun addOrderObserver(observer: TradingExchange.OrderObserver) {
        orderObservers.add(observer)
    }

    suspend fun broadcastOrderPartiallyFilled(order: Order) {
        orderObservers.forEach { it.orderPartiallyFilled(order) }
    }

    fun getName(): String {
        return name
    }

    override fun getPositions(): Collection<Position> {
        return positions.values
    }

    override fun getPosition(market: Market): Position? {
        return positions[market]
    }

    protected fun positionClosed(market: Market) {
        positions.remove(market)
    }

    protected fun setPosition(position: Position) {
        positions[position.market] = position
    }

    open suspend fun orderPlaced(order: Order) {
        orderPersistence.insertOrder(order)
    }


    open suspend fun orderFilled(order: Order) {
        val filledOrder = order.copy(state = Order.State.FILLED)
        orderPersistence.updateOrder(filledOrder)
        orderObservers.forEach { it.orderFilled(filledOrder) }


        store.update {
            val recentOrders = it.recentOrders.toMutableList()
            if (recentOrders.size == 10) {
                recentOrders.removeAt(recentOrders.lastIndex)
            }
            recentOrders.add(0, order)
            it.copy(recentOrders = recentOrders)
        }

    }

    open suspend fun orderCanceled(order: Order) {
        val canceledOrder = order.copy(state = Order.State.CANCELED)
        orderPersistence.updateOrder(canceledOrder)
        orderObservers.forEach { it.orderCanceled(canceledOrder) }
    }


}

//fun main() = runBlocking {
//    val m = Market("okex", "btc", "usd", "190329")
//    val openOrders = OrderPersistence.getOpenOrders(m)
//    println("${openOrders.size} open orders")
//    delay(Long.MAX_VALUE)
//    println("done")
//
//}
