package cointradr.exchanges.impl

import cointradr.exchanges.Order
import cointradr.fin.model.Market
import cointradr.mongo.Mongo
import cointradr.utils.loggingLaunch
import com.mongodb.client.model.Filters
import com.mongodb.client.model.IndexOptions
import kotlinx.coroutines.GlobalScope
import mu.KotlinLogging
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.litote.kmongo.coroutine.*
import org.litote.kmongo.eq
import org.litote.kmongo.orderBy

class OrderPersistence: KoinComponent {

    companion object {
        private val logger = KotlinLogging.logger {}
    }

    private val mongo: Mongo by inject()
    private val ordersCollection = mongo.getMongoDatabase().getCollectionOfName<Order>("orders")

    init {
        GlobalScope.loggingLaunch {
            logger.debug { "creating search index for orders" }
            val openOrdersIndexName = ordersCollection.createIndex(
                    """ {
                            "mktSym": 1,
                            "state": 1
                        }
                    """.trimIndent())
            logger.debug { "created search index for orders: $openOrdersIndexName" }
            logger.debug { "creating id index for orders: $openOrdersIndexName" }
            val idIndexName = ordersCollection.createIndex("{id:1}")
            logger.debug { "created id index for orders: $idIndexName" }
            logger.debug { "creating unique id+mktSym index for orders: $openOrdersIndexName" }
            val uniqueIndexName = ordersCollection.createIndex("{id:1, mktSym:1}", IndexOptions().unique(true))
            logger.debug { "created id index for orders: $uniqueIndexName" }
        }
    }
    suspend fun insertOrder(order: Order) {
        val result = ordersCollection.insertOne(order)
        logger.debug { "inserted order: $order; result=$result" }
    }

    suspend fun updateOrder(order: Order) {
        val filter = Filters.eq("id", order.id)
        val result = ordersCollection.updateOne(filter, order)
        logger.debug { "updated order: $order; result=$result" }
    }

    suspend fun getOpenOrders(market: Market): List<Order> {
//        val rv = mutableListOf<Order>()
        return ordersCollection.find(Filters.and(
                Filters.eq("mktSym", market.toString()),
                Filters.eq("state", Order.State.ACTIVE)
        )).toList()
    }

    suspend fun getFilledOrders(start: Int, limit: Int): List<Order>{
        return ordersCollection
                .find(Order::state eq Order.State.FILLED)
                .skip(start)
                .limit(limit)
                .sort(orderBy(Order::whenFilled))
                .toList()
    }

}