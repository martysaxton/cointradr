package cointradr.exchanges.impl

import cointradr.exchanges.Wallet

interface WalletProvider {
    fun getWallet(): Wallet
}