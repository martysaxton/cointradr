package cointradr.exchanges.impl.okex.mockfill

import cointradr.exchanges.Direction
import cointradr.exchanges.Position
import cointradr.fin.model.Market

interface FillPriceProvider {
    suspend fun getFillPrice(market: Market, direction: Direction): Double
    suspend fun getLiquidationValue(position: Position?): Double
    suspend fun getOrderSize(market: Market, usd: Double): Double

}