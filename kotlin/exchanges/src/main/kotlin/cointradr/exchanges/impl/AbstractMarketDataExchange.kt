package cointradr.exchanges.impl

import cointradr.exchanges.Exchange
import cointradr.exchanges.MarketDataExchange
import cointradr.exchanges.TradeHandler
import cointradr.fin.model.Trade
import mu.KotlinLogging
import org.koin.core.KoinComponent

abstract class AbstractMarketDataExchange: MarketDataExchange, KoinComponent {

    private val tradeHandlers = mutableListOf<TradeHandler>()

    companion object {
        private val logger = KotlinLogging.logger {}
    }

    override fun addTradeHandler(tradeHandler: TradeHandler) {
        tradeHandlers.add(tradeHandler)
    }

    protected suspend fun broadcastTrade(trade: Trade) {
        tradeHandlers.forEach { it(trade) }
    }

}