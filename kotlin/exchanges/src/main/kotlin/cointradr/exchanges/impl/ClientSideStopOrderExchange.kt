package cointradr.exchanges.impl

import cointradr.exchanges.Direction
import cointradr.exchanges.Order
import cointradr.exchanges.OrderRequest
import cointradr.exchanges.TradeProvider
import cointradr.fin.model.Trade
import cointradr.state.Store
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import mu.KotlinLogging
import org.koin.core.inject

/**
 * Implements stop orders by monitoring trades and placing market orders when the price threshold is crossed.
 */
abstract class ClientSideStopOrderExchange(name: String) : AbstractTradingExchange(name) {

    companion object {
        private val logger = KotlinLogging.logger {}
        internal fun thresholdReached(trade: Trade?, orderPrice: Double, direction: Direction): Boolean{
            when (direction) {
                Direction.BUY -> {
                    // BUY stops is when market price rises above order price
                    trade?.let { pt ->
                        if (pt.price >= orderPrice) {
                            return true
                        }
                    }
                }
                Direction.SELL -> {
                    // SELL stops is when market price falls below order price
                    trade?.let { pt ->
                        if (pt.price <= orderPrice) {
                            return true
                        }
                    }
                }
            }
            return false
        }
    }

    private val store: Store by inject()
    protected val tradeProvider: TradeProvider by inject()
    protected val exchangeMutex = Mutex()

    private val openStopOrders = mutableListOf<Order>()

    override suspend fun placeOrder(orderRequest: OrderRequest): Order {
        logger.debug { "placeOrders: $orderRequest" }
        var newOrder: Order? = null
        exchangeMutex.withLock {
            logger.debug { "orderRequest submitted:  $orderRequest" }
            newOrder = when(orderRequest.type) {
                OrderRequest.Type.MARKET -> {
                    val placedOrder = createPlacedOrder(orderRequest, null, null)
                    executeMarketOrder(placedOrder)
                    return placedOrder
                }
                OrderRequest.Type.STOP -> addStopOrder(orderRequest)
                else -> TODO("not implemented")
            }
        }
        orderPlaced(newOrder!!)
        return newOrder!!
    }

    protected suspend fun addStopOrder(orderRequest: OrderRequest): Order {
        if (orderRequest.price == null) {
            throw throw IllegalArgumentException("price is required for stop orders")
        }
        val placedOrder = AbstractExchange.createPlacedOrder(orderRequest, null, null)
        val prevTrade = tradeProvider.getLastTrade()
        val executeImmediately = thresholdReached(prevTrade, orderRequest.price, orderRequest.direction)

        if (executeImmediately) {
            logger.debug { "price threshold exceeded for new stop orderRequest: $orderRequest" }
            executeMarketOrder(placedOrder)
        } else {
            logger.debug { "price threshold not exceeded for new stop orderRequest: $placedOrder" }
            openStopOrders.add(placedOrder)
            if (openStopOrders.size > 1) {
                logger.warn { "more than 1 open orderRequest unexpected:  ${getOpenOrders()} " }
            }
        }
        return placedOrder

    }

    override suspend fun cancelOrder(orderId: String): Boolean {
        var openOrder: Order? = null
        exchangeMutex.withLock {
            logger.debug { "searching for open stop order $orderId;  open orders=${getOpenOrders()}" }
            val iter = openStopOrders.iterator()
            while (iter.hasNext()) {
                val o = iter.next()
                if (o.id == orderId) {
                    iter.remove()
                    openOrder = o
                    break
                }
            }
        }
        logger.debug { "searched for open order, found $openOrder; open orders=${getOpenOrders()}" }

        openOrder?.let {
            if (openStopOrders.size != 0) {
                logger.warn { "expected 0 open orders:  ${getOpenOrders()}" }
            }
            logger.debug { "broadcasting cancellation" }
            orderCanceled(it)
            return true
        } ?: return false
    }

    private fun getOpenOrders() : List<String> {
        return openStopOrders.map { it.id }
    }

    internal suspend fun tradeHandler(trade: Trade, isRealTime: Boolean) {

        store.update {
            it.copy( lastTrade=trade )
        }
        exchangeMutex.withLock {

            val iter = openStopOrders.iterator()
            while (iter.hasNext()) {
                val openOrder = iter.next()
                logger.debug { "trade ${trade.price} open stop order: ${openOrder.direction} ${openOrder.price}" }
                var execute = false
                if (openOrder.direction == Direction.BUY && trade.price >= openOrder.price!!) {
                    execute = true
                } else if (openOrder.direction == Direction.SELL && trade.price <= openOrder.price!!) {
                    execute = true
                }
                if (execute) {
                    logger.debug { "price=${trade.price} threshold reached for stop order: $openOrder" }
                    iter.remove()
                    if (openStopOrders.size != 0) {
                        logger.warn { "expected 0 open orders:  ${getOpenOrders()}" }
                    }
                    executeMarketOrder(openOrder)
                    logger.debug { "${openStopOrders.size} open orders remaining: ${getOpenOrders()}" }
                }
            }
        }
    }

    init {
        tradeProvider.addHandler(::tradeHandler)
    }

//    override fun startTradeProviders(market: Market): Job {
//        return tradeProvider?.start(market) ?: GlobalScope.loggingLaunch {
//            while(true) {
//                delay(Long.MAX_VALUE)
//            }
//        }
//    }

    abstract suspend fun executeMarketOrder(order: Order)

}