@file:Suppress("DEPRECATION")

package cointradr.exchanges.impl

import cointradr.fin.model.Market
import cointradr.fin.model.Trade
import cointradr.mongo.Mongo
import com.mongodb.async.client.MongoCollection
import com.mongodb.client.model.IndexOptions
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import mu.KotlinLogging
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.litote.kmongo.coroutine.createIndex

const val MAX_TRADE_COUNT = 1000L
const val TRADE_DOCUMENT_SIZE = 200L

@Suppress("DEPRECATION")
class MongoTradeQueues: KoinComponent {

    companion object {
        private val logger = KotlinLogging.logger {}
    }
    private val mongo: Mongo by inject()

    private val marketToTradeQueueCollection = mutableMapOf<Market, MongoCollection<Trade>>()
    private val tradeQueueMutex = Mutex()

    suspend fun getCollection(trade: Trade): MongoCollection<Trade> {
        val market = Market.toMarket(trade.exch, trade.symb)
        return getCollection(market)
    }

    suspend fun getCollection(market: Market): MongoCollection<Trade> {
        tradeQueueMutex.withLock {
            var collection = marketToTradeQueueCollection[market]
            if (collection == null) {
                logger.debug { "trade queue for $market not present in cache, getting from Db"}
                val collectionName = getCollectionName(market)
                collection = mongo.getCappedCollection(collectionName, MAX_TRADE_COUNT * TRADE_DOCUMENT_SIZE, MAX_TRADE_COUNT, Trade::class.java)
                createUniqueIndexes(collection)
                marketToTradeQueueCollection[market] = collection
            }
            return collection
        }
    }

    private suspend fun createUniqueIndexes(collection: MongoCollection<Trade>) {
        logger.debug("creating unique indexes")
        val uniqueIndexOptions = IndexOptions()
        uniqueIndexOptions.unique(true)
        collection.createIndex("{exch: 1, symb: 1, id: 1}", uniqueIndexOptions)
    }

    private fun getCollectionName(market: Market): String {
        return "trades.$market"
    }



}