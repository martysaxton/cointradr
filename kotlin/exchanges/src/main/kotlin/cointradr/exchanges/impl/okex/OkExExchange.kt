//package cointradr.exchanges.impl.okex
//
//import cointradr.clients.okex.*
//import cointradr.exchanges.*
//import cointradr.exchanges.impl.AbstractExchange
//import cointradr.exchanges.impl.ClientSideStopOrderExchange
//import cointradr.exchanges.TradeProvider
//import cointradr.fin.model.Market
//import cointradr.fin.model.Trade
//import cointradr.geoip.GeoIP
//import cointradr.state.Store
//import cointradr.utils.ShortUuid
//import cointradr.utils.loggingAsync
//import cointradr.utils.loggingLaunch
//import cointradr.utils.loggingRunBlocking
//import com.soywiz.klock.DateTime
//import mu.KotlinLogging
//import java.lang.RuntimeException
//import com.okcoin.commons.okex.open.api.config.APIConfiguration
//import com.okcoin.commons.okex.open.api.enums.FuturesTransactionTypeEnum
//import com.okcoin.commons.okex.open.api.service.futures.FuturesTradeAPIService
//import kotlinx.coroutines.*
//import kotlinx.coroutines.sync.Mutex
//import kotlinx.coroutines.sync.withLock
//
//
//// get instruments from https://www.okex.com/api/futures/v3/instruments
//// curl -q https://www.okex.com/api/futures/v3/instruments | python -m json.tool | grep BTC
//
//
//
//
//class OkExExchange(
//        tradeProvider: TradeProvider? = null
//    ) : ClientSideStopOrderExchange(name = "okex", tradeProvider = tradeProvider) {
//
//    companion object {
//        private val logger = KotlinLogging.logger {}
//    }
//
//    private val restClient = OkExRestClient()
//    private var okexApiConfig: APIConfiguration? = null
//    private lateinit var futuresTradingApi: FuturesTradeAPIService
//    private lateinit var lastTrade: Trade // used only during trade collector
//    private val openOrders = mutableMapOf<String, String>()
//    private val openOrderMutex = Mutex()
//    private val accounts= mutableMapOf<String, OkExAccount>()
//    private val accountsMutex = Mutex()
//    private val positions = mutableMapOf<String, OkExPosition>()
//    private val positionsMutex = Mutex()
//
//    private val makerMarketOrderHandler: OrderFilledHandler = { orderResults ->
//        logger.info { "orders filled: $orderResults" }
//    }
//
//    private val makerMarketOrderManager = OkExMakerMarketOrderManager(OkExStreamV3, restClient)
//
//    private  val okexTradeHandler: OkExTradeHandler = { trades ->
//        if (!trades.isEmpty()) {
//            val okexMarket = trades[0].market
//            val marketStr = "${okexMarket.base}/${okexMarket.quote}${if (okexMarket.futureExpiry == null) "" else "-f"}"
//
//            // TODO
//            // each OkExTrade.size unit is in # of contracts,
//            // each contract is worth $100
//            // size in the 'exchange' world should be in terms of the base currency
//            // not sure if other okex futures markets (e.g., eth/usd) have contracts worth $100 each
//
//            val normalizedTrades = trades.map {
//                Trade(
//                    id = it.id,
//                    exch = "okex",
//                    symb = marketStr,
//                    t = it.t,
//                    price = it.price,
//                    size = it.size,
//                    bid = it.bid)
//            }
//            lastTrade = normalizedTrades.last()
//            normalizedTrades.forEach { trade ->
//                onLiveTrade(trade)
//            }
//        }
//    }
//
//    override suspend fun collectTrades(market: Market) {
//        val subscription = market.toOkExSubscription("trade")
//        OkExStreamV3.subscribe(subscription)
//    }
//
//    private val accountObserver = object : OkExStreamV3.AccountObserver {
//        override suspend fun onOrders(orders: List<OkExOrder>) {
//            openOrderMutex.withLock {
//                logger.debug { "got order update:  $orders" }
//                orders.forEach {
//                    val exchangeOrderId = it.order_id
//                    val clientOrderId = openOrders[exchangeOrderId]
//                    val filled = it.size == it.filled_qty
//                    logger.debug { "order clientId=$clientOrderId filled=$filled $it" }
//                    if (filled) {
//                        openOrders.remove(exchangeOrderId)
//                    }
//                }
//            }
//        }
//
//        override suspend fun onAccounts(accounts: Map<String, OkExAccount>) {
//            logger.debug { "got account update:  $accounts" }
//        }
//
//        override suspend fun onPositions(positions: List<OkExPosition>) {
//            logger.debug { "got position update:  $positions" }
//            positions.forEach {
//                updatePositions(it)
//            }
//        }
//
//    }
//
//
//    init {
//        OkExStreamV3.addTradeHandler(okexTradeHandler)
//        OkExStreamV3.addObserver(accountObserver)
//
//        // TODO where is the retry logic here?
//        GlobalScope.loggingLaunch { OkExStreamV3.connect() }
//    }
//
//        override suspend fun executeMarketOrder(order: Order) {
//
//        val clientId = ShortUuid.create()
//        val orderList = translateOrders(order)
//        var makerOrderDeferred: Deferred<List<OrderResult>> = makerMarketOrderManager.placeOrders(clientId, orderList)
////        var makerOrderDeferred: Deferred<List<OrderResult>>? = null
////        if (asMaker) {
////            makerOrderDeferred = makerMarketOrderManager.placeOrders(clientId, orderList)
////        } else {
////            val deferredOrderResults = orderList.map { order ->
////                GlobalScope.loggingAsync { restClient.order(order) }
////            }
////
////            deferredOrderResults.forEach {
////                val orderResult = it.await()
////                if (!orderResult.isResult) {
////                    logger.error { "result ${orderResult.isResult} serverId=${orderResult.order_id} error_code=${orderResult.error_code} error_message=${orderResult.error_messsage}" }
////                }
////                openOrderMutex.withLock {
////                    logger.debug { "order placed clientId=clientId  exchangeId=${orderResult.order_id}" }
////                    openOrders[orderResult.order_id] = clientId
////                }
////            }
////        }
//
//        GlobalScope.loggingLaunch {
//            makerOrderDeferred?.let {
//                try {
//                    val result = it.await()
//                    logger.debug { "order result: $result" }
//                } catch(t: Throwable) {
//                    logger.error(t) { "exception while filling market order as maker"}
//                }
//            }
//        }
//
////        return Order(
////                id = clientId,
////                mktSym = orderRequest.market.toString(),
////                type = orderRequest.type,
////                direction = orderRequest.direction,
////                size = orderRequest.size,
////                price = orderRequest.price,
////                filledQuantity = 0.0,
////                filledPrice = null,
////                whenPlaced = DateTime.nowUnixLong(),
////                whenFilled = 0L,
////                state = Order.State.ACTIVE)
//    }
//
//    private suspend fun translateOrders(orderRequest: Order): List<com.okcoin.commons.okex.open.api.bean.futures.param.Order> {
//
////        when (orderRequest.type) {
////            OrderRequest.Type.MARKET -> {
////                order.match_price = 1
////            }
////        }
//
//        val rv = mutableListOf<com.okcoin.commons.okex.open.api.bean.futures.param.Order>()
////        val instrumentId = with(orderRequest.market) {
////            "${baseCurrency.toUpperCase()}-${quoteCurrency.toUpperCase()}-$expiry"
////        }
//        val instrumentId = orderRequest.mktSym
//        val contractSize = orderRequest.size.toInt()
//        val currentPositions = getPositions(instrumentId)
//        val existingContracts = if (orderRequest.direction == Direction.BUY) currentPositions.second.toInt() else currentPositions.first.toInt()
//        val contractsToClose = minOf(contractSize,existingContracts)
//        val contractsToOpen = contractSize - contractsToClose
//        logger.debug { "translateOrders ${orderRequest.direction} ${orderRequest.size}  existing=$existingContracts toOpen=$contractsToOpen toClose=$contractsToClose" }
//        if (contractsToClose > 0) {
//            val transactionType = when (orderRequest.direction) {
//                Direction.BUY -> FuturesTransactionTypeEnum.CLOSE_SHORT
//                Direction.SELL -> FuturesTransactionTypeEnum.CLOSE_LONG
//            }
//            logger.debug { "order to close existing position:  $instrumentId $transactionType $contractsToClose" }
//            val closeOrder = createMarketOrder(instrumentId, contractsToClose, transactionType)
//            rv.add(closeOrder)
//        }
//        if (contractsToOpen > 0) {
//            val transactionType = when (orderRequest.direction) {
//                Direction.BUY -> FuturesTransactionTypeEnum.OPEN_LONG
//                Direction.SELL -> FuturesTransactionTypeEnum.OPEN_SHORT
//            }
//            logger.debug { "order to open:  $instrumentId $transactionType $contractsToOpen" }
//            val openOrder = createMarketOrder(instrumentId, contractsToOpen, transactionType)
//            rv.add(openOrder)
//        }
//
//        return rv
//    }
//
//    private fun createMarketOrder(instrumentId: String, size: Int, type: FuturesTransactionTypeEnum): com.okcoin.commons.okex.open.api.bean.futures.param.Order {
//        val order = com.okcoin.commons.okex.open.api.bean.futures.param.Order()
//
//        order.type = type.code()
//        order.client_oid = ShortUuid.create()
//
//        order.setinstrument_id(instrumentId)
//        order.size = size
//        order.leverage = 10.00
//        order.match_price = 1
//        return order
//    }
//
//    override suspend fun cancelOrder(orderId: String): Boolean {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }
//
//    override fun getWallet(): Wallet {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }
//
//    override fun getPositions(): List<Position> {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }
//
//    override suspend fun getLiquidationValue(market: Market): Double {
//        positionsMutex.withLock {
//            prevTrade?.let { pt ->
//                val instrumentId = market.toOkExInstrumentId()
//                val btcPositions = positions[instrumentId]!!
//                val long = btcPositions.long_qty.toDouble() * pt.price
//                val short = btcPositions.short_qty.toDouble() * pt.price
//                return long - short
//            }
//            ?: return Double.NaN
//        }
//    }
//
//    override suspend fun getCashValue(): Double {
//        accountsMutex.withLock {
//            prevTrade?.let { pt ->
//                val btcAccount = accounts["btc"]!!
//                val btcAvailable = btcAccount.total_avail_balance.toDouble()
//                return btcAvailable * pt.price
//            }
//            ?: return Double.NaN
//        }
//    }
//
//    override suspend fun getOrderSize(market: Market, usd: Double): Double {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }
//
//    override suspend fun getOrderBook(market: Market): OrderBook {
//        // TODO figure out how to come up with expiry
//        val expiry= "190329"
//        val resp = restClient.getDepthFutures(market.baseCurrency, market.quoteCurrency, expiry, 1)
//
//        return OrderBook(
//                bids = mapToOrderBookEntryList(OrderBook.Side.BID, resp.bids),
//                asks = mapToOrderBookEntryList(OrderBook.Side.ASK, resp.asks),
//                time = DateTime.now()
//        )
//    }
//
//    override suspend fun startOrderBookStream(market: Market) {
//        val subscription = market.toOkExSubscription("depth")
//        OkExStreamV3.subscribe(subscription)
//    }
//
//
//
//    private fun mapToOrderBookEntryList(side: OrderBook.Side, entries: List<List<Double>>) : List<OrderBook.Entry> {
//        return entries.map { OrderBook.Entry(
//                side = side,
//                size = it[1],
//                price = it[0]
//        ) }
//
//    }
//    //        send("{\"op\": \"subscribe\", \"args\": [\"futures/depth:BTC-USD-190329\"]}")
//
//    suspend fun authenticate()  {
//        val myCountry = GeoIP.getMyCountry()
//        if (myCountry == "US") {
//            throw RuntimeException("trading from US is forbidden")
//        }
//        OkExStreamV3.login()
//
//        val deferredAccount = GlobalScope.loggingAsync { restClient.getAccount() }
//        val deferredPositions = GlobalScope.loggingAsync { restClient.getPositions() }
//        val account = deferredAccount.await()
//        updateAccounts(account)
//        val positions = deferredPositions.await()
//        logger.debug { "account from rest: $account" }
//        logger.debug { "positions from rest: $positions" }
//        positions.forEach { updatePositions(it) }
//
//    }
//
//    private suspend fun updateAccounts(accounts: Map<String, OkExAccount>) {
//        var usdTotal = 0.0
//        accountsMutex.withLock {
//            accounts.forEach { (key, value) ->
//                this.accounts[key] = value
//                val amount = value.total_avail_balance.toDouble()
//                when (key) {
//                    "usd" -> usdTotal += amount
//                    "btc" -> usdTotal += amount * prevTrade!!.price
//                }
//            }
//        }
//        Store.update { it.copy( wallet = it.wallet.copy( usdCash = usdTotal ) ) }
//    }
//
//    private suspend fun updatePositions(position: OkExPosition) {
//        positionsMutex.withLock {
//            positions[position.instrument_id] = position
//            logger.debug { "positions updated:  $positions" }
//        }
//    }
//
//    private suspend fun getPositions(marketSymbol: String): Pair<Double, Double> {
//        positionsMutex.withLock {
//            logger.debug { "getting positions for $marketSymbol  positions=$positions" }
//            val position = positions[marketSymbol]
//            return if (position == null) {
//                Pair(0.0, 0.0)
//            } else {
//                val rv = Pair(position.long_qty.toDouble(), position.short_qty.toDouble())
//                logger.debug { "returning positions for $marketSymbol $rv" }
//                rv
//            }
//        }
//
//    }
//
//}
//
//fun main() = loggingRunBlocking {
//    System.setProperty("logback.statusListenerClass", "ch.qos.logback.core.status.OnConsoleStatusListener")
//    System.setProperty("logback.configurationFile", "logback-eraseme.xml")
////    val exch = OkExExchange()
////    val market = Market("okex", "btc", "usd", "190329")
////    exch.startOrderBookStream(market)
////    exch.authenticate()
////    val orderRequestOpen = OrderRequest(
////            market = market,
////            type =  OrderRequest.Type.MARKET,
////            direction = Direction.BUY,
////            size = 1.0,
////            price = null)
////    var order = exch.placeOrder(orderRequestOpen)
////    println(order)
////
////    delay(10000)
//
////    val orderRequestReverse = orderRequestOpen.copy(direction = Direction.SELL, size = 2.0)
////    order = exch.placeOrder(orderRequestReverse)
////    println(order)
////    delay(10000)
////
////    order = exch.placeOrder(orderRequestOpen)
////    println(order)
////
////    delay(Long.MAX_VALUE)
//
//}