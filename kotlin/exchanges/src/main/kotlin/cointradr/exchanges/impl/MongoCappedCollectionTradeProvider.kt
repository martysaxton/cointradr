package cointradr.exchanges.impl

import cointradr.exchanges.TradeProvider
import cointradr.fin.model.Market
import cointradr.fin.model.Trade
import cointradr.utils.loggingLaunch
import com.mongodb.CursorType
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import org.joda.time.DateTime
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.util.concurrent.TimeUnit

const val CAUGHT_UP_THRESHOLD = 1000L

class MongoCappedCollectionTradeProvider : TradeProvider(), KoinComponent {
    companion object {
        private val logger = KotlinLogging.logger {}
    }
    private val mongoTradeQueues: MongoTradeQueues by inject()
    private var caughtUp = false

    override fun start(market: Market): Job {
        return GlobalScope.loggingLaunch {
            try {
                logger.info { "starting"}
                val tradesCollection = mongoTradeQueues.getCollection(market)
                val cursor = tradesCollection.find().cursorType(CursorType.TailableAwait).noCursorTimeout(true).maxAwaitTime(1000, TimeUnit.MILLISECONDS)
                var previousTrade: Trade? = null
                cursor.forEach( { trade ->
                    if (!caughtUp) {
                        val now = DateTime().millis
                        val timeDiff = now - trade.t
                        if (timeDiff < CAUGHT_UP_THRESHOLD) {
                            caughtUp = true
                        }

                    }
                    logger.debug { "new trade $trade time=${DateTime(trade.t)} caughtUp=$caughtUp" }
                    var outOfOrder = false
                    if (!caughtUp) {
                        previousTrade?.let {
                            outOfOrder = it.t > trade.t
                        }
                    }
                    if (outOfOrder) {
                        logger.warn { "out of order trade!" }
                    } else {
                        runBlocking {
                            logger.debug { "broadcasting trade" }
                            broadcastTrade(trade, caughtUp)
                            logger.debug { "broadcasted trade" }
                        }
                        previousTrade = trade
                    }
                }, { result, t ->
                    logger.error(t) { "result $result  throwable $t" }
                })
            } catch(t: Throwable) {
                logger.error(t) { "caught exception in TradeProvider" }
            }
        }
    }

}