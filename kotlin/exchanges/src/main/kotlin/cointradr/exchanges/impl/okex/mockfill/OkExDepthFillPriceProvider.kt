package cointradr.exchanges.impl.okex.mockfill

import cointradr.clients.okex.OkExDepthHandler
import cointradr.clients.okex.OkExStreamV3
import cointradr.exchanges.Direction
import cointradr.exchanges.OrderBook
import cointradr.exchanges.Position
import cointradr.exchanges.impl.okex.OkExMarketDataExchange
import cointradr.exchanges.impl.okex.toOkExSubscription
import cointradr.fin.model.Market
import cointradr.state.BestBidAsk
import cointradr.state.Store
import cointradr.utils.loggingLaunch
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import org.koin.core.KoinComponent
import org.koin.core.inject

class OkExDepthFillPriceProvider: KoinComponent, AbstractFillPriceProvider() {

    private val okExExchange = OkExMarketDataExchange()
    // still used to approximate position liquidation values
    private var bestBid = Double.MIN_VALUE
    private var bestAsk = Double.MAX_VALUE
    private var bestBidAskMutex = Mutex()
    private val okExStream by inject<OkExStreamV3>()
    private val store: Store by inject()

    override suspend fun getFillPrice(market: Market, direction: Direction): Double {
        val book = getDepth(market)
        // TODO check sizes and return avg price (need conversion from money to # of contracts)
        if (direction == Direction.BUY) {
            return book.asks[0].price
        }
        return book.bids[0].price
    }

    override suspend fun getLiquidationValue(position: Position?): Double {
        val size = position?.size ?: 0.0
        val price =  if (size > 0) getBestAsk() else getBestBid()
        return size * price
    }

    override suspend fun getOrderSize(market: Market, usd: Double): Double {
        val price = (getBestAsk() + getBestBid()) / 2.0
        return usd / price
    }

    suspend fun getDepth(market: Market): OrderBook {
        return okExExchange.getOrderBook(market)
    }

    private suspend fun getBestBid(): Double { bestBidAskMutex.withLock { return bestBid } }
    private suspend fun getBestAsk(): Double { bestBidAskMutex.withLock { return bestAsk } }


    private val okexDepthHandler: OkExDepthHandler = { book ->
        bestBidAskMutex.withLock {
            val bba = book.getBestBidAsk()
            if (bestBid != bba.bestBid || bestAsk != bba.bestAsk) {
                bestBid = bba.bestBid
                bestAsk = bba.bestAsk
                store.update { it.copy(book = BestBidAsk(bestBid, bestAsk)) }
            }
        }
    }

    init {
        okExStream.addDepthHandler(okexDepthHandler)
        GlobalScope.loggingLaunch {
            okExStream.connect()
            val market = Market("okex", "btc", "usd", "190329")
//            val depthSubscription = "ok_sub_future${market.quoteCurrency}_${market.baseCurrency}_depth_${market.expiry}"
            okExStream.subscribe( market.toOkExSubscription("depth"))
        }
    }



}