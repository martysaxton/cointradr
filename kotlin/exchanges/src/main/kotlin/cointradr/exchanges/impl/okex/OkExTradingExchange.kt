package cointradr.exchanges.impl.okex

import cointradr.clients.okex.OkExAccount
import cointradr.clients.okex.OkExPosition
import cointradr.clients.okex.OkExRestClient
import cointradr.clients.okex.OkExStreamV3
import cointradr.exchanges.*
import cointradr.exchanges.impl.ClientSideStopOrderExchange
import cointradr.fin.model.Market
import cointradr.geoip.GeoIP
import cointradr.state.Params
import cointradr.state.Store
import cointradr.utils.ShortUuid
import cointradr.utils.loggingAsync
import cointradr.utils.loggingLaunch
import cointradr.utils.loggingRunBlocking
import com.okcoin.commons.okex.open.api.enums.FuturesTransactionTypeEnum
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import mu.KotlinLogging
import org.koin.core.context.startKoin
import org.koin.core.inject
import org.koin.core.logger.Level
import org.koin.dsl.module


class OkExTradingExchange(val market: Market): ClientSideStopOrderExchange("okex") {

    companion object {
        private val logger = KotlinLogging.logger {}
    }

    private val store: Store by inject()
    private val restClient by inject<OkExRestClient>()
    private val makerMarketOrderManager by inject<OkExMakerMarketOrderManager>()
    private val positions = mutableMapOf<String, OkExPosition>()
    private val positionsMutex = Mutex()
    private val accounts= mutableMapOf<String, OkExAccount>()
    private val accountsMutex = Mutex()
    private val okExStream by inject<OkExStreamV3>()

    override suspend fun executeMarketOrder(order: Order) {

        val clientId = ShortUuid.create()
        val orderList = translateOrders(order)
        var makerOrderDeferred: Deferred<List<OrderResult>> = makerMarketOrderManager.placeOrders(clientId, orderList)

        GlobalScope.loggingLaunch {
            val result = makerOrderDeferred.await()
            logger.debug { "order result: $result" }
        }
    }

    override fun getWallet(): Wallet {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override suspend fun getLiquidationValue(market: Market): Double {
        positionsMutex.withLock {
            val pt = tradeProvider.getLastTrade()
            val instrumentId = market.toOkExInstrumentId()
            val btcPositions = positions[instrumentId]
            return if (btcPositions == null) {
                0.0
            } else {
                val long = btcPositions.long_qty.toDouble() * pt.price
                val short = btcPositions.short_qty.toDouble() * pt.price
                long - short
            }
        }

    }

    private suspend fun getLiquidationValueUsd(positions: OkExPosition): Double {
        val pt = tradeProvider.getLastTrade()
        val long = positions.long_qty.toDouble() * pt.price
        val short = positions.short_qty.toDouble() * pt.price
        return long - short
    }

    override suspend fun getCashValue(): Double {
        var usdTotal = 0.0
        accountsMutex.withLock {
            accounts.forEach { (key, value) ->
                this.accounts[key] = value
                val amount = value.total_avail_balance.toDouble()
                when (key) {
                    "usd" -> usdTotal += amount
                    "btc" -> usdTotal += amount * tradeProvider.getLastTrade().price
                }
            }
        }
        return usdTotal
    }

    override suspend fun getOrderSize(market: Market, usd: Double): Double {
        return 1.0
    }

    private suspend fun translateOrders(orderRequest: Order): List<com.okcoin.commons.okex.open.api.bean.futures.param.Order> {

//        when (orderRequest.type) {
//            OrderRequest.Type.MARKET -> {
//                order.match_price = 1
//            }
//        }

        val rv = mutableListOf<com.okcoin.commons.okex.open.api.bean.futures.param.Order>()
//        val instrumentId = with(orderRequest.market) {
//            "${baseCurrency.toUpperCase()}-${quoteCurrency.toUpperCase()}-$expiry"
//        }
        val instrumentId = orderRequest.mktSym
        val contractSize = orderRequest.size.toInt()
        val currentPositions = getPositions(instrumentId)
        val existingContracts = if (orderRequest.direction == Direction.BUY) currentPositions.second.toInt() else currentPositions.first.toInt()
        val contractsToClose = minOf(contractSize,existingContracts)
        val contractsToOpen = contractSize - contractsToClose
        logger.debug { "translateOrders ${orderRequest.direction} ${orderRequest.size}  existing=$existingContracts toOpen=$contractsToOpen toClose=$contractsToClose" }
        if (contractsToClose > 0) {
            val transactionType = when (orderRequest.direction) {
                Direction.BUY -> FuturesTransactionTypeEnum.CLOSE_SHORT
                Direction.SELL -> FuturesTransactionTypeEnum.CLOSE_LONG
            }
            logger.debug { "order to close existing position:  $instrumentId $transactionType $contractsToClose" }
            val closeOrder = createMarketOrder(instrumentId, contractsToClose, transactionType)
            rv.add(closeOrder)
        }
        if (contractsToOpen > 0) {
            val transactionType = when (orderRequest.direction) {
                Direction.BUY -> FuturesTransactionTypeEnum.OPEN_LONG
                Direction.SELL -> FuturesTransactionTypeEnum.OPEN_SHORT
            }
            logger.debug { "order to open:  $instrumentId $transactionType $contractsToOpen" }
            val openOrder = createMarketOrder(instrumentId, contractsToOpen, transactionType)
            rv.add(openOrder)
        }

        return rv
    }

    private suspend fun getPositions(marketSymbol: String): Pair<Double, Double> {
        positionsMutex.withLock {
            logger.debug { "getting positions for $marketSymbol  positions=$positions" }
            val position = positions[marketSymbol]
            return if (position == null) {
                Pair(0.0, 0.0)
            } else {
                val rv = Pair(position.long_qty.toDouble(), position.short_qty.toDouble())
                logger.debug { "returning positions for $marketSymbol $rv" }
                rv
            }
        }

    }

    private fun createMarketOrder(instrumentId: String, size: Int, type: FuturesTransactionTypeEnum): com.okcoin.commons.okex.open.api.bean.futures.param.Order {
        val order = com.okcoin.commons.okex.open.api.bean.futures.param.Order()
        order.type = type.code()
        order.client_oid = ShortUuid.create()
        order.setinstrument_id(instrumentId)
        order.size = size
        order.leverage = 10.00
        order.match_price = 1
        return order
    }


    suspend fun authenticate()  {
        val myCountry = GeoIP.getMyCountry()
        if (myCountry == "US") {
            throw RuntimeException("trading from US is forbidden")
        }
        okExStream.login()

        val deferredAccount = GlobalScope.loggingAsync { restClient.getAccount() }
        val deferredPositions = GlobalScope.loggingAsync { restClient.getPositions() }
        val account = deferredAccount.await()
        updateAccounts(account)
        val positions = deferredPositions.await()
        logger.debug { "account from rest: $account" }
        logger.debug { "positions from rest: $positions" }
        positions.forEach { updatePositions(it) }
        updateBalances()



    }

    private suspend fun updateAccounts(accounts: Map<String, OkExAccount>) {
        accountsMutex.withLock {
            accounts.forEach { (key, value) ->
                this.accounts[key] = value
            }
        }
    }

    private suspend fun updatePositions(position: OkExPosition) {
        positionsMutex.withLock {
            positions[position.instrument_id] = position
            logger.debug { "positions updated:  $positions" }
        }
    }



    private suspend fun getAllPositionsLiquidationValue(): Double {
        var rv = 0.0
        positionsMutex.withLock {
            positions.forEach { (key, value) ->
                rv += getLiquidationValueUsd(value)
            }
        }
        return rv
    }


    private suspend fun updateBalances() {
        var positionsUsd =  getAllPositionsLiquidationValue()
        val usdCash = getCashValue()
        store.update {
            it.copy(
                    wallet = it.wallet.copy(
                            usdCash = usdCash,
                            usdInPosition = positionsUsd,
                            usdTotal = usdCash + positionsUsd
                    )
            )
        }
    }


}

fun main() = loggingRunBlocking {
    System.setProperty("logback.statusListenerClass", "ch.qos.logback.core.status.OnConsoleStatusListener")
    System.setProperty("logback.configurationFile", "logback-eraseme.xml")

    val market = Market("okex", "btc", "usd", "190329")
    val koinApplication = startKoin {
        modules(exchangeModule(market))
        logger(level = Level.DEBUG)
    }

    with(koinApplication.koin) {
        val okExStream: OkExStreamV3 = get()
        okExStream.connect()
        val tradeProvider: TradeProvider = get()
        tradeProvider.start(market)

        val exch: OkExTradingExchange = get()
        exch.authenticate()
        get<OkExMakerMarketOrderManager>()

        val market = Market("okex", "btc", "usd", "190329")
//        exch.startOrderBookStream(market)
        exch.authenticate()
        val orderRequestOpen = OrderRequest(
                market = market,
                type =  OrderRequest.Type.MARKET,
                direction = Direction.BUY,
                size = 1.0,
                price = null)
        var order = exch.placeOrder(orderRequestOpen)
        println(order)

//    val orderRequestReverse = orderRequestOpen.copy(direction = Direction.SELL, size = 2.0)
//    order = exch.placeOrder(orderRequestReverse)
//    println(order)
//    delay(10000)
//
//    order = exch.placeOrder(orderRequestOpen)
//    println(order)
//
        delay(Long.MAX_VALUE)
    }

}
