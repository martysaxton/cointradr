@file:Suppress("DEPRECATION")

package cointradr.exchanges.impl

import cointradr.exchanges.*
import cointradr.fin.model.Market
import cointradr.fin.model.Trade
import cointradr.mongo.Mongo
import cointradr.state.Store
import cointradr.utils.ShortUuid
import cointradr.utils.loggingLaunch
import com.soywiz.klock.DateTime
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.litote.kmongo.coroutine.*


abstract class AbstractExchange(private val name: String) : Exchange, KoinComponent {


    companion object {

        private val logger = KotlinLogging.logger {}


        fun createPlacedOrder(orderRequest: OrderRequest, filledPrice: Double?, whenFilled: DateTime?): Order {
            val now = whenFilled ?: DateTime.now()
            return Order(
                    id = ShortUuid.create(),
                    mktSym = orderRequest.market.toString(),
                    type = orderRequest.type,
                    direction = orderRequest.direction,
                    size = orderRequest.size,
                    price = orderRequest.price,
                    filledPrice = filledPrice,
                    filledQuantity = if (filledPrice == null) 0.0 else orderRequest.size,
                    whenPlaced = now.unixMillisLong,
                    whenFilled = whenFilled?.unixMillisLong,
                    state = Order.State.ACTIVE)
        }

    }

    private val store: Store by inject()
    private val mongoTradeQueues: MongoTradeQueues by inject()
    private val orderPersistence: OrderPersistence by inject()

    private val orderObservers = mutableListOf<Exchange.OrderObserver>()

    private val positions = mutableMapOf<Market, Position>()
    private val tradeHandlers = mutableListOf<TradeHandler>()

    override fun addOrderObserver(observer: Exchange.OrderObserver) {
        orderObservers.add(observer)
    }


    override fun addTradeHandler(tradeHandler: TradeHandler) {
        tradeHandlers.add(tradeHandler)
    }

    protected suspend fun broadcastTrade(trade: Trade) {
        tradeHandlers.forEach { it(trade) }
    }

    protected suspend fun onLiveTrade(trade: Trade) {
        logger.debug{ "got trade $trade time: ${DateTime.fromUnix(trade.t)}" }
        val tradesCollection = mongoTradeQueues.getCollection(trade)
        GlobalScope.loggingLaunch {
            tradesCollection.insertOne(trade)
        }
    }

    suspend fun broadcastOrderPartiallyFilled(order: Order) {
        orderObservers.forEach { it.orderPartiallyFilled(order) }
    }

    fun getName(): String {
        return name
    }

    override fun getPositions(): Collection<Position> {
        return positions.values
    }

    override fun getPosition(market: Market): Position? {
        return positions[market]
    }

    protected fun positionClosed(market: Market) {
        positions.remove(market)
    }

    protected fun setPosition(position: Position) {
        positions[position.market] = position
    }

    open suspend fun orderPlaced(order: Order) {
        orderPersistence.insertOrder(order)
    }


    open suspend fun orderFilled(order: Order) {
        val filledOrder = order.copy(state = Order.State.FILLED)
        orderPersistence.updateOrder(filledOrder)
        orderObservers.forEach { it.orderFilled(filledOrder) }


        store.update {
            val recentOrders = it.recentOrders.toMutableList()
            if (recentOrders.size == 10) {
                recentOrders.removeAt(recentOrders.lastIndex)
            }
            recentOrders.add(0, order)
            it.copy(recentOrders = recentOrders)
        }

    }

    open suspend fun orderCanceled(order: Order) {
        val canceledOrder = order.copy(state = Order.State.CANCELED)
        orderPersistence.updateOrder(canceledOrder)
        orderObservers.forEach { it.orderCanceled(canceledOrder) }
    }


}

//fun main() = runBlocking {
//    val m = Market("okex", "btc", "usd", "190329")
////    for (i in 1..10) {
////        val order = Order(
////                id = ShortUuid.create(),
////                mktSym = m.toString(),
////                type = OrderRequest.Type.STOP,
////                direction = Direction.BUY,
////                size = 1.0,
////                price = 3000.0,
////                filledPrice = 0.0,
////                filledQuantity = 0.0,
////                whenPlaced = DateTime.nowUnixLong(),
////                whenFilled = null,
////                state = Order.State.ACTIVE)
////        val result = orderPersistence.insertOrder(order)
////        if (i%1000 == 0) {
////            println("$i $result")
////        }
////    }
//    val openOrders = orderPersistence.getOpenOrders(m)
//    println("${openOrders.size} open orders")
//    delay(Long.MAX_VALUE)
//    println("done")
//
//}
