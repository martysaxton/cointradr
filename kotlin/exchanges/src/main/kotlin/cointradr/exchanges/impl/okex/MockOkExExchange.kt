package cointradr.exchanges.impl.okex

import cointradr.exchanges.*
import cointradr.exchanges.impl.ClientSideStopOrderExchange
import cointradr.exchanges.impl.okex.mockfill.FillPriceProvider
import cointradr.exchanges.TradeProvider
import cointradr.fin.model.Market
import cointradr.state.Store
import cointradr.utils.loggingLaunch
import com.soywiz.klock.DateTime
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.withLock
import mu.KotlinLogging
import org.koin.core.inject

const val LATENCY = 0L
const val STOP_ORDER_LATENCY = 0L
/**
 * Uses a real exchange for market data
 */
class MockOkExExchange: ClientSideStopOrderExchange ("mockOkEx") {

    companion object {
        private val logger = KotlinLogging.logger {}
    }

    private val availableBalances = mutableMapOf<String, Double>()
    private val store: Store by inject()
    private val fillPriceProvider: FillPriceProvider by inject()
//    private var fillPriceProvider: FillPriceProvider = OkExDepthFillPriceProvider()


    init {
        availableBalances["usd"] = 1000.0
        runBlocking {
            store.update { it.copy(wallet = it.wallet.copy( usdTotal = availableBalances["usd"]!!)) }
        }

        GlobalScope.loggingLaunch {
            while (true) {
                delay(1000*60)
                updatePositionValues()
            }
        }
    }

    override suspend fun placeOrder(orderRequest: OrderRequest): Order {
        logger.debug { "placeOrders: $orderRequest" }
        val latency = if (orderRequest.type == OrderRequest.Type.STOP) STOP_ORDER_LATENCY else LATENCY
        delay(latency) // incoming orderRequest
        val newOrder = super.placeOrder(orderRequest)
        delay(latency) // outgoing latency
        return newOrder
    }

    private suspend fun executeMarketOrder(orderRequest: OrderRequest): Order {
        val placedOrder = createPlacedOrder(orderRequest, null, null)
        executeMarketOrder(placedOrder)
        return placedOrder
    }

    override suspend fun executeMarketOrder(order: Order) {
        logger.debug { "executing market order: $order" }
        val price = executeMarketOrder(order.getMarket(), order.direction, order.size)
        val filledOrder = order.copy(
            filledQuantity = order.size,
            filledPrice = price,
            whenFilled = DateTime.nowUnixLong())
        logger.debug { "market order executed, simulating latency: $order" }
        val latency = if (order.type == OrderRequest.Type.STOP) STOP_ORDER_LATENCY else LATENCY
        delay(latency) // outgoing latency
        logger.debug { "broadcasting fill: $order" }
        orderFilled(filledOrder)
    }


    private fun sizeToDisposition(size: Double): Disposition {
        return when {
            size > 0.0 -> Disposition.LONG
            size < 0.0 -> Disposition.SHORT
            else -> Disposition.NEUTRAL
        }
    }

    private suspend fun executeMarketOrder(market: Market, direction: Direction, size: Double): Double {
        
        // set up
        val sz = if (direction == Direction.BUY) size else size * -1.0
        val currentUsd = availableBalances["usd"]!!
        val currentPosition = getPosition(market)
        val currentPositionSize = currentPosition?.size ?: 0.0
        val fillPrice = fillPriceProvider.getFillPrice(market, direction)
        val lastTradePrice = tradeProvider.getLastTrade()
        logger.debug { "executing market order $market, dir=$direction , size=$size fillPrice=$fillPrice lastTradePrice=$lastTradePrice currentPositionSize=$currentPositionSize"}
        val orderValue = sz * fillPrice

        // new cash value
        val newUsd = currentUsd - orderValue
        availableBalances["usd"] = newUsd
        logger.debug { "new usd = ${availableBalances["usd"]}"}

        // new position
        val newPositionSize = currentPositionSize + sz
        val newDisposition = sizeToDisposition(newPositionSize)
        var positionValue = 0.0
        if (newDisposition == Disposition.NEUTRAL) {
            positionClosed(market)
            logger.debug("position for $market is now closed")
        } else {
            val oldDisposition = sizeToDisposition(currentPositionSize)
            val newPosition = if (oldDisposition != newDisposition) {
                Position(market, newDisposition, newPositionSize, fillPrice, DateTime.now())
            } else {
                throw IllegalStateException("not supporting extending or reducing a position, only flipping.  currentPosition=$currentPosition newPositionSize=$newPositionSize newDisposition=$newDisposition")
            }
            setPosition(newPosition)
            logger.debug("new position is $newPosition")
            positionValue = getLiquidationValue(market)
        }
        logger.debug { "net liquidation = ${newUsd + positionValue}" }
        updatePositionValues()
        return fillPrice
    }

    private suspend fun updatePositionValues() {
        var positionsUsd =  0.0
        getPositions().forEach {
            positionsUsd += getLiquidationValue(it.market)
        }
        val usdCash = availableBalances["usd"]!!

        store.update {
            it.copy(
                wallet = it.wallet.copy(
                    usdCash = usdCash,
                    usdInPosition = positionsUsd,
                    usdTotal = usdCash + positionsUsd
                )
            )
        }
    }

    override fun getWallet(): Wallet {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override suspend fun getLiquidationValue(market: Market): Double {
        val position = getPosition(market)
        return fillPriceProvider.getLiquidationValue(position)
    }

    override suspend fun getCashValue(): Double {
        exchangeMutex.withLock {
            return availableBalances["usd"]!!
        }
    }

    override suspend fun getOrderSize(market: Market, usd: Double): Double {
        return fillPriceProvider.getOrderSize(market, usd)
    }

}