package cointradr.exchanges.impl.okex.mocktrade

import cointradr.exchanges.TradeProvider
import cointradr.fin.model.Market
import cointradr.fin.model.Trade
import cointradr.utils.loggingLaunch
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import org.nustaq.serialization.FSTConfiguration
import org.nustaq.serialization.FSTObjectOutputNoShared
import java.io.File
import java.io.FileNotFoundException
import java.io.Serializable


data class DBTrade(
        val t: java.util.Date,
        val price: String
)

data class MemTrade (
        val t: Long,
        val price: Double
) : Serializable

class HistoricTradeProvider : TradeProvider() {

    companion object {
        private val logger = KotlinLogging.logger {}
        val fst  = FSTConfiguration.createDefaultConfiguration()
        var trades = listOf<MemTrade>()
        init {
            fst.registerClass(MemTrade::class.java)

            try {
                readBin()
            } catch (_: FileNotFoundException) {
                readCsv()
                writeBin()
            }


//        val fSTObjectInputNoShared = FSTObjectInputNoShared(fst)
//        // read
//        fSTObjectInputNoShared.resetForReuseUseArray(buffer, buffer.size)
//        val qwer = fSTObjectInputNoShared.readObject() as List<MemTrade>

        }

        private fun readCsv()  {
            logger.debug("reading trades from csv")
            val mutableTrades= mutableListOf<MemTrade>()
            File("trades.csv").forEachLine {
                val parts = it.split(",")
                if (parts.size == 2) {
                    val (dateStr, priceStr ) = it.split(",")
                    val memTrade = MemTrade(dateStr.toLong(), priceStr.toDouble())
                    mutableTrades.add(memTrade)
                }
            }
            trades = mutableTrades
            logger.debug("done reading trades: ${trades.size}")

        }

        private fun writeBin() {
            val outputNoShared = FSTObjectOutputNoShared(fst)
            logger.debug { "serializing trades to byte array" }
            outputNoShared.writeObject(trades)
            val buffer = outputNoShared.buffer
            val bin = File("trades.bin")
            logger.debug("writing bytes to bin file")
            bin.writeBytes(buffer)
            logger.debug("done writing trades to bin file")
        }

        private fun readBin() {
            logger.debug { "reading trades from bin file to buffer" }
            val bin = File("trades.bin")
            val buffer = bin.readBytes()
            logger.debug { "instantiating objects from buffer" }
            trades  = fst.asObject(buffer) as MutableList<MemTrade>
            logger.debug { "done reading trades from bin: ${trades.size}" }

        }

    }

    override fun start(market: Market): Job {
        return GlobalScope.loggingLaunch {
            var counter = 0L
            trades.forEach { memTrade ->
                val trade = Trade(
                        id = counter.toString(),
                        exch = "okex",
                        symb = "btc/usd-f",
                        t = memTrade.t,
                        price = memTrade.price,
                        size = 1.0,
                        bid = true)
                broadcastTrade(trade, false)
                ++counter
            }
        }

    }

}


fun main() = runBlocking {
    val htp = HistoricTradeProvider()
    val market = Market("okex", "btc", "usd", "190329")
    htp.start(market)
    delay(Long.MAX_VALUE)
}