package cointradr.exchanges.impl.okex

import cointradr.clients.okex.OkExRestClient
import cointradr.clients.okex.OkExStreamV3
import cointradr.clients.okex.OkExTrade
import cointradr.exchanges.OrderBook
import cointradr.exchanges.impl.AbstractMarketDataExchange
import cointradr.fin.model.Market
import cointradr.fin.model.Trade
import cointradr.utils.loggingLaunch
import com.soywiz.klock.DateTime
import kotlinx.coroutines.GlobalScope
import mu.KotlinLogging
import org.koin.core.inject

fun Market.toOkExInstrumentId(): String {
    return "${baseCurrency.toUpperCase()}-${quoteCurrency.toUpperCase()}-$expiry"
}

fun Market.toOkExSubscription(type: String): String {
    if (expiry == null) {
        throw IllegalArgumentException("only supporting futures for now")
    }
    return "futures/$type:${toOkExInstrumentId()}"
}

class OkExMarketDataExchange: AbstractMarketDataExchange() {

    companion object {
        private val logger = KotlinLogging.logger {}
    }

    private var lastTrade: Trade? = null
    private val restClient = OkExRestClient()
    private val okExStream by inject<OkExStreamV3>()

    internal suspend fun okexTradeHandler(trades: List<OkExTrade>) {
        if (!trades.isEmpty()) {
            val okexMarket = trades[0].market
            val marketStr = "${okexMarket.base}/${okexMarket.quote}${if (okexMarket.futureExpiry == null) "" else "-f"}"

            // TODO
            // each OkExTrade.size unit is in # of contracts,
            // each contract is worth $100
            // size in the 'exchange' world should be in terms of the base currency
            // not sure if other okex futures markets (e.g., eth/usd) have contracts worth $100 each

            val normalizedTrades = trades.map {
                Trade(
                        id = it.id,
                        exch = "okex",
                        symb = marketStr,
                        t = it.t,
                        price = it.price,
                        size = it.size,
                        bid = it.bid)
            }
            lastTrade = normalizedTrades.last()
            normalizedTrades.forEach { trade ->
                logger.debug{ "got trade $trade time: ${DateTime.fromUnix(trade.t)}" }
                broadcastTrade(trade)
            }
        }
    }

    init {
        okExStream.addTradeHandler(::okexTradeHandler)
        GlobalScope.loggingLaunch { okExStream.connect() }
    }

    suspend fun subscribeToTrades(market: Market) {
        val subscription = market.toOkExSubscription("trade")
        okExStream.subscribe(subscription)
    }

    override suspend fun getOrderBook(market: Market): OrderBook {
        // TODO figure out how to come up with expiry
        val expiry= "190329"
        val resp = restClient.getDepthFutures(market.baseCurrency, market.quoteCurrency, expiry, 1)

        return OrderBook(
                bids = mapToOrderBookEntryList(OrderBook.Side.BID, resp.bids),
                asks = mapToOrderBookEntryList(OrderBook.Side.ASK, resp.asks),
                time = DateTime.now()
        )
    }

    private fun mapToOrderBookEntryList(side: OrderBook.Side, entries: List<List<Double>>) : List<OrderBook.Entry> {
        return entries.map { OrderBook.Entry(
                side = side,
                size = it[1],
                price = it[0]
        ) }

    }

}

