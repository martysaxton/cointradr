package cointradr.exchanges.impl

import cointradr.exchanges.OrderRequest
import cointradr.exchanges.Order

interface OrderExecuter {
    suspend fun cancelOrder(orderId: String)
    suspend fun placeOrder(orderRequest: OrderRequest): Order

}