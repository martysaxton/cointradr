package cointradr.exchanges.impl.okex.mockfill

import cointradr.exchanges.Direction
import cointradr.exchanges.Position
import cointradr.exchanges.TradeProvider
import cointradr.exchanges.TradeProviderHandler
import cointradr.fin.model.Market
import cointradr.fin.model.Trade


class TradeBasedFillPriceProvider(tradeProvider: TradeProvider) : FillPriceProvider {

    var lastTrade: Trade? = null

    val tradeHandler: TradeProviderHandler = { trade, _ ->
        lastTrade = trade

    }

    init {
        tradeProvider.addHandler(tradeHandler)
    }

    override suspend fun getLiquidationValue(position: Position?): Double {
        val size = position?.size ?: 0.0
        val price =  lastTrade!!.price
        return size * price
    }

    override suspend fun getOrderSize(market: Market, usd: Double): Double {
        val price = lastTrade!!.price
        return usd / price
    }


    override suspend fun getFillPrice(market: Market, direction: Direction): Double {
        return lastTrade!!.price
    }

}