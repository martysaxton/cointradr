package cointradr.exchanges

import cointradr.fin.model.Trade
import cointradr.fin.model.Market
import kotlinx.coroutines.Job

typealias TradeHandler  = suspend (trade: Trade) -> Unit

interface MarketDataExchange {

    fun addTradeHandler(tradeHandler: TradeHandler)

    suspend fun getOrderBook(market: Market): OrderBook


}