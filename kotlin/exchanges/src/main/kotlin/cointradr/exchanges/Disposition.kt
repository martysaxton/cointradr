package cointradr.exchanges

enum class Disposition {
    LONG, SHORT, NEUTRAL
}
