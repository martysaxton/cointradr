package cointradr.exchanges

import cointradr.fin.model.Market
import com.fasterxml.jackson.annotation.JsonIgnore
import com.soywiz.klock.DateTime

data class Order (
        val id: String,
        val mktSym: String,
        val type: OrderRequest.Type,
        val direction: Direction,
        val size: Double,
        val price: Double?,
        val filledQuantity: Double,
        val filledPrice: Double?,
        val whenPlaced: Long,
        val whenFilled: Long?,
        val state: State) {

    enum class State { ACTIVE, FILLED, CANCELED }

    @JsonIgnore
    fun getMarket(): Market = Market.toMarket(mktSym)

}
