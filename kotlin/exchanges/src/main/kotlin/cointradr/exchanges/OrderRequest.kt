package cointradr.exchanges

import cointradr.fin.model.Market
import com.soywiz.klock.DateTime



data class OrderRequest (
        val market: Market,
        val type: Type,
        val direction: Direction,
        val size: Double,
        val price: Double? = null) {

    enum class Type {
        LIMIT, MARKET, STOP
    }

}
