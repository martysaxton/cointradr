package cointradr.rest

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.client.HttpClient
import io.ktor.client.engine.apache.Apache
import io.ktor.client.request.get
import io.ktor.client.request.header
import io.ktor.features.origin
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.request.host
import io.ktor.request.port
import io.ktor.response.respond
import io.ktor.response.respondRedirect
import io.ktor.routing.Route
import io.ktor.routing.Routing
import io.ktor.routing.route
import io.ktor.sessions.*
import io.ktor.util.hex
import mu.KotlinLogging

val googleOauthProvider = OAuthServerSettings.OAuth2ServerSettings(
        name = "google",
        authorizeUrl = "https://accounts.google.com/o/oauth2/auth",
        accessTokenUrl = "https://www.googleapis.com/oauth2/v3/token",
        requestMethod = HttpMethod.Post,

        clientId = "86567387187-g6jb91b74cmtpt92bboh2a704d3c8t8v.apps.googleusercontent.com", // @TODO: Remember to change this!
        clientSecret = "wL2SGKDlUXyiFoLW5TW5wx-Z", // @TODO: Remember to change this!
        defaultScopes = listOf("email", "profile") // no email, but gives full name, picture, and id
)

class MySession(val userId: String)

fun Application.installGoogleOauthProvider(cookieName: String, cookieSignKey: String) {
    install(Authentication) {
        oauth("google-oauth") {
            client = HttpClient(Apache)
            providerLookup = { googleOauthProvider }
            urlProvider = {
                redirectUrl("/login/google")
            }
        }
    }
    install(Sessions) {
        cookie<MySession>(cookieName) {
            val secretSignKey = hex(cookieSignKey) // @TODO: Remember to change this!
            transform(SessionTransportTransformerMessageAuthentication(secretSignKey))
            cookie.path = "/"
        }
//        header<MySession>(cookieName) {
//            val secretSignKey = hex(cookieSignKey) // @TODO: Remember to change this!
//            transform(SessionTransportTransformerMessageAuthentication(secretSignKey))
//        }

    }

}

fun Routing.installGoogleOauthAuthentication(redirectUrl: String) {
    val logger = KotlinLogging.logger {}
    authenticate("google-oauth") {
        route("/login/google") {
            handle {
                val principal = call.authentication.principal<OAuthAccessTokenResponse.OAuth2>()
                        ?: error("No principal")

                val json = HttpClient(Apache).get<String>("https://www.googleapis.com/oauth2/v3/userinfo") {
                    header("Authorization", "Bearer ${principal.accessToken}")
                }

                val data = ObjectMapper().readValue<Map<String, Any?>>(json)
                val id = data["email"] as String?

                if (id != null) {
                    call.sessions.set(MySession(id))
                }
                logger.debug { "redirecting to $redirectUrl" }
                call.respondRedirect(redirectUrl)
            }
        }
    }
}

fun Route.installAuthInterceptor() {
    intercept(ApplicationCallPipeline.Features) {
        val session = call.sessions.get<MySession>()
        val validUsers = setOf("marty.saxton@gmail.com", "cointradr@gmail.com")
        if (session == null || !validUsers.contains(session.userId)) {
            // TODO couldn't figure out how to do a redirect
            call.respond(HttpStatusCode.Forbidden)
            // this ends up as a 404
            return@intercept finish()
        }
    }
}


private fun ApplicationCall.redirectUrl(path: String): String {
    val defaultPort = if (request.origin.scheme == "http") 80 else 443
    val hostPort = request.host() + request.port().let { port -> if (port == defaultPort) "" else ":$port" }
    val protocol = request.origin.scheme
    return "$protocol://$hostPort$path"
}