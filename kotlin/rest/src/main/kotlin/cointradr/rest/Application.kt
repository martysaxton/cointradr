package cointradr.rest


import cointradr.exchanges.Exchange
import cointradr.fin.model.Market
import cointradr.state.AppState
import cointradr.state.AppStateObserver
import cointradr.state.Store
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.flipkart.zjsonpatch.DiffFlags
import com.flipkart.zjsonpatch.JsonDiff
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.CORS
import io.ktor.features.CallLogging
import io.ktor.features.ContentNegotiation
import io.ktor.features.DefaultHeaders
import io.ktor.http.cio.websocket.Frame
import io.ktor.http.cio.websocket.readText
import io.ktor.http.content.*
import io.ktor.jackson.jackson
import io.ktor.response.respond
import io.ktor.routing.get
import io.ktor.routing.route
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import io.ktor.websocket.DefaultWebSocketServerSession
import io.ktor.websocket.WebSockets
import io.ktor.websocket.webSocket
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.ClosedReceiveChannelException
import kotlinx.coroutines.channels.ClosedSendChannelException
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import mu.KotlinLogging
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.util.*

data class Dummy(
        val foo: String,
        val bar: String
)


class RestServer: KoinComponent {
    companion object {
        private val logger = KotlinLogging.logger {}
    }
    val webSockets = mutableListOf< DefaultWebSocketServerSession>()
    val webSocketMutex = Mutex()
    private lateinit var objectMapper: ObjectMapper
    private val store: Store by inject()

    private val appStateObserver: AppStateObserver = { old: AppState, new: AppState ->
        val oldNode = objectMapper.valueToTree<JsonNode>(old)
        val newNode = objectMapper.valueToTree<JsonNode>(new)
        val flags = DiffFlags.dontNormalizeOpIntoMoveAndCopy().clone()
        val diff = JsonDiff.asJson(oldNode, newNode, flags)
        diff?.let {
            sendDiffToWebSockets(it)
        }
    }


    private suspend fun sendDiffToWebSockets(diff: JsonNode) {
        webSocketMutex.withLock {
            val toRemove = mutableListOf<DefaultWebSocketServerSession>()
            webSockets.forEach {
                try {
                    val text = objectMapper.writeValueAsString(diff)
                    it.outgoing.send(Frame.Text(text))
                } catch (e: Throwable) {
                    toRemove.add(it)
                    val closedReason = "websocket closed: ${it.closeReason.await()}"
                    if (e is ClosedSendChannelException) {
                        logger.info { closedReason }
                    } else {
                        logger.error(e) { closedReason }
                    }
                }
            }
            toRemove.forEach { webSockets.remove(it) }
        }
    }

    fun startRest() {

        val devMode = System.getProperty("DEV_MODE") != null
        val inContainer = System.getProperty("container") != null
        val port = if (inContainer) 80 else 8080
        logger.debug { "starting http server on port $port" }
        val server = embeddedServer(Netty, port) {
            install(DefaultHeaders)
            install(CallLogging)
            install(CORS) {
                anyHost()
//                    allowCredentials = true
            }
            install(ContentNegotiation) {
                jackson {
                    objectMapper = this
                    store.addObserver(appStateObserver)
                }
            }
            install(WebSockets)

            if (!devMode) {
                installGoogleOauthProvider("ctauth", "734143530405060708090a0b0c0d0e0f")
            }
            routing {

                if (!devMode) {
                    installGoogleOauthAuthentication("http://cointradr.dynu.net/")
                }

                get("/json/jackson") {
                    call.respond(mapOf("hello" to "world"))
                }
                static("/") {
                    resources("static")
                }
                resource("/", "static/index.html")



                route("api") {
                    if (!devMode) {
                        installAuthInterceptor()
                    }
                    get("state") {
                        call.respond(store.appState)
                    }
                    webSocket("websocket") {

                        logger.debug { "websocket connected" }
                        println( "websocket connected" )

                        val webSocketSession = this
                        webSocketMutex.withLock {
                            webSockets.add(this)
                        }

                        outgoing.send(Frame.Text(objectMapper.writeValueAsString(store.appState)))

                        try {
                            while (true) {
                                val text = (incoming.receive() as Frame.Text).readText()
                                println("onMessage $text")
                                outgoing.send(Frame.Text(text))
                            }
                        } catch (e: Throwable) {
                            webSocketMutex.withLock {
                                webSockets.remove(webSocketSession)
                            }
                            val closedReason = "websocket closed: ${closeReason.await()}"
                            if (e is ClosedReceiveChannelException) {
                                logger.info { closedReason }
                            } else {
                                logger.error(e) { closedReason }
                            }
                        }
                    }
                }

            }
        }
        server.start(wait = false)
    }

}

