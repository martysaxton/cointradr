package cointradr.clients.bitforex

import kotlin.test.assertEquals
import kotlin.test.Test
//import org.testng.annotations.Test


class TestSource {
    @Test fun computeHashIsCorrect() {
        val path = "/api/v1/trade/placeOrder"
        val queryParams = mapOf(
                "symbol" to "coin-usd-eth",
                "price" to "1000",
                "amount" to "1",
                "tradeType" to "1",
                "nonce" to "1501234567890"
        )
        val hmacResult = computeHash(path, queryParams, "fd91cd9ba2cc78fed6bb40e0bcff29ba", "82f192d903f363f22a031dfdbbeaf851")
        assertEquals("2a0a848d76920a425190c5f2c509b45ef730956fac5331c79a988671223fd367", hmacResult)
    }
}

