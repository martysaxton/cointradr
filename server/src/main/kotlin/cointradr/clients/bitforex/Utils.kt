package cointradr.clients.bitforex

import cointradr.hmac


fun computeHash(path: String, queryParams: Map<String, String>, accessKey: String, secret: String) : String {
    val sortedParams = queryParams.toMutableMap().toSortedMap()
    sortedParams["accessKey"] = accessKey
    val content = path + "?" + sortedParams.map { it.key + "=" + it.value }.joinToString("&")
    return hmac(content, secret).toLowerCase()
}