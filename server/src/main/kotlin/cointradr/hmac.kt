package cointradr

import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec
import javax.xml.bind.DatatypeConverter

const val HMAC_SHA256_NAME = "HmacSHA256"
val HMAC_SHA256 = Mac.getInstance(HMAC_SHA256_NAME) as Mac

fun hmac(content: String, secret: String): String {
    val secretKeyBytes = secret.toByteArray()
    val secretKeySpec = SecretKeySpec(secretKeyBytes, HMAC_SHA256_NAME)
    val mac = HMAC_SHA256.clone() as Mac
    mac.init(secretKeySpec)
    val hmacBytes = mac.doFinal(content.toByteArray())
    return DatatypeConverter.printHexBinary(hmacBytes)
}