package cointradr.server.extensions.kotlin

import cointradr.server.controllers.TradesController
import org.joda.time.format.ISODateTimeFormat
import java.util.*

/**
 * Calls the specified function [block] with the given [receiver] as its receiver and returns Unit.
 */
inline fun <T> configure(receiver: T, block: T.() -> Unit) = receiver.block()


val dateParser =  ISODateTimeFormat.dateTimeParser()

fun String?.toDate(): Date? {
    if (this == null) {
        return null
    }
    val dateTime = dateParser.parseDateTime(this)
    return dateTime.toDate()
}