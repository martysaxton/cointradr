package cointradr.server.model

data class Greeting(val id: Long, val content: String)