package cointradr.server.model

import com.fasterxml.jackson.annotation.JsonFormat
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.util.*

@Document(collection = "trades")
data class Trade (
        @Id val id: String,
        val exch: String,
        val symb: String,
        @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
        val t: Date,
        val price: String,
        val size: String,
        val bid: Boolean
        )

