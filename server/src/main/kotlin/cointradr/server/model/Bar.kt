package cointradr.server.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.util.*

@Document(collection = "bars")
data class Bar (
        @Id val id: String,
        val exch: String,
        val symb: String,
        val start: Date,
        val dur: Int,
        val open: Double,
        val high: Double,
        val low: Double,
        val close: Double,
        val vol: Double,
        val vwap: Double,
        val bv: Double,
        val av: Double,
        val ret: Double
        )


