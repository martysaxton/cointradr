package cointradr.server.model

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.TypeAlias
import org.springframework.data.mongodb.core.mapping.Document

@TypeAlias("user")
@Document(collection = "users")
data class User(
        @JsonIgnore @Id val id: String,
        val username: String?,
        val email: String,
        val authorities: Array<String>?
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as User

        if (id != other.id) return false
        if (username != other.username) return false
        if (email != other.email) return false
        if (authorities != null) {
            if (other.authorities == null) return false
            if (!authorities.contentEquals(other.authorities)) return false
        } else if (other.authorities != null) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + (username?.hashCode() ?: 0)
        result = 31 * result + email.hashCode()
        result = 31 * result + (authorities?.contentHashCode() ?: 0)
        return result
    }
}



