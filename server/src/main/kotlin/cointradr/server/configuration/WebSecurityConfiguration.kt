package cointradr.server.configuration

import cointradr.server.extensions.kotlin.configure
import cointradr.server.security.GoogleAuthoritiesExtractor
import cointradr.server.security.OAuth2PrincipalExtractor
import cointradr.server.security.OAuth2SsoAuthenticationSuccessHandler
import cointradr.server.service.UserService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.security.oauth2.resource.UserInfoTokenServices
import org.springframework.boot.web.servlet.FilterRegistrationBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.oauth2.client.OAuth2ClientContext
import org.springframework.security.oauth2.client.OAuth2RestTemplate
import org.springframework.security.oauth2.client.filter.OAuth2ClientAuthenticationProcessingFilter
import org.springframework.security.oauth2.client.filter.OAuth2ClientContextFilter
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter
import org.springframework.web.filter.CompositeFilter
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.CorsConfigurationSource
import org.springframework.web.cors.UrlBasedCorsConfigurationSource
import org.springframework.core.env.Environment

@Configuration
@EnableOAuth2Client
class WebSecurityConfiguration(private val oauth2ClientContext: OAuth2ClientContext,
                               private val facebookConfig: FacebookConfig,
                               private val googleConfig: GoogleConfig,
                               private val userService: UserService,
                               @Value("\${security.enabled}") private val isSecurityEnabled: Boolean,
                               private val environment: Environment,
                               private val authenticationManagerBuilder: AuthenticationManagerBuilder) : WebSecurityConfigurerAdapter() {

    companion object {
        val logger = LoggerFactory.getLogger(WebSecurityConfiguration::class.java)
    }

    override fun configure(auth: AuthenticationManagerBuilder) = configure(auth) {
        userDetailsService(userService).passwordEncoder(BCryptPasswordEncoder())
    }

    @Bean
    fun myAuthenticationManager(): AuthenticationManager {
        return super.authenticationManager()
    }

    override fun configure(http: HttpSecurity) = configure(http) {
        logEnvironment()
        http.cors()
        if (isSecurityEnabled) {
            logger.info("security is enabled")
            antMatcher("/**")
                .authorizeRequests().antMatchers( "/login**", "/assets/**").permitAll()
                .anyRequest().hasAuthority("cointradr")
                .and().formLogin().loginPage("/login").permitAll()
//                .and().httpBasic()
                .and().addFilterBefore(ssoFilter(), BasicAuthenticationFilter::class.java)

        } else {
            logger.warn("security is not enabled")
            antMatcher("/**").authorizeRequests().anyRequest().permitAll()
        }
    }


    fun addCorsMappings(registry: CorsRegistry) {
        registry.addMapping("/**")
                .allowedMethods("HEAD", "GET", "PUT", "POST", "DELETE", "PATCH")
    }
    @Bean
    fun oauth2ClientFilterRegistration(filter: OAuth2ClientContextFilter) = FilterRegistrationBean<OAuth2ClientContextFilter>().apply {
        this.filter = filter
        order = -100
    }

    private fun ssoFilter() = CompositeFilter().apply {
        val facebookFilter = facebookConfig.filter("/login/facebook") { userId -> "$userId@facebook.com" }
        val googleFilter = googleConfig.filter("/login/google") { userId -> "$userId@google.com" }

        setFilters(listOf(facebookFilter, googleFilter))
    }

    private fun ClientResources.filter(path: String, usernameMapper: (String) -> String) = OAuth2ClientAuthenticationProcessingFilter(path).apply {
        val template = OAuth2RestTemplate(this@filter.client, oauth2ClientContext)

        setRestTemplate(template)
        setTokenServices(UserInfoTokenServices(this@filter.resource.userInfoUri, this@filter.client.clientId).apply {
            setRestTemplate(template)
            setPrincipalExtractor(OAuth2PrincipalExtractor(usernameMapper))
            setAuthoritiesExtractor(GoogleAuthoritiesExtractor(userService))
        })
        setAuthenticationSuccessHandler(OAuth2SsoAuthenticationSuccessHandler())

    }

    @Bean
    fun corsConfigurationSource(): CorsConfigurationSource {
        val configuration = CorsConfiguration()
        configuration.allowedOrigins = listOf("*")
        configuration.allowedMethods = listOf("HEAD", "GET", "POST", "PUT", "DELETE", "PATCH")
        // setAllowCredentials(true) is important, otherwise:
        // The value of the 'Access-Control-Allow-Origin' header in the response must not be the wildcard '*' when the request's credentials mode is 'include'.
        configuration.allowCredentials = true
        // setAllowedHeaders is important! Without it, OPTIONS preflight request
        // will fail with 403 Invalid CORS request
        configuration.allowedHeaders = listOf("Authorization", "Cache-Control", "Content-Type")
        val source = UrlBasedCorsConfigurationSource()
        source.registerCorsConfiguration("/**", configuration)
        return source
    }

    private fun logEnvironment() {
        System.getProperties().forEach { k, v -> logger.info("system property {} = {}", k, v) }
        environment.activeProfiles.forEach { logger.info("spring profile {}", it) }
    }

}