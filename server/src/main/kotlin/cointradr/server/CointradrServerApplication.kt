package cointradr.server

//import org.slf4j.LoggerFactory
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer


@SpringBootApplication
class CointradrServerApplication : SpringBootServletInitializer() {
    companion object {
//        val log = LoggerFactory.getLogger(CointradrServerApplication::class.java)
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(CointradrServerApplication::class.java, *args)
        }
    }
}

