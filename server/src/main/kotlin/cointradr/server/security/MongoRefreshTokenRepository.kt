package cointradr.server.security

import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

/**
 * @version 1.0
 * @author: Iain Porter
 * @since 23/05/2013
 */
@Repository
interface OAuth2RefreshTokenRepository : MongoRepository<RefreshToken, String> {

    fun findByTokenId(tokenId: String): RefreshToken
}