package cointradr.server.security

import org.springframework.security.oauth2.common.OAuth2AccessToken
import org.springframework.security.oauth2.provider.OAuth2Authentication

/**
 * @version 1.0
 * @author: Iain Porter
 * @since 22/05/2013
 */
data class AccessToken(val oAuth2AccessToken: OAuth2AccessToken, val authentication: OAuth2Authentication, val authenticationId: String) {

    val tokenId: String = oAuth2AccessToken.value
    val userName: String = authentication.name
    val clientId: String = authentication.oAuth2Request.clientId
    val refreshToken: String = oAuth2AccessToken.refreshToken.value

}