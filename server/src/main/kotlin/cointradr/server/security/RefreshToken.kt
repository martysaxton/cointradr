package cointradr.server.security

import org.springframework.security.oauth2.common.OAuth2RefreshToken
import org.springframework.security.oauth2.provider.OAuth2Authentication

/**
 * @version 1.0
 * @author: Iain Porter
 * @since 23/05/2013
 */
class RefreshToken(private val oAuth2RefreshToken: OAuth2RefreshToken, val authentication: OAuth2Authentication) {

    val tokenId: String = oAuth2RefreshToken.value

    fun getoAuth2RefreshToken(): OAuth2RefreshToken {
        return oAuth2RefreshToken
    }
}