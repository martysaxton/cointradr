package cointradr.server.security


import org.springframework.security.oauth2.common.OAuth2AccessToken
import org.springframework.security.oauth2.common.OAuth2RefreshToken
import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.security.oauth2.provider.token.DefaultAuthenticationKeyGenerator
import org.springframework.security.oauth2.provider.token.TokenStore
import org.springframework.stereotype.Component

import java.util.ArrayList

@Component
class MongoTokenStore(
        private val oAuth2AccessTokenRepository: AccessTokenRepository,
        private val oAuth2RefreshTokenRepository: OAuth2RefreshTokenRepository) : TokenStore {

    private val authenticationKeyGenerator = DefaultAuthenticationKeyGenerator()

    override fun readAuthentication(token: OAuth2AccessToken): OAuth2Authentication? {
        return readAuthentication(token.value)
    }

    override fun readAuthentication(tokenId: String): OAuth2Authentication? {
        return oAuth2AccessTokenRepository.findByTokenId(tokenId)?.authentication
    }

    override fun storeAccessToken(token: OAuth2AccessToken, authentication: OAuth2Authentication) {
        val oAuth2AuthenticationAccessToken = AccessToken(token, authentication, authenticationKeyGenerator.extractKey(authentication))
        oAuth2AccessTokenRepository.save(oAuth2AuthenticationAccessToken)
    }

    override fun readAccessToken(tokenValue: String): OAuth2AccessToken? {
        val token = oAuth2AccessTokenRepository.findByTokenId(tokenValue)
                ?: return null //let spring security handle the invalid token
        return token.oAuth2AccessToken
    }

    override fun removeAccessToken(token: OAuth2AccessToken) {
        val accessToken = oAuth2AccessTokenRepository.findByTokenId(token.value)
        if (accessToken != null) {
            oAuth2AccessTokenRepository.delete(accessToken)
        }
    }

    override fun storeRefreshToken(refreshToken: OAuth2RefreshToken, authentication: OAuth2Authentication) {
        oAuth2RefreshTokenRepository.save(RefreshToken(refreshToken, authentication))
    }

    override fun readRefreshToken(tokenValue: String): OAuth2RefreshToken {
        return oAuth2RefreshTokenRepository.findByTokenId(tokenValue).getoAuth2RefreshToken()
    }

    override fun readAuthenticationForRefreshToken(token: OAuth2RefreshToken): OAuth2Authentication {
        return oAuth2RefreshTokenRepository.findByTokenId(token.value).authentication
    }

    override fun removeRefreshToken(token: OAuth2RefreshToken) {
        oAuth2RefreshTokenRepository.delete(oAuth2RefreshTokenRepository.findByTokenId(token.value))
    }

    override fun removeAccessTokenUsingRefreshToken(refreshToken: OAuth2RefreshToken) {
        val accessToken = oAuth2AccessTokenRepository.findByRefreshToken(refreshToken.value)
        if (accessToken != null) {
            oAuth2AccessTokenRepository.delete(accessToken)
        }
    }

    override fun getAccessToken(authentication: OAuth2Authentication): OAuth2AccessToken? {
        val token = oAuth2AccessTokenRepository.findByAuthenticationId(authenticationKeyGenerator.extractKey(authentication))
        return token?.oAuth2AccessToken
    }

    override fun findTokensByClientId(clientId: String): Collection<OAuth2AccessToken> {
        val tokens = oAuth2AccessTokenRepository.findByClientId(clientId)
        return extractAccessTokens(tokens)
    }

    override fun findTokensByClientIdAndUserName(clientId: String, userName: String): Collection<OAuth2AccessToken> {
        val tokens = oAuth2AccessTokenRepository.findByClientIdAndUserName(clientId, userName)
        return extractAccessTokens(tokens)
    }

    private fun extractAccessTokens(tokens: List<AccessToken>): Collection<OAuth2AccessToken> {
        val accessTokens = ArrayList<OAuth2AccessToken>()
        for (token in tokens) {
            accessTokens.add(token.oAuth2AccessToken)
        }
        return accessTokens
    }

}