package cointradr.server.security

import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

/**
 * @version 1.0
 * @author: Iain Porter
 * @since 22/05/2013
 */
@Repository
interface AccessTokenRepository : MongoRepository<AccessToken, String> {

    fun findByTokenId(tokenId: String): AccessToken?

    fun findByRefreshToken(refreshToken: String): AccessToken?

    fun findByAuthenticationId(authenticationId: String): AccessToken?

    fun findByClientIdAndUserName(clientId: String, userName: String): List<AccessToken>

    fun findByClientId(clientId: String): List<AccessToken>
}