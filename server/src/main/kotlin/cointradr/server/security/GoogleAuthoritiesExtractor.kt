package cointradr.server.security

import cointradr.server.service.UserService
import org.springframework.boot.autoconfigure.security.oauth2.resource.AuthoritiesExtractor
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority

class GoogleAuthoritiesExtractor(private val userService: UserService): AuthoritiesExtractor {

    companion object{
        const val EMAIL_KEY = "email"
    }
    override fun extractAuthorities(map: MutableMap<String, Any>): MutableList<GrantedAuthority> {
        val email = map[EMAIL_KEY] as String?
        val user = email?.let{ userService.findByEmail(it) }
//        val x = user.authorities.map { it -> SimpleGrantedAuthority(it) }
//        return x.toMutableList()
        if (user != null) {
            return mutableListOf(SimpleGrantedAuthority("cointradr"))
        }
        return mutableListOf()
    }
}