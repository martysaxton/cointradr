package cointradr.server.service

import org.springframework.security.core.userdetails.UserDetailsService
import cointradr.server.model.User

interface UserService : UserDetailsService {
    fun findByEmail(email: String): User?
}
