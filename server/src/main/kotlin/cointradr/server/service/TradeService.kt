package cointradr.server.service

import cointradr.server.model.Trade
import cointradr.server.repository.Market
import org.springframework.data.util.CloseableIterator
import java.util.*

interface TradeService  {
    fun findInDateRange(start: Date?, end: Date?, markets: List<Market>): CloseableIterator<Trade>
}
