package cointradr.server.service

import cointradr.server.model.Trade
import cointradr.server.repository.Market
import org.springframework.stereotype.Service
import cointradr.server.repository.TradeRepository
import org.springframework.data.util.CloseableIterator
import java.util.*

@Service
class TradeServiceImpl(private val tradeRepository: TradeRepository) : TradeService {
    override fun findInDateRange(start: Date?, end: Date?,  markets: List<Market>): CloseableIterator<Trade> {
        return tradeRepository.findByDateRange(start, end, markets)
    }


}
