package cointradr.server.service

import org.springframework.security.core.userdetails.User.withUsername
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import cointradr.server.model.User
import cointradr.server.repository.UserRepository
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder

@Service
@Transactional
class UserServiceImpl(private val userRepository: UserRepository) : UserService {

    val encoder = BCryptPasswordEncoder()

    override fun loadUserByUsername(username: String?): UserDetails {

        if (username == null) {
            throw RuntimeException("user not found $username")
        }

        return withUsername(username)
                .password(encoder.encode("ilikepancakes"))
                .authorities("USER", "cointradr")
                .build()
    }

    override fun findByEmail(email: String): User? {
        return userRepository.findByEmail(email)
     }


}
