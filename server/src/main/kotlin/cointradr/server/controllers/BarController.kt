package cointradr.server.controllers

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.slf4j.LoggerFactory
import java.io.OutputStreamWriter
import java.io.BufferedWriter
import javax.servlet.http.HttpServletResponse
import cointradr.server.extensions.kotlin.toDate
import cointradr.server.model.Bar
import cointradr.server.repository.BarRepository
import cointradr.server.repository.Market
import org.springframework.data.util.CloseableIterator

@RestController
class BarController(private val barRepository: BarRepository): BaseController() {

    companion object {
        val logger = LoggerFactory.getLogger(BarController::class.java)

    }

    @RequestMapping("/bars")
    fun getBars(
            @RequestParam(value = "market", required = false) markets: Array<String>?,
            @RequestParam(value = "start", required = false) startTimeStr: String?,
            @RequestParam(value = "end", required = false) endTimeStr: String?,
            @RequestParam(value = "dur", required = true) durStr: String,
            @RequestParam(value = "threshold", required = false) thresholdStr: String?,
            @RequestParam(value = "longOnly", required = false) longOnly: Boolean = false,
            response: HttpServletResponse) {

        val dur = durStr.toInt()
        val start = startTimeStr.toDate()
        val end = endTimeStr.toDate()
        val marketList: List<Market> = parseMarkets(markets)

        val barIterator = if (thresholdStr != null) {
            val threshold = thresholdStr.toDouble()
            barRepository.findByReturnTheshold(start, end, marketList, dur, threshold)
        } else {
            barRepository.findByDateRange(start, end, marketList, dur)
        }

        streamResponse(response, barIterator, Bar::class.java)
    }

    private fun streamResponse(response: HttpServletResponse, barIterator: CloseableIterator<Bar>, clazz: Class<Bar>) {
        val objectWriter = ObjectMapper().writerFor(clazz)
        val responseWriter = BufferedWriter(OutputStreamWriter(response.outputStream))
        responseWriter.use {
            val sequenceWriter = objectWriter.writeValuesAsArray(responseWriter)
            barIterator.forEach {
                sequenceWriter.write(it)
            }
            sequenceWriter.close()
        }
    }

}