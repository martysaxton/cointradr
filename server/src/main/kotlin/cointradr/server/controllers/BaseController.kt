package cointradr.server.controllers

import cointradr.server.repository.Market
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.isEqualTo

open class BaseController {
//    companion object {
        fun parseMarkets(markets: Array<String>?): List<Market> {
            return markets?.map {
                val parts = it.split("_")
                val exch = parts[0]
                val symbol = parts[1] + '/' + parts[2]
                Market(exch, symbol)
            } ?: listOf()

        }
//    }

}