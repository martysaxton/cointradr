package cointradr.server.controllers

import cointradr.server.model.Trade
import cointradr.server.service.TradeService
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.joda.time.format.ISODateTimeFormat
import org.slf4j.LoggerFactory
import java.io.OutputStreamWriter
import java.io.BufferedWriter
import javax.servlet.http.HttpServletResponse
import cointradr.server.extensions.kotlin.toDate
import cointradr.server.repository.Market

@RestController
class TradesController(private val tradeService: TradeService): BaseController() {

    companion object {
        val logger = LoggerFactory.getLogger(TradesController::class.java)

    }

    @RequestMapping("/trades")
    fun getTrades(
            @RequestParam(value = "market", required = false) markets: Array<String>?,
            @RequestParam(value = "start", required = false) startTimeStr: String?,
            @RequestParam(value = "end", required = false) endTimeStr: String?,
            response: HttpServletResponse) {


        val start = startTimeStr.toDate()
        val end = endTimeStr.toDate()
        val marketList = parseMarkets(markets)

        val tradesIterator = tradeService.findInDateRange(start, end, marketList)

        val objectWriter = ObjectMapper().writerFor(Trade::class.java)
        val responseWriter = BufferedWriter(OutputStreamWriter(response.outputStream))
        responseWriter.use {
            val sequenceWriter = objectWriter.writeValuesAsArray(responseWriter)
            tradesIterator.forEach {
                sequenceWriter.write(it)
            }
            sequenceWriter.close()
        }
    }

}