package cointradr.server.repository

import org.springframework.data.mongodb.repository.MongoRepository
import cointradr.server.model.User


interface UserRepository : MongoRepository<User, String> {
    fun findByEmail(email: String): User?
}
