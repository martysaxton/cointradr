package cointradr.server.repository

import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.isEqualTo

open class BaseRepository {

    companion object {
        fun createMarketsCriteria(markets: List<Market>): MutableList<Criteria> {
            var ands = mutableListOf<Criteria>()
            if (!markets.isEmpty()) {
                val marketList = markets.map { Criteria().andOperator(Criteria.where("exch").isEqualTo(it.exchange), Criteria.where("symb").isEqualTo(it.symbol)) }
                ands.add(Criteria().orOperator(*marketList.toTypedArray()))
            }
            return ands
        }
    }
}