package cointradr.server.repository

import cointradr.server.model.Trade
import org.springframework.data.domain.Sort
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.query.Criteria
import java.util.*


import org.springframework.data.mongodb.core.query.Criteria.where
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.core.query.isEqualTo
import org.springframework.data.util.CloseableIterator
import org.springframework.stereotype.Component


@Component
class TradeRepository(val mongoTemplate: MongoTemplate) : BaseRepository() {
    fun findByDateRange(start: Date?, end: Date?, markets: List<Market>): CloseableIterator<Trade> {
        var andedList = createMarketsCriteria(markets)
        start?.let { andedList.add(where("t").gte(start))}
        end?.let {andedList.add(where("t").lt(end))}
        andedList.addAll(createMarketsCriteria(markets))
        val anded  = Criteria().andOperator(*andedList.toTypedArray())
        val sort = Sort.by(Sort.Order(Sort.Direction.ASC, "t"))
        val q = Query(anded).with(sort)
        return mongoTemplate.stream(q, Trade::class.java)
    }
}
