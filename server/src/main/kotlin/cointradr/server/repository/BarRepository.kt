package cointradr.server.repository

import cointradr.server.model.Bar
import com.google.gson.Gson
import org.joda.time.format.ISODateTimeFormat
import org.springframework.data.domain.Sort
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.query.Criteria
import java.util.*


import org.springframework.data.mongodb.core.query.Criteria.where
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.core.query.isEqualTo
import org.springframework.data.util.CloseableIterator
import org.springframework.stereotype.Component
import org.springframework.data.mongodb.core.query.BasicQuery
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

@Component
class BarRepository(val mongoTemplate: MongoTemplate) : BaseRepository() {

    val gson = Gson()

    fun findByDateRange(start: Date?, end: Date?, markets: List<Market>, durationInSeconds: Int): CloseableIterator<Bar> {
        val ands = createBaseCriteria(start, end, markets, durationInSeconds)
        return stream(ands)
//        val queryStr = "{ \$or: [" +
//                "{exch: 'bina', symb: 'btc/usd'}, " +
//                "{exch: 'okex', symb: 'btc/usd'}, " +
//                "{exch: 'okex', symb: 'btc/usd-f'}, " +
//                "{exch: 'huob', symb: 'btc/usd'}],  " +
//                "start : { \$gte : ISODate(\"2018-11-01T00:00:00Z\")   }, " +
//                "dur: 600000, " +
//                "aret: {\$gte: 0.8}  }"
//        val fmt = ISODateTimeFormat.dateTime()
//        var marketList = ArrayList<Any>()
//        marketList.add(mapOf("exch" to "bina"))
//        marketList.add(mapOf("symb" to "btc/usd"))
//        val queryMap = HashMap<String, Any>()
//        queryMap.put("\$or", marketList)
//        start?.let {
//            var ddd= fmt.print(it.time)
//            queryMap.put("start", mapOf("\$gte" to "ISODate(\"$ddd\"")) }
//        var json = gson.toJson(queryMap)
//        return stream(queryStr)
    }

    fun findByReturnTheshold(start: Date?, end: Date?, markets: List<Market>, durationInSeconds: Int, ret: Double, longOnly: Boolean = false): CloseableIterator<Bar> {
        val ands = createBaseCriteria(start, end, markets, durationInSeconds)
        ands.add(where("ret").gte(ret))
        return stream(ands)
    }

    private fun stream(ands: List<Criteria>): CloseableIterator<Bar> {
        val andCriteria = Criteria().andOperator(*ands.toTypedArray())
        return stream(Query(andCriteria))
    }
    private fun stream(queryString: String): CloseableIterator<Bar> {
        val query = BasicQuery(queryString)
        return stream(query)
    }

    private fun stream(query: Query): CloseableIterator<Bar> {
        val sort = Sort.by(Sort.Order(Sort.Direction.ASC, "start"))
        return mongoTemplate.stream(query.with(sort), Bar::class.java)
//        return mongoTemplate.stream(Query(Criteria()), Bar::class.java)
    }

    private fun createBaseCriteria(start: Date?, end: Date?, markets: List<Market>, durationInSeconds: Int): MutableList<Criteria> {
        var ands = createMarketsCriteria(markets)
        start?.let { ands.add(where("start").gte(start))}
        end?.let {ands.add(where("start").lt(end))}
        ands.add(where("dur").isEqualTo(durationInSeconds * 1000))
        return ands
    }

}
