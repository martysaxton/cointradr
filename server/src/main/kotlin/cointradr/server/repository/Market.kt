package cointradr.server.repository

data class Market(val exchange: String, val symbol: String)
