#!/bin/bash
set +e
set -x
sudo docker-compose down
set -e
sudo docker-compose pull
sudo docker-compose up -d
sudo docker container logs server_cointradr-trade-collector_1 --tail 100 -f


