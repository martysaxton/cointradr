#!/bin/bash
set -e
set -x
if [[ -n $(git status --porcelain) ]]; then echo "repo is dirty"; fi
./gradlew clean build
docker build --tag cointradr/backend --label githash=$(git rev-parse --short HEAD) .
docker push cointradr/backend
